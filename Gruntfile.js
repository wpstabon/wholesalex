'use strict';
module.exports = function ( grunt ) {
	const pkg = grunt.file.readJSON( 'package.json' );
	const sass = require( 'node-sass' );

	grunt.initConfig( {
		addtextdomain: {
			options: {
				textdomain: 'wholesalex',
			},
			update_all_domains: {
				options: {
					updateDomains: true,
				},
				src: [
					'*.php',
					'**/*.php',
					'!node_modules/**',
					'!src/**',
					'!php-tests/**',
					'!bin/**',
					'!build/**',
					'!assets/**',
				],
			},
		},

		sass: {
			options: {
				implementation: sass,
				sourceMap: false,
				removeComments: true,
				collapseWhitespace: true,
			},
			dist: {
				cwd: './',
				files: {
					'./assets/css/whx_form.css':
						'./reactjs/src/assets/scss/Form.scss',
				},
			},
		},

		// Generate POT files.
		// makepot: {
		// 	target: {
		// 		options: {
		// 			exclude: [
		// 				'build/.*',
		// 				'node_modules/*',
		// 				// 'assets/*',
		// 				'tests/*',
		// 				'bin/*',
		// 			],
		// 			mainFile: 'wholesalex.php',
		// 			domainPath: '/languages/',
		// 			potFilename: 'wholesalex.pot',
		// 			type: 'wp-plugin',
		// 			updateTimestamp: true,
		// 			potHeaders: {
		// 				'report-msgid-bugs-to': 'https://wpxpo.com/contact/',
		// 				poedit: true,
		// 				'x-poedit-keywordslist': false,
		// 			},
		// 		},
		// 	},
		// },

		// Clean up build directory
		clean: {
			main: [ 'build/' ],
		},

		// Copy the plugin into the build directory
		copy: {
			main: {
				src: [
					'**',
					'!assets/js/*.js.map',
					'!changelog.txt',
					'!phpcs.xml',
					'!webpack.config.dev.js',
					'!webpack.config.prod.js',
					'!reactjs',
					'!reactjs/**',
					'!node_modules/**',
					'!build/**',
					'!tmp/**',
					'!bin/**',
					'!.git/**',
					'!Gruntfile.js',
					'!CONTRIBUTING.md',
					'!package.json',
					'!package-lock.json',
					'!composer.json',
					'!composer.lock',
					'!config.json',
					'!phpcs.xml.dist',
					'!webpack.config.js',
					'!debug.log',
					'!phpunit.xml',
					'!*.xml',
					'!.gitignore',
					'!.gitmodules',
					'!npm-debug.log',
					'!secret.json',
					'!plugin-deploy.sh',
					'!assets/src/**',
					'!src/**',
					'!assets/css/style.css.map',
					'!tests/**',
					'!**/Gruntfile.js',
					'!**/wp-cli.phar',
					'!**/package.json',
					'!**/README.md',
					'!assets/js/wholesalex_bulkorder.js',
					'!assets/js/wholesalex_subaccount.js',
					'!assets/js/wholesalex_wallet.js',
					'!assets/js/whx_wallet_gateway.js',
					'!assets/js/whx_regi_form.js',
					'!assets/js/whx_conversation.js',
					'!assets/js/wholesalex_license.js',
					'!assets/js/whx_migration_tools.js',
					'!**/*~',
				],
				dest: 'build/',
			},
		},
		// Configure grunt-remove-logging
		removelogging: {
			dist: {
				// Clean up all js file inside "dist" or its subfolders
				src: 'reactjs/**/*.js',
			},
		},
		search: {
			remove_printr: {
				files: {
					src: [
						'*.php',
						'**/*.php',
						'!node_modules/**',
						'!src/**',
						'!php-tests/**',
						'!bin/**',
						'!build/**',
						'!assets/**',
					],
				},
				options: {
					searchString: /(var_dump|print_r)/g,
					logFile: 'tmp/results.xml',
					logFormat: 'xml',
					failOnMatch: true,
					onMatch: function ( match ) {
						// called when a match is made. The parameter is an object of the
						// following structure: { file: "", line: X, match: "" }
					},
					onComplete: function ( matches ) {
						// called when all files have been parsed for the target. The
						// matches parameter is an object of the format:
						// `{ numMatches: N, matches: {} }`. The matches /property is
						// an object of filename => array of matches
					},
				},
			},
		},

		//Compress build directory into <name>.zip and <name>-<version>.zip
		compress: {
			main: {
				options: {
					mode: 'zip',
					archive: './build/wholesalex v' + pkg.version + '.zip',
				},
				expand: true,
				cwd: 'build/',
				src: [ '**/*' ],
				dest: 'wholesalex',
			},
		},

		run: {
			options: {},
			build: {
				cmd: 'npm',
				args: [ 'run', 'build' ],
			},
			version: {
				cmd: 'npm',
				args: [ 'run', 'version' ],
			},

			devBuild: {
				cmd: 'npm',
				args: [ 'run', 'dev:build' ],
			},
		},
	} );

	grunt.registerTask( 'default', [] );

	// Load NPM tasks to be used here
	grunt.loadNpmTasks( 'grunt-contrib-clean' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.loadNpmTasks( 'grunt-remove-logging' );
	grunt.loadNpmTasks( 'grunt-contrib-compress' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-search' );
	grunt.loadNpmTasks( 'grunt-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );

	grunt.registerTask( 'clear_log', [ 'removelogging' ] );

	// file auto generation
	// grunt.registerTask( 'i18n', [ 'makepot' ] );
	grunt.registerTask( 'version', [ 'run:version' ] );
	grunt.registerTask( 'check_printr', [ 'search' ] );

	grunt.registerTask( 'buildFormCSS', [ 'sass' ] );

	grunt.registerTask( 'release', [
		'readme',
		'run:version',
		'run:devBuild',
		'run:build',
		// 'i18n',
	] );

	grunt.registerTask( 'zip', [
		'check_printr',
		'clean',
		// 'makepot',
		'buildFormCSS',
		'copy',
		'compress',
	] );
};
