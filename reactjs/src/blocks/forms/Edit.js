import React from 'react';
import {
	getFormStyle,
	getInputFieldVariation,
} from '../../pages/registration_form_builder/Utils';
const { useState, useEffect } = wp.element;
const {
	Spinner,
	Placeholder,
	ToolbarButton,
	Toolbar,
	PanelBody,
	ToggleControl,
	SelectControl,
} = wp.components;
const { BlockControls, InspectorControls } = wp.blockEditor;
import '../../assets/scss/Form.scss';

const Edit = ( props ) => {
	const [ formPreview, setFormPreview ] = useState( props?.attributes?.role );
	const [ isLoading, setIsLoading ] = useState( false );
	const [ formData, setFormData ] = useState( '' );

	useEffect( () => {
		setIsLoading( true );
		setFormData( '' );
		fetch(
			wpApiSettings.root +
				'wholesalex/v1/blockPreview/forms?' +
				'role=' +
				formPreview,
			{
				method: 'GET',
			}
		)
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				setFormData( JSON.parse( res.data.formdata ) );
				setIsLoading( false );
			} );
	}, [ formPreview ] );

	const renderColumns = ( field ) => {
		return getInputFieldVariation(
			formData.settings.inputVariation,
			field,
			false
		);
	};

	const renderRows = ( row ) => {
		const role = props?.attributes?.registration_role;

		const checkDepends = ( col ) => {
			let status = true;
			if ( role && col.excludeRoles ) {
				for (
					let index = 0;
					index < col.excludeRoles.length;
					index++
				) {
					const element = col.excludeRoles[ index ];
					if ( element.value === role ) {
						status = false;
					}
				}
			}
			return status;
		};

		return row.columns.map( ( col ) => {
			return (
				checkDepends( col ) &&
				col.status && (
					<div
						className={ `wholesalex-registration-form-column ${ col?.columnPosition }` }
					>
						<div className="wholesalex-row-field">
							{ renderColumns( col ) }
						</div>
					</div>
				)
			);
		} );
	};

	const loginForm = () => {
		return (
			<form className="wholesalex-login-form">
				{ formData?.loginFormHeader?.isShowFormTitle && (
					<div className="wholesalex-login-form-title wsx-reg-form-heading">
						<div className="wsx-login-form-title-text">
							{ formData?.loginFormHeader?.title }
						</div>
						{ ! formData?.loginFormHeader?.isHideDescription && (
							<div className="wholesalex-login-form-subtitle-text">
								{ formData?.loginFormHeader?.description }
							</div>
						) }
					</div>
				) }

				<div
					className={ `wholesalex-fields-wrapper wsx_${ formData.settings?.inputVariation }` }
				>
					<div className="wsx-login-fields wsx-fields-container">
						{ formData.loginFields?.map( ( row, index ) => {
							return (
								<div key={ index } className="wsx-reg-form-row">
									{ getInputFieldVariation(
										formData?.settings?.inputVariation,
										row.columns[ 0 ]
									) }
								</div>
							);
						} ) }
					</div>
				</div>
				<div className="wsx-form-btn-wrapper">
					<button
						className={ `button wsx-login-btn ${ formData.style?.layout?.button?.align }` }
						onChange={ () => {} }
					>
						{ formData?.loginFormButton?.title }
					</button>
				</div>
			</form>
		);
	};

	const registrationForm = () => {
		return (
			<form className="wholesalex-registration-form">
				{ formData?.settings?.isShowFormTitle && (
					<div className="wsx-reg-form-heading">
						<div className="wsx-reg-form-heading-text">
							{ formData?.registrationFormHeader?.title }
						</div>
						{ ! formData?.registrationFormHeader
							?.isHideDescription && (
							<div className="wholesalex-registration-form-subtitle-text">
								{
									formData?.registrationFormHeader
										?.description
								}
							</div>
						) }
					</div>
				) }

				<div className={ `wsx_${ formData.settings?.inputVariation }` }>
					<div className="wholesalex-fields-wrapper wsx-reg-fields wsx-field-container">
						{ formData.registrationFields &&
							formData.registrationFields?.map(
								( row, index ) => {
									return (
										<div
											key={ index }
											className={ `wsx-reg-form-row ${
												row?.isMultiColumn
													? 'double-column'
													: ''
											}` }
										>
											{ renderRows( row ) }
										</div>
									);
								}
							) }
					</div>
				</div>
				<div className="wsx-form-btn-wrapper">
					<button
						className={ `button wsx-register-btn ${ formData.style?.layout?.button?.align }` }
					>
						{ formData?.registrationFormButton?.title }
					</button>
				</div>
			</form>
		);
	};

	const renderForm = () => {
		const type = props.attributes.tag;
		if ( type === 'wholesalex_registration' ) {
			// Render Only Registration Form
			return (
				<div
					className={ `wholesalex-form-wrapper  wsx_${ formData?.settings?.inputVariation }` }
					style={ getFormStyle(
						formData.style,
						formData?.loginFormHeader?.styles,
						formData?.registrationFormHeader?.styles
					) }
				>
					{ registrationForm() }
				</div>
			);
		} else if ( type === 'wholesalex_login_registration' ) {
			// Render Both Login and Registration
			return (
				<div
					className={ `wholesalex-form-wrapper wsx_${ formData?.settings?.inputVariation }` }
					style={ getFormStyle(
						formData?.style,
						formData?.loginFormHeader?.styles,
						formData?.registrationFormHeader?.styles
					) }
				>
					{ loginForm() }
					<span className="wsx-form-separator"></span>
					{ registrationForm() }
				</div>
			);
		}
	};

	const getRoleOptions = () => {
		const options = [];
		{
			Object.keys( wholesalex.wholesalex_roles ).forEach( ( role ) => {
				if ( role !== 'wholesalex_guest' ) {
					options.push( {
						value: role,
						label: wholesalex.wholesalex_roles[ role ]._role_title,
					} );
				}
			} );
		}
		return options;
	};

	return (
		<>
			{
				<BlockControls>
					<Toolbar>
						<ToolbarButton
							icon={ 'edit' }
							label="Customize Form"
							onClick={ () =>
								window.open(
									wholesalex_block_data.form_builder_url,
									'_blank'
								)
							}
						/>
					</Toolbar>
				</BlockControls>
			}
			{
				<InspectorControls>
					<PanelBody title="WholesaleX Form">
						<SelectControl
							label={ 'Select Form' }
							value={ props?.attributes?.registration_role } // e.g: value = [ 'a', 'c' ]
							onChange={ ( val ) => {
								setFormPreview( val );
								props.setAttributes( {
									registration_role: val,
								} );
							} }
							options={ [
								{
									value: '',
									label: 'Select Role',
									disabled: true,
								},
								{ value: 'global', label: 'Global' },
								{ value: 'all_b2b', label: 'B2B Global Form' },
								...getRoleOptions(),
							] }
						/>

						<ToggleControl
							label="With Login"
							checked={
								props?.attributes?.tag ===
								'wholesalex_login_registration'
							}
							onChange={ ( newValue ) => {
								if ( newValue ) {
									props.setAttributes( {
										tag: 'wholesalex_login_registration',
									} );
								} else {
									props.setAttributes( {
										tag: 'wholesalex_registration',
									} );
								}
							} }
						/>
					</PanelBody>
				</InspectorControls>
			}
			{ formData === '' && (
				<div>
					<Placeholder>
						<div
							className="wholesalex-form-preview-block-placeholder"
							style={ { margin: '0px auto 20px' } }
						>
							<span className="wholesalex-icon">
								<img
									src={
										wholesalex.url +
										'assets/img/logo-option.svg'
									}
									style={ {
										filter: 'none',
										height: '30px',
										marginRight: '20px',
										width: 'initial',
										marginBottom: '25px',
									} }
									alt="WholesaleX"
								/>
							</span>

							{ isLoading && <Spinner /> }

							{ ! isLoading && (
								<SelectControl
									label={ 'Select Form' }
									value={
										props?.attributes?.registration_role
									} // e.g: value = [ 'a', 'c' ]
									onChange={ ( val ) => {
										setFormPreview( val );
										props.setAttributes( {
											registration_role: val,
										} );
									} }
									options={ [
										{
											value: '',
											label: 'Select Role',
											disabled: true,
										},
										{ value: 'global', label: 'Global' },
										...getRoleOptions(),
									] }
								/>
							) }
						</div>
					</Placeholder>
				</div>
			) }
			{ ! isLoading && renderForm() }
		</>
	);
};

export default Edit;
