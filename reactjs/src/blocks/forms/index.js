import Edit from './Edit';

// Register the block
wp.blocks.registerBlockType( 'wholesalex/forms', {
	title: 'Forms',
	icon: (
		<img
			src={ wholesalex_block_data?.url + 'assets/img/icon.svg' }
			alt={ 'WholesaleX Form Preview' }
			style={ { height: '20px', width: '20px' } }
		/>
	),
	edit: Edit,
	save( props ) {
		let tag = props?.attributes?.tag;
		if ( ! tag ) {
			tag = 'wholesalex_registration';
		}
		return (
			<div className="wholesalex-form">{ `[${ tag } registration_role='${ props?.attributes?.registration_role }']` }</div>
		);
	},
	category: 'embed',
	tag: 'wholesalex_form',
	attributes: {
		registration_role: {
			type: 'string',
			default: '',
		},
		tag: {
			type: 'string',
			default: 'wholesalex_registration',
		},
	},
	transforms: {
		from: [
			{
				type: 'shortcode',
				tag: [
					'wholesalex_registration',
					'wholesalex_login_registration',
				],
				attributes: {
					registration_role: {
						type: 'string',
						shortcode( e ) {
							return e.named.registration_role;
						},
					},
					tag: {
						type: 'string',
						shortcode( e, a ) {
							return a.shortcode.tag;
						},
					},
				},
			},
		],
	},
	description: 'Preview WholesaleX Registration Form',
} );
