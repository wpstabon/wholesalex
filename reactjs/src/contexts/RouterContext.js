import React, {
	forwardRef,
	useState,
	useEffect,
	createContext,
	useContext,
} from 'react';

const RouterContext = createContext();

export const useRouter = () => useContext( RouterContext );

// Custom useNavigate hook
export const useNavigate = () => {
	const { push } = useRouter();
	return ( hash ) => {
		push( hash );
	};
};

export const RouterProvider = ( { children } ) => {
	const [ currentHash, setCurrentHash ] = useState( window.location.hash );

	const push = ( hash ) => {
		window.location.hash = hash;
	};

	const redirectTo = ( hash, time ) => {
		setTimeout( () => {
			push( hash );
		}, time );
	};

	useEffect( () => {
		const handleHashChange = () => {
			setCurrentHash( window.location.hash );
		};

		window.addEventListener( 'hashchange', handleHashChange );

		return () =>
			window.removeEventListener( 'hashchange', handleHashChange );
	}, [] );

	return (
		<RouterContext.Provider value={ { currentHash, push, redirectTo } }>
			{ children }
		</RouterContext.Provider>
	);
};

// A custom hook to create a link component
export const Link = forwardRef(
	( { to, children, className, navLink, isHidden = false }, ref ) => {
		const { push, currentHash } = useRouter();

		const handleClick = ( event ) => {
			event.preventDefault();
			push( to );
		};

		const isActive = ( path, currHash ) => {
			const formattedPath = path.replace( /^#/, '' );
			const formattedHash = currHash.replace( /^#/, '' );

			const pathParts = formattedPath.split( '/' );
			const hashParts = formattedHash.split( '/' );

			if ( hashParts[ 1 ] === pathParts[ 1 ] ) {
				return true;
			}
			return false;
		};

		return (
			<a
				ref={ ref }
				data-ref={ ref }
				href={ `#${ to }` }
				className={ `wsx-link ${ navLink ? 'wsx-nav-link' : '' } ${
					className ? className : ''
				} ${ isActive( to, currentHash ) ? 'active' : '' }` }
				onClick={ handleClick }
				style={ {
					visibility: `${ isHidden && 'hidden' }`,
					position: `${ isHidden && 'absolute' }`,
					zIndex: `${ isHidden && '-1' }`,
				} }
			>
				{ children }
			</a>
		);
	}
);

export const Route = ( { path, component: Component } ) => {
	const { currentHash } = useRouter();

	// Function to match the current hash with the route path
	const match = ( routePath, currtHash ) => {
		const routeParts = routePath.split( '/' );
		const currentParts = currtHash.replace( /^#/, '' ).split( '/' );

		if ( routeParts.length !== currentParts.length ) {
			return false;
		}

		const params = {};

		for ( let i = 0; i < routeParts.length; i++ ) {
			if ( routeParts[ i ].startsWith( ':' ) ) {
				const paramName = routeParts[ i ].substring( 1 );
				params[ paramName ] = currentParts[ i ];
			} else if ( routeParts[ i ] !== currentParts[ i ] ) {
				return false;
			}
		}

		return params;
	};

	const params = match( path, currentHash );

	return params ? <Component { ...params } /> : null;
};
