import React, { createContext, useState } from 'react';
export const DataContext = createContext();

export const DataProvider = ( { children } ) => {
	const [ contextDataRule, setContextDataRule ] = useState( {
		newRules: wholesalex_overview.whx_dr_rule,
		savedRule: wholesalex_overview.whx_dr_rule,
	} );
	const [ contextDataRole, setContextDataRole ] = useState( {
		newRoles: wholesalex_overview.whx_roles_data,
	} );
	const [ contextData, setContextData ] = useState( {
		globalCurrentPage: '',
		startDateRange: '',
		endDateRange: '',
		isConversationActive: false,
		globalNewItem: [],
		globalNewDynamicRules: [],
	} );

	return (
		<DataContext.Provider
			value={ {
				contextData,
				setContextData,
				contextDataRule,
				setContextDataRule,
				contextDataRole,
				setContextDataRole,
			} }
		>
			{ children }
		</DataContext.Provider>
	);
};
