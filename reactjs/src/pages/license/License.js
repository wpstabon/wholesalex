import React, { useContext, useState } from 'react';
import Toast from '../../components/Toast';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import Icons from '../../utils/Icons';
import LoadingGif from '../../components/LoadingGif';
import './License.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const License = () => {
	const [ licenseKey, setLicenseKey ] = useState( '' );
	const [ loader, setLoader ] = useState( false );
	const { dispatch } = useContext( ToastContexts );
	const [ licenseStatus, setLicenseStatus ] = useState(
		wholesalex_overview?.status
	);
	const [ isLifeTime ] = useState( wholesalex_overview?.isLifetime );
	const fetchData = () => {
		setLoader( true );
		const formDate = new FormData();
		formDate.append( 'action', 'edd_wholesalex_activate_license' );
		formDate.append( 'edd_wholesalex_license_key', licenseKey );
		formDate.append( 'nonce', wholesalex_overview.nonce );
		fetch( wholesalex_overview.ajax, {
			method: 'POST',
			body: formDate,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.status ) {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data,
						},
					} );
					setLicenseStatus( 'valid' );
				} else {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'error',
							message: res.data,
						},
					} );
				}
				setLoader( false );
			} );
	};

	const deactiveLicense = () => {
		setLoader( true );
		const formDate = new FormData();
		formDate.append( 'action', 'edd_wholesalex_deactivate_license' );
		formDate.append( 'nonce', wholesalex_overview.nonce );
		formDate.append( 'is_wholesalex_license_deactive', 'yes' );
		fetch( wholesalex_overview.ajax, {
			method: 'POST',
			body: formDate,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.status ) {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data,
						},
					} );
					setLicenseStatus( 'deactivated' );
				} else {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'error',
							message: res.data,
						},
					} );
				}

				setLoader( false );
			} );
	};

	const activateLicense = () => {
		fetchData();
	};

	const expireLicenseeContent = () => {
		return (
			<div className="wsx-container-left">
				<div className="wsx-card wsx-p-32 wsx-bg-base2 wsx-shadow-none">
					<div className="wsx-title wsx-font-20 wsx-mb-20">
						{ __( 'How to renew existing license?', 'wholesalex' ) }
					</div>
					<div className="wsx-note-wrapper wsx-mb-32">
						<span className="wsx-font-bold">
							{ __( 'Note:', 'wholesalex' ) }
						</span>{ ' ' }
						{ __(
							'Make sure you have installed both the',
							'wholesalex'
						) }{ ' ' }
						<span className="wsx-font-bold">
							{ __(
								'free and pro version of WholesaleX.',
								'wholesalex'
							) }
						</span>
					</div>
					<div className="wsx-title wsx-font-18 wsx-mb-8">
						{ __( 'Step 1:', 'wholesalex' ) }
					</div>
					<div className="wsx-mb-24">
						{ __(
							'To renew your existing license you need to go the Upgrade your current plan from wpxpo.com > my account > My Order > View Licenses',
							'wholesalex'
						) }
					</div>
					<div className="wsx-title wsx-font-18 wsx-mb-8">
						{ __( 'Step 2:', 'wholesalex' ) }
					</div>
					<div className="wsx-mb-32">
						{ __(
							'Then click on the “renew now” button and complete the payment process.',
							'wholesalex'
						) }
					</div>
					<div className="wsx-title wsx-font-20 wsx-mb-20">
						{ __(
							'Want to get rid of the renewing hassle?',
							'wholesalex'
						) }
					</div>
					<div className="wsx-note-wrapper wsx-mb-32">
						{ __(
							'Purchase a lifetime plan of WholesaleX and keep using all pro features forever without any renewal charges. Use this coupon,',
							'wholesalex'
						) }{ ' ' }
						<span className="wsx-font-bold wsx-color-primary">
							{ ' ' }
							Special30{ ' ' }
						</span>
						{ /*  eslint-disable-next-line @wordpress/i18n-translator-comments */ }
						{ __(
							'to get 30% discount for any lifetime plan.',
							'wholesalex'
						) }
					</div>
					<Button
						label={ __( 'Claim Your Discount', 'wholesalex' ) }
						background="secondary"
						buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&&utm_medium=license_page-renew_guide-discount&&utm_campaign=wholesalex-DB"
					/>
				</div>
			</div>
		);
	};
	const upgradeLicenseContent = () => {
		return (
			<div className="wsx-upgrade-license-wrapper wsx-md-column-1">
				<div className="wsx-card wsx-p-32 wsx-bg-base2 wsx-shadow-none">
					<div className="wsx-title wsx-font-20 wsx-mb-20">
						{ __(
							'How to upgrade the existing License of WholesaleX?',
							'wholesalex'
						) }
					</div>
					<div className="wsx-note-wrapper wsx-mb-32">
						<span className="wsx-font-bold">
							{ __( 'Note:', 'wholesalex' ) }
						</span>{ ' ' }
						{ __(
							'Make sure you have installed both the',
							'wholesalex'
						) }{ ' ' }
						<span className="wsx-font-bold">
							{ __(
								'free and pro version of WholesaleX.',
								'wholesalex'
							) }
						</span>
					</div>
					<div className="wsx-title wsx-font-18 wsx-mb-8">
						{ __( 'Step 1:', 'wholesalex' ) }
					</div>
					<div className="wsx-mb-24">
						{ __(
							'Upgrade your current plan from wpxpo.com > my account > My Order > View Licenses > View Upgrades > Select Your Desired Plan',
							'wholesalex'
						) }
					</div>
					<div className="wsx-title wsx-font-18 wsx-mb-8">
						{ __( 'Step 2:', 'wholesalex' ) }
					</div>
					<div className="wsx-mb-32">
						{ __(
							'Complete the Payment and enjoy your plan.',
							'wholesalex'
						) }
					</div>
					<Button
						label={ __( 'Upgrade Now', 'wholesalex' ) }
						background="secondary"
						buttonLink="https://account.wpxpo.com/?utm_source=wholesalex-menu&&utm_medium=license_page-license_upgrade_guide&&utm_campaign=wholesalex-DB"
					/>
				</div>
				<div className="wsx-card wsx-p-32">
					<div className="wsx-title wsx-font-20 wsx-mb-16">
						{ __(
							'Getting Started with WholesaleX?',
							'wholesalex'
						) }
					</div>
					<div className="wsx-license-desc wsx-mb-32">
						{ __(
							'Check out the documentation to learn how you can effectively use the features of WholesaleX.',
							'wholesalex'
						) }
					</div>
					<Button
						label={ __( 'Explore Documentation', 'wholesalex' ) }
						borderColor="primary"
						background="base2"
						iconName="doc"
						buttonLink="https://getwholesalex.com/documentation/?utm_source=wholesalex-menu&&utm_medium=license_page-explore_documentation&&utm_campaign=wholesalex-DB"
					/>
				</div>
			</div>
		);
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container-wrapper">
				<div
					className={ `${
						licenseStatus === 'valid'
							? 'wsx-w-full wsx-text-center'
							: 'wsx-container-wrapper wsx-gap-32 wsx-md-column-1'
					}` }
				>
					{ licenseStatus !== 'expired' &&
						licenseStatus !== 'valid' && (
							<>
								<div className="wsx-container-left">
									{ loader && <LoadingGif /> }
									<div className="wsx-card wsx-p-32 wsx-mb-32">
										<div className="wsx-title wsx-font-20 wsx-mb-16">
											{ __(
												'Add New License',
												'wholesalex'
											) }
										</div>
										<div className="wsx-d-flex wsx-item-end wsx-gap-16">
											<div className="wsx-w-full">
												<label
													htmlFor="wsx-license-key"
													className="wsx-label wsx-input-label wsx-font-medium wsx"
												>
													{ __(
														'Enter your License Key',
														'wholesalex'
													) }
												</label>
												<input
													className="wsx-input"
													type="password"
													value={ licenseKey }
													name="wsx-license-key"
													placeholder="Enter your license key here"
													onChange={ ( e ) => {
														setLicenseKey(
															e.target.value
														);
													} }
												></input>
											</div>
											<Button
												background="primary"
												customClass="wsx-br-lg wsx-shrink-0"
												onClick={ activateLicense }
												label={ __(
													'Active License',
													'wholesalex'
												) }
											/>
										</div>
									</div>
									<div className="wsx-card wsx-p-32 wsx-bg-base2 wsx-shadow-none">
										<div className="wsx-title wsx-font-20 wsx-mb-20">
											{ __(
												'How to use the license key to activate the pro version?',
												'wholesalex'
											) }
										</div>
										<div className="wsx-note-wrapper wsx-mb-32">
											<span className="wsx-font-bold">
												{ __( 'Note:', 'wholesalex' ) }
											</span>{ ' ' }
											{ __(
												'Make sure you have installed both the',
												'wholesalex'
											) }{ ' ' }
											<span className="wsx-font-bold">
												{ __(
													'free and pro version of WholesaleX.',
													'wholesalex'
												) }
											</span>
										</div>
										<div className="wsx-title wsx-font-18 wsx-mb-8">
											{ __( 'Step 1:', 'wholesalex' ) }
										</div>
										<div className="wsx-mb-24">{ `Copy the License Key from wpxpo.com > My Account > Order > View License. Then Click Key Icon` }</div>
										<div className="wsx-title wsx-font-18 wsx-mb-8">
											{ __( 'Step 2:', 'wholesalex' ) }
										</div>
										<div className="wsx-mb-32">
											{ __(
												'Paste the copied license on the above field. Then Click on the Activate License Button.',
												'wholesalex'
											) }
										</div>
										<Button
											label={ __(
												'See Documentation',
												'wholesalex'
											) }
											borderColor="primary"
											background="base1"
											iconName="doc"
											buttonLink="https://getwholesalex.com/docs/wholesalex/getting-started/pro-version-installation/"
										/>
									</div>
								</div>
								<div className="wsx-container-right">
									<div className="wsx-card wsx-p-32">
										<div className="wsx-title wsx-font-20 wsx-mb-16">
											{ __(
												'Getting Started with WholesaleX?',
												'wholesalex'
											) }
										</div>
										<div className="wsx-license-desc wsx-mb-32">
											{ __(
												'Check out the documentation to learn how you can effectively use the features of WholesaleX.',
												'wholesalex'
											) }
										</div>
										<Button
											label={ __(
												'Explore Documentation',
												'wholesalex'
											) }
											borderColor="primary"
											background="base2"
											iconName="doc"
											buttonLink="https://getwholesalex.com/documentation/?utm_source=wholesalex-menu&&utm_medium=license_page-explore_documentation&&utm_campaign=wholesalex-DB"
										/>
									</div>
								</div>
							</>
						) }
					{ licenseStatus === 'valid' && (
						<>
							<div className="wsx-card wsx-m-0 wsx-pt-48 wsx-pb-48 ">
								{ loader && <LoadingGif /> }

								<div className="wsx-lh-0 wsx-mb-32">
									{ Icons.activeStatus }
								</div>
								<div className="wsx-title wsx-font-20 wsx-mb-16">
									{ __(
										'Congratulations! Your License Key is ',
										'wholesalex'
									) }
									<strong>
										{ __( 'Activated', 'wholesalex' ) }
									</strong>
									.{ ' ' }
									{ ! isLifeTime && (
										<div
											style={ {
												textTransform: 'capitalize',
											} }
										>
											{ __(
												'Expiry Date:',
												'wholesalex'
											) }{ ' ' }
											{ wholesalex_overview.expire_date }{ ' ' }
										</div>
									) }
								</div>
								<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mb-48">
									{ __(
										'Want to deactive this license?',
										'wholesalex'
									) }
									<a
										href="#/license"
										className="wsx-link wsx-color-primary"
										onClick={ deactiveLicense }
									>
										{ __( 'Click here', 'wholesalex' ) }
									</a>
								</div>
								<Button
									label={ __(
										'Go back to Dashboard',
										'wholesalex'
									) }
									background="primary"
									customClass="wsx-center-hz"
									buttonLink="#/"
									sameTab={ true }
								/>
							</div>
							{ ! isLifeTime && upgradeLicenseContent() }
						</>
					) }
					{ licenseStatus === 'expired' && (
						<>
							{ expireLicenseeContent() }

							<div className="wsx-container-right">
								<div className="wsx-card wsx-p-32">
									{ loader && <LoadingGif /> }

									<div className="wsx-color-negative wsx-mb-24">
										<span className="wsx-lh-0">
											{ Icons.warningMessage }{ ' ' }
										</span>
										<span>
											{ __(
												'Your WholesaleX license has been',
												'wholesalex'
											) }{ ' ' }
											<span className="wsx-font-bold">
												{ __(
													'expired',
													'wholesalex'
												) }
											</span>
											.
											{ __(
												'Please Renew your license to start using the pro features again.',
												'wholesalex'
											) }
										</span>
									</div>
									<form
										method="post"
										action="https://account.wpxpo.com/checkout/"
									>
										<input
											type="hidden"
											name="edd_renew_key"
											value={
												wholesalex_overview?.license
											}
										/>
										<input
											type="hidden"
											name="edd_action"
											value="apply_license_renewal_from_plugin_dashboard"
										/>
										<input
											type="submit"
											id="edd-add-license-renewal"
											className="wsx-btn wsx-bg-primary wsx-w-full"
											value="Renew License"
										/>
									</form>
								</div>
							</div>
						</>
					) }
				</div>
			</div>
			<Toast delay={ 5000 } position="top_right" />
		</div>
	);
};

export default License;
