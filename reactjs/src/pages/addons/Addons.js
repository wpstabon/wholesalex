import React, { useContext, useState } from 'react';
import { __, sprintf } from '@wordpress/i18n';
import Toast from '../../components/Toast';
import Card from './Card';

import Icons from '../../utils/Icons';
import Button from '../../components/Button';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';
import './Addons.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const Addons = () => {
	const [ addonOptions, setAddonOptions ] = useState(
		wholesalex_overview.addons
	);
	const { dispatch } = useContext( ToastContexts );

	const [ installText, setInstallText ] = useState( {
		wsx_addon_dokan_integration: 'Install & Activate',
		wsx_addon_wcfm_integration: 'Install & Activate',
		wsx_addon_migration_integration: 'Install & Activate',
	} );
	const [ loading, setLoading ] = useState( {
		wsx_addon_dokan_integration: false,
		wsx_addon_wcfm_integration: false,
		wsx_addon_migration_integration: false,
	} );

	const [ popupStatus, setPopUpStatus ] = useState( false );
	const [ whiteLabelPopup, setWhiteLabelPopup ] = useState( false );
	const proCardFeatures = [
		'Get more pro addons & features.',
		'Get regular updates.',
		'Get dedicated support.',
	];

	const updateAddonStatus = ( addonName, status ) => {
		const _addonData = { ...addonOptions };
		_addonData[ addonName ] = { ..._addonData[ addonName ], status };
		if (
			addonName === 'wsx_addon_conversation' &&
			_addonData[ addonName ].status === false
		) {
			_addonData.wsx_addon_raq.status = false;
		}
		setAddonOptions( _addonData );
		fetchData( 'post', 'update_status', addonName, status );
	};

	const fetchData = async (
		type = 'post',
		request_for = '',
		addonName = '',
		updatedStatus = ''
	) => {
		const attr = {
			type,
			action: 'addons',
			request_for: 'update_status',
			addon: addonName,
			status: updatedStatus ? 'yes' : 'no',
			nonce: wholesalex.nonce,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/addons',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Success',
					},
				} );
			} else {
				const _addonData = { ...addonOptions };
				_addonData[ addonName ] = {
					..._addonData[ addonName ],
					status: updatedStatus,
				};
				setAddonOptions( _addonData );
			}
		} );
	};

	const installPlugin = ( addonName = '' ) => {
		setInstallText( { ...installText, [ addonName ]: 'Installing...' } );
		setLoading( { ...loading, [ addonName ]: true } );
		const attr = {
			type: 'post',
			action: 'addons',
			request_for: 'install_plugin',
			addon: addonName,
			nonce: wholesalex.nonce,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/addons',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: res.data,
					},
				} );
				setInstallText( {
					...installText,
					[ addonName ]: 'Installed',
				} );
				window.location.reload();
			} else {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Error Occure! Please try again',
					},
				} );
			}

			setLoading( { ...loading, [ addonName ]: false } );
		} );
	};

	const onLockIconClickHandler = ( id ) => {
		if ( 'wsx_addon_whitelabel' === id ) {
			setWhiteLabelPopup( true );
		} else {
			setPopUpStatus( true );
		}
	};

	return (
		<div className="wsx-wrapper">
			{ ! wholesalex?.is_pro_active && (
				<>
					<div className="wsx-pt-48"></div>
					<section className="wsx-pro-active-card">
						<img
							src={ `${ wholesalex.url }assets/img/addon_pro_image.png` }
							alt="pro-image"
						/>
						<div className="wsx-pro-active-card-content">
							<div className="wsx-font-20 wsx-font-medium wsx-color-primary wsx-mb-16">
								{ __( 'WholesaleX Pro', 'wholesalex' ) }
							</div>
							<div className="wsx-font-16 wsx-color-text-medium wsx-mb-20">
								{ __(
									'Get started with the pro version of WholesaleX and Unlock more addons & features.',
									'wholesalex'
								) }
							</div>
							<div className="wsx-list wsx-d-flex wsx-flex-column wsx-gap-20">
								{ proCardFeatures &&
									proCardFeatures.map( ( feature ) => {
										return (
											<div
												className="wsx-list-item wsx-d-flex wsx-item-center wsx-gap-8"
												key={ feature }
											>
												{ ' ' }
												<span className="wsx-lh-0">
													{ Icons.listCheckMark }
												</span>{ ' ' }
												{ feature }
											</div>
										);
									} ) }
							</div>
							<Button
								label={ __( 'Upgrade to Pro ➤', 'wholesalex' ) }
								background="secondary"
								buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=addons-upgrade_to_pro&utm_campaign=wholesalex-DB"
								customClass="wsx-mt-32"
							/>
						</div>
					</section>
				</>
			) }
			<div className="wsx-container">
				<div className="wsx-text-center wsx-mb-48">
					<div className="wsx-title wsx-mb-16">
						{ sprintf(
							// translators: %s Allowed File Types.
							__(
								'Ready to Grow Your Business With %s?',
								'wholesalex'
							),
							wholesalex?.plugin_name
						) }
					</div>
					<div className="wsx-color-text-medium">
						{ __(
							'Use these conversion-focused features and earn more profit',
							'wholesalex'
						) }
					</div>
				</div>
				<div className="wsx-column-3 wsx-gap-24 wsx-row-gap-40 wsx-lg-column-2 wsx-sm-column-1">
					{ Object.keys( addonOptions ).map( ( addon ) => {
						return (
							<Card
								key={ addon }
								{ ...addonOptions[ addon ] }
								id={ addon }
								alt={ addonOptions[ addon ].name }
								icon={ addonOptions[ addon ].img }
								onUpdateStatus={ updateAddonStatus }
								onLockClick={ onLockIconClickHandler }
								installPlugin={ installPlugin }
								installText={ installText }
								loading={ loading }
								is_conversation_active={
									addonOptions[ addon ]
										?.is_conversation_active
								}
							/>
						);
					} ) }
				</div>
			</div>

			{ popupStatus && (
				<UpgradeProPopUp
					renderContent={ __(
						'Unlock all of these Features with',
						'wholesalex'
					) }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
			{ whiteLabelPopup && (
				<UpgradeProPopUp
					title={ __(
						'Unlock this Features with Lifetime',
						'wholesalex'
					) }
					onClose={ () => setWhiteLabelPopup( false ) }
				/>
			) }
			<Toast position={ 'top_right' } delay={ 5000 } />
		</div>
	);
};

export default Addons;
