import React, { useContext, useEffect, useState, useRef } from 'react';
import Slider from '../../components/Slider';
import PopupModal from '../../components/PopupModal';
import SettingsModal from '../../components/SettingsModal';
import Button from '../../components/Button';
import Alert from '../../components/Alert';
import { DataContext } from '../../contexts/DataProvider';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';
const Card = ( props ) => {
	const {
		name,
		icon,
		docs,
		proUrl,
		video,
		setting_id: settingId,
		status,
		alt,
		desc,
		demo,
		is_pro: isPro,
		onUpdateStatus,
		id,
		onLockClick,
		depends_message: dependsMessage,
	} = props;
	const isLocked = props.lock_status;
	const [ videoPopupStatus, setVideoPopupStatus ] = useState( false );
	const [ showModalSettings, setShowModalSettings ] = useState( false );

	const { contextData, setContextData } = useContext( DataContext );
	const [ settings, setSettings ] = useState( [] );
	const [ fields, setFields ] = useState( {} );
	const [ activeFields, setActiveFields ] = useState( 'bulkorder' );
	const [ showLoader, setShowLoader ] = useState( false );
	const [ , setPopUpStatus ] = useState( false );
	const { dispatch } = useContext( ToastContexts );
	const [ alert, setAlert ] = useState( false );

	const isRTL = wholesalex_overview?.is_rtl_support;

	useEffect( () => {
		if (
			( id === 'wsx_addon_conversation' && status === 'yes' ) ||
			( id === 'wsx_addon_raq' && status === 'yes' )
		) {
			setContextData( ( prevData ) => ( {
				...prevData,
				isConversationActive: true,
			} ) );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );
	const ref = useRef( null );

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	useEffect( () => {
		setFields( wholesalex_overview.whx_settings_fields );
		setSettings( wholesalex_overview.whx_settings_data );
	}, [ status ] );

	const toggleSettingsModal = () => {
		if ( showModalSettings ) {
			const style = ref?.current?.style;
			if ( ! style ) {
				return;
			}
			sleep( 200 ).then( () => {
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = `translateX(${ isRTL ? '-50%' : '50%' })`;
				style.opacity = '0';

				sleep( 300 ).then( () => {
					setShowModalSettings( ! showModalSettings );
				} );
			} );
		} else {
			setShowModalSettings( ! showModalSettings );
			setTimeout( () => {
				const style = ref?.current?.style;
				if ( ! style ) {
					return;
				}
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = 'translateX(0%)';
				style.opacity = '1';
			}, 10 );
		}
	};

	useEffect( () => {
		const fieldMap = {
			'#bulkorder': 'bulkorder',
			'#conversation': 'conversation',
			'#recaptcha': 'recaptcha',
			'#subaccounts': 'subaccounts',
			'#wallet': 'wallet',
			'#whitelabel': 'whitelabel',
			'#dokan_wholesalex': 'dokan_wholesalex',
			'#wcfm_wholesalex': 'wcfm_wholesalex',
		};
		if ( fieldMap[ settingId ] ) {
			setActiveFields( fieldMap[ settingId ] );
		}
	}, [ settingId, fields ] );

	const fetchData = async ( isType = 'get' ) => {
		setShowLoader( true );
		const attr = {
			type: isType,
			action: 'settings_action',
			nonce: wholesalex.nonce,
			settings,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/settings_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( isType === 'get' ) {
					setSettings( res.data.value );
					setFields( res.data.default );
				} else {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data,
						},
					} );
				}
			} else {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: res.data,
					},
				} );
			}
			setShowLoader( false );
		} );
	};

	const settingOnChangeHandler = ( e ) => {
		const isLock = e.target.value.startsWith( 'pro_' );
		if ( isLock ) {
			setPopUpStatus( true );
		} else if ( e.target.type === 'checkbox' ) {
			setSettings( {
				...settings,
				[ e.target.name ]: e.target.checked ? 'yes' : 'no',
			} );
		} else {
			setSettings( { ...settings, [ e.target.name ]: e.target.value } );
		}
	};

	const getSlideValue = ( fieldName, fieldData ) => {
		if ( settings[ fieldName ] === undefined ) {
			return fieldData.default;
		}
		return settings[ fieldName ];
	};

	const getValue = ( fieldName, fieldData ) =>
		settings[ fieldName ] ?? fieldData.default;

	const saveSettings = ( e ) => {
		e.preventDefault();
		fetchData( 'set' );
	};
	const handleAlertClose = () => {
		setAlert( false );
	};
	const tabData = fields[ activeFields ];

	const sliderOnChangeHandler = async () => {
		if ( isLocked ) {
			onLockClick( id );
		} else {
			if (
				! contextData.isConversationActive &&
				id === 'wsx_addon_raq'
			) {
				setAlert( true );
				return;
			}
			if ( 'no' === status || ! status ) {
				if ( id === 'wsx_addon_conversation' ) {
					setContextData( ( prevData ) => ( {
						...prevData,
						isConversationActive: true,
					} ) );
				}
				await onUpdateStatus( id, true );
				await fetchData( 'get' ); // Fetch settings data after enabling slider
				if ( showModalSettings ) {
					const style = ref?.current?.style;
					if ( ! style ) {
						return;
					}
					sleep( 200 ).then( () => {
						style.transition =
							'all var(--transition-md) ease-in-out';
						style.transform = `translateX(${
							isRTL ? '-50%' : '50%'
						})`;
						style.opacity = '0';

						sleep( 300 ).then( () => {
							setShowModalSettings( ! showModalSettings );
						} );
					} );
				} else {
					setShowModalSettings( ! showModalSettings );
					setTimeout( () => {
						const style = ref?.current?.style;
						if ( ! style ) {
							return;
						}
						style.transition =
							'all var(--transition-md) ease-in-out';
						style.transform = 'translateX(0%)';
						style.opacity = '1';
					}, 10 );
				}
			} else {
				if ( id === 'wsx_addon_conversation' ) {
					setContextData( ( prevData ) => ( {
						...prevData,
						isConversationActive: false,
					} ) );
				}
				onUpdateStatus( id, false );
			}
		}
	};

	const handleSettingsButtonClick = async () => {
		if ( ! showModalSettings ) {
			await fetchData( 'get' ); // Fetch settings data before showing the modal
		}
		toggleSettingsModal();
	};

	const addonVideoModalContent = () => {
		return (
			<div className="wsx-addon-video-container">
				{
					<iframe
						className="wsx-addon-modal-video"
						src={ video }
						allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
						title={ name }
						loading="lazy"
					/>
				}
			</div>
		);
	};

	return (
		<>
			<div className="wsx-addon-card-container">
				{ isLocked && (
					<Button
						onClick={ () => onLockClick( id ) }
						iconName="proLock"
						iconColor="secondary"
						padding="0"
						customClass="wsx-absolute wsx-top-16 wsx-right-16 wsx-pro-lock"
					/>
				) }
				<div
					className={ `wsx-addon-card-body ${
						isPro ? 'wsx-card-pro' : ''
					}` }
				>
					<div className="wsx-addon-card-header wsx-d-flex wsx-gap-16 wsx-item-center wsx-mb-16">
						<img
							src={ icon }
							alt={ alt }
							className="wsx-card-icon"
						/>
						<div className="wsx-font-20 wsx-font-medium wsx-color-tertiary">
							{ name }
						</div>
					</div>
					<div className="wsx-addon-card-desc wsx-color-text-light">
						{ desc }
					</div>
					{ dependsMessage && (
						<div
							className="wsx-depends-message"
							dangerouslySetInnerHTML={ {
								__html: dependsMessage,
							} }
						/>
					) }
				</div>
				<div className="wsx-addon-card-footer">
					{ props?.is_different_plugin && ! props.is_installed && (
						<Button
							label={ id && props?.installText[ id ] }
							background="tertiary"
							onClick={ () => props.installPlugin( id ) }
							customClass="wsx-plr-16"
						>
							{ id && props?.loading[ id ] && <LoadingGif /> }
						</Button>
					) }
					{ props?.is_different_plugin && props.is_installed && (
						<Slider
							name={ id }
							key={ id }
							value={
								isLocked
									? false
									: status === 'yes' || status === true
							}
							onChange={ sliderOnChangeHandler }
							className="wsx-slider-md"
						/>
					) }
					{ ! props?.is_different_plugin && (
						<Slider
							name={ id }
							key={ id }
							value={
								isLocked
									? false
									: status === 'yes' || status === true
							}
							onChange={ sliderOnChangeHandler }
							className="wsx-slider-md"
						/>
					) }
					<div className="wsx-d-flex wsx-item-center wsx-gap-8">
						{ demo && (
							<a href={ demo } className="wsx-link wsx-card-btn">
								{ ' ' }
								View Demo{ ' ' }
							</a>
						) }
						{ docs && (
							<Button
								buttonLink={ docs }
								background="base3"
								iconName="doc"
								padding="6px 4px 6px 6px"
							/>
						) }
						{ video && (
							<Button
								background="base3"
								iconName="play"
								padding="6px 4px 6px 6px"
								onClick={ () => setVideoPopupStatus( true ) }
							/>
						) }
						{ settingId &&
							! props?.is_different_plugin &&
							! isLocked &&
							( status === 'yes' || status === true ) && (
								<Button
									background="base3"
									iconName="settings"
									padding="6px"
									onClick={ handleSettingsButtonClick }
								/>
							) }
						{ settingId &&
							props?.is_different_plugin &&
							props?.is_dependency_active &&
							props.is_installed &&
							status && (
								<Button
									background="base3"
									iconName="settings"
									padding="6px"
									onClick={ handleSettingsButtonClick }
								/>
							) }
						{ isPro && proUrl && (
							<a
								href={ proUrl }
								className="wsx-link wsx-card-btn"
							>
								{ ' ' }
								View Pro{ ' ' }
							</a>
						) }
					</div>
				</div>
				{ video && videoPopupStatus && (
					<PopupModal
						className="wsx-video-modal"
						renderContent={ addonVideoModalContent }
						onClose={ () => setVideoPopupStatus( false ) }
					/>
				) }
			</div>
			{ showModalSettings && (
				<SettingsModal
					showModal={ showModalSettings }
					toggleModal={ toggleSettingsModal }
					name={ activeFields }
					tabData={ tabData }
					fields={ fields }
					saveSettings={ saveSettings }
					showLoader={ showLoader }
					getValue={ getValue }
					getSlideValue={ getSlideValue }
					settingOnChangeHandler={ settingOnChangeHandler }
					settings={ settings }
					setSettings={ setSettings }
					windowRef={ ref }
				/>
			) }
			{ alert && (
				<Alert
					title="Conversation Addon Required to use this feature!"
					description="Please activate conversation addon to use this feature."
					onClose={ handleAlertClose }
				/>
			) }
		</>
	);
};

export default Card;
