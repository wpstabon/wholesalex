import React, { useEffect, useState } from 'react';
import MultiSelect from './MultiSelect';
import Tier from '../../components/Tier';
import Modal from '../../components/Modal';

import Select from '../../components/Select';
import Icons from '../../utils/Icons';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';

function Profile() {
	const [ fields, setFields ] = useState( {} );
	const [ appState, setAppState ] = useState( {
		loading: true,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: false,
	} );

	const [ value, setValue ] = useState( {} );
	const [ tierStatus, setTierStatus ] = useState( {} );
	const [ tier, setTier ] = useState( {} );
	const inititalTier = {
		_id: Date.now().toString(),
		_discount_type: '',
		_discount_amount: '',
		_min_quantity: '',
		_product_filter: '',
		src: 'profile',
	};
	const _isProActivate = wholesalex.is_pro_activated;
	const [ popUpStatus, setPopUpStatus ] = useState( false );

	const fetchData = async ( type = 'get', userAction = '' ) => {
		const attr = {
			type,
			action: 'profile_action',
			nonce: wholesalex.nonce,
			user_id: document.getElementById( 'user_id' ).value,
		};
		if ( type === 'post' ) {
			attr.user_action = userAction;
			attr.user_role = value._wholesalex_role;
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/profile_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( type === 'get' ) {
					setFields( res.data.default );
					setAppState( {
						...appState,
						loading: false,
					} );
					if ( res.data.tiers ) {
						setTier( res.data.tiers );
					}
					if ( res.data.settings ) {
						setValue( res.data.settings );
					}
				} else {
					if ( userAction === 'delete_user' ) {
						window.location = res.data.redirect;
					}

					if ( res.data.settings ) {
						setValue( { ...value, ...res.data.settings } );
					}
				}
			}
		} );
	};

	useEffect( () => {
		fetchData();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const dependencyCheck = ( deps ) => {
		let _flag = true;
		deps.forEach( ( dep ) => {
			if ( value[ dep.key ] !== dep.value ) {
				_flag = false;
			}
		} );
		return _flag;
	};

	const inputData = ( fieldData, field, fieldLabel = '', inputClass ) => {
		const defValue = value[ field ] ? value[ field ] : fieldData.default;
		const depsFullfilled = fieldData.depends_on
			? dependencyCheck( fieldData.depends_on )
			: true;
		return (
			depsFullfilled && (
				<div key={ field } className="wsx-profile-item-wrapper">
					<label
						className="wsx-label wsx-input-label wsx-text-space-break"
						htmlFor={ field }
					>
						{ fieldLabel }
					</label>
					<input
						disabled={ fieldData.is_disable ? true : false }
						type={ fieldData.type }
						onWheel={ ( e ) => {
							e.target.blur();
						} }
						id={ field }
						name={ field }
						value={ defValue }
						onChange={ ( e ) => {
							setValue( { ...value, [ field ]: e.target.value } );
						} }
						className={ `wsx-input ${ inputClass }` }
					/>
				</div>
			)
		);
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option === '' ? 'default' : option,
			label: options[ option ],
		} ) );
	};

	const selectData = ( fieldData, field, fieldLabel = '' ) => {
		const defValue = value[ field ] ? value[ field ] : fieldData.default;
		const depsFullfilled = fieldData.depends_on
			? dependencyCheck( fieldData.depends_on )
			: true;

		return (
			depsFullfilled && (
				<Select
					wrapperClass="wsx-profile-item-wrapper"
					labelClass="wsx-text-space-break"
					id={ field }
					label={ fieldLabel }
					options={ getOptionsArray( fieldData.options ) }
					optionCustomClass="wsx-w-full wsx-text-space-break"
					selectedOptionClass="wsx-text-space-break"
					value={ defValue }
					onChange={ ( e ) => {
						setValue( { ...value, [ field ]: e.target.value } );
					} }
				/>
			)
		);
	};

	const multiselectData = ( fieldData, field, fieldLabel = '' ) => {
		const defValue = value[ field ] ? value[ field ] : fieldData.default;
		const depsFullfilled = fieldData.depends_on
			? dependencyCheck( fieldData.depends_on )
			: true;
		const optionsDependend = fieldData.options_dependent_on
			? fieldData.options_dependent_on
			: false;

		const _options = fieldData.options;
		let flag = true;
		let dependsValue = '';
		if ( optionsDependend ) {
			if ( value[ optionsDependend ] ) {
				dependsValue = value[ optionsDependend ];
			} else {
				flag = false;
			}
		}
		if ( dependsValue !== '' ) {
			flag = true;
		}

		return (
			depsFullfilled &&
			flag && (
				<div className="wsx-profile-item-wrapper">
					<label
						className="wsx-label wsx-input-label wsx-text-space-break"
						htmlFor={ field }
					>
						{ fieldLabel }
					</label>
					<MultiSelect
						name={ field }
						value={ defValue }
						options={ _options }
						placeholder={ fieldData.placeholder }
						onMultiSelectChangeHandler={ (
							fieldName,
							selectedValues
						) => {
							const copy = { ...value };
							copy[ fieldName ] = [ ...selectedValues ];
							setValue( { ...value, ...copy } );
						} }
						isAjax={ fieldData?.is_ajax }
						ajaxAction={ fieldData?.ajax_action }
						ajaxSearch={ fieldData?.ajax_search }
						dependsValue={ optionsDependend ? dependsValue : false }
					/>
				</div>
			)
		);
	};

	const buttonData = ( fieldData, tierName, type = '' ) => {
		return type === 'add' ? (
			<Button
				label={ fieldData.label }
				background="tertiary"
				iconName="plus_20"
				onClick={ ( e ) => {
					e.preventDefault();
					const copy = { ...tier };
					copy[ tierName ].tiers.push( inititalTier );
					setTier( copy );
				} }
			/>
		) : (
			<Button
				label={ fieldData.label }
				background="tertiary"
				onClick={ ( e ) => {
					e.preventDefault();
					const copy = { ...tier };
					copy[ tierName ].tiers.push( inititalTier );
					setTier( copy );
				} }
			/>
		);
	};
	const UpgradeButton = ( fieldData ) => {
		return (
			<Button
				label={ fieldData.label }
				onClick={ ( e ) => {
					e.preventDefault();
					setPopUpStatus( true );
				} }
				background="secondary"
				iconName="angleRight_24"
				iconPosition="after"
				iconAnimation="icon-left"
			/>
		);
	};

	const tierData = ( fieldData, tierName, lockStatus ) => {
		const defaultTier = fieldData._tiers.data;
		return (
			<div key={ tierName } className="wsx-accordion-wrapper-profile">
				{ tier &&
					tier[ tierName ] &&
					tier[ tierName ].tiers.map( ( t, index ) => (
						<Tier
							key={ `wholesalex_${ tierName }_tier_${ index }` }
							fields={ defaultTier }
							tier={ tier }
							setTier={ setTier }
							index={ index }
							tierName={ tierName }
							tierFieldClass="wsx-w-full"
							deleteSpaceLeft="auto"
						/>
					) ) }
				{ ! lockStatus &&
					fieldData._tiers.add.type === 'button' &&
					buttonData( fieldData._tiers.add, tierName, 'add' ) }
				{ lockStatus &&
					fieldData._tiers.upgrade_pro.type === 'button' &&
					UpgradeButton( fieldData._tiers.upgrade_pro, tierName ) }
			</div>
		);
	};

	const tiersData = ( fieldsData, section, fieldLabel = '' ) => {
		const parent = { ...tier };

		const copy = parent[ section ] ? parent[ section ] : {};
		if ( ! copy.tiers ) {
			copy.tiers = [];
			copy.tiers.push( { ...inititalTier } );
			parent[ section ] = copy;
			setTier( parent );
		}

		let _limit = 99999999999;
		const isPro = fieldsData.is_pro;
		if ( isPro ) {
			_limit = fieldsData.pro_data?.value;
		}

		if ( ! _limit ) {
			_limit = 99999999999;
		}

		const isLock =
			tier?.[ section ]?.tiers.length >= _limit && ! _isProActivate;
		return (
			<section
				key={ `wsx-${ section }` }
				className={ section + ' wsx-accordion-wrapper' }
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setTierStatus( {
							...tierStatus,
							[ section ]: ! tierStatus[ section ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setTierStatus( {
								...tierStatus,
								[ section ]: ! tierStatus[ section ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx-role-header` }
					>
						{ fieldLabel }
					</div>
					<span
						className={ `wsx-icon ${
							tierStatus[ section ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ tierStatus[ section ] && (
					<div className="wsx-accordion-body">
						{ fieldsData.attr &&
							Object.keys( fieldsData.attr ).map(
								( fieldData ) => {
									switch (
										fieldsData.attr[ fieldData ].type
									) {
										case 'tier':
											return tierData(
												fieldsData.attr[ fieldData ],
												section,
												isLock
											);
										default:
											return [];
									}
								}
							) }
					</div>
				) }
			</section>
		);
	};

	const [ deleteUserModal, setDeleteUserModal ] = useState( false );
	const profileSettingsData = ( sectionData ) => {
		const onButtonClick = ( e ) => {
			e.preventDefault();

			switch ( e.target.getAttribute( 'data-target' ) ) {
				case 'approve_user':
				case 'active_user':
					fetchData( 'post', 'approve_user' );
					break;
				case 'reject_user':
					fetchData( 'post', 'reject_user' );
					break;
				case 'delete_user':
					setDeleteUserModal( true );
					break;
				case 'deactive_user':
					fetchData( 'post', 'deactive_user' );
					break;
				default:
					break;
			}
		};

		return (
			<div
				key={ 'profile_settings' }
				className="wsx-profile-section wsx-profile-settings-section wsx-accordion-wrapper"
			>
				<div className="wsx-accordion-header wsx-accordion-title">
					{ sectionData.label }
				</div>
				{ deleteUserModal && (
					<Modal
						status={ deleteUserModal }
						setStatus={ setDeleteUserModal }
						title={ wholesalex_profile.i18n.this_user }
						onDelete={ () => {
							fetchData( 'post', 'delete_user' );
						} }
					/>
				) }
				<div className="wsx-accordion-body">
					{ Object.keys( sectionData.attr ).map( ( fieldName ) => {
						const _data = sectionData.attr[ fieldName ];

						switch ( sectionData.attr[ fieldName ].type ) {
							case 'select':
								return selectData(
									_data,
									fieldName,
									sectionData.attr[ fieldName ].label
								);
							case 'text':
								return inputData(
									_data,
									fieldName,
									sectionData.attr[ fieldName ].label,
									'wsx-input-inner-wrapper'
								);
							case 'buttons':
								return (
									<div
										key={ 'profile_settings_buttons' }
										className="profile-setting-buttons"
									>
										<button
											className="hidden"
											onClick={ ( e ) => {
												e.preventDefault();
											} }
										></button>

										{ ( value.__wholesalex_status ===
											'pending' ||
											value.__wholesalex_status ===
												'' ) && (
											<div className="user-status-pending-buttons wsx-btn-group">
												<Button
													label={ _data.btn_approve }
													onClick={ onButtonClick }
													dataTarget="approve_user"
													background="approve"
												/>
												<Button
													label={ _data.btn_reject }
													onClick={ onButtonClick }
													dataTarget="reject_user"
													background="reject"
												/>
												<Button
													label={ _data.btn_delete }
													onClick={ onButtonClick }
													dataTarget="delete_user"
													background="delete"
													iconName="delete"
												/>
											</div>
										) }
										{ value.__wholesalex_status ===
											'reject' && (
											<div className="user-status-pending-buttons wsx-btn-group">
												<Button
													label={ _data.btn_approve }
													onClick={ onButtonClick }
													dataTarget="approve_user"
													background="approve"
												/>
												<Button
													label={ _data.btn_delete }
													onClick={ onButtonClick }
													dataTarget="delete_user"
													background="delete"
													iconName="delete"
												/>
											</div>
										) }
										{ value.__wholesalex_status ===
											'inactive' && (
											<div className="user-status-pending-buttons wsx-btn-group">
												<Button
													label={ _data.btn_active }
													onClick={ onButtonClick }
													dataTarget="active_user"
													background="approve"
												/>
												<Button
													label={ _data.btn_delete }
													onClick={ onButtonClick }
													dataTarget="delete_user"
													background="delete"
													iconName="delete"
												/>
											</div>
										) }
										{ value.__wholesalex_status ===
											'active' && (
											<div className="user-status-active-buttons wsx-btn-group">
												<Button
													label={ _data.btn_delete }
													onClick={ onButtonClick }
													dataTarget="delete_user"
													background="delete"
													iconName="delete"
												/>
												<Button
													label={ _data.btn_deactive }
													onClick={ onButtonClick }
													dataTarget="deactive_user"
													background="deactive"
												/>
											</div>
										) }
									</div>
								);
							default:
								return [];
						}
					} ) }
				</div>
			</div>
		);
	};

	return (
		<div className="wsx-profile wsx-card">
			{ appState.loading && <LoadingGif /> }
			{ ! appState.loading && (
				<>
					{ Object.keys( fields ).map( ( sections ) => (
						<div className="wsx-profile-settings" key={ sections }>
							<div className="wsx-profile-heading">
								{ fields[ sections ].label }
							</div>
							<div className="wsx-profile-body">
								{ Object.keys( fields[ sections ].attr ).map(
									( section ) => {
										switch ( section ) {
											case '_profile_user_settings_section':
												return profileSettingsData(
													fields[ sections ].attr[
														section
													]
												);
											default:
												return (
													<div
														key={ section }
														className={ `wsx-profile-section ${ section }` }
													>
														{ Object.keys(
															fields[ sections ]
																.attr[ section ]
																.attr
														).map( ( field ) => {
															const _fieldData =
																fields[
																	sections
																].attr[
																	section
																].attr[ field ];
															const _fieldLabel =
																_fieldData.label;
															switch (
																_fieldData.type
															) {
																case 'number':
																case 'text':
																	return inputData(
																		_fieldData,
																		field,
																		_fieldLabel,
																		'wsx-input'
																	);
																case 'select':
																	return selectData(
																		_fieldData,
																		field,
																		_fieldLabel
																	);
																case 'multiselect':
																	return multiselectData(
																		_fieldData,
																		field,
																		_fieldLabel
																	);
																case 'tiers':
																	return tiersData(
																		_fieldData,
																		field,
																		_fieldLabel
																	);
																default:
																	return [];
															}
														} ) }
													</div>
												);
										}
									}
								) }
							</div>
						</div>
					) ) }
				</>
			) }
			<input
				type="hidden"
				value={ JSON.stringify( tier ) }
				name="wholesalex_profile_tiers"
			/>
			<input
				type="hidden"
				value={ JSON.stringify( value ) }
				name="wholesalex_profile_settings"
			/>
			{ popUpStatus && (
				<UpgradeProPopUp
					title={ wholesalex_profile.i18n.unlock_heading }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
		</div>
	);
}

export default Profile;
