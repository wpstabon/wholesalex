import React, { useEffect, useRef, useState } from 'react';
import Icons from '../../utils/Icons';
import Tooltip from '../../components/Tooltip';

const MultiSelect = ( {
	name,
	value,
	placeholder,
	customClass,
	onMultiSelectChangeHandler,
	isDisable,
	isAjax,
	ajaxAction,
	ajaxSearch,
	dependsValue,
} ) => {
	const [ showList, setShowList ] = useState( false );
	const [ selectedOptions, setSelectedOptions ] = useState( value );
	const [ optionList, setOptionList ] = useState( [] );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ isSearching, setIsSearching ] = useState( false );
	const [ tempSearchValue, setTempSearchValue ] = useState( '' );
	const [ allOptions, setAllOptions ] = useState( [] );
	const myRef = useRef();

	const onInputChangeHandler = ( e ) => {
		setShowList( true );
		setTempSearchValue( e.target.value );
	};

	const selectOption = ( option ) => {
		setSelectedOptions( [ ...selectedOptions, option ] );

		const searchResult = optionList.filter( ( op ) => {
			const { value: optionValue } = op;
			return (
				optionValue.toString().toLowerCase() !==
				option.value.toString().toLowerCase()
			);
		} );
		setOptionList( searchResult );
		setShowList( false );
		setTempSearchValue( '' );

		onMultiSelectChangeHandler( name, [ ...selectedOptions, option ] );
	};

	const deleteOption = ( option ) => {
		const selectedOptionAfterDeleted = selectedOptions.filter( ( op ) => {
			const { value: optionValue } = op;
			return (
				optionValue.toString().toLowerCase() !==
				option.value.toString().toLowerCase()
			);
		} );
		setSelectedOptions( selectedOptionAfterDeleted );
		setTempSearchValue( '' );
		onMultiSelectChangeHandler( name, selectedOptionAfterDeleted );
	};

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setShowList( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const performAjaxSearch = async ( signal ) => {
		setIsSearching( true );
		const attr = {
			type: 'get',
			action: 'dynamic_rule_action',
			nonce: wholesalex.nonce,
			query: searchValue,
			ajax_action: ajaxAction,
		};
		try {
			const res = await wp.apiFetch( {
				path: '/wholesalex/v1/dynamic_rule_action',
				method: 'POST',
				data: attr,
				signal,
			} );

			if ( res.status ) {
				let selectedOptionValues = [];
				if ( selectedOptions.length > 0 ) {
					selectedOptionValues = selectedOptions.map(
						( option ) => option.value
					);
				}
				const searchResult = res?.data?.filter( ( option ) => {
					const { value: optionValue } = option;
					return selectedOptionValues.indexOf( optionValue ) === -1;
				} );

				searchResult.sort( ( a, b ) => a.name.length - b.name.length );

				setOptionList( searchResult );

				setIsSearching( false );
			} else {
				setIsSearching( false );
			}
		} catch ( error ) {
			if ( error.name === 'AbortError' ) {
				// Request was cancelled
			}
		}
	};

	const getOptions = async ( signal ) => {
		setIsSearching( true );
		const attr = {
			type: 'get',
			action: 'dynamic_rule_action',
			nonce: wholesalex.nonce,
			ajax_action: ajaxAction,
		};
		if ( dependsValue ) {
			attr.depends = dependsValue;
		}
		try {
			const res = await wp.apiFetch( {
				path: '/wholesalex/v1/dynamic_rule_action',
				method: 'POST',
				data: attr,
				signal,
			} );

			if ( res.status ) {
				let selectedOptionValues = [];
				if ( selectedOptions.length > 0 ) {
					selectedOptionValues = selectedOptions.map(
						( option ) => option.value
					);
				}

				const searchResult = res?.data?.filter( ( option ) => {
					const { value: optionValue } = option;
					return selectedOptionValues.indexOf( optionValue ) === -1;
				} );

				searchResult.sort( ( a, b ) => a.name.length - b.name.length );

				setAllOptions( res.data );

				setOptionList( searchResult );

				setIsSearching( false );
			} else {
				setIsSearching( false );
			}
		} catch ( error ) {
			if ( error.name === 'AbortError' ) {
				// Request was cancelled
			}
		}
	};

	const performNonAjaxSearch = () => {
		let selectedOptionValues = [];
		if ( selectedOptions.length > 0 ) {
			selectedOptionValues = selectedOptions.map(
				( option ) => option.value
			);
		}
		const searchResult = allOptions.filter( ( option ) => {
			const { name: optionName, value: optionValue } = option;
			return (
				optionName
					.toLowerCase()
					.includes( searchValue.toLowerCase() ) &&
				selectedOptionValues.indexOf( optionValue ) === -1
			);
		} );
		setOptionList( searchResult );
	};

	const abortController = useRef( null );
	useEffect( () => {
		abortController.current = new AbortController();
		const { signal } = abortController.current;
		if ( ! ajaxSearch && isAjax ) {
			getOptions( signal );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	useEffect( () => {
		if ( ajaxSearch ) {
			abortController.current = new AbortController();
			const { signal } = abortController.current;
			if ( searchValue.length >= 2 ) {
				performAjaxSearch( signal );
			}
			return () => {
				if ( abortController.current ) {
					abortController.current.abort( 'Duplicate' );
				}
			};
		}
		performNonAjaxSearch();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ searchValue, setSearchValue ] );

	useEffect( () => {
		if ( ajaxSearch ) {
			if ( tempSearchValue.length > 1 ) {
				const delay = setTimeout(
					() => setSearchValue( tempSearchValue ),
					500
				);
				return () => clearTimeout( delay );
			}
		} else {
			setSearchValue( tempSearchValue );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ tempSearchValue ] );

	return (
		<div
			className={ `wsx-multiselect-wrapper ${
				isDisable ? 'locked' : ''
			}` }
			key={ `wsx-multiselect-${ name }` }
		>
			<div className="wsx-multiselect-inputs">
				<div className="wsx-multiselect-input-wrapper">
					{ selectedOptions.length > 0 &&
						selectedOptions.map( ( option, index ) => {
							return (
								<span
									key={ `wsx-multiselect-opt-${ name }-${ option.value }-${ index }` }
									className="wsx-selected-option"
								>
									<span
										tabIndex={ -1 }
										className="wsx-icon-cross wsx-lh-0"
										onClick={ () => deleteOption( option ) }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												deleteOption( option );
											}
										} }
										role="button"
									>
										{ Icons.cross }
									</span>
									<Tooltip
										content={ option.name }
										position="top"
										onlyText={ true }
									>
										<div className="multiselect-option-name">
											{ option.name }
										</div>
									</Tooltip>
								</span>
							);
						} ) }
					<div
						className={ `wsx-multiselect-option-wrapper ${
							selectedOptions.length &&
							selectedOptions.length !== 0
								? ''
								: 'wsx-w-full'
						}` }
					>
						<input
							key={ `wsx-input-${ name }` }
							disabled={ isDisable ? true : false }
							id={ name }
							tabIndex={ 0 }
							autoComplete="off"
							value={ tempSearchValue }
							className={ `wsx-input ${ customClass }` }
							placeholder={
								selectedOptions.length > 0 ? '' : placeholder
							}
							onChange={ ( e ) => onInputChangeHandler( e ) }
							onClick={ ( e ) => onInputChangeHandler( e ) }
						/>
					</div>
				</div>
			</div>
			<div ref={ myRef } key={ `wsx-${ name }` }>
				{ showList && (
					<>
						{ ! isSearching &&
							optionList.length > 0 &&
							tempSearchValue.length > 1 && (
								<div
									className="wsx-card wsx-multiselect-options wsx-scrollbar"
									key={ `wsx-opt-${ name }` }
								>
									{ optionList.map( ( option, index ) => {
										return (
											<div
												className="wsx-multiselect-option"
												key={ `wsx-opt-${ name }-${ option.value }-${ index }` }
												onClick={ () =>
													selectOption( option )
												}
												onKeyDown={ ( e ) => {
													if (
														e.key === 'Enter' ||
														e.key === ' '
													) {
														selectOption( option );
													}
												} }
												role="button"
												tabIndex="0"
											>
												{ option.name }
											</div>
										);
									} ) }
								</div>
							) }
						{ ! isSearching &&
							tempSearchValue.length > 1 &&
							optionList.length === 0 && (
								<div
									key={ `wsx-${ name }-not-found` }
									className="wsx-card wsx-multiselect-options wsx-scrollbar"
								>
									<div className="wsx-multiselect-option-message">
										{
											wholesalex_profile.i18n
												.no_data_found
										}
									</div>
								</div>
							) }
						{ ! isSearching && tempSearchValue.length < 2 && (
							<div
								key={ `wsx-${ name }-not-found` }
								className="wsx-card wsx-multiselect-options wsx-scrollbar"
							>
								<div className="wsx-multiselect-option-message">
									{
										wholesalex_profile.i18n
											.enter_more_character
									}
								</div>
							</div>
						) }
						{ isSearching && (
							<div
								key={ `wsx-${ name }-not-found` }
								className="wsx-card wsx-multiselect-options wsx-scrollbar"
							>
								<div className="wsx-multiselect-option-message">
									{ wholesalex_profile.i18n.searching }
								</div>
							</div>
						) }
					</>
				) }
			</div>
		</div>
	);
};
export default MultiSelect;
