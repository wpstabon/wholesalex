import React from 'react';
import ReactDOM from 'react-dom';
import Profile from './Profile';

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( '_wholesalex_edit_profile' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<Profile />
			</React.StrictMode>,
			document.getElementById( '_wholesalex_edit_profile' )
		);
	}
} );
