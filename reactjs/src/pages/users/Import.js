import React, { useContext, useEffect, useState } from 'react';
import OverlayWindow from '../../components/OverlayWindow';
import RadioButtons from '../../components/RadioButtons';
import DragDropFileUpload from '../../components/DragDropFileUpload';
import Input from '../../components/Input';
import PopupModal from '../../components/PopupModal';

import { __ } from '@wordpress/i18n';
import Slider from '../../components/Slider';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';

const Import = ( { windowRef, toggleOverlayWindow } ) => {
	const [ selectedFile, setSelectedFile ] = useState( null );
	const [ , setMessages ] = useState( [] );
	const [ loader, setLoader ] = useState( false );
	const [ isUpdateExisting, setIsUpdateExisting ] = useState( false );
	const [ showPopup, setShowPopUp ] = useState( false );
	const [ countData, setCountData ] = useState( {} );
	const [ log, setLog ] = useState( '' );

	const [ processPerIteration, setProcessPerIteration ] = useState( 10 );
	const [ updateUsername ] = useState( '' );
	const [ findExistingUser, setFindExistingUser ] = useState( 'username' );
	const { dispatch } = useContext( ToastContexts );

	const fileUpload = () => {
		if ( ! selectedFile ) {
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'success',
					message:
						'Please Select a valid csv file to process import!',
				},
			} );
			return;
		}

		if ( loader ) {
			alert( 'Please Wait to complete existing import request!' );
			return;
		}
		setLoader( true );

		const formData = new FormData();
		formData.append( 'file', selectedFile );
		formData.append( 'action', 'wholesalex_import_users' );
		formData.append( 'update_existing', isUpdateExisting ? 'yes' : 'no' );
		formData.append( 'nonce', wholesalex.nonce );
		formData.append( 'process_per_iteration', processPerIteration );
		if ( isUpdateExisting ) {
			formData.append( 'find_user_by', findExistingUser );
			formData.append(
				'is_update_username',
				updateUsername ? 'yes' : 'no'
			);
		}
		fetch( wholesalex.ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.success ) {
					setMessages( res.data );
					if ( res?.data?.message ) {
						dispatch( {
							type: 'ADD_MESSAGE',
							payload: {
								id: Date.now().toString(),
								type: 'error',
								message: res.data.message,
							},
						} );
						setLoader( false );
						return;
					}

					if ( res.data.process < res.data.total ) {
						//import user
						handleImport();
					} else {
						completeImport( res );
					}
				} else {
					window.alert( 'Error Occured!' );
				}
			} );
	};

	const handleImport = async () => {
		const formData = new FormData();

		formData.append( 'action', 'wholesalex_process_import_users' );
		formData.append( 'update_existing', isUpdateExisting ? 'yes' : 'no' );
		formData.append( 'nonce', wholesalex.nonce );
		formData.append( 'process_per_iteration', processPerIteration );
		if ( isUpdateExisting ) {
			formData.append( 'find_user_by', findExistingUser );
			formData.append( 'is_update_username', updateUsername );
		}

		fetch( wholesalex.ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.success ) {
					if ( res.data.process < res.data.total ) {
						//import user
						handleImport();
					} else {
						completeImport( res );
					}
				} else {
					window.alert( 'Error Occured!' );
				}
			} );
	};

	const completeImport = ( res ) => {
		setLoader( false );
		const _data = {};
		if ( res?.data?.insert_count ) {
			_data.insert_count = res.data.insert_count;
		}
		if ( res?.data?.update_count ) {
			_data.update_count = res.data.update_count;
		}
		if ( res?.data?.skipped_count ) {
			_data.skipped_count = res.data.skipped_count;
		}
		setCountData( _data );
		if ( res?.data?.log ) {
			setLog( res.data.log );
			setShowPopUp( true );
		}
	};

	const downloadLog = () => {
		const element = document.createElement( 'a' );
		const file = new Blob( [ log ], { type: 'text/plain' } );
		element.href = URL.createObjectURL( file );
		element.download = `import_log_${ Date.now() }`;
		document.body.appendChild( element ); // Required for this to work in FireFox
		element.click();
	};

	const content = () => {
		const renderPopupContent = () => {
			return (
				<div className="wholesalex-progress-form-content  wholesalex-importer">
					<section className="wholesalex-importer-done success-popup-content">
						<span className="success-icon">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 52 52"
								className="wholesalex-animation"
							>
								<circle
									className="wholesalex__circle"
									cx="26"
									cy="26"
									r="25"
									fill="none"
								/>
								<path
									className="wholesalex__check"
									fill="none"
									d="M14.1 27.2l7.1 7.2 16.7-16.8"
								/>
							</svg>
						</span>
						<div className="import-complete-text message">
							{ __( 'Import Complete!', 'wholesalex' ) }
						</div>

						{ countData.update_count && (
							<div className="wholesalex-imported">
								{ countData.update_count } Users Updated{ ' ' }
							</div>
						) }
						{ countData.insert_count && (
							<div className="wholesalex-updated">
								{ ' ' }
								{ countData.insert_count } Users Inserted{ ' ' }
							</div>
						) }
						{ countData.skipped_count && (
							<div className="wholesalex-skipped">
								{ countData.skipped_count } Users Skipped{ ' ' }
							</div>
						) }

						<div className="log_message">
							{ ' ' }
							<span
								className="wsx-link"
								role="button"
								tabIndex={ 0 }
								onClick={ downloadLog }
								onKeyPress={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										downloadLog();
									}
								} }
							>
								Download
							</span>{ ' ' }
							Log For More Info{ ' ' }
						</div>
						<span
							className="wsx-btn wsx-bg-delete"
							role="button"
							tabIndex={ 0 }
							onClick={ () => {
								setShowPopUp( false );
							} }
							onKeyPress={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setShowPopUp( false );
								}
							} }
						>
							Close
						</span>
					</section>
				</div>
			);
		};
		const existingUsersOptions = {
			username: 'Username',
			email: 'Email',
		};

		return (
			<div className="wsx-d-flex wsx-flex-column wsx-gap-20 wsx-relative">
				{ loader && <LoadingGif insideContainer={ true } /> }
				{ showPopup && (
					<PopupModal
						renderContent={ renderPopupContent }
						onClose={ () => setShowPopUp( false ) }
					/>
				) }

				<DragDropFileUpload
					name={ 'input_file' }
					label={ __( 'Upload CSV', 'wholesalex' ) }
					help={ __(
						'You can upload only csv file format',
						'wholesalex'
					) }
					onChange={ ( file ) => {
						setSelectedFile( file );
					} }
					allowedTypes={ [ 'text/csv' ] }
				/>

				<Slider
					label={ __( 'Update Existing Users', 'wholesalex' ) }
					help={ __(
						'Selecting "Update Existing Users" will only update existing users. No new user will be added.',
						'wholesalex'
					) }
					onChange={ () => setIsUpdateExisting( ! isUpdateExisting ) }
					value={ isUpdateExisting }
					className="wsx-slider-sm"
					isLabelSide={ true }
				/>
				{ isUpdateExisting && (
					<div className="wholesalex_find_existing_users">
						<RadioButtons
							label={ __(
								'Find Existing Users By:',
								'wholesalex'
							) }
							options={ existingUsersOptions }
							name={ 'wsx_how_find_existing_users' }
							value={ findExistingUser }
							onChange={ ( e ) => {
								setFindExistingUser( e.target.value );
							} }
							defaultValue={ 'username' }
							help={ __(
								"Option to detect user from the uploaded CSV's email or username field.",
								'wholesalex'
							) }
							flexView={ true }
							labelSpace="8"
						/>
					</div>
				) }
				<Input
					className="wholesalex_import_export_ppi_field"
					type="text"
					label={ 'Process Per Iteration' }
					name={ 'process_per_iteration' }
					value={ processPerIteration }
					onChange={ ( e ) =>
						setProcessPerIteration( e.target.value )
					}
					help={
						"Low process per iteration (PPI) increases the import's accuracy and success rate. A (PPI) higher than your server's maximum execution time might fail the import."
					}
				/>

				<Button
					label={ __( 'Import', 'wholesalex' ) }
					background="primary"
					customClass="wsx-center-hz wsx-w-half wsx-text-center wsx-mt-20"
					onClick={ fileUpload }
				/>
			</div>
		);
	};

	return (
		<OverlayWindow
			windowRef={ windowRef }
			heading={ 'Import Users' }
			onClose={ toggleOverlayWindow }
			content={ content }
		/>
	);
};

export default Import;
