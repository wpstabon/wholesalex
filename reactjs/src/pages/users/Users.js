import React, { useContext, useEffect, useRef, useState } from 'react';
import Search from '../../components/Search';
import Slider from '../../components/Slider';
import Import from './Import';
import Export from './Export';
import Dropdown from '../../components/Dropdown';
import Modal from '../../components/Modal';
import Toast from '../../components/Toast';
import SelectWithOptGroup from '../../components/SelectWithOptGroup';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import Icons from '../../utils/Icons';
import UserPagination from '../../components/UserPagination';
import Alert from '../../components/Alert';
import LoadingGif from '../../components/LoadingGif';
import Select from '../../components/Select';
import { ToastContexts } from '../../context/ToastsContexts';

const Users = () => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	const [ initialLoader, setInitialLoader ] = useState( false );
	const [ users, setUsers ] = useState( [] );
	const [ paginationData, setPaginationData ] = useState( {
		totalUsers: 0,
		currentPage: 1,
		loader: false,
	} );

	const [ alert, setAlert ] = useState( false );

	const [ isAlertVisible, setIsAlertVisible ] = useState( false );
	const [ pendingAction, setPendingAction ] = useState( null );

	const [ bulkAction, setBulkAction ] = useState( '' );
	const [ selectedUsers, setSelectedUsers ] = useState( [] );
	const [ filterStatus, setFilterStatus ] = useState( '' );
	const [ filterRole, setFilterRole ] = useState( '' );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ tempSearchValue, setTempSearchValue ] = useState( searchValue );
	const [ isInitialRender, setIsInitialRender ] = useState( false );
	const [ columns, setColumns ] = useState(
		wholesalex_overview?.whx_users_heading
	);
	const [ overlayWindow, setOverlayWindow ] = useState( {
		status: false,
		type: '',
	} );
	const [ currentUser, setCurrentUser ] = useState( '' );
	const [ deleteModalStatus, setDeleteModalStatus ] = useState( false );
	const { state, dispatch } = useContext( ToastContexts );

	const [ isBulkAction, setIsBulkAction ] = useState( false );
	const [ transactionFields, setTransactionFields ] = useState( [] );
	const [ isPopupOpen, setIsPopupOpen ] = useState( false );
	const [ isLoading, setIsLoading ] = useState( false );

	const fetchData = async (
		type = 'get',
		page = 1,
		isInitial = true,
		message = '',
		itemsPerPage = 10
	) => {
		if ( isInitial ) {
			setInitialLoader( true );
		} else {
			setPaginationData( { ...paginationData, loader: true } );
		}
		const attr = {
			type,
			action: 'users',
			nonce: wholesalex.nonce,
			page: page ? page : 1,
			search: searchValue,
			itemsPerPage,
		};

		if ( filterStatus ) {
			attr.status = filterStatus;
		}
		if ( filterRole ) {
			attr.role = filterRole;
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/users',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				setUsers( res.data.users );
				setPaginationData( {
					...paginationData,
					totalUsers: res.data.total_users,
					loader: false,
					currentPage: page,
				} );
				setSelectedUsers( [] );
				if ( message ) {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message,
						},
					} );
				}
			}

			if ( isInitial ) {
				setInitialLoader( false );
			}
		} );
	};
	const updateStatus = async (
		type = 'get',
		page = 1,
		userAction = '',
		ids = ''
	) => {
		setPaginationData( { ...paginationData, loader: true } );
		const attr = {
			type,
			action: 'users',
			nonce: wholesalex.nonce,
			page: page ? page : 1,
			search: searchValue,
		};

		if ( filterStatus ) {
			attr.status = filterStatus;
		}
		if ( filterRole ) {
			attr.role = filterRole;
		}

		switch ( type ) {
			case 'update_status':
				attr.user_action = userAction;
				attr.id = ids;
				break;
			case 'bulk_action':
				attr.user_action = userAction;
				attr.ids = ids;
				break;

			default:
				break;
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/users',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				fetchData( 'get', paginationData.currentPage, false, res.data );
			}
		} );
	};

	const hasPagination = () => {
		return (
			wholesalex_overview.whx_users_user_per_page <=
			paginationData.totalUsers
		);
	};

	useEffect( () => {
		// fetchData();
		setIsInitialRender( true );
	}, [] );

	const handleTransactionHistory = ( userId ) => {
		setIsPopupOpen( true );
		setIsLoading( true );
		const data = new FormData();
		data.append( 'action', 'get_user_transactions' );
		data.append( 'user_id', userId );

		fetch( ajaxurl, {
			method: 'POST',
			body: data,
		} )
			.then( ( response ) => response.json() )
			.then( ( result ) => {
				if ( result.success ) {
					setTransactionFields( result.data );
				} else {
					setTransactionFields( [] );
				}
			} )
			.catch( () => {
				setTransactionFields( [] );
			} )
			.finally( () => {
				setIsLoading( false );
			} );
	};

	const handleTogglePopup = () => {
		setIsPopupOpen( ! isPopupOpen );
		setTransactionFields( [] );
	};

	//Transaction Markup
	const generateTransactionTable = () => {
		return (
			<>
				{ isPopupOpen && (
					<div
						className="wsx-popup-overlay wsx-transaction-popup"
						onClick={ handleTogglePopup }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								handleTogglePopup( e );
							}
						} }
						role="button"
						tabIndex="0"
					>
						<div
							className="wsx-card"
							onClick={ ( e ) => e.stopPropagation() }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									e.stopPropagation();
								}
							} }
							role="button"
							tabIndex="0"
						>
							<div
								className="wsx-lh-0 wsx-icon"
								onClick={ handleTogglePopup }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										handleTogglePopup( e );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.cross }
							</div>
							{ isLoading ? (
								<div>Loading transactions...</div>
							) : (
								<>
									{ ! transactionFields ||
									transactionFields.length === 0 ? (
										<div>No transactions available.</div>
									) : (
										<div className="wsx-lists-table-wrapper wsx-scrollbar">
											<table className="wsx-table wsx-lists-table wsx-w-full wsx-shadow-none">
												<thead className="wsx-lists-table-header wsx-bg-text-medium">
													<tr className="wsx-lists-table-header-row wsx-color-text-reverse">
														<td className="wsx-lists-table-column">
															Transactions ID
														</td>
														<td className="wsx-lists-table-column">
															Type
														</td>
														<td className="wsx-lists-table-column">
															Amount
														</td>
														<td className="wsx-lists-table-column">
															Date
														</td>
														<td className="wsx-lists-table-column">
															Details
														</td>
													</tr>
												</thead>
												<tbody className="wsx-lists-table-body">
													{ transactionFields.map(
														( transaction, i ) => (
															<tr
																key={ `wallet_transaction_${ i }` }
															>
																<td className="wsx-transaction-id wsx-lists-table-column">
																	{
																		transaction._id
																	}
																</td>
																<td className="wsx-transaction-type wsx-lists-table-column">
																	{
																		transaction.type
																	}
																</td>
																<td className="wsx-amount wsx-lists-table-column">
																	{ wholesalex_overview.whx_dr_currency +
																		( transaction.amount !==
																			undefined &&
																		transaction.amount !==
																			null
																			? parseFloat(
																					transaction.amount
																			  ).toFixed(
																					2
																			  )
																			: '0.00' ) }
																</td>
																<td className="wsx-date wsx-lists-table-column">
																	{ new Date(
																		parseInt(
																			transaction.timestamp
																		) * 1000
																	)
																		.toDateString()
																		.split(
																			' '
																		)
																		.slice(
																			1
																		)
																		.join(
																			' '
																		) }
																</td>
																<td className="wsx-details wsx-lists-table-column">
																	{
																		transaction.reason
																	}
																</td>
															</tr>
														)
													) }
												</tbody>
											</table>
										</div>
									) }
								</>
							) }
						</div>
					</div>
				) }
			</>
		);
	};

	const renderTableCell = ( data, fieldType, name, user = '' ) => {
		if ( fieldType === '3dot' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<Dropdown
						className="wsx-3dot-wrapper"
						renderContent={ renderDropDownContent }
						padding="4"
						onClickCallback={ () => {
							setCurrentUser( user );
						} }
						iconName="dot3"
						iconRotation="none"
					/>
				</td>
			);
		} else if ( fieldType === 'html' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
					dangerouslySetInnerHTML={ { __html: data } }
				></td>
			);
		} else if ( fieldType === 'btn' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<span
						role="button"
						tabIndex="0"
						onClick={ () => handleTransactionHistory( user.ID ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								handleTransactionHistory( user.ID );
							}
						} }
						className="wsx-btn wsx-bg-primary wsx-btn-border wsx-btn-border-primary wsx-btn-sm"
					>
						View
					</span>
				</td>
			);
		}
		if ( name === 'full_name' || name === 'email' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<a
						target="_blank"
						href={ user?.edit_profile }
						rel="noreferrer"
					>
						<span className="wsx-link wsx-ellipsis">{ data }</span>
					</a>
				</td>
			);
		}
		return (
			<td
				className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
			>
				<span
					className={ `${
						Number.isInteger( data ) ? '' : 'wsx-ellipsis'
					}` }
				>
					{ data }
				</span>
			</td>
		);
	};

	const updateAllUserSelection = () => {
		if ( selectedUsers.length === users.length ) {
			setSelectedUsers( [] );
		} else {
			const tempUsers = [ ...users ];
			const _selectedUsers = [];
			tempUsers.forEach( ( user ) => {
				_selectedUsers.push( user.ID );
			} );
			setSelectedUsers( _selectedUsers );
		}
	};

	const selectUser = ( e ) => {
		const userId = Number( e.target.getAttribute( 'data-id' ) );
		if ( selectedUsers.includes( userId ) ) {
			const _selectedUsers = selectedUsers.filter(
				( id ) => id !== userId
			);
			setSelectedUsers( _selectedUsers );
		} else {
			setSelectedUsers( [ ...selectedUsers, userId ] );
		}
	};

	const onBulkActionChange = ( e ) => {
		setBulkAction( e.target.value );
	};

	const searchUser = () => {
		fetchData( 'get', 1, false );
	};

	// Fetch data on component load
	useEffect( () => {
		wp.apiFetch( { path: '/wholesalex/v1/get-users-filters' } )
			.then( ( response ) => {
				const updatedColumns = { ...columns };
				Object.keys( response.filters ).forEach( ( key ) => {
					const columnKey = key.replace( 'users_filter_', '' );
					if ( updatedColumns[ columnKey ] ) {
						updatedColumns[ columnKey ].status = response.filters[
							key
						]
							? true
							: false;
					}
				} );
				setColumns( updatedColumns );
			} )
			.catch( () => {} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	// Function to update a specific column's status
	const setColumnStatus = ( columnName, status ) => {
		const _columns = { ...columns };
		const _column = { ..._columns[ columnName ], status };
		_columns[ columnName ] = _column;
		setColumns( _columns );
	};

	// Function to save the updated column status to the backend
	const handleSaveData = ( filterKey, column ) => {
		wp.apiFetch( {
			path: '/wholesalex/v1/save-users-filters',
			method: 'POST',
			data: {
				filters: {
					[ filterKey ]: ! columns[ column ].status,
				},
			},
		} )
			.then( () => {} )
			.catch( () => {} );
	};

	const columnsSelectionContent = () => {
		return Object.keys( columns ).map( ( column ) => {
			const filterKey = `users_filter_${ column }`;
			return (
				<div
					className="wsx-dropdown-actions-list"
					onClick={ ( e ) => e.stopPropagation() }
					key={ filterKey }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							e.stopPropagation();
						}
					} }
					role="button"
					tabIndex="0"
				>
					<Slider
						label={ columns[ column ].title }
						name={ `wsx-overview-column-${ column }` }
						value={ columns[ column ].status }
						isLabelSide={ true }
						className="wsx-slider-md"
						onChange={ ( e ) => {
							e.stopPropagation();
							setColumnStatus(
								column,
								! columns[ column ].status
							);
							handleSaveData( filterKey, column );
						} }
					/>
				</div>
			);
		} );
	};

	const onFilterStatusChange = ( e ) => {
		setFilterStatus( e.target.value );
	};
	const onFilterRoleChange = ( e ) => {
		setFilterRole( e.target.value );
	};

	useEffect( () => {
		fetchData( 'get', 1, false );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ filterStatus ] );

	useEffect( () => {
		fetchData( 'get', 1, false );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ filterRole ] );

	useEffect( () => {
		if ( users.length === 0 && searchValue === '' && ! isInitialRender ) {
			fetchData();
		} else {
			searchUser( searchValue );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ searchValue ] );

	useEffect( () => {
		const delay = setTimeout(
			() => setSearchValue( tempSearchValue ),
			500
		);
		return () => clearTimeout( delay );
	}, [ tempSearchValue ] );

	const rowActionHandler = ( action, id ) => {
		switch ( action ) {
			case 'edit':
				window.open( currentUser.edit_profile, '_blank' );
				break;
			case 'delete':
				setDeleteModalStatus( true );
				break;
			case 'active':
			case 'reject':
			case 'pending':
				updateStatus(
					'update_status',
					paginationData.currentPage,
					action,
					id
				);
				break;

			default:
				break;
		}
	};

	const renderDropDownContent = () => {
		const _isPending = currentUser?.wholesalex_status === 'pending';
		const options = [
			{
				label: __( 'Edit', 'wholesalex' ),
				iconClass: 'edit',
				action: 'edit',
				url: '',
			},
			_isPending
				? {
						label: __( 'Active', 'wholesalex' ),
						iconClass: 'tick',
						action: 'active',
						url: '',
				  }
				: '',
			_isPending
				? {
						label: __( 'Reject', 'wholesalex' ),
						iconClass: 'cross',
						action: 'reject',
						url: '',
				  }
				: '',
			! _isPending
				? {
						label: __( 'Pending', 'wholesalex' ),
						iconClass: 'dot3',
						action: 'pending',
						url: '',
				  }
				: '',
			{
				label: __( 'Delete', 'wholesalex' ),
				iconClass: 'delete',
				action: 'delete',
				url: '',
			},
		];

		return options.map( ( option ) => {
			return (
				option && (
					<div
						className="wsx-row-actions-list"
						onClick={ () => {
							rowActionHandler( option?.action, currentUser.ID );
						} }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								rowActionHandler(
									option?.action,
									currentUser.ID
								);
							}
						} }
						role="button"
						tabIndex="0"
					>
						<span className="wsx-icon wsx-lh-0">
							{ Icons[ option.iconClass ] }
						</span>
						<span className="wsx-row-actions-list-link">
							<span className="wsx-row-actions-list-link-label">
								{ option.label }
							</span>
						</span>
					</div>
				)
			);
		} );
	};

	const ref = useRef( null );

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const toggleOverlayWindow = ( e, type ) => {
		if ( overlayWindow.status ) {
			const style = ref?.current?.style;
			if ( ! style ) {
				return;
			}
			sleep( 200 ).then( () => {
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = `translateX(${ isRTL ? '-50%' : '50%' })`;
				style.opacity = '0';

				sleep( 300 ).then( () => {
					setOverlayWindow( { ...overlayWindow, status: false } );
				} );
			} );
		} else {
			setOverlayWindow( { ...overlayWindow, type, status: true } );
			setTimeout( () => {
				const style = ref?.current?.style;
				if ( ! style ) {
					return;
				}
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = 'translateX(0%)';
				style.opacity = '1';
			}, 10 );
		}
	};

	const onImportClick = ( e ) => {
		toggleOverlayWindow( e, 'import' );
	};

	const onExportClick = ( type ) => {
		toggleOverlayWindow( null, type );
	};

	const onExportSuccess = () => {
		setSelectedUsers( [] );
	};

	const renderOverlayWindow = () => {
		if ( ! overlayWindow.status ) {
			return;
		}
		const renderOverlayContent = () => {
			switch ( overlayWindow.type ) {
				case 'import':
					return (
						<Import
							toggleOverlayWindow={ toggleOverlayWindow }
							overlayWindowStatus={ overlayWindow.status }
							windowRef={ ref }
						/>
					);
				case 'export':
				case 'export_all':
					return (
						<Export
							onExportSuccess={ onExportSuccess }
							getFilterStatus={ filterStatus }
							getFilterRole={ filterRole }
							getSearchValue={ searchValue }
							getSelectedUserIds={
								overlayWindow.type === 'export'
									? selectedUsers
									: []
							}
							toggleOverlayWindow={ toggleOverlayWindow }
							overlayWindowStatus={ overlayWindow.status }
							windowRef={ ref }
						/>
					);
				default:
					return null;
			}
		};

		return <>{ renderOverlayContent() }</>;
	};

	const deleteModal = () => {
		return (
			deleteModalStatus && (
				<Modal
					smallModal={ true }
					title={
						isBulkAction
							? __( 'Selected Users', 'wholesalex' )
							: currentUser.full_name
					}
					status={ deleteModalStatus }
					setStatus={ setDeleteModalStatus }
					onDelete={ () => {
						updateStatus(
							isBulkAction ? 'bulk_action' : 'update_status',
							paginationData.currentPage,
							'delete',
							isBulkAction ? selectedUsers : currentUser.ID
						);
						setDeleteModalStatus( false );
						setSelectedUsers( [] );
					} }
				/>
			)
		);
	};

	const handleBulkAction = () => {
		if (
			! bulkAction ||
			bulkAction === 'default' ||
			bulkAction === '' ||
			selectedUsers.length === 0
		) {
			setAlert( true );
			return;
		}

		setPendingAction( bulkAction );
		setIsAlertVisible( true );
	};

	const confirmBulkAction = () => {
		setIsBulkAction( true );
		if ( 'export' === bulkAction ) {
			onExportClick( 'export' );
		} else if ( 'delete' === bulkAction ) {
			setDeleteModalStatus( true );
		} else if ( 'export_all' === bulkAction ) {
			onExportClick( 'export_all' );
		} else {
			updateStatus(
				'bulk_action',
				paginationData.currentPage,
				bulkAction,
				selectedUsers
			);
		}

		setIsAlertVisible( false );
	};

	const handleAlertClose = () => {
		setIsAlertVisible( false );
		setPendingAction( null );
		setAlert( false );
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				<div className="wsx-user-list-wrapper">
					<div className="wsx-justify-wrapper wsx-slg-justify-wrapper wsx-gap-12 wsx-mb-24">
						<div className="wsx-d-flex wsx-gap-32 wsx-item-center">
							<div className="wsx-d-flex wsx-gap-8 wsx-item-center">
								<SelectWithOptGroup
									selectClass={ 'wsx-ellipsis' }
									selectionText={ __(
										'Bulk Actions',
										'wholesalex'
									) }
									optionGroup={
										wholesalex_overview.whx_users_bulk_actions
									}
									name={ 'wsx-lists-bulk-action' }
									value={ bulkAction }
									onChange={ onBulkActionChange }
									minWidth="212px"
									maxWidth="212px"
								/>
								<Button
									label={ __( 'Apply', 'wholesalex' ) }
									onClick={ handleBulkAction }
									background="base2"
									borderColor="primary"
									color="primary"
									customClass="wsx-font-14"
									padding="11px 20px"
								/>
							</div>
							<div className="wsx-d-flex wsx-gap-8 wsx-item-center wsx-user-select-container">
								<Select
									options={ getOptionsArray(
										wholesalex_overview.whx_users_statuses
									) }
									value={ filterStatus }
									onChange={ onFilterStatusChange }
									inputBackground="base1"
									minWidth="150px"
									maxWidth="150px"
									selectionText={ __(
										'Select Status',
										'wholesalex'
									) }
								/>
								<Select
									options={ getOptionsArray(
										wholesalex_overview.whx_users_roles
									) }
									value={ filterRole }
									onChange={ onFilterRoleChange }
									inputBackground="base1"
									minWidth="150px"
									maxWidth="150px"
									selectionText={ __(
										'Select Role',
										'wholesalex'
									) }
								/>
							</div>
						</div>
						<div className="wsx-d-flex wsx-gap-8 wsx-item-center">
							<Button
								label={ __( 'Import', 'wholesalex' ) }
								background="base2"
								iconName="import"
								onClick={ onImportClick }
								customClass="wsx-font-regular"
							/>
							<Search
								type="text"
								name={ 'wsx-lists-search-user' }
								value={ tempSearchValue }
								onChange={ ( e ) =>
									setTempSearchValue( e.target.value )
								}
								iconName="search"
								iconColor="#070707"
								background="transparent"
								borderColor="#868393"
								maxHeight="38px"
								placeholder={ __( 'Search…', 'wholesalex' ) }
							/>
							<Dropdown
								className="wsx-relative wsx-bg-base2 wsx-border-default wsx-bc-primary wsx-p-10 wsx-mb-0 wsx-btn"
								iconName="menu"
								iconRotation="half"
								renderContent={ columnsSelectionContent }
							/>
						</div>
					</div>
					<div className="wsx-lists-table-wrapper wsx-scrollbar">
						<table className="wsx-table wsx-lists-table wsx-w-full">
							<thead className="wsx-lists-table-header">
								<tr className="wsx-lists-table-header-row">
									<th className="wsx-checkbox-column wsx-lists-table-column">
										<label
											className="wsx-label wsx-checkbox-option-wrapper"
											htmlFor="wsx-label"
										>
											{ ' ' }
											<input
												type="checkbox"
												checked={
													selectedUsers.length ===
														users.length &&
													users.length > 0
												}
												onChange={
													updateAllUserSelection
												}
											/>
											<span className="wsx-checkbox-mark"></span>
										</label>
									</th>
									{ Object.keys( columns ).map(
										( heading, index ) => {
											return (
												columns[ heading ].status && (
													<th
														key={ index }
														className={ `wsx-lists-table-column-${ columns[ heading ].name } wsx-lists-table-column` }
													>
														{
															columns[ heading ]
																.title
														}
													</th>
												)
											);
										}
									) }
								</tr>
							</thead>

							<tbody className="wsx-lists-table-body wsx-relative">
								{ /* Loading Placeholder Content-- Start */ }
								{ initialLoader && (
									<tr className="wsx-lists-table-row wsx-lists-empty">
										<td
											colSpan={ 100 }
											className={ `wsx-lists-table-column` }
											style={ { height: '213px' } }
										>
											{
												<LoadingGif
													insideContainer={ true }
												/>
											}
										</td>
									</tr>
								) }

								{ ! initialLoader &&
									Object.keys( users ).map(
										( user, index ) => (
											<tr key={ index }>
												<td className="wsx-checkbox-column wsx-lists-table-column">
													<label
														className="wsx-label wsx-checkbox-option-wrapper"
														htmlFor="wsx-label"
													>
														{ ' ' }
														<input
															type="checkbox"
															data-id={
																users[ user ].ID
															}
															checked={ selectedUsers.includes(
																Number(
																	users[
																		user
																	].ID
																)
															) }
															onChange={
																selectUser
															}
														/>
														<span className="wsx-checkbox-mark"></span>
													</label>
												</td>
												{ columns &&
													Object.keys( columns ).map(
														( heading ) =>
															columns[ heading ]
																.status && (
																<>
																	{ renderTableCell(
																		users[
																			user
																		][
																			columns[
																				heading
																			]
																				.name
																		],
																		columns[
																			heading
																		].type,
																		columns[
																			heading
																		].name,
																		users[
																			user
																		],
																		index
																	) }
																</>
															)
													) }
											</tr>
										)
									) }
								{ ! initialLoader &&
									Object.keys( users ).length === 0 && (
										<tr className="wsx-lists-table-row wsx-lists-empty">
											<td
												colSpan={ 100 }
												className={ `wsx-lists-table-column` }
											>
												{ Icons.noData }
											</td>
										</tr>
									) }
							</tbody>
						</table>
					</div>
					{ hasPagination() && (
						<UserPagination
							paginationData={ paginationData }
							fetchData={ fetchData }
							spaceTop={ 48 }
						/>
					) }
				</div>
				{ renderOverlayWindow() }
				{ deleteModal() }
				{ state.length > 0 && (
					<Toast position={ 'top_right' } delay={ 5000 } />
				) }
				{ generateTransactionTable() }
				{ alert && (
					<Alert
						title="Please Select Properly!"
						description="Please select users from the list and an action to perform bulk action."
						onClose={ handleAlertClose }
					/>
				) }
				{ isAlertVisible && (
					<Alert
						title="Confirm Bulk Action"
						description={ `Are you sure you want to ${ pendingAction } the selected ${
							selectedUsers.length === 1 ? 'user' : 'users'
						}?` }
						onClose={ handleAlertClose }
						onConfirm={ confirmBulkAction }
					/>
				) }
			</div>
		</div>
	);
};

export default Users;
