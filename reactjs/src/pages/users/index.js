import React from 'react';
import ReactDOM from 'react-dom';
import Users from './Users';
import Header from '../../components/Header';
import { ToastContextProvider } from '../../context/ToastsContexts';

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholeslex_users_root' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ whx_users.i18n.users } />
					<Users />
				</ToastContextProvider>
			</React.StrictMode>,
			document.getElementById( 'wholeslex_users_root' )
		);
	}
} );
