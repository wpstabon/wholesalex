import React, { useContext, useEffect, useState } from 'react';
import OverlayWindow from '../../components/OverlayWindow';
import Switch from '../../components/Switch';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';

const Export = ( {
	getFilterStatus,
	getFilterRole,
	getSearchValue,
	getSelectedUserIds,
	windowRef,
	toggleOverlayWindow,
	onExportSuccess,
} ) => {
	const [ columns, setColumns ] = useState( [] );
	const [ selectedColumns, setSelectedColumns ] = useState( [] );
	const [ loader, setLoader ] = useState( false );
	const { dispatch } = useContext( ToastContexts );
	useEffect( () => {
		// fetchColumns();
		setColumns( wholesalex_overview.whx_users_exportable_columns );
		setSelectedColumns(
			Object.keys( wholesalex_overview.whx_users_exportable_columns )
		);
	}, [] );

	const handleColumnToggle = ( column ) => {
		if ( selectedColumns.includes( column ) ) {
			setSelectedColumns(
				selectedColumns.filter( ( col ) => col !== column )
			);
		} else {
			setSelectedColumns( [ ...selectedColumns, column ] );
		}
	};

	const handleExport = async () => {
		if ( loader ) {
			return;
		}
		setLoader( true );
		try {
			const formData = new FormData();
			formData.append( 'action', 'wholesalex_export_users' );
			formData.append( 'columns', JSON.stringify( selectedColumns ) );
			formData.append( 'getFilterStatus', getFilterStatus );
			formData.append( 'getFilterRole', getFilterRole );
			formData.append( 'getSearchValue', getSearchValue );
			formData.append( 'getSelectedUserIds', getSelectedUserIds );
			formData.append( 'nonce', wholesalex.nonce );
			fetch( wholesalex.ajax, {
				method: 'POST',
				body: formData,
			} )
				.then( ( res ) => res.json() )
				.then( ( res ) => {
					const fileUrl = window.URL.createObjectURL(
						new Blob( [ res.data ] )
					);
					const link = document.createElement( 'a' );
					link.href = fileUrl;
					link.setAttribute( 'download', 'wholesalex_users.csv' );
					document.body.appendChild( link );
					link.click();
					document.body.removeChild( link );
					setLoader( false );
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: 'Users Export Successful!',
						},
					} );
					if ( onExportSuccess ) {
						onExportSuccess();
					}
				} )
				.then( () => {} );
		} catch ( error ) {
			// handle error
		}
	};

	const content = () => {
		return (
			<>
				{ loader && <LoadingGif /> }
				<div className="wsx-font-16 wsx-font-medium wsx-color-text-dark">
					{ __( 'Select Fields to Export', 'wholesalex' ) }
				</div>
				<div className=" wsx-column-2 wsx-sm-column-1 wsx-gap-24">
					{ Object.keys( columns ).map( ( column ) => (
						<div key={ column } className="">
							<Switch
								className={
									'wsx-d-flex wsx-item-center wsx-justify-start wsx-flex-reverse'
								}
								label={ columns[ column ] }
								labelClass="wsx-mb-0"
								value={ selectedColumns.includes( column ) }
								onChange={ () => handleColumnToggle( column ) }
							/>
						</div>
					) ) }
				</div>
				<div className="wsx-help-message wsx-export-user-help">
					{ __(
						'Warning: If any of the fields contain a comma (,), it might break the CSV file. Ensure the selected column value contains no comma(,).',
						'wholesalex'
					) }
				</div>
				<Button
					label={ __( 'Download CSV', 'wholesalex' ) }
					onClick={ handleExport }
					disable={ loader }
					background="primary"
				/>
			</>
		);
	};

	return (
		<OverlayWindow
			windowRef={ windowRef }
			heading={ __( 'Export Users', 'wholesalex' ) }
			onClose={ toggleOverlayWindow }
			content={ content }
		/>
	);
};

export default Export;
