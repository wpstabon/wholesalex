import React, { useContext, useEffect, useState } from 'react';
import RadioButtons from '../../components/RadioButtons';
import Switch from '../../components/Switch';
import Input from '../../components/Input';
import TextArea from '../../components/TextArea';
import DragList from '../../components/DragList';
import Choosebox from '../../components/Choosebox';
import ColorPicker from '../../components/ColorPicker';
import Shortcode from '../../components/Shortcode';
import Toast from '../../components/Toast';
import { __ } from '@wordpress/i18n';

import Button from '../../components/Button';
import Slider from '../../components/Slider';
import Icons from '../../utils/Icons';
import ToggleSlider from '../../components/ToggleSlider';
import FontSize from '../../components/FontSize';
import DragListEdit from '../../components/DragListEdit';
import TireTablePreview from '../../components/TireTablePreview';
import TierClassicTablePreview from '../../components/TierClassicTablePreview';
import LoadingGif from '../../components/LoadingGif';
import Select from '../../components/Select';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';
import '../../assets/scss/SideModal.scss';
import './PreviewContainer.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const Setting = () => {
	const [ settings, setSettings ] = useState( [] );
	const [ fields, setFields ] = useState( {} );
	const [ activeTab, setActiveTab ] = useState( '' );
	const [ showLoader, setShowLoader ] = useState( false );
	const [ popupStatus, setPopUpStatus ] = useState( false );
	const [ openAccordions, setOpenAccordions ] = useState( {} );
	const { dispatch } = useContext( ToastContexts );

	//Design Setting Tab
	//Tier Table Preview Title Row
	const columnData = [
		{
			label: 'Quantity_Range',
			value: 'Quantity Range',
			status: true,
		},
		{
			label: 'Discount',
			value: 'Discount',
			status: true,
		},
		{
			label: 'Price_Per_Unit',
			value: 'Price Per Unit',
			status: true,
		},
		// {
		//     label: 'Savings_Per_Unit',
		//     value: 'Savings Per Unit',
		//     status: true
		// }
	];

	const designSettings = {
		tableStyle:
			settings?._settings_tier_table_style_design ?? 'table_style',
		verticalStyle: settings?._settings_vertical_style ?? 'no',
		tableBorderRadius: settings?._settings_tier_table_radius_style ?? 'no',
		headerBackgroundColor:
			settings?._settings_tier_table_title_bg_color ?? '#F7F7F7',
		headerTextColor:
			settings?._settings_tier_table_title_text_color ?? '#3A3A3A',
		tableBorderColor:
			settings?._settings_tier_table_border_color ?? '#E5E5E5',
		activeRowBackgroundColor:
			settings?._settings_active_tier_bg_color ?? '#6C6CFF',
		activeRowTextColor:
			settings?._settings_active_tier_text_color ?? '#FFFFFF',
		tableTextColor: settings?._settings_tier_table_text_color ?? '#494949',
		tableFontSize: settings?._settings_tier_font_size ?? '14',
		columnPriority:
			settings?._settings_tier_table_columns_priority ?? columnData,
		isFixedPrice: settings?._settings_tier_price_show_discount ?? 'fixed',
		tableHeading:
			settings?._settings_tier_price_table_heading ??
			'Buy More, Save More',
		isClassicalHorizontalTable:
			settings?._settings_tier_table_style_design === 'classic_style' &&
			settings?._settings_vertical_style === 'yes',
		discountTextColor:
			settings?._settings_tier_discount_text_color ?? '#ffffff',
		discountBackgroundColor:
			settings?._settings_tier_discount_bg_color ?? '#070707',
	};

	const tierTableData = [
		{
			Quantity_Range: '4-5',
			Discount: '30.00',
			Price_Per_Unit: '20.00$',
			// Savings_Per_Unit: '30.00$'
		},
		{
			Quantity_Range: '6-7',
			Discount: '40.00',
			Price_Per_Unit: '30.00$',
			// Savings_Per_Unit: '20.00$'
		},
		{
			Quantity_Range: '8+',
			Discount: '43.00',
			Price_Per_Unit: '70.00$',
			// Savings_Per_Unit: '43.00$'
		},
	];
	const classicalHorizontalData = [
		{
			pricePerUnit: 'USD 20.00$',
			quantityRange: '6-7',
			discount: '75',
		},
		{
			pricePerUnit: 'USD 23.00$',
			quantityRange: '6-7',
			discount: '75',
		},
		{
			pricePerUnit: 'USD 300.00$',
			quantityRange: '6-7',
			discount: '75',
		},
		{
			pricePerUnit: 'USD 20.00$',
			quantityRange: '6-7',
			discount: '75',
		},
	];
	const keysToRemove = [
		'bulkorder',
		'subaccounts',
		'wallet',
		'whitelabel',
		'recaptcha',
		'conversation',
	];

	const removeKeysFromSettingData = ( settingData, keysNeedToRemove ) => {
		const updatedData = { ...settingData };
		keysNeedToRemove.forEach( ( key ) => {
			delete updatedData[ key ];
		} );
		return updatedData;
	};

	const fetchData = async ( isType = 'get' ) => {
		setShowLoader( true );
		const attr = {
			type: isType,
			action: 'settings_action',
			nonce: wholesalex.nonce,
			settings,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/settings_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( isType === 'get' ) {
					setSettings( res.data.value );
					setFields( res.data.default );
					const hash = window.location.hash.slice( 1 );
					setActiveTab(
						hash && res.data.default[ hash ] ? hash : 'general'
					);
				} else {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data,
						},
					} );
				}
			} else {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: res.data,
					},
				} );
			}
			setShowLoader( false );
		} );
	};

	useEffect( () => {
		// fetchData();
		setFields(
			removeKeysFromSettingData(
				wholesalex_overview.whx_settings_fields,
				keysToRemove
			)
		);
		setSettings( wholesalex_overview.whx_settings_data );
		const hash = window.location.hash.slice( 1 );
		setActiveTab(
			hash && wholesalex_overview.whx_settings_fields[ hash ]
				? hash
				: 'general'
		);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	useEffect( () => {
		const handleHashChange = () => {
			const hash = window.location.hash.slice( 1 );
			setActiveTab( hash && fields[ hash ] ? hash : 'general' );
		};
		window.addEventListener( 'hashchange', handleHashChange );
		return () => {
			window.removeEventListener( 'hashchange', handleHashChange );
		};
	}, [ fields ] );

	const tabClickHandler = ( tabID ) => {
		setActiveTab( tabID );
	};
	const getValue = ( fieldName, fieldData ) => {
		if ( settings[ fieldName ] === undefined ) {
			return fieldData.default;
		}
		return settings[ fieldName ];
	};
	const settingOnChangeHandler = ( e, name = '', value = '' ) => {
		const isLock = e.target.value && e.target.value.startsWith( 'pro_' );
		if ( isLock ) {
			setPopUpStatus( true );
		} else if ( e.target.type === 'checkbox' ) {
			setSettings( {
				...settings,
				[ e.target.name ]: e.target.checked ? 'yes' : 'no',
			} );
		} else {
			setSettings( {
				...settings,
				[ name ? name : e.target.name ]: value ? value : e.target.value,
			} );
		}
	};

	const getSlideValue = ( fieldName, fieldData ) => {
		if ( settings[ fieldName ] === undefined ) {
			return fieldData.default;
		}
		return settings[ fieldName ];
	};

	const saveSettings = ( e ) => {
		e.preventDefault();
		fetchData( 'set' );
	};

	const toggleAccordion = ( key ) => {
		setOpenAccordions( ( prevState ) => ( {
			...prevState,
			[ key ]: ! prevState[ key ],
		} ) );
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	const dynamicRulePromoSettings = ( fields ) => {
		return (
			<div className="wsx-accordion-wrapper">
				<div
					className="wsx-accordion-header"
					onClick={ () => toggleAccordion( fields.label ) }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							toggleAccordion( fields.label );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div className="wsx-accordion-title">{ fields.label }</div>
					<span
						className={ `wsx-icon ${
							openAccordions[ fields.label ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				<div
					className={ `wsx-accordion-body ${
						openAccordions[ fields.label ]
							? 'wsx-open'
							: 'wsx-close'
					}` }
				>
					{ fields &&
						Object.keys( fields.attr ).map( ( field, k ) => {
							return (
								<div
									key={ `settings-field-content-${ k }` }
									className={
										fields.attr[ field ].type ===
										'choosebox'
											? 'wsx-choosebox wsx-settings-item'
											: 'wsx-settings-item'
									}
								>
									{ fields.attr[ field ].type === 'radio' && (
										<RadioButtons
											key={ field }
											label={ fields.attr[ field ].label }
											options={
												fields.attr[ field ].options
											}
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											defaultValue={
												fields.attr[ field ].default
											}
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'select' && (
										<Select
											key={ field }
											name={ field }
											label={ fields.attr[ field ].label }
											options={ getOptionsArray(
												fields.attr[ field ].options
											) }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											tooltip={
												fields.attr[ field ].tooltip
											}
											help={ tabData.attr[ field ]?.help }
											inputBackground="base1"
										/>
									) }
									{ fields.attr[ field ].type ===
										'switch' && (
										<Switch
											key={ field }
											helpClass="wholesalex_settings_help_text"
											className={
												'wholesalex_settings_field'
											}
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											defaultValue={
												fields.attr[ field ].default
											}
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'slider' && (
										<Slider
											key={ field }
											className="wsx-slider-sm"
											label={ fields.attr[ field ].label }
											name={ field }
											value={
												getSlideValue(
													field,
													fields.attr[ field ]
												) === 'yes'
													? true
													: false
											}
											onChange={ settingOnChangeHandler }
											tooltip={
												fields.attr[ field ].tooltip
											}
											isLabelSide={ true }
											help={ fields.attr[ field ]?.help }
										/>
									) }
									{ fields.attr[ field ].type ===
										'number' && (
										<Input
											key={ field }
											className="wholesalex_settings_field"
											type="number"
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type === 'text' && (
										<Input
											key={ field }
											className="wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
											labelClass="wsx-mt-8 wsx-sm-mt-0 wsx-text-space-break"
											inputClass="wsx-bg-base1"
											type="text"
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
											smartTags={
												fields.attr[ field ]
													?.smart_tags || false
											}
										/>
									) }
									{ fields.attr[ field ].type === 'url' && (
										<Input
											key={ field }
											className="wholesalex_settings_field"
											type="url"
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type === 'color' && (
										<ColorPicker
											key={ field }
											type="color"
											label={ fields.attr[ field ].label }
											name={ field }
											value={ settings }
											setValue={ setSettings }
											defaultValue={
												fields.attr[ field ].default
											}
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'textarea' && (
										<TextArea
											key={ field }
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'dragList' && (
										<DragList
											label={ fields.attr[ field ].label }
											name={ field }
											value={ settings }
											setValue={ setSettings }
											defaultValue={
												fields.attr[ field ].default
											}
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											options={ getValue(
												field,
												fields.attr[ field ]
											) }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'choosebox' && (
										<Choosebox
											label={ fields.attr[ field ].label }
											name={ field }
											value={ getValue(
												field,
												fields.attr[ field ]
											) }
											onChange={ settingOnChangeHandler }
											defaultValue={
												fields.attr[ field ].default
											}
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											options={
												fields.attr[ field ].options
											}
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
									{ fields.attr[ field ].type ===
										'shortcode' && (
										<Shortcode
											label={ fields.attr[ field ].label }
											name={ field }
											value={ settings }
											setValue={ setSettings }
											defaultValue={
												fields.attr[ field ].default
											}
											desc={ fields.attr[ field ].desc }
											help={ fields.attr[ field ]?.help }
											tooltip={
												fields.attr[ field ].tooltip
											}
										/>
									) }
								</div>
							);
						} ) }
				</div>
			</div>
		);
	};

	const renderTabData = () => {
		const tabData = fields[ activeTab ];
		const renderField = ( fieldData, fieldKey ) => {
			switch ( fieldData.type ) {
				case 'language_n_text_section':
				case 'dynamic_rule_promo_section':
					return dynamicRulePromoSettings( fieldData );

				case 'select':
					return (
						<Select
							key={ fieldKey }
							name={ fieldKey }
							wrapperClass="wsx-gap-48 wsx-item-center wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							options={ getOptionsArray( fieldData.options ) }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							tooltip={ fieldData.tooltip }
							help={ fieldData?.help }
							inputBackground="base1"
						/>
					);

				case 'switch':
					return (
						<Switch
							key={ fieldKey }
							helpClass="wholesalex_settings_help_text"
							className={ `wholesalex_settings_field ${ fieldData.class }` }
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'slider':
					return (
						<Slider
							key={ fieldKey }
							className="wsx-slider-sm wsx-settings-design"
							label={ fieldData.label }
							name={ fieldKey }
							value={
								getSlideValue( fieldKey, fieldData ) === 'yes'
							}
							onChange={ settingOnChangeHandler }
							tooltip={ fieldData.tooltip }
							isLabelSide={ true }
							help={ fieldData?.help }
							contentClass={
								fieldData.help
									? 'wsx-d-flex wsx-item-center wsx-gap-16'
									: ''
							}
						/>
					);

				case 'toggleSlider':
					return (
						<ToggleSlider
							key={ fieldKey }
							className="wsx-slider-sm"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getSlideValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							labelClass="wsx-mb-16 wsx-font-medium"
							labelColor="text-medium"
						/>
					);

				case 'fontSize':
					return (
						<FontSize
							key={ fieldKey }
							className="wsx-d-flex wsx-item-center wsx-gap-12"
							type="number"
							label={ fieldData.label }
							name={ fieldKey }
							value={
								parseInt( getValue( fieldKey, fieldData ) ) ||
								14
							}
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
							labelClass="wsx-mb-0"
						/>
					);

				case 'radio':
					return (
						<RadioButtons
							key={ fieldKey }
							label={ fieldData.label }
							options={ fieldData.options }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							tooltip={ fieldData.tooltip }
							labelSpace="20"
							flexView={ true }
						/>
					);

				case 'number':
					return (
						<Input
							key={ fieldKey }
							className="wholesalex_settings_field"
							type="number"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'text':
					return (
						<Input
							key={ fieldKey }
							className="wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-mt-8 wsx-sm-mt-0 wsx-text-space-break"
							inputClass="wsx-bg-base1"
							type="text"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
							smartTags={ fieldData.smart_tags || false }
						/>
					);

				case 'url':
					return (
						<Input
							key={ fieldKey }
							className="wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-mt-8 wsx-sm-mt-0 wsx-text-space-break"
							type="url"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'color':
					return (
						<ColorPicker
							key={ fieldKey }
							type="color"
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
							labelClass="wsx-font-regular wsx-color-text-light"
						/>
					);

				case 'textarea':
					return (
						<TextArea
							key={ fieldKey }
							className="wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'dragList':
					return (
						<DragList
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							options={ getValue( fieldKey, fieldData ) }
							tooltip={ fieldData.tooltip }
							labelSpace="20"
							className="wsx-card-2 wsx-card-border"
							descClass="wsx-mt-16"
						/>
					);

				case 'dragListEdit':
					return (
						<DragListEdit
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							options={ getValue( fieldKey, fieldData ) }
							tooltip={ fieldData.tooltip }
							labelSpace="20"
							className="wsx-card-2 wsx-card-border"
							descClass="wsx-mt-16"
							onChange={ settingOnChangeHandler }
						/>
					);

				case 'choosebox':
					return (
						<Choosebox
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							options={ fieldData.options }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'shortcode':
					return (
						<Shortcode
							key={ fieldKey }
							className="wsx-gap-48 wsx-item-center wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				default:
					return null;
			}
		};

		const renderAttributes = ( attributes ) => {
			return Object.keys( attributes ).map( ( fieldKey, index ) => {
				if ( fieldKey !== 'type' ) {
					let content = null;

					if (
						fieldKey === '_settings_tier_table_columns_priority'
					) {
						if ( designSettings.tableStyle === 'table_style' ) {
							content = (
								<div
									key={ `settings-field-content-${ index }` }
									className={
										attributes[ fieldKey ].type ===
										'choosebox'
											? 'wsx-choosebox wsx-settings-item'
											: 'wsx-settings-item'
									}
								>
									{ renderField(
										attributes[ fieldKey ],
										fieldKey
									) }
								</div>
							);
						}
					} else if (
						fieldKey === '_settings_tier_discount_text_color' ||
						fieldKey === '_settings_tier_discount_bg_color'
					) {
						if (
							designSettings.tableStyle === 'classic_style' &&
							designSettings.verticalStyle === 'no'
						) {
							content = (
								<div
									key={ `settings-field-content-${ index }` }
									className={
										attributes[ fieldKey ].type ===
										'choosebox'
											? 'wsx-choosebox wsx-settings-item'
											: 'wsx-settings-item'
									}
								>
									{ renderField(
										attributes[ fieldKey ],
										fieldKey
									) }
								</div>
							);
						}
					} else {
						content = (
							<div
								key={ `settings-field-content-${ index }` }
								className={
									attributes[ fieldKey ].type === 'choosebox'
										? 'wsx-choosebox wsx-settings-item'
										: 'wsx-settings-item'
								}
							>
								{ renderField(
									attributes[ fieldKey ],
									fieldKey
								) }
							</div>
						);
					}

					return content;
				}

				return null;
			} );
		};

		const getDynamicClass = ( type, defaultClass ) => {
			const classMap = {
				general_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				general_one: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				general_two:
					'wsx-card-2 wsx-card-border wsx-pt-32 wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-30',
				registration_zero:
					'wsx-card-2 wsx-card-border wsx-item-divider-wrapper',
				registration_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				price_zero:
					'wsx-card-2 wsx-card-border wsx-pt-32 wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-20',
				price_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				price_two: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				price_three:
					'wsx-card-2 wsx-card-border wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-30',
				convertions_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				dynamic_rules_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				language_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				design_zero: 'wsx-design_zero',
				design_one: 'wsx-d-flex wsx-item-center wsx-gap-40',
				design_two:
					'wsx-design_two wsx-d-flex wsx-flex-column wsx-gap-32',
				design_three: 'wsx-design_three',
				recaptcha_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				white_label_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				bulk_order_zero: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				bulk_order_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				sub_account_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				wallet_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				dokan_wholesalex_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				wcfm_wholesalex_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
			};

			return classMap[ type ] || defaultClass;
		};

		// Usage
		const dynamicClassAttr = () =>
			getDynamicClass( tabData?.attr?.type, 'wsx-setting-zero' );
		const dynamicClassOne = () =>
			getDynamicClass( tabData?.attrGroupOne?.type, 'wsx-setting-one' );
		const dynamicClassTwo = () =>
			getDynamicClass( tabData?.attrGroupTwo?.type, 'wsx-setting-two' );
		const dynamicClassThree = () =>
			getDynamicClass(
				tabData?.attrGroupThree?.type,
				'wsx-setting-three'
			);
		const renderTabAttributes = () => (
			<>
				<div className={ dynamicClassAttr() }>
					{ tabData?.attr && renderAttributes( tabData.attr ) }
				</div>
				{ tabData?.attrGroupOne && (
					<div className={ dynamicClassOne() }>
						{ renderAttributes( tabData.attrGroupOne ) }
					</div>
				) }
				{ tabData?.attrGroupTwo && (
					<div className={ dynamicClassTwo() }>
						{ renderAttributes( tabData.attrGroupTwo ) }
					</div>
				) }
				{ tabData?.attrGroupThree && (
					<div className={ dynamicClassThree() }>
						{ renderAttributes( tabData.attrGroupThree ) }
					</div>
				) }
			</>
		);

		const columns = designSettings.columnPriority.map( ( column ) => ( {
			key: column.label,
			label: column.value,
		} ) );

		return (
			<div className="wsx-side-menu-body">
				<div className="wsx-side-menu-header">
					<span className="wsx-title wsx-font-20">
						{ tabData?.label }
					</span>
					<Button
						label={ fields._save?.label }
						onClick={ saveSettings }
						background="positive"
						customClass="wsx-font-14"
					/>
				</div>
				<div className="wsx-side-menu-content">
					{ activeTab === 'design' ? (
						<div className="wsx-settings-design-container wsx-d-flex wsx-gap-20">
							<div className="wsx-tier-design-settings wsx-w-full">
								{ renderTabAttributes() }
							</div>
							<div className="wsx-tier-design-right">
								<div className="wsx-preview-container">
									<div className="wsx-preview-header">
										<div className="wsx-font-16 wsx-font-medium wsx-color-text-medium">
											{ __( 'Preview', 'wholesalex' ) }
										</div>
									</div>
									<div className="wsx-preview-body">
										<div className="wsx-font-20 wsx-font-medium wsx-color-tertiary wsx-mb-12">
											{ designSettings.tableHeading }
										</div>
										<div className="wsx-preview-table-wrapper wsx-scrollbar">
											{ designSettings.tableStyle ===
											'table_style' ? (
												<TireTablePreview
													data={ tierTableData }
													columns={ columns }
													isHorizontal={
														designSettings.tableStyle ===
															'table_style' &&
														designSettings.verticalStyle ===
															'yes'
													}
													isTableBorderRadius={
														designSettings.tableBorderRadius ===
														'yes'
													}
													headerBackgroundColor={
														designSettings.headerBackgroundColor
													}
													headerTextColor={
														designSettings.headerTextColor
													}
													tableBorderColor={
														designSettings.tableBorderColor
													}
													activeRowBackgroundColor={
														designSettings.activeRowBackgroundColor
													}
													activeRowTextColor={
														designSettings.activeRowTextColor
													}
													tableTextColor={
														designSettings.tableTextColor
													}
													tableFontSize={
														designSettings.tableFontSize
													}
													ColumnPriority={
														designSettings.columnPriority
													}
													isFixedPrice={
														designSettings.isFixedPrice ===
														'fixed'
													}
												/>
											) : (
												<TierClassicTablePreview
													data={
														classicalHorizontalData
													}
													columns={ columns }
													isHorizontal={
														designSettings.tableStyle ===
															'table_style' &&
														designSettings.verticalStyle ===
															'yes'
													}
													isTableBorderRadius={
														designSettings.tableBorderRadius ===
														'yes'
													}
													headerBackgroundColor={
														designSettings.headerBackgroundColor
													}
													headerTextColor={
														designSettings.headerTextColor
													}
													tableBorderColor={
														designSettings.tableBorderColor
													}
													activeRowBackgroundColor={
														designSettings.activeRowBackgroundColor
													}
													activeRowTextColor={
														designSettings.activeRowTextColor
													}
													tableTextColor={
														designSettings.tableTextColor
													}
													tableFontSize={
														designSettings.tableFontSize
													}
													ColumnPriority={
														designSettings.columnPriority
													}
													isFixedPrice={
														designSettings.isFixedPrice ===
														'fixed'
													}
													isClassicalHorizontalTable={
														designSettings.isClassicalHorizontalTable
													}
													selectedItem={ 1 }
													discountTextColor={
														designSettings.discountTextColor
													}
													discountBackgroundColor={
														designSettings.discountBackgroundColor
													}
												/>
											) }
										</div>
									</div>
								</div>
							</div>
						</div>
					) : (
						renderTabAttributes()
					) }
				</div>
			</div>
		);
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				<div className="wsx-side-menu-wrapper wsx-relative">
					{ showLoader && <LoadingGif /> }
					<div className="wsx-side-menu-list">
						{ Object.keys( fields ).map(
							( section, k ) =>
								section !== '_save' &&
								fields[ section ].attr &&
								Object.keys( fields[ section ].attr ).length >
									0 && (
									<div
										key={ k }
										onClick={ () => {
											tabClickHandler( section );
										} }
										className={ `wsx-side-menu-item ${
											activeTab === section
												? 'active'
												: ''
										}` }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												tabClickHandler( section );
											}
										} }
										role="button"
										tabIndex="0"
									>
										<span className="wsx-side-menu-item-title">
											{ fields[ section ].label }
										</span>
									</div>
								)
						) }
					</div>
					{ renderTabData() }
				</div>
			</div>

			{ popupStatus && (
				<UpgradeProPopUp
					title={ __( 'Unlock All Features', 'wholesalex' ) }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
			{ <Toast delay={ 3000 } position="top_right" /> }
		</div>
	);
};

export default Setting;
