import React, { useEffect, useState } from 'react';
import Tier from '../../components/Tier';
import Input from '../../components/Input';

import Icons from '../../utils/Icons';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';

const Product = ( { id, isWCFMVariation, type = 'simple' } ) => {
	const initialTier = {
		_id: Date.now().toString(),
		_discount_type: '',
		_discount_amount: '',
		_min_quantity: '',
		src: 'single_product',
	};
	const [ tier, setTier ] = useState( {} );
	const [ appState, setAppState ] = useState( {
		loading: true,
		currentWindow: 'new',
		ruleWindow: 'new',
		ruleIndex: -1,
		index: -1,
		loadingOnSave: false,
	} );
	const [ fields, setFields ] = useState( {} );
	const _isProActivate = wholesalex?.is_pro_active;
	const [ popUpStatus, setPopUpStatus ] = useState( false );

	const getPostID = () => {
		let productId = '';
		//Check If request comes from dokan dashboard or not
		if ( ! id ) {
			if ( wholesalex_single_product.is_dokan_dashboard ) {
				productId = document.getElementById(
					'dokan-edit-product-id'
				)?.value;
			} else {
				// document.getElementById('pro_id')?.value is added to to support wcfm. In wcfm, product id set in pro_id field
				productId =
					document.getElementById( 'post_ID' )?.value ||
					document.getElementById( 'pro_id' )?.value;
			}
		}
		return id ? id : productId;
	};

	const postId = getPostID();

	useEffect( () => {
		if ( wholesalex_single_product.fields ) {
			setFields( wholesalex_single_product.fields );
		}
		const sections = Object.keys( wholesalex_single_product.fields );
		const initialRoleData = {};
		sections.map( ( section ) => {
			Object.keys( wholesalex_single_product.fields[ section ].attr ).map(
				( role ) => {
					initialRoleData[ role ] = {
						tiers: [ { ...initialTier } ],
					};
					return null;
				}
			);
			return null;
		} );
		const _temp = {
			...initialRoleData,
			...wholesalex_single_product.discounts[ postId ],
		};
		setTier( _temp );
		setAppState( {
			...appState,
			loading: false,
		} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const buttonData = ( fieldData, tierName ) => {
		return (
			<Button
				label={ fieldData.label }
				background="tertiary"
				onClick={ ( e ) => {
					e.preventDefault();
					const copy = { ...tier };
					copy[ tierName ].tiers.push( initialTier );
					setTier( copy );
				} }
			/>
		);
	};
	const UpgradeButton = ( fieldData ) => {
		return (
			<Button
				label={ fieldData.label }
				onClick={ ( e ) => {
					e.preventDefault();
					setPopUpStatus( true );
				} }
				background="secondary"
				iconName="angleRight_24"
				iconPosition="after"
				iconAnimation="icon-left"
			/>
		);
	};

	const [ tierStatus, setTierStatus ] = useState( {} );

	const tierData = ( fieldData, tierName, lockStatus ) => {
		const defaultTier = fieldData._tiers.data;
		return (
			<div key={ tierName } className="wsx-settings-tiers-wrapper">
				<div className="wsx-settings-tiers-container">
					{ tier &&
						tier[ tierName ] &&
						tier[ tierName ].tiers.map( ( t, index ) => (
							<Tier
								key={ `wholesalex_${ tierName }_tier_${ index }` }
								fields={ defaultTier }
								tier={ tier }
								setTier={ setTier }
								index={ index }
								tierName={ tierName }
							/>
						) ) }
				</div>
				{ ! lockStatus &&
					fieldData._tiers.add.type === 'button' &&
					buttonData( fieldData._tiers.add, tierName ) }
				{ lockStatus &&
					fieldData._tiers.upgrade_pro.type === 'button' &&
					UpgradeButton( fieldData._tiers.upgrade_pro, tierName ) }
			</div>
		);
	};

	const inputData = ( fieldData, fieldName, tierName ) => {
		const flag = tier[ tierName ] && tier[ tierName ][ fieldName ];
		const defValue = flag
			? tier[ tierName ][ fieldName ]
			: fieldData.default;
		return (
			<Input
				className="wholesalex_single_product_field"
				label={ fieldData.label }
				type={ fieldData.type }
				name={ fieldName }
				value={ defValue }
				onChange={ ( e ) => {
					const parent = { ...tier };
					const copy = parent[ tierName ];
					copy[ fieldName ] = e.target.value;
					parent[ tierName ] = copy;
					setTier( { ...tier, ...parent } );
				} }
			/>
		);
	};

	const pricesData = ( fieldData, tierName ) => {
		return (
			<div key={ `wholesalex_${ tierName }_separate` }>
				<div
					key={ `wholesalex_prices_${ tierName }` }
					className="wsx-role-based-prices-section wsx-column-2 wsx-sm-column-1 wsx-gap-24"
				>
					{ Object.keys( fieldData.attr ).map(
						( field ) =>
							fieldData.attr[ field ].type === 'number' &&
							inputData(
								fieldData.attr[ field ],
								field,
								tierName
							)
					) }
				</div>
			</div>
		);
	};

	const tiersData = ( fieldsData, section ) => {
		let _limit = 99999999999;
		const isPro = fieldsData.is_pro;
		if ( isPro ) {
			_limit = fieldsData.pro_data?.value;
		}
		if ( ! _limit ) {
			_limit = 99999999999;
		}

		const isLock =
			tier[ section ].tiers.length >= _limit && ! _isProActivate;

		return (
			<section
				key={ `wholesalex_${ section }` }
				className={ section + ' wsx-accordion-wrapper' }
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setTierStatus( {
							...tierStatus,
							[ section ]: ! tierStatus[ section ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setTierStatus( {
								...tierStatus,
								[ section ]: ! tierStatus[ section ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx_role_header` }
					>
						{ fieldsData.label }
					</div>
					<span
						className={ `wsx-icon ${
							tierStatus[ section ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ tierStatus[ section ] && (
					<div className="wsx-accordion-body">
						{ tierStatus[ section ] &&
							fieldsData.attr &&
							Object.keys( fieldsData.attr ).map(
								( fieldData ) => {
									switch (
										fieldsData.attr[ fieldData ].type
									) {
										case 'tier':
											return tierData(
												fieldsData.attr[ fieldData ],
												section,
												isLock
											);
										case 'prices':
											return pricesData(
												fieldsData.attr[ fieldData ],
												section
											);
										default:
											return null;
									}
								}
							) }
					</div>
				) }
			</section>
		);
	};

	return (
		<>
			{ appState.loading && <LoadingGif /> }
			{ ! appState.loading &&
				Object.keys( fields ).map(
					( section, c ) =>
						Object.keys( fields[ section ].attr ).length > 0 && (
							<div
								key={ `wholesalex_product_key_${ section }_${ c }` }
								className={ `wsx-tier-wrapper ${
									section === '_b2c_section' ? 'b2c' : ''
								}` }
							>
								{ fields[ section ].label && (
									<div
										key={ `wholesalex_b2c_b2b_sep_2` }
										className="wsx-font-18 wsx-font-bold"
									>
										{ fields[ section ].label }
									</div>
								) }
								{ Object.keys( fields[ section ].attr ).map(
									( field ) => {
										switch (
											fields[ section ].attr[ field ].type
										) {
											case 'tiers':
												return tiersData(
													fields[ section ].attr[
														field
													],
													field
												);
											default:
												return null;
										}
									}
								) }
							</div>
						)
				) }
			{ wholesalex_single_product?.is_wcfm_dashboard &&
				isWCFMVariation && (
					<input
						type="hidden"
						className="wholesalex_rolewise_pricing"
						id={ `wholesalex_wcfm_product_variation_tiers_${
							id ? id : postId
						}` }
						value={ JSON.stringify( tier ) }
						name={ `wholesalex_wcfm_product_variation_tiers_${
							id ? id : postId
						}` }
					></input>
				) }
			{ wholesalex_single_product?.is_wcfm_dashboard &&
				! isWCFMVariation && (
					<input
						type="hidden"
						className="wholesalex_rolewise_pricing"
						id={ `wholesalex_wcfm_simple_product_tiers_${
							id ? id : postId
						}` }
						value={ JSON.stringify( tier ) }
						name={ `wholesalex_wcfm_simple_product_tiers_${
							id ? id : postId
						}` }
					></input>
				) }
			{ ! wholesalex_single_product?.is_wcfm_dashboard && (
				<input
					type="hidden"
					className="wholesalex_rolewise_pricing"
					id={ `wholesalex_single_product_tiers_${
						id ? id : postId
					}_${ type }` }
					value={ JSON.stringify( tier ) }
					name={ `wholesalex_single_product_tiers_${
						id ? id : postId
					}_${ type }` }
				></input>
			) }

			{ popUpStatus && (
				<UpgradeProPopUp
					title={ wholesalex_product.i18n.unlock_heading }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
		</>
	);
};

export default Product;
