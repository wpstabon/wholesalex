import React from 'react';
import ReactDOM from 'react-dom';
import Product from './Product';
import ProductTab from './ProductTab';
document.addEventListener( 'DOMContentLoaded', function () {
	if ( document.body.contains( document.getElementById( 'wsx_tab_data' ) ) ) {
		ReactDOM.render(
			<React.StrictMode>
				<ProductTab />
			</React.StrictMode>,
			document.getElementById( 'wsx_tab_data' )
		);
	}
	let elements = document.getElementsByClassName(
		'wsx-single-product-settings-wrapper'
	);
	if ( elements.length ) {
		ReactDOM.render(
			<React.StrictMode>
				<Product />
			</React.StrictMode>,
			elements[ 0 ]
		);
	}

	jQuery( '#woocommerce-product-data' ).on(
		'woocommerce_variations_loaded',
		function () {
			elements = document.getElementsByClassName(
				'wsx-single-product-settings-wrapper'
			);
			Array.from( elements ).forEach( ( element ) => {
				element.classList.add(
					'wsx-single-product-variation-settings'
				);
			} );
			const ids = document.getElementsByClassName( 'variable_post_id' );

			for ( let i = 1; i < elements.length; i++ ) {
				ReactDOM.render(
					<React.StrictMode>
						<Product id={ ids[ i - 1 ].value } />
					</React.StrictMode>,
					elements[ i ]
				);
			}
		}
	);

	if ( wholesalex_single_product?.is_dokan_dashboard ) {
		jQuery( '.dokan-product-variation-wrapper' ).on(
			'dokan_variations_loaded',
			function () {
				elements = document.getElementsByClassName(
					'wsx-single-product-settings-wrapper'
				);

				for ( let i = 1; i < elements.length; i++ ) {
					const variationId = document.getElementsByName(
						`variable_post_id[${ i - 1 }]`
					)?.[ 0 ]?.value;
					ReactDOM.render(
						<React.StrictMode>
							<Product id={ variationId } />
						</React.StrictMode>,
						elements[ i ]
					);
				}
			}
		);
	}

	// <Product /> React component is loaded inside the "YITH WooCommerce Product Bundles" plugin menu in the product edit screen.
	const yithElements = document.getElementsByClassName(
		'wsx-bundle-product-wrapper'
	);

	if ( yithElements.length > 0 ) {
		ReactDOM.render(
			<React.StrictMode>
				<Product type="yith" />
			</React.StrictMode>,
			yithElements[ 0 ]
		);
	}

	if ( wholesalex_single_product?.is_wcfm_dashboard ) {
		elements = document.getElementsByClassName(
			'_wholesalex_wcfm_single_product_settings'
		);
		if ( elements.length ) {
			ReactDOM.render(
				<React.StrictMode>
					<Product />
				</React.StrictMode>,
				elements[ 0 ]
			);
		}
		elements = document.getElementsByClassName(
			'_wholesalex_wcfm_variable_product_settings'
		);

		for ( let i = 0; i < elements.length; i++ ) {
			const variationId = document.getElementById(
				`variations_id_${ i }`
			)?.value;

			ReactDOM.render(
				<React.StrictMode>
					<Product id={ variationId } isWCFMVariation={ true } />
				</React.StrictMode>,
				elements[ i ]
			);
		}
	}

	wholesalex_product?.roles?.forEach( ( role ) => {
		jQuery( 'select.variation_actions' ).on(
			'wholesalex_product_price_' + role.value + '_base',
			function () {
				const data = {};
				const value = window.prompt(
					woocommerce_admin_meta_boxes_variations.i18n_enter_a_value
				);

				if ( value !== null ) {
					data.value = value;
				} else {
					return;
				}

				jQuery.ajax( {
					url: woocommerce_admin_meta_boxes_variations.ajax_url,
					data: {
						action: 'wholesalex_bulk_edit_variations',
						security:
							woocommerce_admin_meta_boxes_variations.bulk_edit_variations_nonce,
						product_id:
							woocommerce_admin_meta_boxes_variations.post_id,
						product_type: jQuery( '#product-type' ).val(),
						bulk_action:
							'wholesalex_product_price_' + role.value + '_base',
						data: data,
					},
					type: 'POST',
					success: () => {
						location.reload();
					},
				} );
			}
		);

		jQuery( 'select.variation_actions' ).on(
			'wholesalex_product_price_' + role.value + '_sale',
			function () {
				const data = {};
				const value = window.prompt(
					woocommerce_admin_meta_boxes_variations.i18n_enter_a_value
				);

				if ( value !== null ) {
					data.value = value;
				} else {
					return;
				}

				jQuery.ajax( {
					url: woocommerce_admin_meta_boxes_variations.ajax_url,
					data: {
						action: 'wholesalex_bulk_edit_variations',
						security:
							woocommerce_admin_meta_boxes_variations.bulk_edit_variations_nonce,
						product_id:
							woocommerce_admin_meta_boxes_variations.post_id,
						product_type: jQuery( '#product-type' ).val(),
						bulk_action:
							'wholesalex_product_price_' + role.value + '_sale',
						data,
					},
					type: 'POST',
					success: () => {
						location.reload();
					},
				} );
			}
		);
	} );
} );
