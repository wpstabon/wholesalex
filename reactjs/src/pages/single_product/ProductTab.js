import React, { useEffect, useState } from 'react';
import MultiSelect from '../../components/MultiSelect';
import Choosebox from '../../components/Choosebox';
import Switch from '../../components/Switch';

import Slider from '../../components/Slider';
import Icons from '../../utils/Icons';
import RadioButtons from '../../components/RadioButtons';
import Select from '../../components/Select';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';

const ProductTab = () => {
	const [ productSettings, setProductSettings ] = useState( {} );
	const [ visibilitySectionStatus, setVisibilitySectionStatus ] =
		useState( true );
	const [ fields, setFields ] = useState( {} );
	const [ appState, setAppState ] = useState( {} );
	const [ popUpStatus, setPopUpStatus ] = useState( false );

	useEffect( () => {
		if ( wholesalex_product_tab.fields ) {
			setFields( wholesalex_product_tab.fields );
		}
		if ( wholesalex_product_tab.settings ) {
			setProductSettings( wholesalex_product_tab.settings );
		}
		setAppState( {
			...appState,
			loading: false,
		} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	const selectData = ( fieldData, field ) => {
		const defValue = productSettings[ field ] || fieldData.default || '';
		return (
			<Select
				label={ fieldData.label }
				options={ getOptionsArray( fieldData.options ) }
				value={ defValue }
				onChange={ ( e ) => {
					setProductSettings( {
						...productSettings,
						[ field ]: e.target.value,
					} );
				} }
				help={ fieldData.help }
				inputBackground="base1"
				wrapperClass="wsx-column-2 wsx-gap-8 wsx-item-center wsx-text-space-nowrap wsx-basis-50"
				labelClass="wsx-mb-0"
			/>
		);
	};
	const chooseboxData = ( fieldData, field ) => {
		const defValue = productSettings[ field ]
			? productSettings[ field ]
			: fieldData.default;

		return (
			<Choosebox
				label={ fieldData.label }
				name={ field }
				value={ defValue }
				onChange={ ( e ) => {
					const isLock = e.target.value.startsWith( 'pro_' );
					if ( isLock ) {
						setPopUpStatus( true );
					} else {
						setProductSettings( { [ field ]: e.target.value } );
					}
				} }
				defaultValue={ fieldData.default }
				desc={ fieldData.desc }
				help={ fieldData?.help }
				options={ fieldData.options }
			/>
		);
	};

	const switchData = ( fieldData, field ) => {
		let defValue;

		if ( productSettings[ field ] ) {
			defValue = productSettings[ field ] === 'yes' ? true : false;
		} else {
			defValue = fieldData.default;
		}
		const _className = wholesalex_single_product?.is_wcfm_dashboard
			? 'wcfm-checkbox'
			: '';
		return (
			<Switch
				name={ field }
				value={ defValue }
				onChange={ ( e ) => {
					const _final = e.target.checked ? 'yes' : 'no';
					setProductSettings( {
						...productSettings,
						[ field ]: _final,
					} );
				} }
				defaultValue={ fieldData.default }
				desc={ fieldData.label }
				help={ fieldData?.help }
				inputFieldClass={ _className }
			/>
		);
	};
	const getValue = ( fieldName, fieldData ) => {
		if ( productSettings[ fieldName ] === undefined ) {
			return fieldData.default;
		}
		return productSettings[ fieldName ];
	};
	const settingOnChangeHandler = ( e, name = '', value = '' ) => {
		const isLock = e.target.value && e.target.value.startsWith( 'pro_' );
		if ( isLock ) {
			setPopUpStatus( true );
		} else if ( e.target.type === 'checkbox' ) {
			setProductSettings( {
				...productSettings,
				[ e.target.name ]: e.target.checked ? 'yes' : 'no',
			} );
		} else {
			setProductSettings( {
				...productSettings,
				[ name ? name : e.target.name ]: value ? value : e.target.value,
			} );
		}
	};
	const radioData = ( fieldData, field ) => {
		return (
			<RadioButtons
				className={ 'wsx-mb-32' }
				key={ field }
				label={ fieldData.label }
				options={ fieldData.options }
				name={ field }
				value={ getValue( field, fieldData ) }
				onChange={ settingOnChangeHandler }
				defaultValue={ fieldData.default }
				labelSpace="8"
				flexView={ true }
			/>
		);
	};

	const sliderData = ( fieldData, field, customClass ) => {
		let defValue;

		if ( productSettings[ field ] ) {
			defValue = productSettings[ field ] === 'yes' ? true : false;
		} else {
			defValue = fieldData.default;
		}

		return (
			<Slider
				className={ `wsx-single-product-settings-slider ${ customClass }` }
				label={ fieldData.label }
				isLabelSide={ true }
				labelClass="wsx-font-regular"
				name={ field }
				value={ defValue }
				onChange={ ( e ) => {
					const _final = e.target.checked ? 'yes' : 'no';
					setProductSettings( {
						...productSettings,
						[ field ]: _final,
					} );
				} }
			/>
		);
	};

	const multiselectData = ( fieldData, field ) => {
		const defValue = productSettings[ field ]
			? productSettings[ field ]
			: fieldData.default;
		return (
			<MultiSelect
				key={ field }
				name={ field }
				value={ defValue }
				options={ fieldData.options }
				placeholder={ fieldData.placeholder }
				onMultiSelectChangeHandler={ ( fieldName, selectedValues ) => {
					const copy = { ...productSettings };
					copy[ fieldName ] = [ ...selectedValues ];
					setProductSettings( { ...productSettings, ...copy } );
				} }
			/>
		);
	};

	const visibilitySectionData = ( fieldData, section ) => {
		return (
			<div
				key={ section }
				className="wholesalex-product-data__visibility-section wsx-accordion-wrapper"
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setVisibilitySectionStatus( ! visibilitySectionStatus );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setVisibilitySectionStatus(
								! visibilitySectionStatus
							);
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx_role_header_${ section }` }
					>
						{ fieldData.label }
					</div>
					<span
						className={ `wsx-icon ${
							visibilitySectionStatus ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ visibilitySectionStatus && (
					<div className="wsx-accordion-body">
						<div className="wsx-visibility-slider-fields">
							{ visibilitySectionStatus &&
								Object.keys( fieldData.attr ).map(
									( field ) => {
										switch (
											fieldData.attr[ field ].type
										) {
											case 'switch':
												return switchData(
													fieldData.attr[ field ],
													field
												);
											case 'slider':
												return sliderData(
													fieldData.attr[ field ],
													field,
													'wsx-slider-sm'
												);
											default:
												return null;
										}
									}
								) }
						</div>
						<div className="wsx-visibility-select-options">
							{ visibilitySectionStatus &&
								Object.keys( fieldData.attr ).map(
									( field ) => {
										switch (
											fieldData.attr[ field ].type
										) {
											case 'select':
												return selectData(
													fieldData.attr[ field ],
													field
												);
											case 'multiselect':
												if (
													productSettings._hide_for_b2b_role_n_user ===
														'b2b_specific' &&
													field === '_hide_for_roles'
												) {
													return multiselectData(
														fieldData.attr[ field ],
														field
													);
												}
												if (
													productSettings._hide_for_b2b_role_n_user ===
														'user_specific' &&
													field === '_hide_for_users'
												) {
													return multiselectData(
														fieldData.attr[ field ],
														field
													);
												}
												break;
											default:
												break;
										}
										return null;
									}
								) }
						</div>
					</div>
				) }
			</div>
		);
	};

	const tabData = ( fieldData ) => {
		return Object.keys( fieldData ).map( ( field ) => {
			switch ( fieldData[ field ].type ) {
				case 'choosebox':
					return chooseboxData( fieldData[ field ], field );
				case 'switch':
					return switchData( fieldData[ field ], field );
				case 'radio':
					return radioData( fieldData[ field ], field );
				case 'slider':
					return sliderData(
						fieldData[ field ],
						field,
						'wsx-slider-md wsx-mb-40'
					);
				case 'select':
					return selectData( fieldData[ field ], field );
				case 'visibility_section':
					return visibilitySectionData( fieldData[ field ], field );

				default:
					return null;
			}
		} );
	};

	return (
		<>
			{ appState.loading && <LoadingGif /> }
			{ ! appState.loading && (
				<>
					{ Object.keys( fields ).map(
						( section ) =>
							section === '_product_settings_tab' &&
							tabData( fields[ section ].attr, section )
					) }
				</>
			) }
			<input
				type="hidden"
				value={ JSON.stringify( productSettings ) }
				name="wholesalex_product_settings"
			/>
			{ popUpStatus && (
				<UpgradeProPopUp
					title={ wholesalex_product.i18n.unlock_heading }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
		</>
	);
};

export default ProductTab;
