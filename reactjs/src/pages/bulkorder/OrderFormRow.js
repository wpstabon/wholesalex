import { __ } from '@wordpress/i18n';
import { useState, useEffect, useRef } from 'react';
import MultiSelect from './MultiSelect';
import Tooltip from '../../components/Tooltip';
import Icons from '../../utils/Icons';
import LoadingGif from '../../components/LoadingGif';

const OrderFormRow = ( {
	index,
	list,
	setList,
	setCurState,
	tags,
	categories,
} ) => {
	const [ filterValue, setFilterValue ] = useState( 'all_products' );
	const [ loading, setLoading ] = useState( false );

	const fields = wholesalex_bulkorder.fields._form.attr;
	const allProducts = [];
	const initialState = {
		_product_select: [],
		_product_quantity: 1,
		_price: '',
		_product_sku: '',
		_formatted_price: '',
	};

	const [ item, setItem ] = useState(
		list.items[ index ] ? list.items[ index ] : { ...initialState }
	);

	const onChangeHandler = ( type = '', value = '' ) => {
		const _item = { ...item };
		setCurState( type );
		switch ( type ) {
			case 'select_product':
				_item._product_select = value;
				_item._product_sku = value?.[ 0 ]?.sku;

				setItem( _item );
				break;
			case 'quantity_change':
				_item._product_quantity = value;
				setItem( _item );
				break;
			case 'price_update':
				_item._price = value._price;
				_item._formatted_price = value._formatted_price;
				setItem( _item );
				break;
			case 'delete_product':
				{
					const _parent = { ...list };
					let _items = [ ..._parent.items ];
					_items = _items.filter( ( _, j ) => {
						return index !== j;
					} );
					_parent.items = _items;
					setList( _parent );
				}
				break;

			default:
				break;
		}
	};

	const inputData = ( fieldData ) => {
		return (
			<input
				className="wholesalex_bulkorder_item_qty wsx-input wsx-br-md wsx-plr-12"
				style={ { maxWidth: '120px' } }
				name={ '_product_quantity' }
				type={ fieldData.type }
				defaultValue={ item._product_quantity }
				onChange={ ( e ) => {
					onChangeHandler( 'quantity_change', e.target.value );
				} }
			/>
		);
	};
	const selectWithSearchData = ( fieldData, idx ) => {
		return (
			<MultiSelect
				key={ `prodduct_select_${ idx }` }
				wrapperClass="wsx-w-full"
				name={ '_product_select' }
				value={
					item._product_select && item._product_select.length
						? item._product_select
						: []
				}
				options={ allProducts }
				tags={ tags }
				categories={ categories }
				placeholder={ __( 'Search for Products...', 'wholesalex-pro' ) }
				filterOnChangeHandler={ ( e ) => {
					setFilterValue( e.target.value );
				} }
				filterValue={ filterValue }
				onMultiSelectChangeHandler={ ( fieldName, selectedValues ) => {
					onChangeHandler( 'select_product', selectedValues );
				} }
			/>
		);
	};
	const priceData = () => {
		return (
			<>
				{ ! loading && (
					<span
						dangerouslySetInnerHTML={ {
							__html: item._price ? item._formatted_price : '',
						} }
					></span>
				) }
				{ loading && (
					<span>{ wholesalex_bulkorder.calculating_text }</span>
				) }
			</>
		);
	};

	const fetchData = async ( type = 'get_price', data = {}, signal ) => {
		setLoading( true );
		const attr = {
			type,
			action: 'order_form',
			nonce: wholesalex_bulkorder.nonce,
		};

		switch ( type ) {
			case 'get_price':
				attr.id = data.id;
				attr.qty = data.qty;
				attr.list = JSON.stringify( list.items );

				break;
			case 'update_all_price':
				break;
			default:
				break;
		}

		wp.apiFetch( {
			path: '/wholesalex/v1/order_form',
			method: 'POST',
			data: attr,
			signal,
		} )
			.then( ( res ) => {
				if ( res.success ) {
					switch ( type ) {
						case 'get_price':
							onChangeHandler( 'price_update', res.data );
							break;
						default:
							break;
					}
				}
				abortController.current = null;
				setLoading( false );
			} )
			.catch( ( error ) => {
				if ( error.name === 'AbortError' ) {
					// handle error
				}
			} );
	};

	const abortController = useRef( null );

	useEffect( () => {
		abortController.current = new AbortController();
		const { signal } = abortController.current;
		if (
			item._product_select &&
			item._product_select.length &&
			item._product_select[ 0 ].value &&
			item._product_quantity
		) {
			const productID = item._product_select[ 0 ].value;
			const quantity = item._product_quantity;
			fetchData( 'get_price', { id: productID, qty: quantity }, signal );
		} else {
			fetchData( 'get_price', { id: 0, qty: 0 }, signal );
		}
		return () => {
			if ( abortController.current ) {
				abortController.current.abort();
			}
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ item._product_quantity, item._product_select ] );

	useEffect( () => {
		const _parent = { ...list };
		const _items = [ ..._parent.items ];
		let _item = _items[ index ];
		_item = { ...item };
		_items[ index ] = _item;
		_parent.items = _items;
		setList( _parent );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ item._price ] );

	return (
		<tr key={ `field_${ index }` } className="bulk_order_item">
			<td>
				<div
					className={ `product_select ${ index } wsx-d-flex wsx-item-center wsx-gap-16` }
				>
					{ fields._product_select.type === 'selectwithsearch' &&
						selectWithSearchData( fields._product_select, index ) }
					<div
						style={ {
							display: `${
								index + 1 === list.items.length
									? 'none'
									: 'block'
							}`,
						} }
					>
						<Tooltip
							content={ __(
								'Delete this product.',
								'wholesalex-pro'
							) }
							direction="top"
							onlyText={ true }
						>
							<div
								className={ `wsx-bulk-item-delete` }
								onClick={ () => {
									onChangeHandler( 'delete_product' );
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										onChangeHandler( 'delete_product' );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.delete_24 }
							</div>
						</Tooltip>
					</div>
				</div>
			</td>
			<td>
				{ fields._product_quantity.type === 'number' &&
					inputData( fields._product_quantity, index ) }
			</td>
			<td className={ `total_price ${ ! loading ? 'p-left-4rem' : '' }` }>
				{ ! loading &&
					fields._price.type === 'price' &&
					priceData( fields._price, index ) }
				{ loading && <LoadingGif /> }
			</td>
		</tr>
	);
};

export default OrderFormRow;
