import { useEffect, useState } from 'react';
import { __ } from '@wordpress/i18n';
import styleGenerate from '../../components/Helper';
import Tooltip from '../../components/Tooltip';
import Icons from '../../utils/Icons';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';

const PurchaseLists = () => {
	const [ backupData, setBackupData ] = useState( [] );
	const [ data, setData ] = useState( [] );
	const [ endpointUrl, setEndpointUrl ] = useState( '' );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ currentPage, setCurrentPage ] = useState( 1 );
	const [ loading, setLoading ] = useState( false );
	const styles = styleGenerate( wholesalex );

	const itemPerPage = 5;

	const fetchData = async ( type = 'get', lists = '' ) => {
		const attr = {
			type,
			action: 'purchase_list',
			nonce: wholesalex_bulkorder.nonce,
		};

		if ( type === 'set' ) {
			attr.data = lists;
		}

		setLoading( true );

		try {
			const res = await wp.apiFetch( {
				path: '/bulkorder/v1/purchase_list',
				method: 'POST',
				data: attr,
			} );

			if ( res.success ) {
				if ( type === 'get' ) {
					// const sortedLists = Object.keys(res.data.lists).sort(function (a, b) { return b - a }).reduce((lists, key) => {
					//     lists[key] = res.data.lists[key];
					//     return lists;
					// }, {});
					// setEndpointUrl(res.data.bulkorder_endpoint);
					// setData(sortedLists);
					// setBackupData(sortedLists);
				} else if ( res.data.message ) {
					window.alert( res.data.message );
				}
			}
		} catch ( error ) {
			// Error fetching data
		} finally {
			setLoading( false );
		}
	};

	useEffect( () => {
		// fetchData();
		const sortedLists = Object.keys( wholesalex_bulkorder.lists )
			.sort( function ( a, b ) {
				return b - a;
			} )
			.reduce( ( lists, key ) => {
				lists[ key ] = wholesalex_bulkorder.lists[ key ];
				return lists;
			}, {} );
		setEndpointUrl( wholesalex_bulkorder.bulkorder_endpoint );
		setData( sortedLists );
		setBackupData( sortedLists );
	}, [] );

	const onClickHandler = ( type = '', id = '' ) => {
		switch ( type ) {
			case 'view':
				if ( id !== '' ) {
					const url = new URL( endpointUrl );
					url.searchParams.append( 'view_purchase_list', id );
					history.pushState( {}, '', url );
				}
				break;
			case 'delete_list':
				{
					const _data = { ...data };
					delete _data[ id ];
					setData( _data );
					setBackupData( _data );
					fetchData( 'set', _data );
				}
				break;

			default:
				break;
		}
	};

	const getPurchaseListLink = ( id = '' ) => {
		if ( id !== '' ) {
			const url = new URL( endpointUrl );
			url.searchParams.append( 'view_purchase_list', id );
			return url;
		}
	};

	const exportToCsv = ( items, name ) => {
		let csv =
			'product_id,sku,product_name,quantity,price_per_unit,total_price' +
			'\n';

		items.forEach( ( item ) => {
			if (
				item._product_select &&
				item._product_select[ 0 ] &&
				item._product_select[ 0 ].value &&
				item._product_select[ 0 ].name &&
				item._product_quantity &&
				item._price
			) {
				const productId = item._product_select[ 0 ].value;
				const sku = item._product_sku;
				const productName = item._product_select[ 0 ].name;
				const quantity = item._product_quantity;
				let ppu = parseFloat( item._price / quantity );
				let price = item._price;

				try {
					if ( accounting ) {
						price = accounting.formatMoney( price, {
							symbol: wholesalex_bulkorder.currency_format_symbol,
							decimal:
								wholesalex_bulkorder.currency_format_decimal_sep,
							thousand:
								wholesalex_bulkorder.currency_format_thousand_sep,
							precision:
								wholesalex_bulkorder.currency_format_num_decimals,
							format: wholesalex_bulkorder.currency_format,
						} );
						ppu = accounting.formatMoney( ppu, {
							symbol: wholesalex_bulkorder.currency_format_symbol,
							decimal:
								wholesalex_bulkorder.currency_format_decimal_sep,
							thousand:
								wholesalex_bulkorder.currency_format_thousand_sep,
							precision:
								wholesalex_bulkorder.currency_format_num_decimals,
							format: wholesalex_bulkorder.currency_format,
						} );
					}
				} catch ( error ) {
					// handle error
				}

				const row =
					'"' +
					productId +
					'"' +
					',' +
					'"' +
					sku +
					'"' +
					',' +
					'"' +
					productName +
					'"' +
					',' +
					'"' +
					quantity +
					'"' +
					',' +
					'"' +
					ppu +
					'"' +
					',' +
					'"' +
					price +
					'"' +
					'\n';
				csv += row;
			}
		} );

		const mimeType = 'text/csv;encoding:utf-8';
		const fileName = name;
		const a = document.createElement( 'a' );

		if ( navigator.msSaveBlob ) {
			navigator.msSaveBlob(
				new Blob( [ csv ], {
					type: mimeType,
				} ),
				fileName
			);
		} else if ( URL && 'download' in a ) {
			a.href = URL.createObjectURL(
				new Blob( [ csv ], {
					type: mimeType,
				} )
			);
			a.setAttribute( 'download', fileName );
			document.body.appendChild( a );
			a.click();
			document.body.removeChild( a );
		} else {
			location.href =
				'data:application/octet-stream,' + encodeURIComponent( csv );
		}
	};

	const rowData = ( list ) => {
		return (
			<tr key={ list.id }>
				<td>
					<div className="wsx-d-flex wsx-item-center wsx-gap-4">
						<Tooltip
							content={ __(
								'Remove this product',
								'wholesalex-pro'
							) }
							direction="top"
							onlyText={ true }
						>
							<div
								className="wsx-icon-cross wsx-icon-round"
								tabIndex={ -1 }
								onClick={ () => {
									onClickHandler( 'delete_list', list.id );
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										onClickHandler(
											'delete_list',
											list.id
										);
									}
								} }
								role="button"
							>
								{ Icons.cross }
							</div>
						</Tooltip>
						{ list.name }
					</div>
				</td>
				<td>{ list.item_count }</td>
				<td>{ list.quantity_count }</td>
				<td
					dangerouslySetInnerHTML={ { __html: list.price_html } }
				></td>
				<td>
					<div className="wsx-rule-actions wsx-btn-group wsx-gap-8 wsx-justify-end">
						<Tooltip
							content={ wholesalex_bulkorder.i18n.view_list }
							direction="top"
						>
							<a
								className="wsx-link wsx-btn-action"
								href={ getPurchaseListLink( list.id ) }
							>
								{ Icons.view }
							</a>
						</Tooltip>
						<Tooltip
							content={ wholesalex_bulkorder.i18n.download }
							direction="top"
						>
							<span
								className="wsx-btn-action"
								onClick={ () => {
									exportToCsv( list.items, list.name );
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										exportToCsv( list.items, list.name );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.download }
							</span>
						</Tooltip>
					</div>
				</td>
			</tr>
		);
	};

	useEffect( () => {
		const _data = { ...backupData };
		const _dataCopy = {};
		const _key = searchValue;
		if ( searchValue ) {
			Object.keys( _data ).forEach( ( list ) => {
				if (
					_data[ list ].name
						.toLowerCase()
						.includes( _key.toLowerCase() )
				) {
					return ( _dataCopy[ list ] = _data[ list ] );
				}
			} );
			setData( _dataCopy );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ searchValue ] );

	const renderPurchaseList = () => {
		const purchaseLists = { ...data };
		const purchaseListsKeys = Object.keys( purchaseLists );
		const getPaginatedData = () => {
			const startIndex = currentPage * itemPerPage - itemPerPage;
			const endIndex = startIndex + itemPerPage;
			return purchaseListsKeys.slice( startIndex, endIndex );
		};

		return getPaginatedData().map( ( key, index ) => {
			return rowData( data[ key ], index );
		} );
	};

	const paginations = () => {
		const length = Object.keys( data ).length;
		const totalPageCount = parseInt(
			( length + itemPerPage - 1 ) / itemPerPage
		);

		const getPages = () => {
			let pages = [];
			if ( totalPageCount <= 5 ) {
				for ( let i = 1; i <= totalPageCount; i++ ) {
					pages.push( i );
				}
			} else if ( currentPage <= 3 ) {
				pages = [ 1, 2, 3, 4, '...', totalPageCount ];
			} else if ( currentPage >= totalPageCount - 2 ) {
				pages = [
					1,
					'...',
					totalPageCount - 3,
					totalPageCount - 2,
					totalPageCount - 1,
					totalPageCount,
				];
			} else {
				pages = [
					1,
					'...',
					currentPage - 1,
					currentPage,
					currentPage + 1,
					'...',
					totalPageCount,
				];
			}
			return pages;
		};

		return (
			<div className="wsx-pagination-wrapper">
				<div className="wsx-font-14">
					{ wholesalex_bulkorder.i18n.showing_x_purchase_lists.replace(
						'##',
						length
					) }
				</div>
				<div className="wsx-btn-group">
					{ totalPageCount > 1 && (
						<Button
							onClick={ () => {
								currentPage > 1
									? setCurrentPage( ( page ) => page - 1 )
									: '';
							} }
							iconName="angleLeft_24"
							borderColor="tertiary"
							iconColor="tertiary"
							disable={ currentPage < 2 }
							key={ `pagination_no_prev}` }
						/>
					) }

					{ totalPageCount > 1 &&
						getPages().map( ( page, i ) => (
							<Button
								key={ `pagination_no_${ i }` }
								borderColor="border-secondary"
								background="base1"
								label={ page }
								onClick={ () => {
									setCurrentPage( i + 1 );
								} }
								customClass={
									currentPage === page ? 'active' : ''
								}
								disable={ page === '...' }
							/>
						) ) }

					{ totalPageCount > 1 && (
						<Button
							onClick={ () => {
								currentPage < totalPageCount
									? setCurrentPage( ( page ) => page + 1 )
									: '';
							} }
							iconName="angleRight_24"
							borderColor="tertiary"
							iconColor="tertiary"
							disable={ currentPage === totalPageCount }
							key={ `pagination_no_next` }
						/>
					) }
				</div>
			</div>
		);
	};

	return (
		<div key="purchase_list">
			<div className="wsx-d-flex wsx-item-center wsx-justify-space wsx-gap-12 wsx-mb-26">
				<div
					className="bulk_order_form_header wsx-title"
					style={ styles.text }
				>
					{ wholesalex_bulkorder.i18n.purchase_list }
				</div>
				<div className="purchase_list search create wsx-d-flex wsx-item-center wsx-gap-16">
					<div className="wsx-search-wrapper">
						<input
							className="wsx-input wsx-m-0"
							type={ 'text' }
							placeholder={ wholesalex_bulkorder.i18n.search }
							value={ searchValue }
							onChange={ ( e ) => {
								setSearchValue( e.target.value );
							} }
						/>
						<div className="wsx-icon wsx-icon-right">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								fill="none"
							>
								<path
									stroke="currentColor"
									strokeLinecap="round"
									strokeLinejoin="round"
									strokeWidth="1.5"
									d="M9.167 15.833a6.667 6.667 0 1 0 0-13.333 6.667 6.667 0 0 0 0 13.333ZM17.5 17.5l-3.625-3.625"
								></path>
							</svg>
						</div>
					</div>
					<a className="wsx-link" href={ `${ endpointUrl }` }>
						<div
							className="wsx-btn wsx-bg-primary wsx-d-flex wsx-gap-6 wsx-item-center wsx-justify-center wsx-font-regular wsx-font-16"
							style={ styles.primary_button }
						>
							<div className="wsx-icon">{ Icons.plus }</div>
							<div className="button_text">
								{ wholesalex_bulkorder.i18n.add_new_list }
							</div>
						</div>
					</a>
				</div>
			</div>

			<div className="wsx-account-table-container">
				<table className="wsx-table bulk_order purchase_lists has-background">
					<thead>
						<tr>
							<th className="heading_list_name">
								{ wholesalex_bulkorder.i18n.name }
							</th>
							<th className="heading_item_numbers">
								{ wholesalex_bulkorder.i18n.items }
							</th>
							<th className="heading_quantity_counts">
								{ __( 'Quantity', 'wholesalex-pro' ) }
							</th>
							<th className="heading_total_price">
								{ wholesalex_bulkorder.i18n.total_price }
							</th>
							<th
								className="heading_actions text-align-right"
								colSpan={ 100 }
							>
								{ wholesalex_bulkorder.i18n.actions }
							</th>
						</tr>
					</thead>
					<tbody>
						{ Object.keys( data ).length === 0 && (
							<tr>
								<td className="no_items" colSpan={ 100 }>
									{
										wholesalex_bulkorder.i18n
											.no_purchase_lists_found
									}
								</td>
							</tr>
						) }
						{ renderPurchaseList() }
					</tbody>
				</table>
			</div>

			{ paginations() }
			{ loading && <LoadingGif /> }
		</div>
	);
};

export default PurchaseLists;
