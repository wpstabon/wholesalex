import React from 'react';
import ReactDOM from 'react-dom';

import BulkOrderForm from './BulkOrderForm';
import PurchaseLists from './PurchaseLists';

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_bulk_order_form' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<BulkOrderForm />
			</React.StrictMode>,
			document.getElementById( 'wholesalex_bulk_order_form' )
		);
	}
} );
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_bulkorder_purchase_lists' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<PurchaseLists />
			</React.StrictMode>,
			document.getElementById( 'wholesalex_bulkorder_purchase_lists' )
		);
	}
} );
