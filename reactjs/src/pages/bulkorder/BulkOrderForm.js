import { useEffect, useState, useRef } from 'react';
import OrderFormRow from './OrderFormRow';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import PopupModal from '../../components/PopupModal';
import LoadingGif from '../../components/LoadingGif';

const BulkOrderForm = () => {
	const initialState = {
		id: Date.now().toString(),
		name: '',
		item_count: 0,
		quantity_count: 0,
		total_price: 0,
		price_html: '0.0',
		items: [ { id: Date.now().toString(), _product_quantity: 1 } ],
	};
	const storedList = sessionStorage.getItem(
		'bulk_order_' + wholesalex_bulkorder.user_id
	)
		? {
				...JSON.parse(
					sessionStorage.getItem(
						'bulk_order_' + wholesalex_bulkorder.user_id
					)
				),
				id: Date.now().toString(),
		  }
		: false;
	const validateList = storedList ? storedList.items.length > 0 : false;
	const [ loading, setLoading ] = useState( {
		initialLoading: true,
		totalPriceLoading: false,
	} );
	const [ saveModalStatus, setSaveModalStatus ] = useState( false );
	const [ list, setList ] = useState(
		validateList ? storedList : initialState
	);
	const [ curState, setCurState ] = useState( '' );
	const [ isUpdate, setIsUpdate ] = useState( false );

	const [ categories, setCategories ] = useState( [] );
	const [ tags, setTags ] = useState( [] );

	const loaderRef = useRef( null );

	const addToCart = () => {
		const formData = new FormData();
		formData.append( 'action', 'bulkorder_add_to_cart' );
		formData.append( 'nonce', wholesalex_bulkorder.nonce );

		const cartItems =
			list.items.length > 0 &&
			list.items.filter( ( _item ) => {
				return (
					_item._price > 0 &&
					_item._product_quantity > 0 &&
					_item._product_select.length > 0
				);
			} );

		if ( cartItems.length === 0 ) {
			return;
		}
		formData.append( 'data', JSON.stringify( cartItems ) );

		if ( loaderRef !== null ) {
			loaderRef.current.style.display = 'block';
		}
		fetch( wholesalex_bulkorder.ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.success ) {
					if ( loaderRef !== null ) {
						loaderRef.current.style.display = 'none';
					}

					if ( 'yes' === res.data.is_clear_session ) {
						sessionStorage.removeItem(
							'bulk_order_' + wholesalex_bulkorder.user_id
						);
					}

					window.location.replace( `${ res.data.redirect_url }` );
				}
			} );
	};

	const fetchData = async ( type = 'get', data = {}, signal = '' ) => {
		const attr = {
			type,
			action: 'order_form',
			nonce: wholesalex_bulkorder.nonce,
		};

		switch ( type ) {
			case 'get_price':
				attr.id = data.id;
				attr.qty = data.qty;
				attr.index = data.index;
				attr.list = JSON.stringify( list );
				break;
			case 'format_price':
				attr.value = data.sum;
				setLoading( { ...loading, totalPriceLoading: true } );
				break;
			case 'save_list':
				attr.data = data;
				if ( loaderRef !== null ) {
					loaderRef.current.style.display = 'block';
				}
				break;
			case 'get_list':
				attr.id = data.id;
				break;

			default:
				setLoading( { ...loading, initialLoading: true } );
				break;
		}

		if ( Object.keys( data ).length !== 0 && data.id ) {
			attr.id = data.id;
		}

		if ( signal === '' ) {
			abortController.current = new AbortController();
			signal = abortController.current.signal;
		}

		wp.apiFetch( {
			path: '/wholesalex/v1/order_form',
			method: 'POST',
			data: attr,
			signal,
		} )
			.then( ( res ) => {
				if ( res.success ) {
					switch ( type ) {
						case 'get':
							setLoading( { ...loading, initialLoading: false } );

							if ( res.data.data ) {
								if ( Object.keys( res.data.data ).length ) {
									setList( res.data.data );
								} else {
									setList( initialState );
								}
								setIsUpdate( true );
							}
							break;
						case 'get_price':
							onChangeHandler(
								'price_update',
								attr.index,
								res.data
							);
							break;
						case 'format_price':
							{
								const _list = { ...list };
								_list.quantity_count = data.quantity;
								_list.total_price = data.sum;
								_list.price_html = res.data;
								_list.item_count = data.item_count;
								setList( { ...list, ..._list } );
								setLoading( {
									...loading,
									totalPriceLoading: false,
								} );
							}
							break;
						case 'save_list':
							setSaveModalStatus( false );
							window.alert( res.data.message );
							if ( 'yes' === res.data.is_clear_session ) {
								sessionStorage.removeItem(
									'bulk_order_' + wholesalex_bulkorder.user_id
								);
								location.reload();
							}
							break;
						default:
							break;
					}
					if ( loaderRef !== null ) {
						loaderRef.current.style.display = 'none';
					}
				}
			} )
			.catch( () => {} );
	};

	const abortController = useRef( null );

	useEffect( () => {
		const param = new URLSearchParams( window.location.search ).get(
			'view_purchase_list'
		);
		if ( param ) {
			abortController.current = new AbortController();
			const { signal } = abortController.current;
			sessionStorage.removeItem(
				'bulk_order_' + wholesalex_bulkorder.user_id
			);
			fetchData( 'get', { id: param }, signal );
			return () => {
				if ( abortController.current ) {
					abortController.current.abort();
				}
			};
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const getData = ( type = 'get_all_categories' ) => {
		setLoading( { ...loading, initialLoading: true } );

		const attr = {
			type,
			action: 'order_form',
			nonce: wholesalex_bulkorder.nonce,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/order_form',
			method: 'POST',
			data: attr,
		} )
			.then( ( res ) => {
				if ( res.success ) {
					switch ( type ) {
						case 'get_all_categories':
							setCategories( res.data );
							break;
						case 'get_all_tags':
							setTags( res.data );
							break;

						default:
							break;
					}
				}
				setLoading( { ...loading, initialLoading: false } );
			} )
			.catch( () => {} );
	};

	const getCategories = () => {
		return getData( 'get_all_categories' );
	};
	const getAllTags = () => {
		return getData( 'get_all_tags' );
	};

	useEffect( () => {
		getCategories();
		getAllTags();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const rowData = ( index ) => {
		return (
			<OrderFormRow
				key={ `row_${ index }_${ list.items[ index ].id }` }
				list={ list }
				setList={ setList }
				index={ index }
				curState={ curState }
				setCurState={ setCurState }
				categories={ categories }
				tags={ tags }
			/>
		);
	};

	const getTotalPrice = ( signal ) => {
		let sum = 0;
		let quantity = 0;
		let itemCount = 0;
		const items = list.items;
		items.map( ( item ) => {
			if ( item._price || ! isEmpty( item._price ) ) {
				sum += parseFloat( item._price );
				quantity += parseInt( item._product_quantity );
				itemCount++;
			}
			return item;
		} );
		fetchData( 'format_price', { sum, quantity, itemCount }, signal );
	};
	const isEmpty = ( str ) =>
		! str?.length ||
		str === undefined ||
		str === '' ||
		parseInt( str ) === 0;

	const onChangeHandler = ( type = '', index = '' ) => {
		setCurState( type );
		switch ( type ) {
			case 'add_product':
				{
					const _parent = { ...list };
					const copy = [ ..._parent.items ];
					index = copy.length - 1;

					if (
						! isEmpty( copy[ index ]._product_select ) &&
						( ! isEmpty( copy[ index ]._product_quantity ) ||
							copy[ index ]._product_quantity )
					) {
						copy.push( {
							id: Date.now().toString(),
							_product_quantity: 1,
						} );
						_parent.items = copy;
						setList( { ...list, ..._parent } );
					} else {
						alert(
							__( 'Please Fill All Fields', 'wholesalex-pro' )
						);
					}
				}

				break;

			default:
				break;
		}
	};

	useEffect( () => {
		if ( curState !== 'add_product' && list?.items ) {
			abortController.current = new AbortController();
			const { signal } = abortController.current;
			getTotalPrice( signal );
			sessionStorage.setItem(
				'bulk_order_' + wholesalex_bulkorder.user_id,
				JSON.stringify( list )
			);
			return () => {
				if ( abortController.current ) {
					abortController.current.abort();
				}
			};
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ list.items ] );

	const savePopupContent = () => {
		return (
			<div className="modal_overlay">
				<div className="wsx-input-wrapper wsx-flex-1">
					<div className="wsx-input-label wsx-font-medium wsx-font-20 wsx-mb-16">
						{ __( 'Order List Name:', 'wholesalex-pro' ) }
					</div>
					<div className="wsx-input-container">
						<input
							className="wsx-input wsx-br-md"
							name={ 'list_name' }
							type={ 'text' }
							value={ list.name }
							onChange={ ( e ) => {
								const _list = { ...list };
								_list.name = e.target.value;
								setList( { ...list, ..._list } );
							} }
							style={ { width: '98%', marginLeft: '2px' } }
						/>
					</div>
				</div>
				<div className="wsx-btn-group wsx-justify-end wsx-mt-12">
					<Button
						label={ __( 'Save', 'wholesalex-pro' ) }
						onClick={ () => {
							fetchData( 'save_list', list );
						} }
						background="approve"
						padding="6px 12px"
					/>
					<Button
						label={ __( 'Cancel', 'wholesalex-pro' ) }
						onClick={ () => {
							setSaveModalStatus( false );
						} }
						background="tertiary"
						padding="6px 12px"
					/>
				</div>
			</div>
		);
	};

	return (
		<>
			<div className="bulk_order_form_header wsx-title wsx-mb-26">
				{ __( 'Quick / Bulk Order Form', 'wholesalex-pro' ) }
			</div>

			<div className="wsx-account-table-container">
				<table className="wsx-table wsx-table bulk_order has-background">
					<thead>
						<tr>
							<th className="products">
								{ __( 'Products', 'wholesalex-pro' ) }
							</th>
							<th className="quantity">
								{ __( 'Quantity', 'wholesalex-pro' ) }
							</th>
							<th className="subtotal p-left-4rem">
								{ __( 'Subtotal', 'wholesalex-pro' ) }
							</th>
						</tr>
					</thead>
					<tbody>
						{ list.items.length > 0 &&
							list.items.map( ( item, i ) => {
								return rowData( i );
							} ) }
						<tr>
							<td colSpan={ 100 }>
								<div className="add_product">
									<Button
										label={ __(
											'Add Products',
											'wholesalex-pro'
										) }
										background="tertiary"
										iconName="plus"
										onClick={ () => {
											onChangeHandler( 'add_product' );
										} }
									/>
								</div>
							</td>
						</tr>
						<tr>
							<td colSpan={ 2 }>
								<div className="wsx-btn-group">
									<Button
										label={
											isUpdate
												? __(
														'Update List',
														'wholesalex-pro'
												  )
												: __(
														'Save Order List',
														'wholesalex-pro'
												  )
										}
										background="tertiary"
										iconName="doc"
										onClick={ () => {
											setSaveModalStatus(
												! saveModalStatus
											);
										} }
									/>
									<Button
										label={ __(
											'Add to Cart',
											'wholesalex-pro'
										) }
										background="primary"
										iconName="doc"
										onClick={ () => {
											addToCart();
										} }
									/>
								</div>
							</td>
							<td
								className={ `price ${
									loading.totalPriceLoading
										? 'price_calculating'
										: ''
								}` }
							>
								{ loading.totalPriceLoading && (
									<span>
										{ __(
											'Calculating...',
											'wholesalex-pro'
										) }
									</span>
								) }
								{ ! loading.totalPriceLoading && (
									<>
										{ __( 'Total:', 'wholesalex-pro' ) }{ ' ' }
										<span
											dangerouslySetInnerHTML={ {
												__html: list.price_html,
											} }
										></span>{ ' ' }
									</>
								) }
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div ref={ loaderRef }>{ <LoadingGif /> }</div>
			{ saveModalStatus && (
				<PopupModal
					wrapperClass="wsx-w-90p wsx-width-512"
					renderContent={ savePopupContent }
					onClose={ () => {
						setSaveModalStatus( ! saveModalStatus );
					} }
				/>
			) }
		</>
	);
};

export default BulkOrderForm;
