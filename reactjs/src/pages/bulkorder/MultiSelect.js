import React, { useEffect, useRef, useState } from 'react';
import Tooltip from '../../components/Tooltip';
import Icons from '../../utils/Icons';
import SelectWithOptGroup from '../../components/SelectWithOptGroup';
import Alert from '../../components/Alert';
import { __ } from '@wordpress/i18n';

const MultiSelect = ( {
	name,
	value,
	options,
	placeholder,
	wrapperClass,
	customClass,
	onMultiSelectChangeHandler,
	isDisable,
	filterValue,
	ajaxAction,
	filterOnChangeHandler,
	categories,
	tags,
} ) => {
	const [ showList, setShowList ] = useState( false );
	const [ selectedOptions, setSelectedOptions ] = useState( value );
	const [ optionList, setOptionList ] = useState( options );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ isSearching, setIsSearching ] = useState( false );
	const [ tempSearchValue, setTempSearchValue ] = useState( '' );
	const [ tempDropdownSearchValue, setTempDropdownSearchValue ] =
		useState( '' );
	const [ stockAlert, setStockAlert ] = useState( false );
	const [ productName, setProductName ] = useState( false );

	const myRef = useRef();

	const onInputChangeHandler = ( e ) => {
		setShowList( true );
		setTempSearchValue( e.target.value );
	};
	const onInputClickHandler = () => {
		setShowList( true );
		setTempDropdownSearchValue( '' );
	};

	const handleAlertClose = () => {
		setStockAlert( false );
		setProductName( '' );
	};

	const selectOption = ( option ) => {
		if ( option.is_manage_stock === 'yes' && option.stock <= 0 ) {
			setStockAlert( true );
			setProductName( option.name );
			return;
		}
		setSelectedOptions( [ option ] );

		const searchResult = optionList.filter( ( op ) => {
			const { value: optionValue } = op;
			return (
				optionValue.toString().toLowerCase() !==
				option.value.toString().toLowerCase()
			);
		} );
		setOptionList( searchResult );
		setTempSearchValue( '' );

		onMultiSelectChangeHandler( name, [ ...selectedOptions, option ] );

		setShowList( false );
	};

	const deleteOption = ( option ) => {
		const selectedOptionAfterDeleted = selectedOptions.filter( ( op ) => {
			const { value: optionValue } = op;
			return (
				optionValue.toString().toLowerCase() !==
				option.value.toString().toLowerCase()
			);
		} );
		setSelectedOptions( selectedOptionAfterDeleted );
		setTempSearchValue( '' );
		onMultiSelectChangeHandler( name, selectedOptionAfterDeleted );
	};

	const performAjaxSearch = async ( signal ) => {
		setIsSearching( true );
		const attr = {
			type: 'search_product',
			action: 'order_form',
			nonce: wholesalex_bulkorder.nonce,
			query: searchValue,
			filter: filterValue,
			ajax_action: ajaxAction,
		};
		try {
			const res = await wp.apiFetch( {
				path: '/wholesalex/v1/order_form',
				method: 'POST',
				data: attr,
				signal,
			} );

			if ( res.status ) {
				let selectedOptionValues = [];
				if ( selectedOptions.length > 0 ) {
					selectedOptionValues = selectedOptions.map(
						( option ) => option.value
					);
				}
				const searchResult = res?.data?.filter( ( option ) => {
					return selectedOptionValues.indexOf( option.value ) === -1;
				} );

				searchResult.sort( ( a, b ) => a.name.length - b.name.length );

				setOptionList( searchResult );

				setIsSearching( false );
			} else {
				setIsSearching( false );
			}
		} catch ( error ) {
			if ( error.name === 'AbortError' ) {
				// Request was cancelled
			}
		}
	};

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setShowList( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const abortController = useRef( null );

	useEffect( () => {
		abortController.current = new AbortController();
		const { signal } = abortController.current;

		if ( searchValue.length >= 2 ) {
			performAjaxSearch( signal );
		}

		return () => {
			if ( abortController.current ) {
				abortController.current.abort( 'Duplicate' );
			}
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ searchValue, setSearchValue ] );

	useEffect( () => {
		if ( tempSearchValue.length > 1 ) {
			const delay = setTimeout(
				() => setSearchValue( tempSearchValue ),
				500
			);
			return () => clearTimeout( delay );
		}
	}, [ tempSearchValue ] );

	useEffect( () => {
		setSearchValue( '' );
		abortController.current = new AbortController();
		const { signal } = abortController.current;
		performAjaxSearch( signal );
		return () => {
			if ( abortController.current ) {
				abortController.current.abort( 'Duplicate' );
			}
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ tempDropdownSearchValue ] );

	const getCategoriesGroup = {
		label: __( 'Category', 'wholesalex-pro' ),
		options: Object.keys( categories ).reduce( ( tempCategories, key ) => {
			tempCategories[ 'cat_' + key ] = categories[ key ];
			return tempCategories;
		}, {} ),
	};
	const getTagsGroup = {
		label: __( 'Tags', 'wholesalex-pro' ),
		options: Object.keys( tags ).reduce( ( tempTags, key ) => {
			tempTags[ 'tag_' + key ] = tags[ key ];
			return tempTags;
		}, {} ),
	};
	const combinedObject = {
		categories: getCategoriesGroup,
		tags: getTagsGroup,
	};

	const filterData = () => {
		return (
			! ( wholesalex_bulkorder.disable_filter === 'yes' ) && (
				<div
					className="filter wsx-filter"
					style={ {
						borderLeft: '1px solid var(--color-border-secondary)',
					} }
				>
					<SelectWithOptGroup
						selectClass={ 'wsx-ellipsis' }
						selectionText={ __( 'Cancel', 'wholesalex-pro' ) }
						selectionValue={ 'all_products' }
						valuePrefix="cat_"
						optionGroup={ combinedObject }
						name={ 'wsx-lists-bulk-action' }
						onChange={ ( e ) => {
							filterOnChangeHandler( e );
							setTempDropdownSearchValue( e );
						} }
						minWidth="154px"
						maxWidth="154px"
						maxHeight="38px"
						borderNone={ true }
						borderRadius="0"
					/>
				</div>
			)
		);
	};

	return (
		<div
			className={ `wsx-multiselect-wrapper ${
				isDisable ? 'locked' : ''
			} ${ wrapperClass }` }
			key={ `wsx-multiselect-${ name }` }
		>
			<div className="wsx-multiselect-inputs wsx-d-flex wsx-item-center">
				{ selectedOptions.length > 0 && (
					<div className="wsx-multiselect-input-wrapper">
						{ selectedOptions.map( ( option, index ) => {
							return (
								<span
									key={ `wsx-multiselect-opt-${ name }-${ option.value }-${ index }` }
									className="wsx-selected-option"
								>
									<span
										tabIndex={ -1 }
										className="wsx-icon-cross wsx-lh-0"
										onClick={ () => deleteOption( option ) }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												deleteOption( option );
											}
										} }
										role="button"
									>
										{ Icons.cross }
									</span>
									<Tooltip
										content={ option.name }
										position="top"
										onlyText={ true }
									>
										<div className="multiselect-option-name">
											{ option.name }
										</div>
									</Tooltip>
								</span>
							);
						} ) }
					</div>
				) }
				<div className="wholsalex_option_input_wrapper">
					<input
						key={ `wholesalex_input_${ name }` }
						disabled={ isDisable ? true : false }
						id={ name }
						tabIndex={ 0 }
						autoComplete="off"
						value={ tempSearchValue }
						className={ `wsx-input wsx-m-0 ${ customClass }` }
						placeholder={
							selectedOptions.length > 0 ? '' : placeholder
						}
						onChange={ ( e ) => onInputChangeHandler( e ) }
						onClick={ ( e ) => onInputClickHandler( e ) }
					/>
				</div>
			</div>
			<div ref={ myRef } key={ `wholesalex_${ name }` }>
				{ ! isSearching &&
					showList &&
					tempSearchValue.length > 1 &&
					optionList.length > 0 && (
						<div
							className="wsx-card wsx-bg-base1 wsx-multiselect-options wsx-scrollbar"
							key={ `wsx-opt-${ name }` }
						>
							{ optionList.map( ( option, index ) => {
								return (
									<div
										className="wsx-multiselect-option"
										key={ `wsx-opt-${ name }-${ option.value }-${ index }` }
										onClick={ () => selectOption( option ) }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												selectOption( option );
											}
										} }
										role="button"
										tabIndex="0"
									>
										{ option.name }
									</div>
								);
							} ) }
						</div>
					) }
				{ ! isSearching &&
					tempSearchValue.length > 1 &&
					showList &&
					optionList.length === 0 && (
						<div
							key={ `wsx-${ name }-not-found` }
							className="wsx-card wsx-multiselect-options wsx-scrollbar  wsx-bg-base1"
						>
							<div className="wsx-multiselect-option">
								{ __(
									'No Data Found! Please try with another keyword.',
									'wholesalex-pro'
								) }
							</div>
						</div>
					) }
				{ ! isSearching && tempSearchValue.length < 2 && showList && (
					<div
						key={ `wsx-${ name }-not-found` }
						className="wsx-card wsx-multiselect-options wsx-scrollbar  wsx-bg-base1"
					>
						<div className="wsx-multiselect-option">
							{ __(
								'Enter 2 or more character to search.',
								'wholesalex-pro'
							) }
						</div>
					</div>
				) }
				{ isSearching && showList && (
					<div
						key={ `wsx-${ name }-not-found` }
						className="wsx-card wsx-multiselect-options wsx-scrollbar  wsx-bg-base1"
					>
						<div className="wsx-multiselect-option">
							{ __( 'Searching', 'wholesalex-pro' ) }
						</div>
					</div>
				) }
				{ stockAlert && (
					<Alert
						title="Out of Stock"
						description={ `${ productName } is not in stock` }
						onClose={ handleAlertClose }
					/>
				) }
			</div>
			{ filterData() }
		</div>
	);
};
export default MultiSelect;
