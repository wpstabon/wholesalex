import React from 'react';
import { Dropdown } from '@wordpress/components';

function FormBuilderDropdown( {
	renderToogle,
	renderContent,
	className,
	contentClassName = '',
	placement = 'bottom-start',
	offset = 15,
} ) {
	return (
		<Dropdown
			className={ `wholesalex-form-builder-dropdown ${ className }` }
			contentClassName={ `wholesalex-form-builder-dropdown-content ${ contentClassName }` }
			popoverProps={ {
				animate: true,
				placement,
				shift: true,
				resize: true,
				offset,
			} }
			renderToggle={ ( { onToggle, isOpen } ) => {
				return renderToogle( isOpen, onToggle );
			} }
			renderContent={ ( { isOpen, onToggle } ) => {
				return renderContent( isOpen, onToggle );
			} }
		/>
	);
}
export default FormBuilderDropdown;
