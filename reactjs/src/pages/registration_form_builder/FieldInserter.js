import React, { useContext, useMemo } from 'react';
import FormBuilderDropdown from './FormBuilderDropdown';
import { __ } from '@wordpress/i18n';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
import { ToastContexts } from '../../context/ToastsContexts';
const FieldInserter = ( { type, index, setUpgradeProPopupStatus } ) => {
	const { dispatch } = useContext( ToastContexts );

	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);

	// Memoize the getUsedFieldsName function to prevent unnecessary recalculations
	const getUsedFieldsName = useMemo( () => {
		const _fieldsName = [];
		registrationForm.registrationFields &&
			registrationForm.registrationFields.forEach( ( row ) => {
				row.columns.forEach( ( _field ) => {
					_fieldsName.push( _field.name );
				} );
			} );
		return _fieldsName;
	}, [ registrationForm ] );

	const defaultFields = useMemo(
		() => ( {
			first_name: 'First Name',
			last_name: 'Last Name',
			user_email: 'Email',
			user_login: 'Username',
			user_pass: 'Password',
			user_confirm_pass: 'Confirm Password',
			description: 'User Bio',
			nickname: 'Nickname',
			wholesalex_registration_role: 'Select Role',
			display_name: 'Display Name',
			url: 'Website',
			user_confirm_email: 'Confirm Email',
			wholesalex_term_condition: 'Term and Condition',

			// 'privacy_policy_text': 'Privacy Policy'
		} ),
		[]
	);

	const extraFields = useMemo(
		() => ( {
			text: 'Text',
			textarea: 'Text Area',
			radio: 'Radio',
			checkbox: 'Checkbox',
			file: 'File',
			select: 'Select',
			email: 'Email',
			number: 'Number',
			date: 'Date',
		} ),
		[]
	);

	const usedFieldNames = getUsedFieldsName;

	const getFieldType = ( name ) => {
		switch ( name ) {
			case 'first_name':
			case 'last_name':
			case 'nickname':
			case 'display_name':
			case 'text_field':
			case 'user_login':
				return 'text';

			case 'user_email':
			case 'user_confirm_email':
			case 'email':
				return 'email';

			case 'user_pass':
			case 'user_confirm_pass':
				return 'password';
			case 'url':
				return 'url';
			case 'description':
				return 'textarea';
			case 'wholesalex_registration_role':
				return 'select';
			case 'wholesalex_term_condition':
				return 'termCondition';
			// case 'privacy_policy_text':
			//     return 'privacy_policy_text';

			default:
				break;
		}
	};

	const addField = ( fieldType, fieldLabel, fieldName, isCustomField ) => {
		switch ( type ) {
			case 'newFieldRow':
				setRegistrationForm( {
					type: 'newFieldRow',
					field: {
						type: fieldType,
						label: fieldLabel,
						name: fieldName,
						isCustomField,
					},
				} );
				break;
			case 'insertFieldOnColumn':
				setRegistrationForm( {
					type: 'insertFieldOnColumn',
					field: {
						type: fieldType,
						label: fieldLabel,
						name: fieldName,
						isCustomField,
					},
					index,
				} );
				break;
			case 'insertFieldOnRightColumn':
				setRegistrationForm( {
					type: 'insertFieldOnRightColumn',
					field: {
						type: fieldType,
						label: fieldLabel,
						name: fieldName,
						isCustomField,
					},
					index,
				} );
				break;
			case 'insertFieldOnLeftColumn':
				setRegistrationForm( {
					type: 'insertFieldOnLeftColumn',
					field: {
						type: fieldType,
						label: fieldLabel,
						name: fieldName,
						isCustomField,
					},
					index,
				} );
				break;

			default:
				break;
		}
	};

	const fieldsPopup = ( setStatus ) => {
		return (
			<div className="wholesalex-form-builder-field-poup">
				<div className="wholesalex-form-builder-popup">
					<div className="wholesalex-popup-heading wsx-font-18-lightBold">
						<div>{ __( 'Default User Fields', 'wholesalex' ) }</div>
						<div
							className="wsx-popup-close"
							onClick={ () => setStatus( false ) }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setStatus( false );
								}
							} }
							role="button"
							tabIndex="0"
						>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								viewBox="0 0 20 20"
								fill="none"
							>
								<path
									d="M15 5L5 15"
									stroke="#343A46"
									strokeWidth="1.5"
									strokeLinecap="round"
									strokeLinejoin="round"
								/>
								<path
									d="M5 5L15 15"
									stroke="#343A46"
									strokeWidth="1.5"
									strokeLinecap="round"
									strokeLinejoin="round"
								/>
							</svg>
						</div>
					</div>

					<div className="wholesalex-popup-content">
						<ul>
							{ Object.keys( defaultFields ).map(
								( fieldKey, idx ) => {
									const _isDisable =
										usedFieldNames.includes( fieldKey );
									return (
										<li
											key={ idx }
											className={ `wsx-insert-field-btn ${
												_isDisable
													? 'wholesalex-disabled'
													: ''
											}` }
											onClick={ () => {
												if ( ! _isDisable ) {
													addField(
														getFieldType(
															fieldKey
														),
														defaultFields[
															fieldKey
														],
														fieldKey,
														false
													);
													setStatus( false );
												} else {
													dispatch( {
														type: 'ADD_MESSAGE',
														payload: {
															id: Date.now().toString(),
															type: 'error',
															message: __(
																'This field is already used!',
																'wholesalex'
															),
														},
													} );
												}
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													if ( ! _isDisable ) {
														addField(
															getFieldType(
																fieldKey
															),
															defaultFields[
																fieldKey
															],
															fieldKey,
															false
														);
														setStatus( false );
													} else {
														dispatch( {
															type: 'ADD_MESSAGE',
															payload: {
																id: Date.now().toString(),
																type: 'error',
																message: __(
																	'This field is already used!',
																	'wholesalex'
																),
															},
														} );
													}
												}
											} }
											role="none"
											tabIndex="0"
										>
											{ defaultFields[ fieldKey ] }
										</li>
									);
								}
							) }
						</ul>
					</div>
				</div>
				<div className="wholesalex-form-builder-popup">
					<div className="wholesalex-popup-heading wsx-font-18-lightBold wsx-extra-field-popup">
						{ __( 'Extra Fields', 'wholesalex' ) }{ ' ' }
						{ ! wholesalex.is_pro_active && (
							<span
								className="dashicons dashicons-lock"
								onClick={ () => {
									setUpgradeProPopupStatus( true );
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										setUpgradeProPopupStatus( true );
									}
								} }
								role="none"
								tabIndex="0"
							></span>
						) }
					</div>
					<div className="wholesalex-popup-content">
						<ul>
							{ Object.keys( extraFields ).map(
								( fieldKey, idx ) => {
									return (
										<li
											key={ idx }
											className={ `wsx-insert-field-btn ${
												! wholesalex.is_pro_active
													? 'wholesalex-disabled'
													: ''
											}` }
											onClick={ () => {
												if (
													wholesalex.is_pro_active
												) {
													addField(
														fieldKey,
														extraFields[ fieldKey ],
														fieldKey,
														true
													);
													setStatus( false );
												} else {
													setUpgradeProPopupStatus(
														true
													);
												}
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													if (
														wholesalex.is_pro_active
													) {
														addField(
															fieldKey,
															extraFields[
																fieldKey
															],
															fieldKey,
															true
														);
														setStatus( false );
													} else {
														setUpgradeProPopupStatus(
															true
														);
													}
												}
											} }
											role="none"
											tabIndex="0"
										>
											{ extraFields[ fieldKey ] }
										</li>
									);
								}
							) }
						</ul>
					</div>
				</div>
			</div>
		);
	};

	return (
		<div className="wholesalex-form-builder-field-inserter">
			<FormBuilderDropdown
				renderToogle={ ( status, setStatus ) => {
					return (
						<span
							className={ `wholesalex-field-insert-btn dashicons dashicons-plus-alt2 ${
								status ? 'is-active wsx-active' : ''
							}` }
							onClick={ () => setStatus( ! status ) }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setStatus( ! status );
								}
							} }
							role="button"
							tabIndex="0"
						></span>
					);
				} }
				renderContent={ ( status, setStatus ) => {
					return fieldsPopup( setStatus );
				} }
				placement="bottom"
				contentClassName="wsx-field-insert-popup"
			/>
		</div>
	);
};

export default FieldInserter;
