import React, { useContext, useCallback } from 'react';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
const { useState, useEffect, useRef } = wp.element;
const { __ } = wp.i18n;

const ChooseInputStyle = () => {
	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);

	const options = [
		'variation_1',
		'variation_2',
		'variation_3',
		'variation_4',
		'variation_5',
		'variation_6',
		'variation_7',
		'variation_8',
	];

	const myRef = useRef();
	const [ isOpen, setOpen ] = useState( false );

	const getImageUrl = ( option ) => {
		return (
			wholesalex.url +
			'assets/img/input-field-variations/' +
			option +
			'.svg'
		);
	};

	const handleClickOutside = useCallback(
		( e ) => {
			if ( ! myRef.current.contains( e.target ) ) {
				setOpen( false );
			}
		},
		[ myRef ]
	);

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [ handleClickOutside ] );
	return (
		<div ref={ myRef } className={ `wholesalex-choose-input-field-wrap` }>
			<div>
				<div className="wholesalex-choose-input-field-label">
					{ __( 'Choose Input Style', 'wholesalex' ) && (
						<label className="wsx-label" htmlFor="wsx-label">
							{ wholesalex_overview?.i18n?.choose_input_style }
						</label>
					) }
				</div>
				<div className={ `wholesalex-choose-input-field-popup` }>
					<span
						tabIndex={ 0 }
						className={
							( isOpen ? 'isOpen ' : '' ) +
							' wsx-chose-input-wrap'
						}
						onClick={ () => setOpen( ! isOpen ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								setOpen( ! isOpen );
							}
						} }
						role="button"
					>
						{ registrationForm?.settings?.inputVariation
							?.replace( '_', ' ' )
							?.replace( 'variation', 'Style' ) }
						<div className="wholesalex-choose-input-selected">
							<span className="wholesalex-choose-input-selected-img">
								{
									<img
										src={ getImageUrl(
											registrationForm.settings
												.inputVariation
										) }
										alt={
											registrationForm.settings
												.inputVariation
										}
									/>
								}
							</span>
							<span className="wholesalex-choose-input-dropdown-icon">
								<i
									className={
										'dashicons dashicons-arrow-' +
										( isOpen ? 'up-alt2' : 'down-alt2' )
									}
								/>
							</span>
						</div>
					</span>
					{ isOpen && (
						<ul>
							{ options.map( ( item, k ) => (
								<li
									key={ k }
									className={ `${
										registrationForm.settings
											.inputVariation === item
											? 'wsx-selected'
											: ''
									}` }
									onClick={ () => {
										setOpen( ! isOpen );
										setRegistrationForm( {
											type: 'updateInputVariation',
											value: item,
										} );
									} }
									value={ item }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											setOpen( ! isOpen );
										}
									} }
									role="none"
								>
									{ 'Style ' }
									{ k + 1 }
									<img
										src={ getImageUrl( item ) }
										alt={ item }
									/>
								</li>
							) ) }
						</ul>
					) }
				</div>
			</div>
		</div>
	);
};

export default ChooseInputStyle;
