import React, { useState, useEffect, useContext } from 'react';
import { __ } from '@wordpress/i18n';
import '../../assets/scss/Form.scss';
import '../../assets/scss/Editor.scss';
import '../../assets/scss/Shortcode.scss';
import Toast from '../../components/Toast';
import Slider from '../../components/Slider';
import ColorPanelDropdown from './ColorPanenDropdown';
import InputWithIncrementDecrement from './InputWithIncrementDecrement';
import ChooseInputStyle from './ChooseInputStyle';
import RegistrationFormBuilder from './RegistrationFormBuilder';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
import LoginForm from './LoginForm';
import { getFormStyle } from './Utils';
import ShortcodesModal from './ShortcodesModal';
import PremadeDesignModal from './PremadeDesignModal';
import Panel from '../../components/Panel';
import Section from '../../components/Section';
import ToggleGroupControl from '../../components/ToggleGroupControl';

import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';
import Icons from '../../utils/Icons';
import { ToastContexts } from '../../context/ToastsContexts';

function Editor() {
	const [ shortcodeModalStatus, setShortcodeModalStatus ] = useState( false );
	const [ premadeDesignModalStatus, setPremadeDesignModalStatus ] =
		useState( false );
	const [ resetData, setResetData ] = useState(
		JSON.parse( wholesalex_overview.whx_form_builder_form_data )
	);
	const [ upgradeProPopupStatus, setUpgradeProPopupStatus ] =
		useState( false );

	const [ appState, setAppState ] = useState( {
		loading: false,
		loadingOnSave: false,
	} );

	const [ settingsData, setSettingsData ] = useState( {
		colorfieldtype: 'signIn',
		colorfieldstate: 'normal',
		colorbuttonstate: 'normal',
		colorbuttontype: 'signIn',
		colorcontainertype: 'main',
		typographyfieldtype: 'label',
		sizeSpacingcontainertype: 'main',
	} );

	const [ enableSidebar, setSidebar ] = useState( true );

	const { dispatch } = useContext( ToastContexts );

	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);

	const fetchData = async ( type = 'get' ) => {
		const attr = {
			type,
			action: 'builder_action',
			nonce: wholesalex.nonce,
		};
		if ( type === 'post' ) {
			attr.data = JSON.stringify( registrationForm );
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/builder_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( type === 'get' ) {
					setAppState( { ...appState, loading: false } );
				} else if ( type === 'post' ) {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: 'Successfully Saved',
						},
					} );
					setAppState( { ...appState, loadingOnSave: false } );
				}
			}
		} );
	};

	useEffect( () => {
		if ( wholesalex_overview.whx_form_builder_form_data ) {
			setRegistrationForm( {
				type: 'init',
				value: JSON.parse(
					wholesalex_overview.whx_form_builder_form_data
				),
			} );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const onSave = () => {
		let userLogin = 'no';
		registrationForm.registrationFields.forEach( ( items ) => {
			items.columns.forEach( ( element ) => {
				if ( element.name === 'user_login' ) {
					userLogin = 'yes';
				}
			} );
		} );

		if (
			wholesalex_overview.whx_form_builder_is_woo_username === 'no' &&
			userLogin === 'no'
		) {
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					delay: 4000,
					type: 'error',
					message: 'Please provide USER NAME',
				},
			} );
			return;
		}
		setAppState( { ...appState, loadingOnSave: true } );
		setResetData( registrationForm );
		fetchData( 'post' );
	};

	const onReset = () => {
		if ( wholesalex_overview.whx_form_builder_form_data ) {
			setRegistrationForm( { type: 'init', value: resetData } );
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'success',
					message: __( 'Reset Successful', 'wholesalex' ),
				},
			} );
		}
	};
	// const proPopupContent = () => {
	// 	return (
	// 		<>
	// 			<img
	// 				className="wsx-addon-popup-image"
	// 				src={ wholesalex.url + '/assets/img/unlock.svg' }
	// 				alt="Unlock Icon"
	// 			/>
	// 			<div className="wsx-title wsx-font-18 wsx-font-medium wsx-mt-4">
	// 				{ __( 'Unlock all Features with', 'wholesalex' ) }{ ' ' }
	// 				{ __( 'with', 'wholesalex' ) }
	// 			</div>
	// 			<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mb-16">
	// 				<div className="wsx-title wsx-font-18 wsx-font-medium">
	// 					{ __( 'WholesaleX', 'wholesalex' ) }
	// 				</div>
	// 				<div className="wsx-title wsx-font-18 wsx-font-medium wsx-color-primary">
	// 					{ __( 'Pro', 'wholesalex' ) }
	// 				</div>
	// 			</div>
	// 			<Button
	// 				buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_features-upgrade_to_pro&utm_campaign=wholesalex-DB"
	// 				label={ __( 'Upgrade to Pro ➤', 'wholesalex' ) }
	// 				background="secondary"
	// 				iconName="growUp"
	// 				customClass="wsx-w-auto wsx-br-lg wsx-justify-center wsx-font-16"
	// 			/>
	// 		</>
	// 	);
	// };

	// const premadeDesignBtnIcon = (
	// 	<svg
	// 		xmlns="http://www.w3.org/2000/svg"
	// 		width="20"
	// 		height="20"
	// 		viewBox="0 0 20 20"
	// 		fill="none"
	// 	>
	// 		<path
	// 			d="M18.4186 5.76875L7.39355 1.3C7.2998 1.2625 7.21855 1.25 7.1248 1.25C6.8498 1.25 6.59355 1.4125 6.48105 1.68125L5.83105 3.29375L6.53105 3.44375L7.13105 1.95L15.1936 5.2125L16.4748 5.73125L18.1498 6.4125L14.8248 14.625L14.2623 17.3688V17.7313C14.2623 17.7688 14.2623 17.8 14.2561 17.8375C14.2811 17.7938 14.3061 17.7563 14.3248 17.7063L18.7936 6.68125C18.9436 6.325 18.7748 5.91875 18.4123 5.76875H18.4186Z"
	// 			fill="white"
	// 		/>
	// 		<path
	// 			d="M16.0195 5.38125L4.60078 3.04375C4.55703 3.0375 4.50703 3.03125 4.46953 3.03125C4.14453 3.03125 3.86953 3.25625 3.79453 3.575L3.42578 5.39375H4.14453L4.48203 3.73125L12.657 5.39375L14.157 5.69375L15.8633 6.04375L14.2633 13.925V17.3625L16.5445 6.18125C16.6195 5.8125 16.382 5.45 16.0133 5.375L16.0195 5.38125Z"
	// 			fill="white"
	// 		/>
	// 		<path
	// 			d="M13.5875 5.39374H1.93125C1.55625 5.39374 1.25 5.69999 1.25 6.07499V17.7312C1.25 18.1062 1.55625 18.4125 1.93125 18.4125H13.5875C13.9625 18.4125 14.2688 18.1062 14.2688 17.7312V6.07499C14.2688 5.69999 13.9625 5.39374 13.5875 5.39374ZM9.45625 7.68124C10.1938 7.68124 10.7938 8.28124 10.7938 9.01874C10.7938 9.75624 10.1938 10.3562 9.45625 10.3562C8.71875 10.3562 8.11875 9.75624 8.11875 9.01874C8.11875 8.28124 8.71875 7.68124 9.45625 7.68124ZM12.4688 17.0875H2.85C2.575 17.0875 2.40625 16.7812 2.55 16.55L6.05 10.9625C6.1875 10.7437 6.50625 10.7437 6.64375 10.9625L8.625 14.125C8.7625 14.3437 9.08125 14.3437 9.21875 14.125L9.93125 12.9875C10.0688 12.7687 10.3875 12.7687 10.525 12.9875L12.7563 16.55C12.9 16.7875 12.7375 17.0875 12.4563 17.0875H12.4688Z"
	// 			fill="white"
	// 		/>
	// 	</svg>
	// );

	const TEXT_TRANSFORMS_OPTIONS = [
		{
			name: __( 'None', 'wholesalex' ),
			value: 'none',
			icon: (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 24 24"
					width="24"
					height="24"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M7 11.5h10V13H7z"></path>
				</svg>
			),
		},
		{
			name: __( 'Uppercase', 'wholesalex' ),
			value: 'uppercase',
			icon: (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 24 24"
					width="24"
					height="24"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M6.1 6.8L2.1 18h1.6l1.1-3h4.3l1.1 3h1.6l-4-11.2H6.1zm-.8 6.8L7 8.9l1.7 4.7H5.3zm15.1-.7c-.4-.5-.9-.8-1.6-1 .4-.2.7-.5.8-.9.2-.4.3-.9.3-1.4 0-.9-.3-1.6-.8-2-.6-.5-1.3-.7-2.4-.7h-3.5V18h4.2c1.1 0 2-.3 2.6-.8.6-.6 1-1.4 1-2.4-.1-.8-.3-1.4-.6-1.9zm-5.7-4.7h1.8c.6 0 1.1.1 1.4.4.3.2.5.7.5 1.3 0 .6-.2 1.1-.5 1.3-.3.2-.8.4-1.4.4h-1.8V8.2zm4 8c-.4.3-.9.5-1.5.5h-2.6v-3.8h2.6c1.4 0 2 .6 2 1.9.1.6-.1 1-.5 1.4z"></path>
				</svg>
			),
		},
		{
			name: __( 'Lowercase', 'wholesalex' ),
			value: 'lowercase',
			icon: (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 24 24"
					width="24"
					height="24"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M11 16.8c-.1-.1-.2-.3-.3-.5v-2.6c0-.9-.1-1.7-.3-2.2-.2-.5-.5-.9-.9-1.2-.4-.2-.9-.3-1.6-.3-.5 0-1 .1-1.5.2s-.9.3-1.2.6l.2 1.2c.4-.3.7-.4 1.1-.5.3-.1.7-.2 1-.2.6 0 1 .1 1.3.4.3.2.4.7.4 1.4-1.2 0-2.3.2-3.3.7s-1.4 1.1-1.4 2.1c0 .7.2 1.2.7 1.6.4.4 1 .6 1.8.6.9 0 1.7-.4 2.4-1.2.1.3.2.5.4.7.1.2.3.3.6.4.3.1.6.1 1.1.1h.1l.2-1.2h-.1c-.4.1-.6 0-.7-.1zM9.2 16c-.2.3-.5.6-.9.8-.3.1-.7.2-1.1.2-.4 0-.7-.1-.9-.3-.2-.2-.3-.5-.3-.9 0-.6.2-1 .7-1.3.5-.3 1.3-.4 2.5-.5v2zm10.6-3.9c-.3-.6-.7-1.1-1.2-1.5-.6-.4-1.2-.6-1.9-.6-.5 0-.9.1-1.4.3-.4.2-.8.5-1.1.8V6h-1.4v12h1.3l.2-1c.2.4.6.6 1 .8.4.2.9.3 1.4.3.7 0 1.2-.2 1.8-.5.5-.4 1-.9 1.3-1.5.3-.6.5-1.3.5-2.1-.1-.6-.2-1.3-.5-1.9zm-1.7 4c-.4.5-.9.8-1.6.8s-1.2-.2-1.7-.7c-.4-.5-.7-1.2-.7-2.1 0-.9.2-1.6.7-2.1.4-.5 1-.7 1.7-.7s1.2.3 1.6.8c.4.5.6 1.2.6 2s-.2 1.4-.6 2z"></path>
				</svg>
			),
		},
		{
			name: __( 'Capitalize', 'wholesalex' ),
			value: 'capitalize',
			icon: (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 24 24"
					width="24"
					height="24"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M7.1 6.8L3.1 18h1.6l1.1-3h4.3l1.1 3h1.6l-4-11.2H7.1zm-.8 6.8L8 8.9l1.7 4.7H6.3zm14.5-1.5c-.3-.6-.7-1.1-1.2-1.5-.6-.4-1.2-.6-1.9-.6-.5 0-.9.1-1.4.3-.4.2-.8.5-1.1.8V6h-1.4v12h1.3l.2-1c.2.4.6.6 1 .8.4.2.9.3 1.4.3.7 0 1.2-.2 1.8-.5.5-.4 1-.9 1.3-1.5.3-.6.5-1.3.5-2.1-.1-.6-.2-1.3-.5-1.9zm-1.7 4c-.4.5-.9.8-1.6.8s-1.2-.2-1.7-.7c-.4-.5-.7-1.2-.7-2.1 0-.9.2-1.6.7-2.1.4-.5 1-.7 1.7-.7s1.2.3 1.6.8c.4.5.6 1.2.6 2 .1.8-.2 1.4-.6 2z"></path>
				</svg>
			),
		},
	];
	const WEIGHT_OPTIONS = {
		100: '100',
		200: '200',
		300: '300',
		400: '400',
		500: '500',
		600: '600',
		700: '700',
		800: '800',
		900: '900',
	};

	const createToggleSelectorMain = ( options, defaultVal ) => ( {
		type: 'toggleSelectorMain',
		options,
		default: defaultVal,
	} );
	const createToggleSelectorState = ( options, defaultVal, title = '' ) => ( {
		type: 'toggleSelectorState',
		options,
		default: defaultVal,
		title,
	} );
	const createToggleSelectorAlignment = (
		options,
		defaultVal,
		title = ''
	) => ( {
		type: 'toggleSelectorAlignment',
		options,
		default: defaultVal,
		title,
	} );
	const createColorSelector = ( options, defaults, dependsOptions ) => ( {
		type: 'colorSelector',
		options,
		default: defaults,
		dependsOptions,
	} );
	const createNumberWithIncrementDecrement = (
		label,
		meta,
		defaultVal,
		depends
	) => ( {
		type: 'numberWIthIncrementDecrement',
		label,
		meta,
		default: defaultVal,
		depends,
	} );
	const createSelect = ( label, options, defaultVal ) => ( {
		type: 'select',
		label,
		options,
		default: defaultVal,
	} );
	const createTransformSelect = ( label, options, defaultVal ) => ( {
		type: 'transformSelect',
		label,
		options,
		default: defaultVal,
	} );

	const createPanel = ( label, children ) => ( {
		type: 'panel',
		label,
		children,
	} );
	const createSection = ( label, children ) => ( {
		type: 'section',
		label,
		children,
	} );

	const fields = {
		color: createSection( __( 'Color Settings', 'wholesalex' ), {
			field: createPanel( __( 'Field', 'wholesalex' ), {
				type: createToggleSelectorMain(
					{
						signIn: __( 'Sign In', 'wholesalex' ),
						signUp: __( 'Sign Up', 'wholesalex' ),
					},
					'signIn'
				),
				state: createToggleSelectorState(
					{
						normal: __( 'Normal', 'wholesalex' ),
						active: __( 'Active', 'wholesalex' ),
						warning: __( 'Warning', 'wholesalex' ),
					},
					'normal',
					__( 'State', 'wholesalex' )
				),
				colors: createColorSelector(
					[
						{
							label: __( 'Label', 'wholesalex' ),
							value: 'label',
							depends: [],
						},
						{
							label: __( 'Input Text', 'wholesalex' ),
							value: 'text',
							depends: [],
						},
						{
							label: __( 'Background', 'wholesalex' ),
							value: 'background',
							depends: [],
						},
						{
							label: __( 'Border', 'wholesalex' ),
							value: 'border',
							depends: [],
						},
						{
							label: __( 'Placeholder', 'wholesalex' ),
							value: 'placeholder',
							depends: [
								{
									key: 'colorfieldstate',
									value: 'normal',
									operator: 'equal',
								},
							],
						},
					],
					{ label: '', text: '', background: '', border: '' }
				),
			} ),
			button: createPanel( __( 'Button', 'wholesalex' ), {
				type: createToggleSelectorMain(
					{
						signIn: __( 'Sign In', 'wholesalex' ),
						signUp: __( 'Sign Up', 'wholesalex' ),
					},
					'signIn'
				),
				state: createToggleSelectorState(
					{
						normal: __( 'Normal', 'wholesalex' ),
						hover: __( 'Hover', 'wholesalex' ),
					},
					'normal',
					__( 'State', 'wholesalex' )
				),
				colors: createColorSelector(
					[
						{ label: __( 'Text', 'wholesalex' ), value: 'text' },
						{
							label: __( 'Background', 'wholesalex' ),
							value: 'background',
						},
						{
							label: __( 'Border', 'wholesalex' ),
							value: 'border',
						},
					],
					{ text: '', background: '', border: '' }
				),
			} ),
			container: createPanel( __( 'Container', 'wholesalex' ), {
				type: createToggleSelectorMain(
					{
						main: __( 'Main', 'wholesalex' ),
						signIn: __( 'Sign In', 'wholesalex' ),
						signUp: __( 'Sign Up', 'wholesalex' ),
					},
					'signIn'
				),
				colors: createColorSelector(
					[
						{
							label: __( 'Background', 'wholesalex' ),
							value: 'background',
						},
						{
							label: __( 'Border', 'wholesalex' ),
							value: 'border',
						},
					],
					{ text: '', background: '', border: '' }
				),
			} ),
		} ),
		typography: createSection( __( 'Typography Settings', 'wholesalex' ), {
			field: createPanel( __( 'Field', 'wholesalex' ), {
				type: createToggleSelectorMain(
					{
						label: __( 'Label', 'wholesalex' ),
						input: __( 'Input', 'wholesalex' ),
					},
					'label'
				),
				size: createNumberWithIncrementDecrement(
					__( 'Size', 'wholesalex' ),
					'unit: px',
					14
				),
				weight: createSelect(
					__( 'Weight', 'wholesalex' ),
					WEIGHT_OPTIONS,
					500
				),
				transform: createTransformSelect(
					__( 'Transform', 'wholesalex' ),
					TEXT_TRANSFORMS_OPTIONS,
					'none'
				),
			} ),
			button: createPanel( __( 'Button', 'wholesalex' ), {
				size: createNumberWithIncrementDecrement(
					__( 'Size', 'wholesalex' ),
					'unit: px',
					14
				),
				weight: createSelect(
					__( 'Weight', 'wholesalex' ),
					WEIGHT_OPTIONS,
					500
				),
				transform: createTransformSelect(
					__( 'Transform', 'wholesalex' ),
					TEXT_TRANSFORMS_OPTIONS,
					'none'
				),
			} ),
		} ),
		sizeSpacing: createSection(
			__( 'Size and Spacing Settings', 'wholesalex' ),
			{
				input: createPanel( __( 'Input', 'wholesalex' ), {
					padding: createNumberWithIncrementDecrement(
						__( 'Padding', 'wholesalex' ),
						'unit: px',
						14
					),
					width: createNumberWithIncrementDecrement(
						__( 'Max Width', 'wholesalex' ),
						'unit: %',
						395
					),
					border: createNumberWithIncrementDecrement(
						__( 'Border', 'wholesalex' ),
						'unit: px',
						1
					),
					borderRadius: createNumberWithIncrementDecrement(
						__( 'Border Radius', 'wholesalex' ),
						'unit: px',
						1
					),
				} ),
				button: createPanel( __( 'Button', 'wholesalex' ), {
					padding: createNumberWithIncrementDecrement(
						__( 'Padding', 'wholesalex' ),
						'unit: px',
						14
					),
					width: createNumberWithIncrementDecrement(
						__( 'Width', 'wholesalex' ),
						'unit: %',
						50
					),
					align: createToggleSelectorAlignment(
						{
							left: __( 'Left', 'wholesalex' ),
							center: __( 'Center', 'wholesalex' ),
							right: __( 'Right', 'wholesalex' ),
						},
						'left',
						__( 'Alignment', 'wholesalex' )
					),
					border: createNumberWithIncrementDecrement(
						__( 'Border', 'wholesalex' ),
						'unit: px',
						1
					),
					borderRadius: createNumberWithIncrementDecrement(
						__( 'Border Radius', 'wholesalex' ),
						'unit: px',
						2
					),
				} ),
				container: createPanel( __( 'Container', 'wholesalex' ), {
					type: createToggleSelectorMain(
						{
							main: __( 'Main', 'wholesalex' ),
							signIn: __( 'Sign In', 'wholesalex' ),
							signUp: __( 'Sign Up', 'wholesalex' ),
						},
						'signIn'
					),
					width: createNumberWithIncrementDecrement(
						__( 'Max Width', 'wholesalex' ),
						'unit: px',
						2
					),
					border: createNumberWithIncrementDecrement(
						__( 'Border', 'wholesalex' ),
						'unit: px',
						2
					),
					borderRadius: createNumberWithIncrementDecrement(
						__( 'Border Radius', 'wholesalex' ),
						'unit: px',
						16
					),
					padding: createNumberWithIncrementDecrement(
						__( 'Padding', 'wholesalex' ),
						'unit: px',
						0
					),
					separator: createNumberWithIncrementDecrement(
						__( 'Separator', 'wholesalex' ),
						'unit: px',
						16,
						[
							{
								key: 'sizeSpacingcontainertype',
								value: 'main',
								operator: 'equal',
							},
						]
					),
				} ),
			}
		),
	};
	const updateFieldStyle = (
		sectionName,
		panelName,
		loginOrSignUp,
		stateName,
		property,
		value
	) => {
		setRegistrationForm( {
			type: 'setStyle',
			sectionName,
			loginOrSignUp,
			stateName,
			panelName,
			property,
			value,
		} );
	};
	const getFieldStyle = (
		sectionName,
		panelName,
		loginOrSignUp,
		stateName,
		property
	) => {
		let val = '';
		if ( stateName && loginOrSignUp ) {
			val =
				registrationForm?.style?.[ sectionName ]?.[ panelName ]?.[
					loginOrSignUp
				]?.[ stateName ]?.[ property ];
		} else if ( loginOrSignUp && ! stateName ) {
			val =
				registrationForm?.style?.[ sectionName ]?.[ panelName ]?.[
					loginOrSignUp
				]?.[ property ];
		} else if ( ! loginOrSignUp && ! stateName ) {
			val =
				registrationForm?.style?.[ sectionName ]?.[ panelName ]?.[
					property
				];
		}
		return val;
	};

	const getSettingFields = () => {
		const checkDepends = ( depends ) => {
			if ( ! depends || depends.length === 0 ) {
				return true;
			}
			let status = false;
			depends.forEach( ( depend ) => {
				const key = depend.key;
				const value = depend.value;
				const operator = depend.operator;
				if ( operator === 'equal' ) {
					status = settingsData[ key ] === value;
				} else if ( operator === 'not_equal' ) {
					status = settingsData[ key ] !== value;
				}
			} );
			return status;
		};

		return Object.keys( fields ).map( ( _section ) => {
			const section = fields[ _section ];

			return (
				section &&
				section.type === 'section' && (
					<Section title={ section.label }>
						{ section.children &&
							Object.keys( section.children ).map(
								( _sectionChild ) => {
									const sectionChildren =
										section.children[ _sectionChild ];
									return (
										sectionChildren.type === 'panel' && (
											<Panel
												title={ sectionChildren.label }
											>
												{ Object.keys(
													sectionChildren.children
												).map( ( _child ) => {
													const field =
														sectionChildren
															.children[ _child ];
													const key =
														_section +
														_sectionChild +
														_child;
													const defVal = settingsData[
														key
													]
														? settingsData[ key ]
														: field.default;

													const loginOrSignupKey =
														_section +
														_sectionChild +
														'type';
													const stateKey =
														_section +
														_sectionChild +
														'state';

													switch ( field.type ) {
														case 'colorSelector':
															return field.options.map(
																(
																	colorField
																) => {
																	return (
																		checkDepends(
																			colorField.depends
																		) && (
																			<ColorPanelDropdown
																				label={
																					colorField.label
																				}
																				value={ getFieldStyle(
																					_section,
																					_sectionChild,
																					settingsData[
																						loginOrSignupKey
																					],
																					settingsData[
																						stateKey
																					],
																					colorField.value
																				) }
																				onChange={ (
																					val
																				) => {
																					updateFieldStyle(
																						_section,
																						_sectionChild,
																						settingsData[
																							loginOrSignupKey
																						],
																						settingsData[
																							stateKey
																						],
																						colorField.value,
																						val
																					);
																				} }
																			/>
																		)
																	);
																}
															);
														case 'toggleSelectorMain':
															return (
																<ToggleGroupControl
																	options={
																		field.options
																	}
																	className={
																		'wsx-control-tab'
																	}
																	value={
																		defVal
																	}
																	onChange={ (
																		val
																	) => {
																		setSettingsData(
																			{
																				...settingsData,
																				[ key ]:
																					val,
																			}
																		);
																	} }
																/>
															);
														case 'toggleSelectorState':
															return (
																<ToggleGroupControl
																	title={
																		field.title
																	}
																	className={
																		'wsx-control-state wsx-d-flex wsx-justify-space wsx-gap-10'
																	}
																	options={
																		field.options
																	}
																	value={
																		defVal
																	}
																	onChange={ (
																		val
																	) => {
																		setSettingsData(
																			{
																				...settingsData,
																				[ key ]:
																					val,
																			}
																		);
																	} }
																/>
															);
														case 'toggleSelectorAlignment':
															return (
																<ToggleGroupControl
																	title={
																		field.title
																	}
																	className={
																		'wsx-control-alignment wsx-d-flex wsx-justify-space wsx-gap-10'
																	}
																	options={
																		field.options
																	}
																	value={ getFieldStyle(
																		_section,
																		_sectionChild,
																		settingsData[
																			loginOrSignupKey
																		],
																		settingsData[
																			stateKey
																		],
																		_child
																	) }
																	onChange={ (
																		val
																	) => {
																		updateFieldStyle(
																			_section,
																			_sectionChild,
																			settingsData[
																				loginOrSignupKey
																			],
																			settingsData[
																				stateKey
																			],
																			_child,
																			val
																		);
																	} }
																/>
															);
														case 'numberWIthIncrementDecrement':
															return (
																checkDepends(
																	field.depends
																) && (
																	<InputWithIncrementDecrement
																		label={
																			field.label
																		}
																		className={
																			'wsx-control-increment'
																		}
																		value={ getFieldStyle(
																			_section,
																			_sectionChild,
																			settingsData[
																				loginOrSignupKey
																			],
																			settingsData[
																				stateKey
																			],
																			_child
																		) }
																		onChange={ (
																			val
																		) => {
																			updateFieldStyle(
																				_section,
																				_sectionChild,
																				settingsData[
																					loginOrSignupKey
																				],
																				settingsData[
																					stateKey
																				],
																				_child,
																				val
																			);
																		} }
																		labelMeta={
																			field.meta
																		}
																	/>
																)
															);
														case 'select':
															return (
																<div className="wholesalex-select-field wholesalex-style-formatting-field">
																	<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
																		{
																			field.label
																		}
																	</div>
																	<select
																		value={ getFieldStyle(
																			_section,
																			_sectionChild,
																			settingsData[
																				loginOrSignupKey
																			],
																			settingsData[
																				stateKey
																			],
																			_child
																		) }
																		onChange={ (
																			e
																		) => {
																			updateFieldStyle(
																				_section,
																				_sectionChild,
																				settingsData[
																					loginOrSignupKey
																				],
																				settingsData[
																					stateKey
																				],
																				_child,
																				e
																					.target
																					.value
																			);
																		} }
																		className="wsx-select wholesalex-style-formatting-field-content"
																	>
																		{ Object.keys(
																			field.options
																		).map(
																			(
																				option
																			) => {
																				return (
																					<option
																						value={
																							option
																						}
																						key={
																							option
																						}
																					>
																						{
																							field
																								.options[
																								option
																							]
																						}
																					</option>
																				);
																			}
																		) }
																	</select>
																</div>
															);
														case 'transformSelect':
															return (
																<div
																	className={
																		'wholesalex-text-transform-control wholesalex-style-formatting-field'
																	}
																>
																	<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
																		{
																			field.label
																		}
																	</div>
																	<div className="wholesalex-text-transform-control__buttons wholesalex-style-formatting-field-content">
																		{ field.options.map(
																			(
																				textTransform
																			) => {
																				return (
																					<span
																						className={ `wholesalex-text-transform-control__button wsx-font-12-normal ${
																							textTransform.value ===
																							getFieldStyle(
																								_section,
																								_sectionChild,
																								settingsData[
																									loginOrSignupKey
																								],
																								settingsData[
																									stateKey
																								],
																								_child
																							)
																								? 'is-pressed'
																								: ''
																						}` }
																						key={
																							textTransform.value
																						}
																						onClick={ () => {
																							updateFieldStyle(
																								_section,
																								_sectionChild,
																								settingsData[
																									loginOrSignupKey
																								],
																								settingsData[
																									stateKey
																								],
																								_child,
																								textTransform.value
																							);
																						} }
																						onKeyDown={ (
																							e
																						) => {
																							if (
																								e.key ===
																									'Enter' ||
																								e.key ===
																									' '
																							) {
																								updateFieldStyle(
																									_section,
																									_sectionChild,
																									settingsData[
																										loginOrSignupKey
																									],
																									settingsData[
																										stateKey
																									],
																									_child,
																									textTransform.value
																								);
																							}
																						} }
																						role="button"
																						tabIndex="0"
																					>
																						{
																							textTransform.icon
																						}
																					</span>
																				);
																			}
																		) }
																	</div>
																</div>
															);

														default:
															break;
													}
												} ) }
											</Panel>
										)
									);
								}
							) }
					</Section>
				)
			);
		} );
	};

	const handleGoBack = () => {
		window.history.back();
	};

	return (
		<div className="wholesalex-form-builder">
			<div className="wholesalex-form-builder__form">
				<div className="wholesalex-form-builder-header wsx-d-flex wsx-justify-space wsx-gap-10">
					<div className="wholesalex-form-builder-header__left wsx-d-flex wsx-item-center wsx-gap-12">
						<Button
							iconName="arrowLeft_24"
							padding="8px"
							onClick={ handleGoBack }
							background="primary"
						/>
						<span className="wholesalex-form-builder-header__heading">
							{ __( 'Form Builder', 'wholesalex' ) }
						</span>
					</div>
					<div className="wholesalex-form-builder-header__right">
						<Button
							onClick={ onReset }
							label={ __( 'Reset', 'wholesalex' ) }
							iconName="reset"
							background="base2"
							iconPosition="after"
							borderColor="border-tertiary"
						/>
						<Button
							onClick={ onSave }
							label={ __( 'Save Changes', 'wholesalex' ) }
							background="primary"
						/>
						<Button
							onClick={ () => {
								setShortcodeModalStatus( true );
							} }
							label={ __( 'Get Shortcodes', 'wholesalex' ) }
							iconName="clipboard"
							background="base2"
							iconPosition="after"
							borderColor="border-tertiary"
						/>
					</div>
				</div>
				{ appState.loadingOnSave && <LoadingGif /> }
				{
					<div className="wsx-form-builder">
						<div className="wholesalex-form-builder-container">
							<div
								className="wholesalex-form-wrapper"
								style={ getFormStyle(
									registrationForm?.style,
									registrationForm?.loginFormHeader?.styles,
									registrationForm?.registrationFormHeader
										?.styles
								) }
							>
								{ registrationForm?.settings
									?.isShowLoginForm && (
									<>
										{ ' ' }
										<LoginForm />
										<span className="wsx-form-separator"></span>
									</>
								) }
								<RegistrationFormBuilder
									upgradeProPopupStatus={
										upgradeProPopupStatus
									}
									setUpgradeProPopupStatus={
										setUpgradeProPopupStatus
									}
								/>
							</div>
						</div>
					</div>
				}
			</div>
			<div
				className={ `wsx-sidebar-container ${
					enableSidebar ? '' : 'wsx-sidebar-hide'
				}` }
			>
				<div
					className={ `wholesalex-form-builder__typography-setting` }
				>
					<div
						className="wsx-form-control_hide wsx-rtl-arrow"
						onClick={ () => setSidebar( ! enableSidebar ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								setSidebar( ! enableSidebar );
							}
						} }
						role="button"
						tabIndex="0"
					>
						<div className="wsx-icon">{ Icons.angleRight }</div>
					</div>
					<div className="wholesalex-form-builder-typography-setting-header">
						<span className="wholesalex-form-builder__typography-setting-heading">
							{ __( 'Styling & Formatting', 'wholesalex' ) }
						</span>
					</div>
					<div className="wholesalex-form-builder-style-formatting-controller">
						<div className="wsx-pt-20 wsx-pb-12">
							<Button
								label={ __( 'Premade Design', 'wholesalex' ) }
								onClick={ () =>
									setPremadeDesignModalStatus(
										! premadeDesignModalStatus
									)
								}
								iconName="preMadeDesign"
								background="primary"
								customClass="wsx-w-auto"
							/>
						</div>
						<div className="wsx-d-flex wsx-flex-column wsx-gap-12">
							<Slider
								name={ 'isShowFormTitle' }
								key={ 'isShowFormTitle' }
								className="wsx-slider-md"
								label={ __( 'Form Title', 'wholesalex' ) }
								isLabelSide={ true }
								value={
									registrationForm.settings.isShowFormTitle
								}
								onChange={ ( e ) => {
									setRegistrationForm( {
										type: 'toogleFormTitle',
										value: e.target.checked,
									} );
								} }
							/>
							<Slider
								name={ 'isShowLoginForm' }
								key={ 'isShowLoginForm' }
								className="wsx-slider-md"
								label={ __( 'Show Login Form', 'wholesalex' ) }
								isLabelSide={ true }
								value={
									registrationForm.settings.isShowLoginForm
								}
								onChange={ ( e ) => {
									setRegistrationForm( {
										type: 'toogleShowLoginFormStatus',
										value: e.target.checked,
									} );
								} }
							/>
						</div>
						<ChooseInputStyle />
						{ getSettingFields() }
					</div>
				</div>
			</div>
			<Toast position={ 'top_right' } delay={ 5000 } />
			{ shortcodeModalStatus && (
				<ShortcodesModal
					setModalStatus={ setShortcodeModalStatus }
					roles={ wholesalex_overview.whx_form_builder_roles }
				/>
			) }
			{ premadeDesignModalStatus && (
				<PremadeDesignModal
					setModalStatus={ setPremadeDesignModalStatus }
				/>
			) }
			{ upgradeProPopupStatus && (
				<UpgradeProPopUp
					title={ __( 'Unlock all Features with', 'wholesalex' ) }
					onClose={ () => setUpgradeProPopupStatus( false ) }
				/>
			) }
		</div>
	);
}

export default Editor;
