import React, { useContext, useState } from 'react';
import { __ } from '@wordpress/i18n';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
import FormBuilderDropdown from './FormBuilderDropdown';
import Slider from '../../components/Slider';
import MultiSelect from '../../components/MultiSelect';
import Input from '../../components/Input';
import {
	getInputFieldVariation,
	getRegistrationFormFieldOptions,
} from './Utils';
import Tier from '../../components/Tier';
import PopupModal from '../../components/PopupModal';
import Tooltip from '../../components/Tooltip';

const ColumnField = ( {
	showDelete,
	column,
	parentIndex,
	colRef,
	columnIndex,
	handleColumnDragEnter,
	handleColumnDragStart,
	isRowActive,
	settingRef,
	setUpgradeProPopupStatus,
} ) => {
	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);
	const [ isDragable, makeDragable ] = useState( false );
	const [ fieldSettingActiveTab, setFieldSettingActiveTab ] =
		useState( 'setting' );
	const [ activeField, setActiveField ] = useState( -1 );
	const [ showVideo, setShowVideo ] = useState( false );

	const icons = {
		drag: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<path
					d="M4.14341 6.85839L2 9.0018L4.14341 11.1452M6.85839 4.14341L9.0018 2L11.1452 4.14341M11.1452 13.9316L9.0018 16.075L6.85839 13.9316M13.9316 6.85839L16.075 9.0018L13.9316 11.1452M2.78592 9.0018H15.2177M9.0018 2.71447V15.2891"
					stroke="#343A46"
					strokeWidth="1.3"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
			</svg>
		),
		delete: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<path
					d="M6.76247 2.42324C6.86385 2.03514 7.18817 1.76758 7.5572 1.76758H10.4429C10.812 1.76758 11.1363 2.03514 11.2377 2.42324L11.4855 3.37197C11.5869 3.76006 11.9112 4.02762 12.2802 4.02762H14.1659C14.5082 4.02762 14.7858 4.33118 14.7858 4.70564C14.7858 5.08009 14.5082 5.38365 14.1659 5.38365H3.83425C3.49189 5.38365 3.21436 5.08009 3.21436 4.70564C3.21436 4.33118 3.49189 4.02762 3.83425 4.02762H5.7199C6.08893 4.02762 6.41325 3.76006 6.51463 3.37197L6.76247 2.42324Z"
					fill="#343A46"
				/>
				<path
					d="M4.04089 6.28767H8.5868V13.0678C8.5868 13.3174 8.77182 13.5198 9.00007 13.5198C9.22832 13.5198 9.41333 13.3174 9.41333 13.0678V6.28767H13.9593L12.9144 14.6681C12.8027 15.564 12.1032 16.2319 11.2765 16.2319H6.72363C5.89697 16.2319 5.19742 15.564 5.08573 14.6681L4.04089 6.28767Z"
					fill="#343A46"
				/>
			</svg>
		),
		setting: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<g clipPath="url(#clip0_134_402)">
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M14.4645 11.9704C14.4204 11.7271 14.4502 11.4762 14.55 11.25C14.6451 11.0282 14.8029 10.839 15.0042 10.7057C15.2054 10.5725 15.4412 10.501 15.6825 10.5H15.75C16.1478 10.5 16.5294 10.342 16.8107 10.0607C17.092 9.77936 17.25 9.39782 17.25 9C17.25 8.60218 17.092 8.22064 16.8107 7.93934C16.5294 7.65804 16.1478 7.5 15.75 7.5H15.6225C15.3812 7.49904 15.1454 7.42753 14.9442 7.29427C14.7429 7.16101 14.5851 6.97183 14.49 6.75V6.69C14.3902 6.46379 14.3604 6.21285 14.4045 5.96956C14.4486 5.72626 14.5646 5.50176 14.7375 5.325L14.7825 5.28C14.922 5.14069 15.0326 4.97526 15.1081 4.79316C15.1836 4.61106 15.2224 4.41587 15.2224 4.21875C15.2224 4.02163 15.1836 3.82644 15.1081 3.64434C15.0326 3.46224 14.922 3.29681 14.7825 3.1575C14.6432 3.01804 14.4778 2.9074 14.2957 2.83191C14.1136 2.75642 13.9184 2.71757 13.7213 2.71757C13.5241 2.71757 13.3289 2.75642 13.1468 2.83191C12.9647 2.9074 12.7993 3.01804 12.66 3.1575L12.615 3.2025C12.4382 3.3754 12.2137 3.49139 11.9704 3.5355C11.7271 3.57962 11.4762 3.54984 11.25 3.45C11.0282 3.35493 10.839 3.19707 10.7057 2.99585C10.5725 2.79463 10.501 2.55884 10.5 2.3175V2.25C10.5 1.85218 10.342 1.47064 10.0607 1.18934C9.77936 0.908035 9.39782 0.75 9 0.75C8.60218 0.75 8.22064 0.908035 7.93934 1.18934C7.65804 1.47064 7.5 1.85218 7.5 2.25V2.3775C7.49904 2.61884 7.42753 2.85463 7.29427 3.05585C7.16101 3.25707 6.97183 3.41493 6.75 3.51H6.69C6.46379 3.60984 6.21285 3.63962 5.96956 3.5955C5.72626 3.55139 5.50176 3.4354 5.325 3.2625L5.28 3.2175C5.14069 3.07804 4.97526 2.9674 4.79316 2.89191C4.61106 2.81642 4.41587 2.77757 4.21875 2.77757C4.02163 2.77757 3.82644 2.81642 3.64434 2.89191C3.46224 2.9674 3.29681 3.07804 3.1575 3.2175C3.01804 3.35681 2.9074 3.52224 2.83191 3.70434C2.75642 3.88644 2.71757 4.08163 2.71757 4.27875C2.71757 4.47587 2.75642 4.67106 2.83191 4.85316C2.9074 5.03526 3.01804 5.20069 3.1575 5.34L3.2025 5.385C3.3754 5.56176 3.49139 5.78626 3.5355 6.02956C3.57962 6.27285 3.54984 6.52379 3.45 6.75C3.36429 6.98305 3.21045 7.18493 3.00847 7.32938C2.8065 7.47384 2.56575 7.55419 2.3175 7.56H2.25C1.85218 7.56 1.47064 7.71804 1.18934 7.99934C0.908035 8.28064 0.75 8.66218 0.75 9.06C0.75 9.45782 0.908035 9.83936 1.18934 10.1207C1.47064 10.402 1.85218 10.56 2.25 10.56H2.3775C2.61884 10.561 2.85463 10.6325 3.05585 10.7657C3.25707 10.899 3.41493 11.0882 3.51 11.31C3.60984 11.5362 3.63962 11.7871 3.5955 12.0304C3.55139 12.2737 3.4354 12.4982 3.2625 12.675L3.2175 12.72C3.07804 12.8593 2.9674 13.0247 2.89191 13.2068C2.81642 13.3889 2.77757 13.5841 2.77757 13.7812C2.77757 13.9784 2.81642 14.1736 2.89191 14.3557C2.9674 14.5378 3.07804 14.7032 3.2175 14.8425C3.35681 14.982 3.52224 15.0926 3.70434 15.1681C3.88644 15.2436 4.08163 15.2824 4.27875 15.2824C4.47587 15.2824 4.67106 15.2436 4.85316 15.1681C5.03526 15.0926 5.20069 14.982 5.34 14.8425L5.385 14.7975C5.56176 14.6246 5.78626 14.5086 6.02956 14.4645C6.27285 14.4204 6.52379 14.4502 6.75 14.55C6.98305 14.6357 7.18493 14.7896 7.32938 14.9915C7.47384 15.1935 7.55419 15.4343 7.56 15.6825V15.75C7.56 16.1478 7.71804 16.5294 7.99934 16.8107C8.28064 17.092 8.66218 17.25 9.06 17.25C9.45782 17.25 9.83936 17.092 10.1207 16.8107C10.402 16.5294 10.56 16.1478 10.56 15.75V15.6225C10.561 15.3812 10.6325 15.1454 10.7657 14.9442C10.899 14.7429 11.0882 14.5851 11.31 14.49C11.5362 14.3902 11.7871 14.3604 12.0304 14.4045C12.2737 14.4486 12.4982 14.5646 12.675 14.7375L12.72 14.7825C12.8593 14.922 13.0247 15.0326 13.2068 15.1081C13.3889 15.1836 13.5841 15.2224 13.7812 15.2224C13.9784 15.2224 14.1736 15.1836 14.3557 15.1081C14.5378 15.0326 14.7032 14.922 14.8425 14.7825C14.982 14.6432 15.0926 14.4778 15.1681 14.2957C15.2436 14.1136 15.2824 13.9184 15.2824 13.7213C15.2824 13.5241 15.2436 13.3289 15.1681 13.1468C15.0926 12.9647 14.982 12.7993 14.8425 12.66L14.7975 12.615C14.6246 12.4382 14.5086 12.2137 14.4645 11.9704ZM11.25 9C11.25 10.2426 10.2426 11.25 9 11.25C7.75736 11.25 6.75 10.2426 6.75 9C6.75 7.75736 7.75736 6.75 9 6.75C10.2426 6.75 11.25 7.75736 11.25 9Z"
						fill="#343A46"
					/>
				</g>
				<defs>
					<clipPath id="clip0_134_402">
						<rect width="18" height="18" fill="white" />
					</clipPath>
				</defs>
			</svg>
		),
	};

	const optionsGenerator = ( options ) => {
		const optionsData = [];
		const optionsString = options.split( '\n' );
		optionsString.forEach( ( option ) => {
			let [ value, label ] = option.split( '|' );
			if ( ! ( label === '' || label === undefined ) ) {
				value = value.trim();
				label = label.trim();
				optionsData.push( { name: label, value } );
			}
		} );
		return optionsData;
	};

	const optionStringGenerator = ( option ) => {
		let optionString = '';
		for ( let i = 0; i < option.length; i++ ) {
			optionString += option[ i ].value + '|' + option[ i ].name + '\n';
		}
		return optionString;
	};

	const fieldSetting = ( setStatus ) => {
		const conditionFields = {
			field: {
				type: 'select',
				label: __( 'Field', 'wholesalex' ),
				options: {
					...{ '': __( 'Select Field', 'wholesalex' ) },
					...getRegistrationFormFieldOptions(
						registrationForm,
						column.name
					),
				},
				default: '',
			},
			condition: {
				type: 'select',
				label: __( 'Condition', 'wholesalex' ),
				options: {
					'': __( 'Select Condition', 'wholesalex' ),
					is: __( 'Equal', 'wholesalex' ),
					not_is: __( 'Not Equal', 'wholesalex' ),
				},
				default: '',
			},
			value: {
				type: 'text',
				label: __( 'Value', 'wholesalex' ),
				placeholder: __( 'Value', 'wholesalex' ),
				default: '',
			},
		};

		const fieldNameHelpMessage = () => {
			if (
				Object.prototype.hasOwnProperty.call(
					column,
					'custom_field'
				) &&
				column.custom_field &&
				! (
					Object.prototype.hasOwnProperty.call(
						column,
						'migratedFromOldBuilder'
					) && column.migratedFromOldBuilder
				)
			) {
				return __(
					"Note: Make sure field name does not contain any space or special character. 'wholesalex_cf' prefix will be added to the field name.",
					'wholesalex'
				);
			}
			return '';
		};
		return (
			<div
				className="wholesalex-form-builder-field-poup wsx-setting-popup_control"
				ref={ settingRef }
			>
				<div className="wholesalex-form-builder-popup">
					<div className="wholesalex-popup-heading wsx-font-18-lightBold">
						<div>{ __( 'Edit Field', 'wholesalex' ) }</div>
						<div
							className="wsx-popup-close"
							onClick={ () => setStatus( false ) }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setStatus( false );
								}
							} }
							role="button"
							tabIndex="0"
						>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								viewBox="0 0 20 20"
								fill="none"
							>
								<path
									d="M15 5L5 15"
									stroke="#343A46"
									strokeWidth="1.5"
									strokeLinecap="round"
									strokeLinejoin="round"
								/>
								<path
									d="M5 5L15 15"
									stroke="#343A46"
									strokeWidth="1.5"
									strokeLinecap="round"
									strokeLinejoin="round"
								/>
							</svg>
						</div>
					</div>
					<div className="wholesalex-popup-tab-heading">
						<div
							className={ `whx-tab-heading ${
								fieldSettingActiveTab === 'setting'
									? 'whx-active-tab'
									: ''
							}` }
							onClick={ () =>
								setFieldSettingActiveTab( 'setting' )
							}
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setFieldSettingActiveTab( 'setting' );
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ __( 'Field Settings', 'wholesalex' ) }
							{ /* <div className="wsx-form-tutorial" onClick={()=> setShowVideo(true)}> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7 12.25C7.68944 12.25 8.37213 12.1142 9.00909 11.8504C9.64605 11.5865 10.2248 11.1998 10.7123 10.7123C11.1998 10.2248 11.5865 9.64605 11.8504 9.00909C12.1142 8.37213 12.25 7.68944 12.25 7C12.25 6.31056 12.1142 5.62787 11.8504 4.99091C11.5865 4.35395 11.1998 3.7752 10.7123 3.28769C10.2248 2.80018 9.64605 2.41347 9.00909 2.14963C8.37213 1.8858 7.68944 1.75 7 1.75C5.60761 1.75 4.27226 2.30312 3.28769 3.28769C2.30312 4.27226 1.75 5.60761 1.75 7C1.75 8.39239 2.30312 9.72774 3.28769 10.7123C4.27226 11.6969 5.60761 12.25 7 12.25ZM6.29008 4.66083L9.58242 6.49017C9.67331 6.5407 9.74904 6.61462 9.80177 6.70426C9.85449 6.7939 9.88229 6.896 9.88229 7C9.88229 7.104 9.85449 7.2061 9.80177 7.29574C9.74904 7.38538 9.67331 7.4593 9.58242 7.50983L6.29008 9.33917C6.18348 9.39842 6.06324 9.42879 5.94129 9.42728C5.81933 9.42576 5.69989 9.3924 5.59479 9.33051C5.48969 9.26862 5.40259 9.18034 5.34211 9.07443C5.28164 8.96851 5.24988 8.84863 5.25 8.72667V5.27333C5.24988 5.15137 5.28164 5.03149 5.34211 4.92557C5.40259 4.81966 5.48969 4.73138 5.59479 4.66949C5.69989 4.6076 5.81933 4.57424 5.94129 4.57272C6.06324 4.57121 6.18348 4.60158 6.29008 4.66083Z" fill="#6C6E77"/>
                                    </svg>
                                    Video
                                </div> */ }
						</div>
						<div
							className={ `whx-tab-heading wsx-field-condition-tab ${
								fieldSettingActiveTab === 'condition'
									? 'whx-active-tab'
									: ''
							}` }
							onClick={ () => {
								if ( wholesalex.is_pro_active ) {
									setFieldSettingActiveTab( 'condition' );
								} else {
									setUpgradeProPopupStatus( true );
								}
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									if ( wholesalex.is_pro_active ) {
										setFieldSettingActiveTab( 'condition' );
									} else {
										setUpgradeProPopupStatus( true );
									}
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ __( 'Field Condition', 'wholesalex' ) }
							<div
								className="wsx-form-tutorial"
								onClick={ () => setShowVideo( true ) }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										setShowVideo( true );
									}
								} }
								role="button"
								tabIndex="0"
							>
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="14"
									height="14"
									viewBox="0 0 14 14"
									fill="none"
								>
									<path
										fillRule="evenodd"
										clipRule="evenodd"
										d="M7 12.25C7.68944 12.25 8.37213 12.1142 9.00909 11.8504C9.64605 11.5865 10.2248 11.1998 10.7123 10.7123C11.1998 10.2248 11.5865 9.64605 11.8504 9.00909C12.1142 8.37213 12.25 7.68944 12.25 7C12.25 6.31056 12.1142 5.62787 11.8504 4.99091C11.5865 4.35395 11.1998 3.7752 10.7123 3.28769C10.2248 2.80018 9.64605 2.41347 9.00909 2.14963C8.37213 1.8858 7.68944 1.75 7 1.75C5.60761 1.75 4.27226 2.30312 3.28769 3.28769C2.30312 4.27226 1.75 5.60761 1.75 7C1.75 8.39239 2.30312 9.72774 3.28769 10.7123C4.27226 11.6969 5.60761 12.25 7 12.25ZM6.29008 4.66083L9.58242 6.49017C9.67331 6.5407 9.74904 6.61462 9.80177 6.70426C9.85449 6.7939 9.88229 6.896 9.88229 7C9.88229 7.104 9.85449 7.2061 9.80177 7.29574C9.74904 7.38538 9.67331 7.4593 9.58242 7.50983L6.29008 9.33917C6.18348 9.39842 6.06324 9.42879 5.94129 9.42728C5.81933 9.42576 5.69989 9.3924 5.59479 9.33051C5.48969 9.26862 5.40259 9.18034 5.34211 9.07443C5.28164 8.96851 5.24988 8.84863 5.25 8.72667V5.27333C5.24988 5.15137 5.28164 5.03149 5.34211 4.92557C5.40259 4.81966 5.48969 4.73138 5.59479 4.66949C5.69989 4.6076 5.81933 4.57424 5.94129 4.57272C6.06324 4.57121 6.18348 4.60158 6.29008 4.66083Z"
										fill="#6C6E77"
									/>
								</svg>
								{ __( 'Video', 'wholesalex' ) }
							</div>
							{ ! wholesalex.is_pro_active && (
								<span
									className="dashicons dashicons-lock"
									onClick={ () => {
										setUpgradeProPopupStatus( true );
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											setUpgradeProPopupStatus( true );
										}
									} }
									role="button"
									tabIndex="0"
								></span>
							) }
						</div>
					</div>
					{ fieldSettingActiveTab === 'setting' && (
						<div className="wholesalex-popup-content wsx-form-setting wsx-header-popup_control">
							<div className="wsx-setting-wrap wsx-toggle-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
								<Slider
									name={ 'status' }
									key={ 'status' }
									className="wsx-toggle-setting"
									label={ __( 'Field Status', 'wholesalex' ) }
									value={ column.status }
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'status',
											value: e.target.checked,
										} );
									} }
								/>
								<Slider
									name={ 'isRequired' }
									key={ 'isRequired' }
									className="wsx-toggle-setting wholesalex-field-setting-field-required"
									label={ __( 'Required', 'wholesalex' ) }
									value={ column.required }
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'required',
											value: e.target.checked,
										} );
									} }
									disabled={
										column.name === 'user_confirm_pass' ||
										column.name === 'user_confirm_email' ||
										column.name === 'password' ||
										column.name === 'user_email'
									}
								/>
								<Slider
									name={ 'isLabelHide' }
									key={ 'isLabelHide' }
									className="wsx-toggle-setting wholesalex-field-setting-field-hide-label"
									label={ __( 'Hide Label', 'wholesalex' ) }
									value={ column.isLabelHide }
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'isLabelHide',
											value: e.target.checked,
										} );
									} }
								/>
							</div>

							{ column.name !==
								'wholesalex_registration_role' && (
								<div className="wsx-setting-wrap wsx-select-wrap">
									<label
										className="wsx-label wsx-label wsx-select-field-label wsx-setting-label wsx-font-12-normal"
										htmlFor="excludeRoles"
									>
										{ ' ' }
										{ __(
											'Exclude Role Items',
											'wholesalex'
										) }
									</label>
									<MultiSelect
										options={
											wholesalex_overview.whx_form_builder_roles
										}
										value={ column.excludeRoles || [] }
										name="excludeRoles"
										onMultiSelectChangeHandler={ (
											fieldName,
											selectedValues
										) => {
											setRegistrationForm( {
												type: 'updateField',
												index: parentIndex,
												columnIndex,
												property: 'excludeRoles',
												value: selectedValues,
											} );
										} }
										placeholder={ __(
											'Choose Roles…',
											'wholesalex'
										) }
										// isDisable={!_isProActivate}
									/>
								</div>
							) }

							<div className="wsx-setting-wrap wsx-input-wrap">
								<Input
									key={ 'label' }
									id="label"
									className="wholesalex_form_builder__settings_field wsx-input-setting"
									label={ __( 'Field Label', 'wholesalex' ) }
									type="text"
									name={ 'label' }
									value={ column.label }
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'label',
											value: e.target.value,
										} );
									} }
									placeholder={ __( 'Label', 'wholesalex' ) }
								/>
							</div>
							{ column.name === 'user_pass' && (
								<>
									<div className="wsx-setting-wrap wsx-select-wrap">
										<label
											className="wsx-label wsx-select-field-label wsx-setting-label wsx-font-12-normal"
											htmlFor="passwordStrength"
										>
											{ ' ' }
											{ __(
												'Password Strength Condition',
												'wholesalex'
											) }
										</label>
										<MultiSelect
											options={
												wholesalex_overview.whx_form_builder_password_condition_options
											}
											value={
												column.passwordStrength || []
											}
											name="password_strength"
											onMultiSelectChangeHandler={ (
												fieldName,
												selectedValues
											) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'passwordStrength',
													value: selectedValues,
												} );
											} }
											placeholder={ __(
												'Choose Password Strength…',
												'wholesalex'
											) }
											// isDisable={!_isProActivate}
										/>
									</div>

									<div className="wsx-setting-wrap wsx-input-wrap wsx-select-wrap wholesalex-field-setting-row">
										<label
											className="wsx-label wsx-select-field-label wsx-setting-label wsx-font-12-normal"
											htmlFor="passwordStrength"
										>
											{ ' ' }
											{ __(
												'Strength Message',
												'wholesalex'
											) }
										</label>
										<textarea
											rows={ 3 }
											id="passwordStrengthMessage"
											className="wsx-textarea wholesalex_form_builder__settings_field wsx-input-setting"
											name="password_strength_message"
											defaultValue={
												column?.password_strength_message
											}
											onChange={ ( e ) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'password_strength_message',
													value: e.target.value,
												} );
											} }
										/>
									</div>
								</>
							) }
							<div className="wsx-setting-wrap wsx-input-wrap">
								<Input
									key={ 'name' }
									id="name"
									className="wholesalex_form_builder__settings_field wsx-input-setting"
									label={ __( 'Field Name', 'wholesalex' ) }
									type="text"
									name={ 'name' }
									value={ column.name }
									onChange={ ( e ) => {
										let newValue = e.target.value;
										newValue = newValue.replace(
											/\s/g,
											'_'
										);
										newValue = newValue.replace(
											/[^\w\s]/g,
											'_'
										);
										if ( /^\d/.test( newValue ) ) {
											newValue = newValue.substring( 1 );
										}
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'name',
											value: newValue,
										} );
									} }
									placeholder={ __(
										'Field Name',
										'wholesalex'
									) }
									disabled={
										column.name === 'user_login' ||
										column.name === 'user_email' ||
										column.name === 'user_pass' ||
										! (
											column.hasOwnProperty(
												'custom_field'
											) && column.custom_field
										) ||
										column.type === 'termCondition'
									}
									help={ fieldNameHelpMessage() }
								/>
							</div>
							{ ( column.type === 'select' ||
								column.type === 'radio' ||
								column.type === 'checkbox' ) && (
								<div className="wholesalex-field-setting-row wsx-setting-wrap">
									<div className="wholesalex_form_builder__settings_field">
										<label
											className="wsx-label wholesalex_form_builder__settings_field_label wsx-setting-label wsx-font-12-normal"
											htmlFor="form-builder"
										>
											{ __( 'Options', 'wholesalex' ) }
										</label>
										<textarea
											rows={ 3 }
											name="option"
											defaultValue={ optionStringGenerator(
												column.option
											) }
											onChange={ ( e ) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property: 'option',
													value: optionsGenerator(
														e.target.value
													),
												} );
											} }
											readOnly={
												column.name ===
												'wholesalex_registration_role'
											}
											disabled={
												column.name ===
												'wholesalex_registration_role'
											}
											className="wsx-textarea"
										/>
										{ column.name ===
											'wholesalex_registration_role' && (
											<span className="wholesalex-warning">
												{ __(
													'You cannot edit Role selection options',
													'wholesalex'
												) }
											</span>
										) }
									</div>
								</div>
							) }
							{ ( column.type === 'text' ||
								column.type === 'number' ||
								column.type === 'textarea' ||
								column.type === 'email' ||
								column.type === 'password' ) && (
								<div className="wsx-setting-wrap wsx-input-wrap wholesalex-field-setting-row">
									<Input
										key={ 'placeholder' }
										id="placeholder"
										className="wholesalex_form_builder__settings_field wsx-input-setting"
										label={ __(
											'Placeholder',
											'wholesalex'
										) }
										type="text"
										name={ 'placeholder' }
										value={ column.placeholder }
										onChange={ ( e ) => {
											setRegistrationForm( {
												type: 'updateField',
												index: parentIndex,
												columnIndex,
												property: 'placeholder',
												value: e.target.value,
											} );
										} }
										placeholder={ __(
											'Placeholder',
											'wholesalex'
										) }
									/>
								</div>
							) }
							{ column.type === 'termCondition' && (
								<div className="wsx-setting-wrap wsx-input-wrap wholesalex-field-setting-row">
									<div className="wsx-setting-wrap wsx-input-wrap">
										<Input
											key={ 'termConditionLink' }
											id="termConditionLink"
											className="wholesalex_form_builder__settings_field wsx-input-setting"
											label={ __(
												'Terms/Policy Link',
												'wholesalex'
											) }
											type="text"
											name={ 'termConditionLink' }
											value={ column?.term_link }
											onChange={ ( e ) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property: 'term_link',
													value: e.target.value,
												} );
											} }
											placeholder={ __(
												'Placeholder (Terms/Policy Link)',
												'wholesalex'
											) }
											help={ __(
												'Note: The provided link will be embedded within the {smart tag}.',
												'wholesalex'
											) }
										/>
									</div>
									<div className="wsx-setting-wrap wsx-input-wrap">
										<Input
											key={ 'termConditionLabel' }
											id="termConditionLabel"
											className="wholesalex_form_builder__settings_field wsx-input-setting"
											label={ __(
												'Terms and Conditions',
												'wholesalex'
											) }
											type="text"
											name={ 'option' }
											value={ column.default_text }
											onChange={ ( e ) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property: 'default_text',
													value: e.target.value,
												} );
											} }
											placeholder={ __(
												'I agree to the Terms and Conditions {Privacy Policy}',
												'wholesalex'
											) }
											help={ __(
												'Note: Terms/Policy Link will be embedded within the smart tag (for example {Privacy Policy}) . Smart tag name (text) can be changed within the brackets.',
												'wholesalex'
											) }
										/>
									</div>
								</div>
							) }
							<div className="wsx-setting-wrap wsx-input-wrap wholesalex-field-setting-row">
								<Input
									key={ 'helpMessage' }
									id="helpMessage"
									className="wholesalex_form_builder__settings_field wsx-input-setting"
									label={ __( 'Help Message', 'wholesalex' ) }
									type="text"
									name={ 'help_message' }
									value={ column?.help_message }
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateField',
											index: parentIndex,
											columnIndex,
											property: 'help_message',
											value: e.target.value,
										} );
									} }
									placeholder={ __(
										'Help Message',
										'wholesalex'
									) }
								/>
							</div>

							{ column.type === 'file' && (
								<>
									<div className="wsx-setting-wrap wsx-select-wrap">
										<label
											className="wsx-label wsx-select-field-label wsx-setting-label wsx-font-12-normal"
											htmlFor="allowed_file_types"
										>
											{ ' ' }
											{ __(
												'Allowed File Types (Comma Separated)',
												'wholesalex'
											) }
										</label>
										<MultiSelect
											options={
												wholesalex_overview.whx_form_builder_file_condition_options
											}
											value={
												column.allowed_file_types || []
											}
											name={ 'allowed_file_types' }
											onMultiSelectChangeHandler={ (
												fieldName,
												selectedValues
											) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'allowed_file_types',
													value: selectedValues,
												} );
											} }
											placeholder={ __(
												'Choose File Type…',
												'wholesalex'
											) }
											// isDisable={!_isProActivate}
										/>
									</div>

									<div className="wsx-setting-wrap wsx-input-wrap">
										<Input
											key={ `maximum_allowed_file_size` }
											id="maximum_allowed_file_size"
											className="wholesalex_form_builder__settings_field wsx-input-setting"
											label={ __(
												'Maximum Allowed File Size in Bytes',
												'wholesalex'
											) }
											type="text"
											name={ 'maximum_file_size' }
											value={ column?.maximum_file_size }
											onChange={ ( e ) => {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'maximum_file_size',
													value: e.target.value,
												} );
											} }
										/>
									</div>
								</>
							) }
						</div>
					) }
					{ fieldSettingActiveTab === 'condition' && (
						<div className="wholesalex-popup-content wholesalex-field-conditions wsx-form-setting">
							<div>
								<div className="wsx-field-condition-desc wsx-setting-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
									<div className="wsx-field-condition-wrap">
										<span
											className={ `wsx-field-condition-btn ${
												column?.conditions?.status ===
												'show'
													? 'wsx-is-active'
													: ''
											}` }
											onClick={ () => {
												const _temp = { ...column };
												if (
													_temp.conditions ==
													undefined
												) {
													_temp.conditions = {
														tiers: [],
													};
												}
												_temp.conditions.status =
													'show';
												setRegistrationForm( {
													type: 'updateFieldCondition',
													parentIndex,
													columnIndex,
													value: _temp,
												} );
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													const _temp = { ...column };
													if (
														_temp.conditions ===
														undefined
													) {
														_temp.conditions = {
															tiers: [],
														};
													}
													_temp.conditions.status =
														'show';
													setRegistrationForm( {
														type: 'updateFieldCondition',
														parentIndex,
														columnIndex,
														value: _temp,
													} );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ __( 'Show', 'wholesalex' ) }
										</span>{ ' ' }
										<span
											className={ `wsx-field-condition-btn ${
												column?.conditions?.status ===
												'hide'
													? 'wsx-is-active'
													: ''
											}` }
											onClick={ () => {
												const _temp = { ...column };
												if (
													_temp.conditions ===
													undefined
												) {
													_temp.conditions = {
														tiers: [],
													};
												}
												_temp.conditions.status =
													'hide';
												setRegistrationForm( {
													type: 'updateFieldCondition',
													parentIndex,
													columnIndex,
													value: _temp,
												} );
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													const _temp = { ...column };
													if (
														_temp.conditions ===
														undefined
													) {
														_temp.conditions = {
															tiers: [],
														};
													}
													_temp.conditions.status =
														'hide';
													setRegistrationForm( {
														type: 'updateFieldCondition',
														parentIndex,
														columnIndex,
														value: _temp,
													} );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ __( 'Hide', 'wholesalex' ) }{ ' ' }
										</span>
									</div>
									<div className="">
										{ __( 'Visibility', 'wholesalex' ) }

										<Tooltip
											content={ __(
												'Select whether you want to display or hide this field when the conditions match.',
												'wholesalex'
											) }
										>
											<span className="dashicons dashicons-editor-help wholesalex_tooltip_icon" />
										</Tooltip>
									</div>
								</div>
								<div className="wsx-field-condition-desc wsx-setting-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
									<div className="wsx-field-condition-wrap">
										<span
											className={ `wsx-field-condition-btn ${
												column?.conditions?.relation ===
												'all'
													? 'wsx-is-active'
													: ''
											}` }
											onClick={ () => {
												const _temp = { ...column };
												if (
													_temp.conditions ===
													undefined
												) {
													_temp.conditions = {
														tiers: [],
													};
												}
												_temp.conditions.relation =
													'all';
												setRegistrationForm( {
													type: 'updateFieldCondition',
													parentIndex,
													columnIndex,
													value: _temp,
												} );
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													const _temp = { ...column };
													if (
														_temp.conditions ===
														undefined
													) {
														_temp.conditions = {
															tiers: [],
														};
													}
													_temp.conditions.relation =
														'all';
													setRegistrationForm( {
														type: 'updateFieldCondition',
														parentIndex,
														columnIndex,
														value: _temp,
													} );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ __( 'All', 'wholesalex' ) }
										</span>{ ' ' }
										<span
											className={ `wsx-field-condition-btn ${
												column?.conditions?.relation ===
												'any'
													? 'wsx-is-active'
													: ''
											}` }
											onClick={ () => {
												const _temp = { ...column };
												if (
													_temp.conditions ===
													undefined
												) {
													_temp.conditions = {
														tiers: [],
													};
												}
												_temp.conditions.relation =
													'any';
												setRegistrationForm( {
													type: 'updateFieldCondition',
													parentIndex,
													columnIndex,
													value: _temp,
												} );
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													const _temp = { ...column };
													if (
														_temp.conditions ===
														undefined
													) {
														_temp.conditions = {
															tiers: [],
														};
													}
													_temp.conditions.relation =
														'any';
													setRegistrationForm( {
														type: 'updateFieldCondition',
														parentIndex,
														columnIndex,
														value: _temp,
													} );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ __( 'Any', 'wholesalex' ) }
										</span>
									</div>

									<div className="">
										{ __( 'Operator', 'wholesalex' ) }
										<Tooltip
											content={ __(
												'When the rule will be triggered? Upon matching all conditions or any of the added conditions.',
												'wholesalex'
											) }
										>
											<span className="dashicons dashicons-editor-help wholesalex_tooltip_icon" />
										</Tooltip>
									</div>
								</div>{ ' ' }
							</div>
							<div className="wsx-field-conditions-tier">
								{ column.conditions &&
									column.conditions.tiers.length !== 0 &&
									column.conditions.tiers.map( ( t, i ) => {
										return (
											<Tier
												key={ i }
												fields={ conditionFields }
												tier={ column }
												setTier={ ( r ) => {
													setRegistrationForm( {
														type: 'updateFieldCondition',
														parentIndex,
														columnIndex,
														value: r,
													} );
												} }
												tierName={ 'conditions' }
												index={ i }
												src="registration_form"
											/>
										);
									} ) }
								{ ( column.conditions === undefined ||
									( column.conditions.tiers &&
										column.conditions.tiers.length ===
											0 ) ) && (
									<Tier
										fields={ conditionFields }
										tier={ column }
										setTier={ ( r ) => {
											setRegistrationForm( {
												type: 'updateFieldCondition',
												parentIndex,
												columnIndex,
												value: r,
											} );
										} }
										tierName={ 'conditions' }
										index={ 0 }
										src="registration_form"
									/>
								) }
							</div>
						</div>
					) }
				</div>
				{ ( ( Object.prototype.hasOwnProperty.call(
					column,
					'custom_field'
				) &&
					column.custom_field ) ||
					column.name === 'wholesalex_registration_role' ) && (
					<div className="wholesalex-form-builder-popup">
						<div className="wholesalex-popup-heading wsx-font-18-lightBold wsx-field-wc-options-popup">
							{ __( 'WooCommerce Option', 'wholesalex' ) }{ ' ' }
							{ ! wholesalex.is_pro_active && (
								<span
									className="dashicons dashicons-lock"
									onClick={ () => {
										setUpgradeProPopupStatus( true );
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											setUpgradeProPopupStatus( true );
										}
									} }
									role="button"
									tabIndex="0"
								></span>
							) }
						</div>
						<div className="wholesalex-popup-content wsx-form-setting">
							{ ! (
								column.name === 'user_email' ||
								column.name === 'user_pass'
							) && (
								<div className="wsx-setting-wrap wsx-toggle-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
									<Slider
										name={
											'isAddToWooCommerceRegistration'
										}
										key={ 'isAddToWooCommerceRegistration' }
										className="wsx-toggle-setting"
										label={ __(
											'Add WooCommerce Registration',
											'wholesalex'
										) }
										value={
											column.isAddToWooCommerceRegistration
										}
										onChange={ ( e ) => {
											if ( ! wholesalex.is_pro_active ) {
												setUpgradeProPopupStatus(
													true
												);
											} else {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'isAddToWooCommerceRegistration',
													value: e.target.checked,
												} );
											}
										} }
										// isDisable = {!wholesalex.is_pro_active}
										isLock={ ! wholesalex.is_pro_active }
									/>
								</div>
							) }
							{ ( column.billingMapping === '' ||
								column.billingMapping === undefined ) && (
								<div className="wsx-setting-wrap wsx-toggle-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
									<Slider
										name={ 'enableForBillingForm' }
										key={ 'enableForBillingForm' }
										className="wsx-toggle-setting"
										label={ __(
											'Add Custom Field to Billing',
											'wholesalex'
										) }
										value={ column?.enableForBillingForm }
										onChange={ ( e ) => {
											if ( ! wholesalex.is_pro_active ) {
												setUpgradeProPopupStatus(
													true
												);
											} else {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'enableForBillingForm',
													value: e.target.checked,
												} );
											}
										} }
										// isDisable = {!wholesalex.is_pro_active}
										isLock={ ! wholesalex.is_pro_active }
									/>
									<Slider
										name={ 'isRequiredInBilling' }
										key={ 'isRequiredInBilling' }
										className="wsx-toggle-setting"
										label={ __(
											'Required in Billing',
											'wholesalex'
										) }
										value={ column?.isRequiredInBilling }
										onChange={ ( e ) => {
											if ( ! wholesalex.is_pro_active ) {
												setUpgradeProPopupStatus(
													true
												);
											} else {
												setRegistrationForm( {
													type: 'updateField',
													index: parentIndex,
													columnIndex,
													property:
														'isRequiredInBilling',
													value: e.target.checked,
												} );
											}
										} }
										// isDisable = {!wholesalex.is_pro_active}
										isLock={ ! wholesalex.is_pro_active }
									/>
								</div>
							) }
						</div>
					</div>
				) }
			</div>
		);
	};

	const columnSetting = () => {
		return (
			<div className="wsx-reg-form-row-setting">
				{ showDelete && (
					<span
						className="move-column-icon"
						onMouseEnter={ () => {
							makeDragable( true );
							colRef.current = 'column_move';
						} }
						onMouseLeave={ () => {
							makeDragable( false );
							colRef.current = null;
						} }
					>
						{ icons.drag }
					</span>
				) }
				{ column.name !== 'user_login' &&
					column.name !== 'user_email' &&
					column.name !== 'user_pass' &&
					showDelete && (
						<span
							className="delete-column-icon"
							onClick={ () => {
								if ( column?.columnPosition === 'left' ) {
									setRegistrationForm( {
										type: 'deleteLeftColumn',
										field: column,
									} );
								} else if (
									column?.columnPosition === 'right'
								) {
									setRegistrationForm( {
										type: 'deleteRightColumn',
										field: column,
									} );
								}
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									if ( column?.columnPosition === 'left' ) {
										setRegistrationForm( {
											type: 'deleteLeftColumn',
											field: column,
										} );
									} else if (
										column?.columnPosition === 'right'
									) {
										setRegistrationForm( {
											type: 'deleteRightColumn',
											field: column,
										} );
									}
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ icons.delete }
						</span>
					) }
				<FormBuilderDropdown
					renderToogle={ ( status, setStatus ) => {
						return (
							<span
								className={ `field-setting-icon ${
									status ? 'wsx-active' : ''
								}` }
								onClick={ () => setStatus( ! status ) }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										setStatus( ! status );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ icons.setting }
							</span>
						);
					} }
					renderContent={ ( status, setStatus ) => {
						return fieldSetting( setStatus );
					} }
					className="wsx-setting-popup_control"
				/>
			</div>
		);
	};
	// isRowActive?columnSetting:false
	const renderField = ( field, columnIdx ) => {
		return getInputFieldVariation(
			registrationForm.settings.inputVariation,
			field,
			activeField === columnIdx && isRowActive ? columnSetting : false
		);
	};

	const videoModalContent = () => {
		return (
			<div className="wholesalex_popup_modal__addon_video_container">
				{
					<iframe
						className="wholesalex_popup_modal__addon_video "
						width="560"
						height="315"
						src={
							'https://www.youtube.com/embed/2O99u8Ipn2Y?si=aMXoms1JUuVSDMhs&amp;start=606&amp;autoplay=1'
						}
						frameBorder="0"
						allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
						title={
							'Create WooCommerce Wholesale Registration Form with Conditional Advance Custom Fields'
						}
						loading="lazy"
					/>
				}
			</div>
		);
	};

	return (
		<div
			className={ `wholesalex-registration-form-column ${
				column?.columnPosition
			} ${ ! column.status ? 'wsx-disable-field' : '' } ${
				activeField !== -1 ? 'wsx-field-active' : ''
			} ` }
			draggable={ isDragable }
			onDragStart={ ( e ) =>
				colRef.current === 'column_move' &&
				handleColumnDragStart( e, columnIndex )
			}
			onDragOver={ ( e ) => e.preventDefault() }
			onDragEnter={ ( e ) =>
				colRef.current === 'column_move' &&
				handleColumnDragEnter( e, columnIndex )
			}
			onDragEnd={ ( e ) => {
				e.preventDefault();
			} }
			onMouseEnter={ () =>
				! settingRef?.current && setActiveField( columnIndex )
			}
			onMouseLeave={ () => ! settingRef?.current && setActiveField( -1 ) }
		>
			{ renderField( column, columnIndex ) }
			{ showVideo && (
				<PopupModal
					renderContent={ videoModalContent }
					onClose={ () => setShowVideo( false ) }
					className="wsx-field-condition-popup"
				/>
			) }
		</div>
	);
};

export default ColumnField;
