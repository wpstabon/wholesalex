import React from 'react';

const InputWithIncrementDecrement = ( {
	label,
	className,
	value,
	onChange,
	labelMeta,
} ) => {
	const decrementSVG = (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="14"
			height="14"
			viewBox="0 0 14 14"
			fill="none"
		>
			<path
				d="M2.91699 7H11.0837"
				stroke="#6C6E77"
				strokeWidth="1.125"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
	const incrementSVG = (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="14"
			height="14"
			viewBox="0 0 14 14"
			fill="none"
		>
			<path
				d="M7 2.91666V11.0833"
				stroke="#6C6E77"
				strokeWidth="1.125"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M2.91699 7H11.0837"
				stroke="#6C6E77"
				strokeWidth="1.125"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);

	return (
		<div className={ `wholesalex-style-formatting-field ${ className }` }>
			<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
				{ label }
				<span className="wholesalex-style-formatting-field-label-meta wsx-font-12-normal">
					{ labelMeta ? '(' + labelMeta + ')' : '(unit: px)' }
				</span>
			</div>
			<div className={ `wholesalex-style-formatting-field-content` }>
				<span
					className="decrement"
					onClick={ () => {
						onChange( parseInt( value ) - 1 );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							onChange( parseInt( value ) - 1 );
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ decrementSVG }
				</span>
				<input
					className="wsx-input"
					type="number"
					value={ value }
					onChange={ ( e ) => {
						const val = e.target.value;
						onChange( parseInt( val ) );
					} }
				/>
				<span
					className="increment"
					onClick={ () => {
						onChange( parseInt( value ) + 1 );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							onChange( parseInt( value ) + 1 );
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ incrementSVG }
				</span>
			</div>
		</div>
	);
};

export default InputWithIncrementDecrement;
