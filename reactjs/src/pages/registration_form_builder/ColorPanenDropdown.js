import React, { createRef } from 'react';
import { ColorPicker, ColorIndicator, Dropdown } from '@wordpress/components';

function ColorPanelDropdown( {
	label,
	value,
	onChange,
	colorPickerRef,
	className,
	placement = 'left-start',
} ) {
	return (
		<Dropdown
			className={
				className ? className : 'wholesalex-color-picker-dropdown'
			}
			ref={ colorPickerRef ? colorPickerRef : createRef() }
			contentClassName="wsx-color-picker-content"
			popoverProps={ {
				placement,
				offset: 36,
				shift: true,
			} }
			renderToggle={ ( { onToggle, isOpen } ) => {
				return (
					<div
						className={ `wholesalex-form-builder-select-color wsx-d-flex wsx-justify-space wsx-gap-10 ${
							isOpen ? 'is-open' : ''
						}` }
						onClick={ onToggle }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								onToggle;
							}
						} }
						role="button"
						tabIndex="0"
					>
						<div className="select-color-details">
							<ColorIndicator
								colorValue={ value }
								className="wholesalex-form-builder__color"
							/>
							{ label && (
								<span className="select-color-label wsx-font-12-normal">
									{ label }
								</span>
							) }
						</div>
						<span className="selected-color-code wsx-font-12-normal">
							{ value }
						</span>
					</div>
				);
			} }
			renderContent={ () => {
				return (
					<div className="wholesalex-form-builder-select-color__dropdown">
						<ColorPicker onChange={ onChange } />
					</div>
				);
			} }
		/>
	);
}

export default ColorPanelDropdown;
