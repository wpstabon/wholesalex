import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';
import Tooltip from '../../components/Tooltip';
import PopupModal from '../../components/PopupModal';
import Icons from '../../utils/Icons';

const ShortcodesModal = ( { setModalStatus, roles } ) => {
	const [ loginShortcodeStatus, setLoginShortcodeStatus ] = useState( {} );
	const [ shortcodes, setShortcodes ] = useState( {} );

	const getShortcode = ( shortcode ) => {
		if (
			wholesalex_overview?.whx_form_builder_whitelabel_enabled &&
			wholesalex_overview?.whx_form_builder_slug
		) {
			shortcode = shortcode.replaceAll(
				'wholesalex_',
				`${ wholesalex_overview.whx_form_builder_slug }_`
			);
		}
		return shortcode;
	};

	const popupContent = () => (
		<>
			<div className="wsx-title wsx-font-20 wsx-pb-20">
				{ __( 'Specific Shortcode List', 'wholesalex' ) }
			</div>

			<div className="wsx-shortcode-container wsx-scrollbar">
				<div className="wsx-shortcode-item wsx-d-grid wsx-item-center">
					<div className="wsx-shortcode-for wsx-font-16 wsx-color-text-medium">
						{ __( 'Global', 'wholesalex' ) }
					</div>
					<div className="wsx-relative wsx-d-flex wsx-item-center wsx-gap-8 wsx-w-fit wsx-checkbox-option-wrapper">
						<input
							type="checkbox"
							id={ `short_code_for_login_global` }
							name={ `short_code_for_login_global` }
							value={ loginShortcodeStatus.global }
							onChange={ () => {
								const _status = ! loginShortcodeStatus.global;
								setLoginShortcodeStatus( {
									...loginShortcodeStatus,
									global: _status,
								} );
								let _shortcode = `[wholesalex_registration]`;
								if ( _status ) {
									_shortcode = `[wholesalex_login_registration]`;
								}
								_shortcode = getShortcode( _shortcode );
								setShortcodes( {
									...shortcodes,
									global: _shortcode,
								} );
							} }
						/>
						<div className="wsx-checkbox-mark"></div>
						<label
							htmlFor={ 'short_code_for_login_global' }
							className="wsx-label wsx-checkbox-option-label wsx-curser-pointer"
						>
							{ __( 'With Login Form', 'wholesalex' ) }
						</label>
					</div>

					<div className="wsx-d-flex wsx-item-center wsx-gap-12">
						<Tooltip
							content={ __(
								'Registration Form with All Roles.',
								'wholesalex'
							) }
						>
							{ Icons.help }
						</Tooltip>
						<div className="wsx-shortcode">
							<input
								className="wsx-input wsx-font-12"
								type="text"
								data-role="global"
								value={
									shortcodes.global
										? getShortcode( shortcodes.global )
										: getShortcode(
												`[wholesalex_registration]`
										  )
								}
								onFocus={ ( e ) => e.target.select() }
								onClick={ ( e ) => e.target.select() }
								readOnly
							/>
							<Tooltip
								content={ __(
									'Copy To Clipboard',
									'wholesalex'
								) }
							>
								<div
									className="wsx-icon"
									onClick={ () => {
										const _element = `[data-role="global"]`;
										let _shortcode =
											document.querySelector( _element );
										if ( navigator.clipboard ) {
											_shortcode = _shortcode.value;
											navigator.clipboard.writeText(
												_shortcode
											);
										} else {
											try {
												_shortcode.focus();
												document.execCommand( 'copy' );
											} catch ( err ) {
												// error
											}
										}
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											const _element = `[data-role="global"]`;
											let _shortcode =
												document.querySelector(
													_element
												);
											if ( navigator.clipboard ) {
												_shortcode = _shortcode.value;
												navigator.clipboard.writeText(
													_shortcode
												);
											} else {
												try {
													_shortcode.focus();
													document.execCommand(
														'copy'
													);
												} catch ( err ) {
													// error
												}
											}
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.copy }
								</div>
							</Tooltip>
						</div>
					</div>
				</div>

				<div className="wsx-shortcode-item wsx-d-grid wsx-item-center">
					<div className="wsx-shortcode-for wsx-font-16 wsx-color-text-medium">
						{ __( 'Login Form', 'wholesalex' ) }
					</div>
					<label
						htmlFor={ 'short_code_for_login' }
						className="wsx-label wsx-checkbox-option-label"
					>
						{ __( 'Global Login Form', 'wholesalex' ) }
					</label>

					<div className="wsx-d-flex wsx-item-center wsx-gap-12">
						<Tooltip
							content={ __(
								'Login Form with All Roles.',
								'wholesalex'
							) }
						>
							{ Icons.help }
						</Tooltip>
						<div className="wsx-shortcode">
							<input
								className="wsx-input wsx-font-12"
								type="text"
								data-role="global_login"
								value={
									shortcodes.global_login
										? getShortcode(
												shortcodes.global_login
										  )
										: getShortcode( `[wholesalex_login]` )
								}
								onFocus={ ( e ) => e.target.select() }
								onClick={ ( e ) => e.target.select() }
								readOnly
							/>
							<Tooltip
								content={ __(
									'Copy To Clipboard',
									'wholesalex'
								) }
							>
								<div
									className="wsx-icon"
									onClick={ () => {
										const _element = `[data-role="global_login"]`;
										let _shortcode =
											document.querySelector( _element );
										if ( navigator.clipboard ) {
											_shortcode = _shortcode.value;
											navigator.clipboard.writeText(
												_shortcode
											);
										} else {
											try {
												_shortcode.focus();
												document.execCommand( 'copy' );
											} catch ( err ) {
												// error
											}
										}
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											const _element = `[data-role="global_login"]`;
											let _shortcode =
												document.querySelector(
													_element
												);
											if ( navigator.clipboard ) {
												_shortcode = _shortcode.value;
												navigator.clipboard.writeText(
													_shortcode
												);
											} else {
												try {
													_shortcode.focus();
													document.execCommand(
														'copy'
													);
												} catch ( err ) {
													// error
												}
											}
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.copy }
								</div>
							</Tooltip>
						</div>
					</div>
				</div>

				<div className="wsx-shortcode-item wsx-d-grid wsx-item-center">
					<div className="wsx-shortcode-for wsx-font-16 wsx-color-text-medium">
						{ __( 'B2B Global Form', 'wholesalex' ) }
					</div>
					<div className="wsx-relative wsx-d-flex wsx-item-center wsx-gap-8 wsx-w-fit wsx-checkbox-option-wrapper">
						<input
							id={ `short_code_for_login_global_b2b` }
							name={ `short_code_for_login_global_b2b` }
							type="checkbox"
							value={ loginShortcodeStatus.global_b2b }
							onChange={ () => {
								const _status =
									! loginShortcodeStatus.global_b2b;
								setLoginShortcodeStatus( {
									...loginShortcodeStatus,
									global_b2b: _status,
								} );
								let _shortcode = `[wholesalex_registration registration_role="all_b2b"]`;
								if ( _status ) {
									_shortcode = `[wholesalex_login_registration registration_role="all_b2b"]`;
								}
								_shortcode = getShortcode( _shortcode );
								setShortcodes( {
									...shortcodes,
									global_b2b: _shortcode,
								} );
							} }
						/>
						<div className="wsx-checkbox-mark"></div>
						<label
							htmlFor={ 'short_code_for_login_global_b2b' }
							className="wsx-label wsx-checkbox-option-label wsx-curser-pointer"
						>
							{ __( 'With Login Form', 'wholesalex' ) }
						</label>
					</div>

					<div className="wsx-d-flex wsx-item-center wsx-gap-12">
						<Tooltip
							content={ __(
								'Registration Form with All Roles.',
								'wholesalex'
							) }
						>
							{ Icons.help }
						</Tooltip>
						<div className="wsx-shortcode">
							<input
								className="wsx-input wsx-font-12"
								type="text"
								data-role="global_b2b"
								value={
									shortcodes.global_b2b
										? getShortcode( shortcodes.global_b2b )
										: getShortcode(
												`[wholesalex_registration registration_role="all_b2b"]`
										  )
								}
								onFocus={ ( e ) => e.target.select() }
								onClick={ ( e ) => e.target.select() }
								readOnly
							/>
							<Tooltip
								content={ __(
									'Copy To Clipboard',
									'wholesalex'
								) }
							>
								<div
									className="wsx-icon"
									onClick={ () => {
										const _element = `[data-role="global_b2b"]`;
										let _shortcode =
											document.querySelector( _element );
										if ( navigator.clipboard ) {
											_shortcode = _shortcode.value;
											navigator.clipboard.writeText(
												_shortcode
											);
										} else {
											try {
												_shortcode.focus();
												document.execCommand( 'copy' );
											} catch ( err ) {
												// error
											}
										}
									} }
									onKeyDown={ () => {
										const _element = `[data-role="global_b2b"]`;
										let _shortcode =
											document.querySelector( _element );
										if ( navigator.clipboard ) {
											_shortcode = _shortcode.value;
											navigator.clipboard.writeText(
												_shortcode
											);
										} else {
											try {
												_shortcode.focus();
												document.execCommand( 'copy' );
											} catch ( err ) {
												// error
											}
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.copy }
								</div>
							</Tooltip>
						</div>
					</div>
				</div>

				{ roles.map( ( role, i ) => {
					return (
						role.value !== 'wholesalex_guest' &&
						role.value && (
							<div className="wsx-shortcode-item wsx-d-grid wsx-item-center">
								<div className="wsx-shortcode-for wsx-font-16 wsx-color-text-medium">
									{ role.name }
								</div>
								<div className="wsx-relative wsx-d-flex wsx-item-center wsx-gap-8 wsx-w-fit wsx-checkbox-option-wrapper">
									<input
										id={ `short_code_for_login_${ i }` }
										name={ `short_code_for_login_${ i }` }
										type="checkbox"
										value={
											loginShortcodeStatus[ role.value ]
										}
										onChange={ () => {
											const _status =
												! loginShortcodeStatus[
													role.value
												];
											setLoginShortcodeStatus( {
												...loginShortcodeStatus,
												[ role.value ]: _status,
											} );
											let _shortcode = `[wholesalex_registration registration_role="${ role.value }"]`;
											if ( _status ) {
												_shortcode = `[wholesalex_login_registration registration_role="${ role.value }"]`;
											}
											_shortcode =
												getShortcode( _shortcode );
											setShortcodes( {
												...shortcodes,
												[ role.value ]: _shortcode,
											} );
										} }
									/>
									<div className="wsx-checkbox-mark"></div>
									<label
										htmlFor={ `short_code_for_login_${ i }` }
										className="wsx-label wsx-checkbox-option-label wsx-curser-pointer"
									>
										{ __(
											'With Login Form',
											'wholesalex'
										) }
									</label>
								</div>

								<div className="wsx-d-flex wsx-item-center wsx-gap-12">
									<Tooltip
										content={ `${ __(
											'Registration Form only for',
											'wholesalex'
										) } ${ role.name } ${ __(
											'Role.',
											'wholesalex'
										) }` }
									>
										{ Icons.help }
									</Tooltip>
									<div className="wsx-shortcode">
										<input
											className="wsx-input wsx-font-12"
											type="text"
											data-role={ getShortcode(
												role.value
											) }
											data-before-copy="Copy To Clipboard"
											data-copied="Copied!"
											value={
												shortcodes[ role.value ]
													? getShortcode(
															shortcodes[
																role.value
															]
													  )
													: getShortcode(
															`[wholesalex_registration registration_role="${ role.value }"]`
													  )
											}
											onFocus={ ( e ) =>
												e.target.select()
											}
											onClick={ ( e ) =>
												e.target.select()
											}
											readOnly
										/>
										<Tooltip
											content={ __(
												'Copy To Clipboard',
												'wholesalex'
											) }
										>
											<div
												className="wsx-icon"
												onClick={ () => {
													const _element = `[data-role="${ role.value }"]`;
													let _shortcode =
														document.querySelector(
															_element
														);
													if ( navigator.clipboard ) {
														_shortcode =
															_shortcode.value;
														navigator.clipboard.writeText(
															_shortcode
														);
													} else {
														try {
															_shortcode.focus();
															document.execCommand(
																'copy'
															);
														} catch ( err ) {
															//error
														}
													}
												} }
												onKeyDown={ ( e ) => {
													if (
														e.key === 'Enter' ||
														e.key === ' '
													) {
														const _element = `[data-role="${ role.value }"]`;
														let _shortcode =
															document.querySelector(
																_element
															);
														if (
															navigator.clipboard
														) {
															_shortcode =
																_shortcode.value;
															navigator.clipboard.writeText(
																_shortcode
															);
														} else {
															try {
																_shortcode.focus();
																document.execCommand(
																	'copy'
																);
															} catch ( err ) {
																//error
															}
														}
													}
												} }
												role="button"
												tabIndex="0"
											>
												{ Icons.copy }
											</div>
										</Tooltip>
									</div>
								</div>
							</div>
						)
					);
				} ) }
			</div>
		</>
	);

	return (
		<>
			{ setModalStatus && (
				<PopupModal
					renderContent={ popupContent }
					onClose={ () => setModalStatus( false ) }
					wrapperClass="wsx-width-90v wsx-width-1230 wsx-shortcode-wrapper"
				/>
			) }
		</>
	);
};

export default ShortcodesModal;
