import React from 'react';

const FontWeightSelect = ( { label, getValue, setValue } ) => {
	const options = {
		100: '100',
		200: '200',
		300: '300',
		400: '400',
		500: '500',
		600: '600',
		700: '700',
		800: '800',
		900: '900',
	};
	return (
		<div className="wholesalex-select-field wholesalex-style-formatting-field">
			<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
				{ label }
			</div>
			<select
				value={ getValue() }
				onChange={ ( e ) => {
					setValue( e.target.value );
				} }
				className="wsx-select wholesalex-style-formatting-field-content"
			>
				{ Object.keys( options ).map( ( option ) => {
					return (
						<option key={ option } value={ option }>
							{ options[ option ] }
						</option>
					);
				} ) }
			</select>
		</div>
	);
};

export default FontWeightSelect;
