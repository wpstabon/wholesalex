import React, { useContext, useRef, useState } from 'react';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
import FormBuilderDropdown from './FormBuilderDropdown';
import Slider from '../../components/Slider';
import Input from '../../components/Input';
import InputWithIncrementDecrement from './InputWithIncrementDecrement';
import FontWeightSelect from './FontWeightSelect';
import TextTransformControl from './TextTransformControl';
import ColorPanelDropdown from './ColorPanenDropdown';
import { getInputFieldVariation } from './Utils';
import { __ } from '@wordpress/i18n';

const LoginForm = () => {
	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);

	const [ isActive, setActive ] = useState( false );
	const [ loginFields ] = useState(
		JSON.parse( wholesalex_overview.whx_form_builder_login_form_data )
	);
	const colorPickerRef = useRef();

	const renderRows = () => {
		return (
			<div className="wsx-login-fields wsx-fields-container">
				{ loginFields &&
					loginFields?.map( ( row ) => {
						return getInputFieldVariation(
							registrationForm?.settings?.inputVariation,
							row.columns[ 0 ]
						);
					} ) }
			</div>
		);
	};

	const formTitleSettingPopupContent = ( setStatus ) => {
		const getTitleStyle = ( property ) => {
			return registrationForm?.loginFormHeader?.styles?.title[ property ];
		};

		const setTitleStyle = ( property, value ) => {
			setRegistrationForm( {
				type: 'updateFormHeadingStyle',
				formType: 'Login',
				styleFor: 'title',
				property,
				value,
			} );
		};

		const getDescriptionStyle = ( property ) => {
			return registrationForm?.loginFormHeader?.styles?.description[
				property
			];
		};

		const setDescriptionStyle = ( property, value ) => {
			setRegistrationForm( {
				type: 'updateFormHeadingStyle',
				formType: 'Login',
				styleFor: 'description',
				property,
				value,
			} );
		};
		return (
			<>
				<div className="wholesalex-popup-heading wsx-font-18-lightBold">
					{ __( 'Form Title Setting', 'wholesalex' ) }
					<div
						className="wsx-popup-close"
						onClick={ () => setStatus( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								setStatus( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="20"
							height="20"
							viewBox="0 0 20 20"
							fill="none"
						>
							<path
								d="M15 5L5 15"
								stroke="#343A46"
								strokeWidth="1.5"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M5 5L15 15"
								stroke="#343A46"
								strokeWidth="1.5"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</div>
				</div>
				<div className="wholesalex-form-builder-field-poup wsx-header-popup_control">
					<div className="wholesalex-form-builder-popup">
						<div className="wholesalex-popup-content wsx-header-popup_control wsx-form-setting">
							<div className="wholesalex-form-title-setting-row wsx-setting-wrap wsx-toggle-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
								<Slider
									name={ 'hideDescription' }
									key={ 'hideDescription' }
									className="wholesalex-form-description-status-field wholesalex_slider_field wsx-toggle-setting"
									label={ __(
										'Hide Form Description',
										'wholesalex'
									) }
									value={
										registrationForm?.loginFormHeader
											?.isHideDescription
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'toogleLoginFormDescriptionStatus',
											value: e?.target?.checked,
										} );
									} }
								/>
							</div>
							<div className="wholesalex-form-title-setting-row wsx-setting-wrap wsx-input-wrap">
								<Input
									key={ 'formTitle' }
									name={ 'formTitle' }
									className="wholesalex-form-title-field wholesalex_form_builder__settings_field wsx-input-setting"
									label={ __( 'Title', 'wholesalex' ) }
									type="text"
									value={
										registrationForm?.loginFormHeader?.title
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateLoginFormTitle',
											value: e?.target?.value,
										} );
									} }
									placeholder={ __( 'Title', 'wholesalex' ) }
								/>
							</div>
							<div className="wholesalex-form-title-setting-row wsx-input-wrap">
								<Input
									key={ 'formDescription' }
									name={ 'formDescription' }
									className="wholesalex-form-title-field wholesalex_form_builder__settings_field wsx-input-setting"
									label={ __( 'Description', 'wholesalex' ) }
									type="text"
									value={
										registrationForm?.loginFormHeader
											?.description
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateLoginFormDescription',
											value: e?.target?.value,
										} );
									} }
									placeholder={ __(
										'Description',
										'wholesalex'
									) }
								/>
							</div>
						</div>
					</div>
					<div className="wholesalex-form-builder-popup">
						<div className="wholesalex-popup-heading wsx-popup-last wsx-font-18-lightBold">
							{ __( 'Style', 'wholesalex' ) }
						</div>
						<div className="wholesalex-popup-content wholesalex-form-title-style">
							<div className="wholesalex-form-title-style-column">
								<div className="wholesalex-form-title-style-column-heading wsx-font-14-normal">
									{ __( 'Title', 'wholesalex' ) }
								</div>
								<div className="wholesalex-form-title-style-row">
									<InputWithIncrementDecrement
										className={
											'wholesalex-form-title-font-size wholesalex-size-selector'
										}
										label={ __(
											'Font Size',
											'wholesalex'
										) }
										value={ getTitleStyle( 'size' ) }
										onChange={ ( val ) => {
											setTitleStyle( 'size', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row ">
									<FontWeightSelect
										label={ __(
											'Font Weight',
											'wholesalex'
										) }
										getValue={ () =>
											getTitleStyle( 'weight' )
										}
										setValue={ ( val ) => {
											setTitleStyle( 'weight', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<TextTransformControl
										label={ __(
											'Font Case',
											'wholesalex'
										) }
										getValue={ () =>
											getTitleStyle( 'transform' )
										}
										setValue={ ( val ) =>
											setTitleStyle( 'transform', val )
										}
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<div className="wholesalex-style-formatting-field">
										<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
											Form Title Color
										</div>
										<ColorPanelDropdown
											colorPickerRef={ colorPickerRef }
											className={
												'wholesalex-style-formatting-field-content wholesalex-outside-click-whitelist'
											}
											value={ getTitleStyle( 'color' ) }
											onChange={ ( val ) =>
												setTitleStyle( 'color', val )
											}
										/>
									</div>
								</div>
							</div>
							<div className="wholesalex-form-title-style-column">
								<div className="wholesalex-form-title-style-column-heading wsx-font-14-normal">
									{ __( 'Description', 'wholesalex' ) }
								</div>
								<div className="wholesalex-form-title-style-row">
									<InputWithIncrementDecrement
										className={
											'wholesalex-form-title-font-size wholesalex-size-selector'
										}
										label={ __(
											'Font Size',
											'wholesalex'
										) }
										value={ getDescriptionStyle( 'size' ) }
										onChange={ ( val ) => {
											setDescriptionStyle( 'size', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<FontWeightSelect
										label={ __(
											'Font Weight',
											'wholesalex'
										) }
										getValue={ () =>
											getDescriptionStyle( 'weight' )
										}
										setValue={ ( val ) => {
											setDescriptionStyle(
												'weight',
												val
											);
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<TextTransformControl
										label={ __(
											'Font Case',
											'wholesalex'
										) }
										getValue={ () =>
											getDescriptionStyle( 'transform' )
										}
										setValue={ ( val ) =>
											setDescriptionStyle(
												'transform',
												val
											)
										}
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<div className="wholesalex-style-formatting-field">
										<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
											Form Title Color
										</div>
										<ColorPanelDropdown
											colorPickerRef={ colorPickerRef }
											className={
												'wholesalex-style-formatting-field-content wholesalex-outside-click-whitelist'
											}
											value={ getDescriptionStyle(
												'color'
											) }
											onChange={ ( val ) =>
												setDescriptionStyle(
													'color',
													val
												)
											}
											placement="right-start"
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>{ ' ' }
			</>
		);
	};
	return (
		<div className="wholesalex-login-form">
			{ registrationForm?.settings?.isShowFormTitle && (
				<div
					className={ `wholesalex-login-form-title ${
						isActive ? 'wsx-row-active' : ''
					}` }
					onMouseEnter={ () => setActive( true ) }
					onMouseLeave={ () => setActive( false ) }
				>
					<div className="wsx-login-form-title-text wsx-editable">
						<input
							type="text"
							className="wsx-editable-area"
							value={ registrationForm?.loginFormHeader?.title }
							onChange={ ( e ) => {
								setRegistrationForm( {
									type: 'updateLoginFormTitle',
									value: e.target.value.replace(
										/["']/g,
										''
									),
								} );
							} }
						/>
						{ isActive && (
							<span className="wsx-editable-edit-icon">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="12"
									height="12"
									viewBox="0 0 14 14"
									fill="none"
								>
									<path
										d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
										stroke="white"
										strokeLinecap="round"
										strokeLinejoin="round"
									/>
									<path
										d="M1.75 12.8333H12.25"
										stroke="white"
										strokeLinecap="round"
										strokeLinejoin="round"
									/>
								</svg>
							</span>
						) }
					</div>
					{ ! registrationForm?.loginFormHeader
						?.isHideDescription && (
						<div className="wholesalex-login-form-subtitle-text wsx-editable">
							<input
								type="text"
								className="wsx-editable-area"
								value={
									registrationForm?.loginFormHeader
										?.description
								}
								onChange={ ( e ) => {
									setRegistrationForm( {
										type: 'updateLoginFormDescription',
										value: e.target.value.replace(
											/["']/g,
											''
										),
									} );
								} }
							/>
							{ isActive && (
								<span className="wsx-editable-edit-icon">
									<svg
										xmlns="http://www.w3.org/2000/svg"
										width="12"
										height="12"
										viewBox="0 0 14 14"
										fill="none"
									>
										<path
											d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
											stroke="white"
											strokeLinecap="round"
											strokeLinejoin="round"
										/>
										<path
											d="M1.75 12.8333H12.25"
											stroke="white"
											strokeLinecap="round"
											strokeLinejoin="round"
										/>
									</svg>
								</span>
							) }
						</div>
					) }
					<FormBuilderDropdown
						renderToogle={ ( status, setStatus ) => {
							return (
								<span
									className={ `dashicons dashicons-admin-generic ${
										status ? 'is-active' : ''
									}` }
									onClick={ () => setStatus( ! status ) }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											setStatus( ! status );
										}
									} }
									role="button"
									tabIndex="0"
								></span>
							);
						} }
						renderContent={ ( status, setStatus ) => {
							return formTitleSettingPopupContent( setStatus );
						} }
						className="wsx-header-popup_control"
					/>
				</div>
			) }
			<div
				className={ `wholesalex-fields-wrapper wsx_${ registrationForm.settings.inputVariation }` }
			>
				{ renderRows() }
			</div>
			<div className="wsx-form-btn-wrapper">
				<button
					contentEditable
					className={ `button wsx-login-btn ${ registrationForm.styles?.layout?.button?.align } wsx-editable` }
				>
					<input
						type="text"
						className="wsx-editable-area"
						value={ registrationForm?.loginFormButton?.title }
						onChange={ ( e ) => {
							setRegistrationForm( {
								type: 'updateLoginButtonText',
								value: e.target.value.replace( /["']/g, '' ),
							} );
						} }
					/>
					<span className="wsx-editable-edit-icon">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="12"
							height="12"
							viewBox="0 0 14 14"
							fill="none"
						>
							<path
								d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
								stroke="white"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M1.75 12.8333H12.25"
								stroke="white"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</span>
				</button>
			</div>
		</div>
	);
};

export default LoginForm;
