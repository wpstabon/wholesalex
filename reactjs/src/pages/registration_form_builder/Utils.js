export const getInputFieldVariation = (
	variation,
	field,
	getFieldSettings = false
) => {
	const isHeadingShow = () => {
		return ! field.isLabelHide || getFieldSettings;
	};

	//Render Term and Condition Field in Builder
	const renderTermConditionField = (
		field,
		isHeadingShow,
		getFieldSettings
	) => {
		return (
			<div className="wsx-form-field wsx-form-checkbox">
				{ isHeadingShow() && (
					<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
						{ ! field.isLabelHide && (
							<div
								className="wsx-form-label"
								htmlFor={ field.name }
							>
								{ field.label }{ ' ' }
								{ field.required && (
									<span aria-label="required">*</span>
								) }
							</div>
						) }
						{ getFieldSettings && getFieldSettings() }
					</div>
				) }
				<div className="wsx-field-content">
					{
						<div className="wsx-label wholesalex-field-wrap">
							<input
								type="checkbox"
								id={ field.name }
								name={ field.name }
								value={ '' }
							/>
							<label className="wsx-label" htmlFor={ field.name }>
								{ field.default_text }
							</label>
						</div>
					}
				</div>
				{ field.help_message && (
					<span className="wsx-form-field-help-message">
						{ field.help_message }
					</span>
				) }
			</div>
		);
	};

	switch ( variation ) {
		case 'variation_1':
		case 'variation_3':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'select':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option, index ) => {
										return (
											<option
												key={ index }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						<div className="wsx-form-field wsx-form-checkbox">
							{ isHeadingShow() && (
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
							) }
							<div className="wsx-field-content">
								{ field.option.map( ( option ) => {
									return (
										<div
											className="wsx-label wholesalex-field-wrap"
											key={ option.value }
										>
											<input
												type="checkbox"
												id={ option.value }
												name={ field.name }
												value={ option.value }
											/>
											<label
												className="wsx-label"
												htmlFor={ option.value }
											>
												{ option.name }
											</label>
										</div>
									);
								} ) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</div>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						<div className="wsx-form-field wsx-field-radio">
							{ isHeadingShow() && (
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
							) }
							<div className="wsx-field-content">
								{ field.option.map( ( option ) => {
									return (
										<div
											className="wsx-label wholesalex-field-wrap"
											key={ option.value }
										>
											<input
												type="radio"
												id={ option.value }
												name={ field.name }
												value={ option.value }
											/>
											<label
												className="wsx-label"
												htmlFor={ option.value }
											>
												{ option.name }
											</label>
										</div>
									);
								} ) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</div>
					);

				case 'file':
					return (
						<div className="wsx-form-field wsx-form-file">
							{ isHeadingShow() && (
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
							) }
							<label
								className="wsx-label wsx-field-content"
								htmlFor="wsx-label"
							>
								<input
									type={ field.type }
									id={ field.id }
									placeholder={ field?.placeholder }
									pattern={ field?.pattern }
									name={ field.name }
								/>
								<div
									className="wsx-file-label"
									htmlFor={ field?.name }
								>
									<span>
										<svg
											xmlns="http://www.w3.org/2000/svg"
											width="18"
											height="18"
											viewBox="0 0 18 18"
											fill="none"
										>
											<path
												d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
												stroke="#6C6CFF"
												strokeWidth="1.5"
												strokeLinecap="round"
												strokeLinejoin="round"
											/>
										</svg>
										Upload File
									</span>
									{ 'No File Chosen' }
								</div>
							</label>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</div>
					);
				case 'tel':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<input
									id={ field.name }
									type={ 'tel' }
									name={ field.name }
									required={ field.required }
									pattern={ field?.inputPattern }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'url':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<input
									id={ field.name }
									type={ 'url' }
									name={ field.name }
									required={ field.required }
									pattern={ field?.inputPattern }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<input
									id={ field.name }
									type={ 'password' }
									name={ field.name }
									required={ field.required }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									pattern={ field?.inputPattern }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<textarea
									className="wsx-textarea"
									id={ field.name }
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				// case 'privacy_policy_text':
				//     return <div className="wsx-privacy-policy">{wholesalex_overview?.whx_form_builder_privacy_policy_text}</div>
				default:
					break;
			}
			break;
		case 'variation_2':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
				case 'url':
				case 'tel':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
								<div className="wsx-form-label wsx-clone-label">
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</div>
								<label
									className="wsx-label wsx-form-label"
									htmlFor={ field.name }
								>
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									type={ field.type }
									className="wsx-form-field__input"
									id={ field.id }
									placeholder={ field?.placeholder }
									pattern={ field?.pattern }
									name={ field.name }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									autoComplete="false"
								/>
								<div className="wsx-form-label wsx-clone-label">
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</div>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-textarea">
								<textarea
									id={ field.name }
									className="wsx-textarea wsx-form-field__textarea"
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
								<div className="wsx-form-label wsx-clone-label">
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</div>
								<label
									className="wsx-label wsx-form-label"
									htmlFor={ field.name }
								>
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'file':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-form-file wsx-file-outline">
								{ /* <div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
                                    {getFieldSettings && getFieldSettings()}
                                </div> */ }
								{ /* {
                                    getFieldSettings && 
                                    <div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
                                        { getFieldSettings()}
                                    </div>
                                } */ }
								<label
									className="wsx-label wsx-field-content"
									htmlFor={ field.name }
								>
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									{ ! field.isLabelHide && (
										<>
											<div className="wsx-form-label wsx-clone-label">
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
											<div className="wsx-form-label">
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										</>
									) }
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										<span>
											<svg
												xmlns="http://www.w3.org/2000/svg"
												width="18"
												height="18"
												viewBox="0 0 18 18"
												fill="none"
											>
												<path
													d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
													stroke="#6C6CFF"
													strokeWidth="1.5"
													strokeLinecap="round"
													strokeLinejoin="round"
												/>
											</svg>
											Upload File
										</span>
										{ 'No File Chosen' }
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'select':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-select">
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
								<div className="wsx-form-label wsx-clone-label">
									{ field.label }
								</div>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor="wsx"
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div className="wsx-form-label">
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<>
												<label
													className="wsx-label wholesalex-field-wrap"
													htmlFor={ field.name }
												>
													<input
														type="checkbox"
														id={ option.value }
														name={ field.name }
														value={ option.value }
													/>
													<div>{ option.name }</div>
												</label>
											</>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-field-radio">
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div className="wsx-form-label">
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		case 'variation_6':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'url':
				case 'tel':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									placeholder={
										field.label +
										( field.required ? '*' : '' )
									}
									required={ field.required }
									autoComplete={ false }
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }{ ' ' }
								</span>
							) }
						</>
					);
				case 'date':
					return (
						// wsx-form-field--focused
						<>
							{ isHeadingShow() && (
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<label
											className="wsx-label wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</label>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
							) }
							<div className="wsx-form-field wsx-outline-focus wsx-form-date wsx-marginTop_36">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									type={ field.type }
									className="wsx-form-field__input"
									id={ field.id }
									placeholder={
										field.label +
										( field.required ? '*' : '' )
									}
									pattern={ field?.pattern }
									name={ field.name }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-textarea">
								<textarea
									id={ field.name }
									className="wsx-textarea wsx-form-field__textarea"
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={
										field.label +
										( field.required ? '*' : '' )
									}
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'file':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-file wsx-marginTop_36">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<label
									className="wsx-label wsx-field-content"
									htmlFor={ field.name }
								>
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										<span>
											<svg
												xmlns="http://www.w3.org/2000/svg"
												width="18"
												height="18"
												viewBox="0 0 18 18"
												fill="none"
											>
												<path
													d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
													stroke="#6C6CFF"
													strokeWidth="1.5"
													strokeLinecap="round"
													strokeLinejoin="round"
												/>
											</svg>
											Upload File
										</span>
										{ 'No File Chosen' }
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'select':
					return (
						<>
							{ isHeadingShow() && (
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
							) }
							<div className="wsx-form-field wsx-outline-focus wsx-form-select wsx-marginTop_36">
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<>
												<label
													className="wsx-label wholesalex-field-wrap"
													htmlFor={ field.name }
												>
													<input
														type="checkbox"
														id={ option.value }
														name={ field.name }
														value={ option.value }
													/>
													<div
														htmlFor={ option.value }
													>
														{ option.name }
													</div>
												</label>
											</>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-field-radio">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		case 'variation_4':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
				case 'url':
				case 'tel':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
								<label
									className="wsx-label wsx-form-label"
									htmlFor={ field.name }
								>
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<label
								className="wsx-label wsx-form-field wsx-outline-focus"
								htmlFor={ field.name }
							>
								<input
									type={ field.type }
									className="wsx-form-field__input"
									id={ field.id }
									placeholder={ field?.placeholder }
									pattern={ field?.pattern }
									name={ field.name }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									autoComplete="false"
								/>
								{ ! field.isLabelHide && (
									<div className="wsx-form-label">
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</div>
								) }
							</label>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-textarea">
								<textarea
									id={ field.name }
									className="wsx-textarea wsx-form-field__textarea"
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={ field.placeholder }
									autoComplete="false"
								/>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'file':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-form-file">
								{ /* <div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
                                    {getFieldSettings && getFieldSettings()}
                                </div> */ }
								<label
									className="wsx-label wsx-field-content"
									htmlFor={ field.name }
								>
									{ ' ' }
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										<div>
											{ ! field.isLabelHide && (
												<label
													className="wsx-label wsx-form-label"
													htmlFor={ field.name }
												>
													{ field.label }{ ' ' }
													{ field.required && (
														<span aria-label="required">
															*
														</span>
													) }
												</label>
											) }
										</div>
										<div className="wsx-file-label_wrap">
											<span>
												<svg
													xmlns="http://www.w3.org/2000/svg"
													width="18"
													height="18"
													viewBox="0 0 18 18"
													fill="none"
												>
													<path
														d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
														stroke="#6C6CFF"
														strokeWidth="1.5"
														strokeLinecap="round"
														strokeLinejoin="round"
													/>
												</svg>
												Upload File
											</span>
											{ 'No File Chosen' }
										</div>
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'select':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor="wsx"
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }{ ' ' }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<>
												<label
													className="wsx-label wholesalex-field-wrap"
													htmlFor={ field.name }
												>
													<input
														type="checkbox"
														id={ option.value }
														name={ field.name }
														value={ option.value }
													/>
													<label
														className="wsx-label"
														htmlFor={ option.value }
													>
														{ option.name }
													</label>
												</label>
											</>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);

				case 'radio':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-field-radio">
								<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
									{ ! field.isLabelHide && (
										<div
											className="wsx-form-label"
											htmlFor={ field.name }
										>
											{ field.label }
											{ field.required && (
												<span aria-label="required">
													*
												</span>
											) }
										</div>
									) }
									{ getFieldSettings && getFieldSettings() }
								</div>
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		case 'variation_5':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
				case 'url':
				case 'tel':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									placeholder={ field?.placeholder }
									autoComplete="false"
								/>
								<label
									className="wsx-label wsx-form-label"
									htmlFor={ field.name }
								>
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<label
								className="wsx-label wsx-form-field wsx-outline-focus"
								htmlFor={ field.name }
							>
								<input
									type={ field.type }
									className="wsx-form-field__input"
									id={ field.id }
									placeholder={ field?.placeholder }
									pattern={ field?.pattern }
									name={ field.name }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
								/>
								{ ! field.isLabelHide && (
									<div className="wsx-form-label">
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</div>
								) }
							</label>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-textarea">
								<textarea
									id={ field.name }
									className="wsx-textarea wsx-form-field__textarea"
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={ field?.placeholder }
								/>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'file':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-form-file">
								<label
									className="wsx-label wsx-field-content"
									htmlFor={ field.name }
								>
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										<div className="wsx-file-label_wrap">
											<span>
												<svg
													xmlns="http://www.w3.org/2000/svg"
													width="18"
													height="18"
													viewBox="0 0 18 18"
													fill="none"
												>
													<path
														d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
														stroke="#6C6CFF"
														strokeWidth="1.5"
														strokeLinecap="round"
														strokeLinejoin="round"
													/>
												</svg>
												Upload File
											</span>
											{ 'No File Chosen' }
										</div>
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'select':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus">
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
								{ ! field.isLabelHide && (
									<label
										htmlFor={ field?.name }
										className="wsx-label wsx-form-label"
									>
										{ field.label }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<>
												<label
													className="wsx-label wholesalex-field-wrap"
													htmlFor={ field.name }
												>
													<input
														type="checkbox"
														id={ option.value }
														name={ field.name }
														value={ option.value }
													/>
													<div
														htmlFor={ option.value }
													>
														{ option.name }
													</div>
												</label>
											</>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-field-radio">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		case 'variation_7':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
				case 'url':
				case 'tel':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-formBuilder-input-width">
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									autoComplete="false"
									placeholder=" "
								/>
								<label
									className="wsx-label wsx-form-label"
									htmlFor={ field.name }
								>
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</label>
								<div className="wsx-clone-label wsx-form-label">
									{ field.label }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'password':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-formBuilder-input-width">
								<input
									type={ field.type }
									className="wsx-form-field__input"
									id={ field.name }
									pattern={ field?.pattern }
									name={ field.name }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									autoComplete="false"
									placeholder=" "
								/>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
								<div className="wsx-clone-label wsx-form-label">
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-textarea wsx-formBuilder-input-width">
								<textarea
									id={ field.name }
									className="wsx-textarea wsx-form-field__textarea"
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									autoComplete="false"
									placeholder=" "
								/>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
								<div className="wsx-clone-label wsx-form-label">
									{ field.label }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'file':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-file wsx-form-file wsx-marginTop_36">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div className="wsx-form-label">
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<label
									className="wsx-label wsx-field-content"
									htmlFor={ field.name }
								>
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										<span>
											<svg
												xmlns="http://www.w3.org/2000/svg"
												width="18"
												height="18"
												viewBox="0 0 18 18"
												fill="none"
											>
												<path
													d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
													stroke="#6C6CFF"
													strokeWidth="1.5"
													strokeLinecap="round"
													strokeLinejoin="round"
												/>
											</svg>
											Upload File
										</span>
										{ 'No File Chosen' }
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'select':
					return (
						// wsx-form-field--focused
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-form-select wsx-formBuilder-input-width">
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor="wsx"
									>
										{ field.label }
									</label>
								) }
								<div className="wsx-clone-label wsx-form-label">
									{ field.label }{ ' ' }
									{ field.required && (
										<span aria-label="required">*</span>
									) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<>
												<label
													className="wsx-label wholesalex-field-wrap"
													htmlFor={ field.name }
												>
													<input
														type="checkbox"
														id={ option.value }
														name={ field.name }
														value={ option.value }
													/>
													<div
														htmlFor={ option.value }
													>
														{ option.name }
													</div>
												</label>
											</>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						// wsx-form-field--focused
						<>
							<div className="wsx-form-field wsx-field-radio">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.name }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		case 'variation_8':
			switch ( field.type ) {
				case 'text':
				case 'email':
				case 'number':
				case 'date':
				case 'url':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<label
								className="wsx-label wsx-form-field wsx-outline-focus wsx-formBuilder-input-width"
								htmlFor="wsx"
							>
								{ ! field.isLabelHide && (
									<div
										className="wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</div>
								) }
								<input
									id={ field.name }
									type={ field.type }
									name={ field.name }
									required={ field.required }
									placeholder={ field?.placeholder }
									autoComplete="false"
								/>
							</label>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'select':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-formBuilder-input-width">
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
								<select
									className={ `wsx-select ${ field.name }` }
									id={ field.name }
								>
									{ field.option.map( ( option ) => {
										return (
											<option
												key={ option.value }
												value={ option.value }
											>
												{ option.name }
											</option>
										);
									} ) }
								</select>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'checkbox':
					return (
						<>
							<div className="wsx-form-field wsx-form-checkbox">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="checkbox"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'termCondition':
					return renderTermConditionField(
						field,
						isHeadingShow,
						getFieldSettings
					);
				case 'radio':
					return (
						<>
							<div className="wsx-form-field wsx-field-radio">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<div className="wsx-field-content">
									{ field.option.map( ( option ) => {
										return (
											<div
												className="wsx-label wholesalex-field-wrap"
												key={ option.value }
											>
												<input
													type="radio"
													id={ option.value }
													name={ field.name }
													value={ option.value }
												/>
												<label
													className="wsx-label"
													htmlFor={ option.value }
												>
													{ option.name }
												</label>
											</div>
										);
									} ) }
								</div>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'file':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-form-file">
								<label
									className="wsx-label wsx-field-content"
									htmlFor="wsx"
								>
									<input
										type={ field.type }
										id={ field.id }
										placeholder={ field?.placeholder }
										pattern={ field?.pattern }
										name={ field.name }
									/>
									<div
										className="wsx-file-label"
										htmlFor={ field?.name }
									>
										{ ! field.isLabelHide && (
											<div
												className="wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</div>
										) }
										<div className="wsx-file-label_wrap">
											<span>
												<svg
													xmlns="http://www.w3.org/2000/svg"
													width="18"
													height="18"
													viewBox="0 0 18 18"
													fill="none"
												>
													<path
														d="M2.25 11.25V14.25C2.25 15.075 2.925 15.75 3.75 15.75H14.25C14.6478 15.75 15.0294 15.592 15.3107 15.3107C15.592 15.0294 15.75 14.6478 15.75 14.25V11.25M12.75 6L9 2.25L5.25 6M9 3.15V10.875"
														stroke="#6C6CFF"
														strokeWidth="1.5"
														strokeLinecap="round"
														strokeLinejoin="round"
													/>
												</svg>
												Upload File
											</span>
											{ 'No File Chosen' }
										</div>
									</div>
								</label>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'tel':
					return (
						<>
							<div className="wsx-form-field">
								{ isHeadingShow() && (
									<div className="wsx-field-heading wsx-d-flex wsx-justify-space wsx-gap-10">
										{ ! field.isLabelHide && (
											<label
												className="wsx-label wsx-form-label"
												htmlFor={ field.name }
											>
												{ field.label }{ ' ' }
												{ field.required && (
													<span aria-label="required">
														*
													</span>
												) }
											</label>
										) }
										{ getFieldSettings &&
											getFieldSettings() }
									</div>
								) }
								<input
									id={ field.name }
									type={ 'tel' }
									name={ field.name }
									required={ field.required }
									pattern={ field?.inputPattern }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);

				case 'password':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-formBuilder-input-width">
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
								<input
									id={ field.name }
									type={ 'password' }
									name={ field.name }
									required={ field.required }
									minLength={ field?.minLength }
									maxLength={ field?.maxLength }
									size={ field?.size }
									pattern={ field?.inputPattern }
									placeholder={ field?.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				case 'textarea':
					return (
						<>
							{ getFieldSettings && getFieldSettings() }
							<div className="wsx-form-field wsx-outline-focus wsx-formBuilder-input-width">
								{ ! field.isLabelHide && (
									<label
										className="wsx-label wsx-form-label"
										htmlFor={ field.name }
									>
										{ field.label }{ ' ' }
										{ field.required && (
											<span aria-label="required">*</span>
										) }
									</label>
								) }
								<textarea
									className="wsx-textarea "
									id={ field.name }
									name={ field.name }
									required={ field.required }
									rows={ field?.rows }
									cols={ field?.cols }
									placeholder={ field?.placeholder }
									autoComplete="false"
								/>
							</div>
							{ field.help_message && (
								<span className="wsx-form-field-help-message">
									{ field.help_message }
								</span>
							) }
						</>
					);
				default:
					break;
			}
			break;
		default:
			break;
	}
};

export const getFormStyle = (
	style,
	loginHeaderStyle,
	RegistrationHeaderStyle
) => {
	const _style = {
		// Color
		// Field Sign Up Normal

		'--wsx-input-color': style?.color?.field?.signUp?.normal?.text,
		'--wsx-input-bg': style?.color?.field?.signUp?.normal?.background,
		'--wsx-input-border-color': style?.color?.field?.signUp?.normal?.border,
		'--wsx-input-placeholder-color':
			style?.color?.field?.signUp?.normal?.placeholder,
		'--wsx-form-label-color': style?.color?.field?.signUp?.normal?.label,

		// Field Sign Up Active
		'--wsx-input-focus-color': style?.color?.field?.signUp?.active?.text,
		'--wsx-input-focus-bg': style?.color?.field?.signUp?.active?.background,
		'--wsx-input-focus-border-color':
			style?.color?.field?.signUp?.active?.border,
		'--wsx-form-label-color-active':
			style?.color?.field?.signUp?.active?.label,

		// Field Sign Up Warning

		'--wsx-input-warning-color': style?.color?.field?.signUp?.warning?.text,
		'--wsx-input-warning-bg':
			style?.color?.field?.signUp?.warning?.background,
		'--wsx-input-warning-border-color':
			style?.color?.field?.signUp?.warning?.border,
		'--wsx-form-label-color-warning':
			style?.color?.field?.signUp?.warning?.label,

		// Field Sign In Normal

		'--wsx-login-input-color': style?.color?.field?.signIn?.normal?.text,
		'--wsx-login-input-bg': style?.color?.field?.signIn?.normal?.background,
		'--wsx-login-input-border-color':
			style?.color?.field?.signIn?.normal?.border,
		'--wsx-login-input-placeholder-color':
			style?.color?.field?.signIn?.normal?.placeholder,
		'--wsx-login-form-label-color':
			style?.color?.field?.signIn?.normal?.label,

		// Field Sign In Active
		'--wsx-login-input-focus-color':
			style?.color?.field?.signIn?.active?.text,
		'--wsx-login-input-focus-bg':
			style?.color?.field?.signIn?.active?.background,
		'--wsx-login-input-focus-border-color':
			style?.color?.field?.signIn?.active?.border,
		'--wsx-login-form-label-color-active':
			style?.color?.field?.signIn?.active?.label,

		// Field Sign In Warning
		'--wsx-login-input-warning-color':
			style?.color?.field?.signIn?.warning?.text,
		'--wsx-login-input-warning-bg':
			style?.color?.field?.signIn?.warning?.background,
		'--wsx-login-input-warning-border-color':
			style?.color?.field?.signIn?.warning?.border,
		'--wsx-login-form-label-color-warning':
			style?.color?.field?.signIn?.warning?.label,

		// Button Sign UP Normal
		'--wsx-form-button-color': style?.color?.button?.signUp?.normal?.text,
		'--wsx-form-button-bg':
			style?.color?.button?.signUp?.normal?.background,
		'--wsx-form-button-border-color':
			style?.color?.button?.signUp?.normal?.border,

		// Button Sign UP Hover
		'--wsx-form-button-hover-color':
			style?.color?.button?.signUp?.hover?.text,
		'--wsx-form-button-hover-bg':
			style?.color?.button?.signUp?.hover?.background,
		'--wsx-form-button-hover-border-color':
			style?.color?.button?.signUp?.hover?.border,

		// Button Sign In Normal
		'--wsx-login-form-button-color':
			style?.color?.button?.signIn?.normal?.text,
		'--wsx-login-form-button-bg':
			style?.color?.button?.signIn?.normal?.background,
		'--wsx-login-form-button-border-color':
			style?.color?.button?.signIn?.normal?.border,

		// Button Sign In Hover
		'--wsx-login-form-button-hover-color':
			style?.color?.button?.signIn?.hover?.text,
		'--wsx-login-form-button-hover-bg':
			style?.color?.button?.signIn?.hover?.background,
		'--wsx-login-form-button-hover-border-color':
			style?.color?.button?.signIn?.hover?.border,

		// Container Main
		'--wsx-form-container-bg': style?.color?.container?.main?.background,
		'--wsx-form-container-border-color':
			style?.color?.container?.main?.border,

		// Container Sign UP
		'--wsx-form-reg-bg': style?.color?.container?.signUp?.background,
		'--wsx-form-reg-border-color': style?.color?.container?.signUp?.border,

		// Container Sign IN
		'--wsx-login-bg': style?.color?.container?.signIn?.background,
		'--wsx-login-border-color': style?.color?.container?.signIn?.border,

		// Typography
		// Field - Label
		'--wsx-form-label-font-size':
			style?.typography?.field?.label?.size + 'px',
		'--wsx-form-label-weight': style?.typography?.field?.label?.weight,
		'--wsx-form-label-case-transform':
			style?.typography?.field?.label?.transform,

		// Field - Input
		'--wsx-input-font-size': style?.typography?.field?.input?.size + 'px',
		'--wsx-input-weight': style?.typography?.field?.input?.weight,
		'--wsx-input-case-transform':
			style?.typography?.field?.input?.transform,

		// Button

		'--wsx-form-button-font-size': style?.typography?.button?.size + 'px',
		'--wsx-form-button-weight': style?.typography?.button?.weight,
		'--wsx-form-button-case-transform':
			style?.typography?.button?.transform,

		// Size and Spacing
		// Input
		'--wsx-input-padding': style?.sizeSpacing?.input?.padding + 'px',
		'--wsx-input-width': style?.sizeSpacing?.input?.width + 'px',
		'--wsx-input-border-width': style?.sizeSpacing?.input?.border + 'px',
		'--wsx-input-border-radius':
			style?.sizeSpacing?.input?.borderRadius + 'px',

		// Button
		'--wsx-form-button-padding': style?.sizeSpacing?.button?.padding + 'px',
		'--wsx-form-button-width': style?.sizeSpacing?.button?.width + '%',
		'--wsx-form-button-border-width':
			style?.sizeSpacing?.button?.border + 'px',
		'--wsx-form-button-border-radius':
			style?.sizeSpacing?.button?.borderRadius + 'px',
		'--wsx-form-button-align': style?.sizeSpacing?.button?.align,

		// Container - Main
		'--wsx-form-container-border-width':
			style?.sizeSpacing?.container?.main?.border + 'px',
		'--wsx-form-container-width':
			style?.sizeSpacing?.container?.main?.width + 'px',
		'--wsx-form-container-border-radius':
			style?.sizeSpacing?.container?.main?.borderRadius + 'px',
		'--wsx-form-container-padding':
			style?.sizeSpacing?.container?.main?.padding + 'px',
		'--wsx-form-container-separator':
			style?.sizeSpacing?.container?.main?.separator + 'px',

		// Container - Sign In
		'--wsx-login-width':
			style?.sizeSpacing?.container?.signIn?.width + 'px',
		'--wsx-login-border-width':
			style?.sizeSpacing?.container?.signIn?.border + 'px',
		'--wsx-login-padding':
			style?.sizeSpacing?.container?.signIn?.padding + 'px',
		'--wsx-login-border-radius':
			style?.sizeSpacing?.container?.signIn?.borderRadius + 'px',

		// Container - Sign Up
		'--wsx-form-reg-width':
			style?.sizeSpacing?.container?.signUp?.width + 'px',
		'--wsx-form-reg-border-width':
			style?.sizeSpacing?.container?.signUp?.border + 'px',
		'--wsx-form-reg-padding':
			style?.sizeSpacing?.container?.signUp?.padding + 'px',
		'--wsx-form-reg-border-radius':
			style?.sizeSpacing?.container?.signUp?.borderRadius + 'px',

		'--wsx-login-title-font-size': loginHeaderStyle?.title?.size + 'px',
		'--wsx-login-title-case-transform': loginHeaderStyle?.title?.transform,
		'--wsx-login-title-font-weight': loginHeaderStyle?.title?.weight,
		'--wsx-login-title-color': loginHeaderStyle?.title?.color,

		'--wsx-login-description-font-size':
			loginHeaderStyle?.description?.size + 'px',
		'--wsx-login-description-case-transform':
			loginHeaderStyle?.description?.transform,
		'--wsx-login-description-font-weight':
			loginHeaderStyle?.description?.weight,
		'--wsx-login-description-color': loginHeaderStyle?.description?.color,

		'--wsx-reg-title-font-size':
			RegistrationHeaderStyle?.title?.size + 'px',
		'--wsx-reg-title-case-transform':
			RegistrationHeaderStyle?.title?.transform,
		'--wsx-reg-title-font-weight': RegistrationHeaderStyle?.title?.weight,
		'--wsx-reg-title-color': RegistrationHeaderStyle?.title?.color,

		'--wsx-reg-description-font-size':
			RegistrationHeaderStyle?.description?.size + 'px',
		'--wsx-reg-description-case-transform':
			RegistrationHeaderStyle?.description?.transform,
		'--wsx-reg-description-font-weight':
			RegistrationHeaderStyle?.description?.weight,
		'--wsx-reg-description-color':
			RegistrationHeaderStyle?.description?.color,
	};

	return _style;
};

export const getRegistrationFormFieldOptions = (
	registrationForm,
	exclude = ''
) => {
	const _fields = {};
	for (
		let index = 0;
		index < registrationForm.registrationFields.length;
		index++
	) {
		const row = registrationForm.registrationFields[ index ];
		for ( let j = 0; j < Object.keys( row.columns ).length; j++ ) {
			const col = row.columns[ j ];
			if ( col.status ) {
				_fields[ col.name ] = col.label;
			}
		}
	}
	if ( exclude ) {
		delete _fields[ exclude ];
	}

	return _fields;
};
