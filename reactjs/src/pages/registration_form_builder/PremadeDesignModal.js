import React, { useContext, useRef } from 'react';
import { __ } from '@wordpress/i18n';
import handleOutsideClick from '../../components/OutsideClick';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';

const PremadeDesignModal = ( { setModalStatus } ) => {
	const { setRegistrationForm } = useContext( RegistrationFormContext );

	const deleteIcon = (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
		>
			<path
				d="M18 6L6 18"
				stroke="#343A46"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M6 6L18 18"
				stroke="#343A46"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);

	const options = [
		'premade_1',
		'premade_2',
		'premade_3',
		'premade_4',
		'premade_5',
		'premade_6',
		'premade_7',
		'premade_8',
		'premade_9',
	];

	const getImageUrl = ( option ) => {
		return wholesalex.url + 'assets/img/premade-forms/' + option + '.svg';
	};

	const dropdownRef = useRef( null );

	handleOutsideClick( dropdownRef, () => {
		setModalStatus( false );
	} );

	const setPremade = ( premade ) => {
		setRegistrationForm( { type: 'setPremade', premade } );
		setModalStatus( false );
	};

	return (
		<div ref={ dropdownRef } className="wsx-reg-premade">
			<div className="wsx-reg-premade_container">
				<div className="wsx-reg-premade_heading wsx-d-flex wsx-justify-space wsx-gap-10">
					{ __(
						'Create a Form Right Away Using a Pre-made Designs',
						'wholesalex'
					) }
					<span
						className="wsx-reg-premade_close"
						onClick={ () => setModalStatus( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								setModalStatus( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ deleteIcon }
					</span>
				</div>
				<div className="wsx-reg-premade__body">
					{ options.map( ( option, idx ) => {
						return (
							<div key={ idx } className="wsx-premade-item">
								<div className="wsx-premade-media">
									<img
										src={ getImageUrl( option ) }
										alt="wsx premade design"
									/>
								</div>
								<div className="wsx-premade-content wsx-font-18-lightBold wsx-d-flex wsx-justify-space wsx-gap-10">
									<span>{ 'Template #' + ( idx + 1 ) }</span>
									<div className="wsx-premade-action">
										{ idx === 0 && (
											<span
												onClick={ () =>
													setPremade( option )
												}
												className="wsx-premade-template wsx-font-14-normal"
												onKeyDown={ ( e ) => {
													if (
														e.key === 'Enter' ||
														e.key === ' '
													) {
														setPremade( option );
													}
												} }
												role="button"
												tabIndex="0"
											>
												{ __(
													'Use Design',
													'wholesalex'
												) }
											</span>
										) }
										{ idx !== 0 &&
											! wholesalex.is_pro_active && (
												<a
													target="_blank"
													className="wsx-link wsx-font-14-normal wsx-premade-upgrade"
													href="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_features-upgrade_to_pro&utm_campaign=wholesalex-DB"
													rel="noreferrer"
												>
													{ __(
														'Upgrade to Pro ➤',
														'wholesalex'
													) }
												</a>
											) }
										{ idx !== 0 &&
											wholesalex.is_pro_active && (
												<span
													onClick={ () =>
														setPremade( option )
													}
													className="wsx-premade-template wsx-font-14-normal"
													onKeyDown={ ( e ) => {
														if (
															e.key === 'Enter' ||
															e.key === ' '
														) {
															setPremade(
																option
															);
														}
													} }
													role="button"
													tabIndex="0"
												>
													{ __(
														'Use Design',
														'wholesalex'
													) }
												</span>
											) }
									</div>
								</div>
							</div>
						);
					} ) }
				</div>
			</div>
		</div>
	);
};

export default PremadeDesignModal;
