import React, { useContext, useState, useRef } from 'react';
import FieldInserter from './FieldInserter';
import RowField from './RowField';
import FormBuilderDropdown from './FormBuilderDropdown';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';
import Slider from '../../components/Slider';
import Input from '../../components/Input';
import { __ } from '@wordpress/i18n';
import InputWithIncrementDecrement from './InputWithIncrementDecrement';
import FontWeightSelect from './FontWeightSelect';
import TextTransformControl from './TextTransformControl';
import ColorPanelDropdown from './ColorPanenDropdown';

const RegistrationFormBuilder = ( { setUpgradeProPopupStatus } ) => {
	const { registrationForm, setRegistrationForm } = useContext(
		RegistrationFormContext
	);
	const [ isActive, setActive ] = useState( false );
	const headingRef = useRef( null );

	const draggingItem = useRef();
	const dragOverItem = useRef();
	const rowRef = useRef();
	const colorPickerRef = useRef();
	const [ activeRow, setActiveRow ] = useState( -1 );
	const handleDragStart = ( e, position ) => {
		draggingItem.current = position;
	};

	const handleDragEnter = ( e, position ) => {
		if ( position === draggingItem.current ) {
			return;
		}
		dragOverItem.current = position;
		const _fields = [ ...registrationForm.registrationFields ];
		const draggingItemContent = _fields[ draggingItem.current ];
		_fields.splice( draggingItem.current, 1 );
		_fields.splice( dragOverItem.current, 0, draggingItemContent );
		draggingItem.current = dragOverItem.current;
		dragOverItem.current = null;
		setRegistrationForm( {
			type: 'updateFullState',
			updatedState: _fields,
		} );
	};

	const settingRef = useRef( false );

	const renderRows = () => {
		return (
			<div className={ `wsx-reg-fields wsx-fields-container` }>
				{ registrationForm.registrationFields &&
					registrationForm.registrationFields.map( ( row, index ) => {
						return (
							<RowField
								key={ index }
								row={ row }
								index={ index }
								handleDragEnter={ handleDragEnter }
								handleDragStart={ handleDragStart }
								rowRef={ rowRef }
								activeRow={ activeRow }
								setActiveRow={ setActiveRow }
								settingRef={ settingRef }
								setUpgradeProPopupStatus={
									setUpgradeProPopupStatus
								}
							/>
						);
					} ) }
			</div>
		);
	};
	const formTitleSettingPopupContent = ( setStatus ) => {
		const getTitleStyle = ( property ) => {
			return registrationForm?.registrationFormHeader?.styles?.title[
				property
			];
		};

		const setTitleStyle = ( property, value ) => {
			setRegistrationForm( {
				type: 'updateFormHeadingStyle',
				formType: 'Registration',
				styleFor: 'title',
				property,
				value,
			} );
		};

		const getDescriptionStyle = ( property ) => {
			return registrationForm?.registrationFormHeader?.styles
				?.description[ property ];
		};

		const setDescriptionStyle = ( property, value ) => {
			setRegistrationForm( {
				type: 'updateFormHeadingStyle',
				formType: 'Registration',
				styleFor: 'description',
				property,
				value,
			} );
		};

		return (
			<>
				<div className="wholesalex-popup-heading wsx-font-18-lightBold">
					{ __( 'Form Title Setting', 'wholesalex' ) }
					<div
						className="wsx-popup-close"
						onClick={ () => setStatus( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								setStatus( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="20"
							height="20"
							viewBox="0 0 20 20"
							fill="none"
						>
							<path
								d="M15 5L5 15"
								stroke="#343A46"
								strokeWidth="1.5"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M5 5L15 15"
								stroke="#343A46"
								strokeWidth="1.5"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</div>
				</div>
				<div
					className="wholesalex-form-builder-field-poup wsx-header-popup_control"
					ref={ headingRef }
				>
					<div className="wholesalex-form-builder-popup">
						<div className="wholesalex-popup-content wsx-form-setting wsx-header-popup_control">
							<div className="wholesalex-form-title-setting-row wsx-setting-wrap wsx-toggle-wrap wsx-d-flex wsx-justify-space wsx-gap-10">
								<Slider
									name={ 'hideDescription' }
									key={ 'hideDescription' }
									className="wholesalex-form-description-status-field wsx-toggle-setting"
									label={ __(
										'Hide Form Description',
										'wholesalex'
									) }
									value={
										registrationForm?.registrationFormHeader
											?.isHideDescription
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'toogleRegistrationFormDescriptionStatus',
											value: e?.target?.checked,
										} );
									} }
								/>
							</div>
							<div className="wholesalex-form-title-setting-row wsx-setting-wrap wsx-input-wrap">
								<Input
									key={ 'formTitle' }
									name={ 'formTitle' }
									className="wholesalex-form-title-field wsx-input-setting"
									label={ __( 'Title', 'wholesalex' ) }
									type="text"
									value={
										registrationForm?.registrationFormHeader
											?.title
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateRegistrationFormTitle',
											value: e?.target?.value,
										} );
									} }
									placeholder={ __( 'Title', 'wholesalex' ) }
								/>
							</div>
							<div className="wholesalex-form-title-setting-row wsx-input-wrap">
								<Input
									key={ 'formDescription' }
									name={ 'formDescription' }
									className="wholesalex-form-title-field wsx-input-setting"
									label={ __( 'Description', 'wholesalex' ) }
									type="text"
									value={
										registrationForm?.registrationFormHeader
											?.description
									}
									onChange={ ( e ) => {
										setRegistrationForm( {
											type: 'updateRegistrationFormDescription',
											value: e?.target?.value,
										} );
									} }
									placeholder={ __(
										'Description',
										'wholesalex'
									) }
								/>
							</div>
						</div>
					</div>
					<div className="wholesalex-form-builder-popup">
						<div className="wholesalex-popup-heading wsx-font-18-lightBold wsx-popup-last">
							{ __( 'Style', 'wholesalex' ) }
						</div>
						<div className="wholesalex-popup-content wholesalex-form-title-style wsx-form-title-setting">
							<div className="wholesalex-form-title-style-column">
								<div className="wholesalex-form-title-style-column-heading wsx-font-14-normal">
									{ __( 'Title', 'wholesalex' ) }
								</div>
								<div className="wholesalex-form-title-style-row">
									<InputWithIncrementDecrement
										className={
											'wholesalex-form-title-font-size wholesalex-size-selector'
										}
										label={ __(
											'Font Size',
											'wholesalex'
										) }
										value={ getTitleStyle( 'size' ) }
										onChange={ ( val ) => {
											setTitleStyle( 'size', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row ">
									<FontWeightSelect
										label={ __(
											'Font Weight',
											'wholesalex'
										) }
										getValue={ () =>
											getTitleStyle( 'weight' )
										}
										setValue={ ( val ) => {
											setTitleStyle( 'weight', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<TextTransformControl
										label={ __(
											'Font Case',
											'wholesalex'
										) }
										getValue={ () =>
											getTitleStyle( 'transform' )
										}
										setValue={ ( val ) =>
											setTitleStyle( 'transform', val )
										}
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<div className="wholesalex-style-formatting-field">
										<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
											Color
										</div>
										<ColorPanelDropdown
											colorPickerRef={ colorPickerRef }
											className={
												'wholesalex-style-formatting-field-content wholesalex-outside-click-whitelist'
											}
											value={ getTitleStyle( 'color' ) }
											onChange={ ( val ) =>
												setTitleStyle( 'color', val )
											}
										/>
									</div>
								</div>
							</div>
							<div className="wholesalex-form-title-style-column">
								<div className="wholesalex-form-title-style-column-heading wsx-font-14-normal">
									{ __( 'Description', 'wholesalex' ) }
								</div>
								<div className="wholesalex-form-title-style-row">
									<InputWithIncrementDecrement
										className={
											'wholesalex-form-title-font-size wholesalex-size-selector'
										}
										label={ __(
											'Font Size',
											'wholesalex'
										) }
										value={ getDescriptionStyle( 'size' ) }
										onChange={ ( val ) => {
											setDescriptionStyle( 'size', val );
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<FontWeightSelect
										label={ __(
											'Font Weight',
											'wholesalex'
										) }
										getValue={ () =>
											getDescriptionStyle( 'weight' )
										}
										setValue={ ( val ) => {
											setDescriptionStyle(
												'weight',
												val
											);
										} }
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<TextTransformControl
										label={ __(
											'Font Case',
											'wholesalex'
										) }
										getValue={ () =>
											getDescriptionStyle( 'transform' )
										}
										setValue={ ( val ) =>
											setDescriptionStyle(
												'transform',
												val
											)
										}
									/>
								</div>
								<div className="wholesalex-form-title-style-row">
									<div className="wholesalex-style-formatting-field">
										<div className="wholesalex-style-formatting-field-label wsx-font-12-normal">
											Color
										</div>
										<ColorPanelDropdown
											colorPickerRef={ colorPickerRef }
											className={
												'wholesalex-style-formatting-field-content wholesalex-outside-click-whitelist'
											}
											value={ getDescriptionStyle(
												'color'
											) }
											onChange={ ( val ) =>
												setDescriptionStyle(
													'color',
													val
												)
											}
											placement="right-start"
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};

	const isFormEmpty = () => {
		let status = false;
		if ( registrationForm.registrationFields ) {
			if ( registrationForm.registrationFields.length === 0 ) {
				status = true;
			} else if ( registrationForm.registrationFields.length === 1 ) {
				status =
					registrationForm.registrationFields[ 0 ].columns.length ===
					0;
			}
		} else {
			status = true;
		}

		return status;
	};

	return (
		<div
			className={ `wholesalex-registration-form ${
				isFormEmpty() ? '' : 'wsx-not-empty'
			}` }
		>
			{ registrationForm?.settings?.isShowFormTitle && (
				<div
					className={ `wsx-reg-form-heading ${
						isActive ? 'wsx-row-active' : ''
					}` }
					onMouseEnter={ () =>
						! headingRef?.current && setActive( true )
					}
					onMouseLeave={ () =>
						! headingRef?.current && setActive( false )
					}
				>
					<div className="wsx-reg-form-heading-text wsx-editable">
						<input
							type="text"
							className="wsx-editable-area"
							value={
								registrationForm?.registrationFormHeader?.title
							}
							onChange={ ( e ) => {
								setRegistrationForm( {
									type: 'updateRegistrationFormTitle',
									value: e.target.value.replace(
										/["']/g,
										''
									),
								} );
							} }
						/>
						{ isActive && (
							<span className="wsx-editable-edit-icon">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="12"
									height="12"
									viewBox="0 0 14 14"
									fill="none"
								>
									<path
										d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
										stroke="white"
										strokeLinecap="round"
										strokeLinejoin="round"
									/>
									<path
										d="M1.75 12.8333H12.25"
										stroke="white"
										strokeLinecap="round"
										strokeLinejoin="round"
									/>
								</svg>
							</span>
						) }
					</div>
					{ ! registrationForm?.registrationFormHeader
						?.isHideDescription && (
						<div className="wholesalex-registration-form-subtitle-text wsx-editable">
							<input
								type="text"
								className="wsx-editable-area"
								value={
									registrationForm?.registrationFormHeader
										?.description
								}
								onChange={ ( e ) => {
									setRegistrationForm( {
										type: 'updateRegistrationFormDescription',
										value: e.target.value.replace(
											/["']/g,
											''
										),
									} );
								} }
							/>
							{ isActive && (
								<span className="wsx-editable-edit-icon">
									<svg
										xmlns="http://www.w3.org/2000/svg"
										width="12"
										height="12"
										viewBox="0 0 14 14"
										fill="none"
									>
										<path
											d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
											stroke="white"
											strokeLinecap="round"
											strokeLinejoin="round"
										/>
										<path
											d="M1.75 12.8333H12.25"
											stroke="white"
											strokeLinecap="round"
											strokeLinejoin="round"
										/>
									</svg>
								</span>
							) }
						</div>
					) }
					<FormBuilderDropdown
						renderToogle={ ( status, setStatus ) => {
							return (
								<span
									className={ `dashicons dashicons-admin-generic ${
										status ? 'is-active wsx-active' : ''
									}` }
									onClick={ () => setStatus( ! status ) }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											setStatus( ! status );
										}
									} }
									role="button"
									tabIndex="0"
								></span>
							);
						} }
						renderContent={ ( status, setStatus ) => {
							return formTitleSettingPopupContent( setStatus );
						} }
						whitelistRef={ colorPickerRef }
						className={ 'wsx-header-popup_control' }
						placement="bottom"
						offset={ 80 }
					/>
				</div>
			) }
			<div
				className={ `wholesalex-fields-wrapper wsx_${ registrationForm.settings.inputVariation }` }
			>
				{ renderRows() }
			</div>
			<FieldInserter
				type={ 'newFieldRow' }
				setUpgradeProPopupStatus={ setUpgradeProPopupStatus }
			/>
			<div className="wsx-form-btn-wrapper">
				<button
					className={ `button wholesalex-btn wsx-register-btn wsx-editable ${ registrationForm.styles?.layout?.button?.align }` }
				>
					<input
						type="text"
						className="wsx-editable-area"
						value={
							registrationForm?.registrationFormButton?.title
						}
						onChange={ ( e ) => {
							setRegistrationForm( {
								type: 'updateRegistrationButtonText',
								value: e.target.value.replace( /["']/g, '' ),
							} );
						} }
					/>
					<span className="wsx-editable-edit-icon">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="12"
							height="12"
							viewBox="0 0 14 14"
							fill="none"
						>
							<path
								d="M8.16667 1.16666L10.5 3.5L4.08333 9.91666H1.75V7.58333L8.16667 1.16666Z"
								stroke="white"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M1.75 12.8333H12.25"
								stroke="white"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</span>
				</button>
			</div>
		</div>
	);
};

export default RegistrationFormBuilder;
