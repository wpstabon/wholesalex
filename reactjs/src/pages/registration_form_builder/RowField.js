import React, { useContext, useState, useRef } from 'react';
import ColumnField from './ColumnField';
import FieldInserter from './FieldInserter';
import { RegistrationFormContext } from '../../context/RegistrationFormContext';

const RowField = ( {
	row,
	index,
	handleDragEnter,
	handleDragStart,
	rowRef,
	activeRow,
	setActiveRow,
	settingRef,
	setUpgradeProPopupStatus,
} ) => {
	const { setRegistrationForm } = useContext( RegistrationFormContext );

	const icons = {
		drag: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<path
					d="M4.14341 6.85839L2 9.0018L4.14341 11.1452M6.85839 4.14341L9.0018 2L11.1452 4.14341M11.1452 13.9316L9.0018 16.075L6.85839 13.9316M13.9316 6.85839L16.075 9.0018L13.9316 11.1452M2.78592 9.0018H15.2177M9.0018 2.71447V15.2891"
					stroke="#343A46"
					strokeWidth="1.3"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
			</svg>
		),
		delete: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<path
					d="M6.76247 2.42324C6.86385 2.03514 7.18817 1.76758 7.5572 1.76758H10.4429C10.812 1.76758 11.1363 2.03514 11.2377 2.42324L11.4855 3.37197C11.5869 3.76006 11.9112 4.02762 12.2802 4.02762H14.1659C14.5082 4.02762 14.7858 4.33118 14.7858 4.70564C14.7858 5.08009 14.5082 5.38365 14.1659 5.38365H3.83425C3.49189 5.38365 3.21436 5.08009 3.21436 4.70564C3.21436 4.33118 3.49189 4.02762 3.83425 4.02762H5.7199C6.08893 4.02762 6.41325 3.76006 6.51463 3.37197L6.76247 2.42324Z"
					fill="#343A46"
				/>
				<path
					d="M4.04089 6.28767H8.5868V13.0678C8.5868 13.3174 8.77182 13.5198 9.00007 13.5198C9.22832 13.5198 9.41333 13.3174 9.41333 13.0678V6.28767H13.9593L12.9144 14.6681C12.8027 15.564 12.1032 16.2319 11.2765 16.2319H6.72363C5.89697 16.2319 5.19742 15.564 5.08573 14.6681L4.04089 6.28767Z"
					fill="#343A46"
				/>
			</svg>
		),
		singleColumn: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="18"
				height="18"
				viewBox="0 0 18 18"
				fill="none"
			>
				<path d="M1 1H17V4H1V1Z" fill="#343A46" />
				<path d="M1 7.5H17V10.5H1V7.5Z" fill="#343A46" />
				<path d="M1 14H17V17H1V14Z" fill="#343A46" />
			</svg>
		),
		doubleColumn: (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="30"
				height="18"
				viewBox="0 0 30 18"
				fill="none"
			>
				<path d="M1 1H13V4H1V1Z" fill="#343A46" />
				<path d="M1 7.5H13V10.5H1V7.5Z" fill="#343A46" />
				<path d="M1 14H13V17H1V14Z" fill="#343A46" />
				<path d="M17 1H29V4H17V1Z" fill="#343A46" />
				<path d="M17 7.5H29V10.5H17V7.5Z" fill="#343A46" />
				<path d="M17 14H29V17H17V14Z" fill="#343A46" />
			</svg>
		),
	};

	const [ isDragable, makeDragable ] = useState( false );

	const colRef = useRef();

	const rowSetting = () => {
		return (
			<div className="wsx-reg-form-row-setting">
				<span
					className="move-row-icon"
					onMouseEnter={ () => {
						makeDragable( true );
						rowRef.current = 'row_move';
					} }
					onMouseLeave={ () => {
						makeDragable( false );
						rowRef.current = null;
					} }
				>
					{ icons.drag }
				</span>
				{ row.columns[ 0 ].name !== 'user_email' &&
					row.columns[ 0 ].name !== 'user_pass' && (
						<span
							className={ `delete-row-icon` }
							onClick={ () => {
								setRegistrationForm( {
									type: 'deleteRow',
									index,
								} );
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setRegistrationForm( {
										type: 'deleteRow',
										index,
									} );
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ icons.delete }
						</span>
					) }
				<span
					className={ `single-column-icon ${
						row?.isMultiColumn ? '' : 'is-active'
					}` }
					onClick={ () => {
						setRegistrationForm( { type: 'makeOneColumn', index } );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setRegistrationForm( {
								type: 'makeOneColumn',
								index,
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ icons.singleColumn }
				</span>
				<span
					className={ `double-column-icon ${
						row?.isMultiColumn ? 'is-active' : ''
					}` }
					onClick={ () => {
						setRegistrationForm( { type: 'makeTwoColumn', index } );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setRegistrationForm( {
								type: 'makeOneColumn',
								index,
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ icons.doubleColumn }
				</span>
			</div>
		);
	};

	const isLeftColumnExist = () => {
		let status = false;
		row.columns.forEach( ( element ) => {
			status = element?.columnPosition === 'left';
		} );

		return status;
	};

	const isRightColumnExist = () => {
		let status = false;
		row.columns.forEach( ( element ) => {
			status = element?.columnPosition === 'right';
		} );

		return status;
	};

	const draggingColumnItem = useRef();
	const dragOverColumnItem = useRef();

	const handleColumnDragStart = ( e, position ) => {
		draggingColumnItem.current = position;
	};

	const handleColumnDragEnter = ( e, position ) => {
		if ( position === draggingColumnItem.current ) {
			return;
		}
		dragOverColumnItem.current = position;
		const _fields = [ ...row.columns ];
		const draggingColumnItemContent = _fields[ draggingColumnItem.current ];
		_fields.splice( draggingColumnItem.current, 1 );
		_fields.splice(
			dragOverColumnItem.current,
			0,
			draggingColumnItemContent
		);
		draggingColumnItem.current = dragOverColumnItem.current;
		dragOverColumnItem.current = null;
		setRegistrationForm( {
			type: 'updateColumns',
			index,
			updatedColumns: _fields,
		} );
	};

	return (
		( row.columns.length ||
			( ! row.columns.length && row?.isMultiColumn ) ) && (
			<div
				className={ `wsx-reg-form-row-wrapper ${
					activeRow === index ? 'wsx-row-active' : ''
				}` }
				draggable={ isDragable }
				onDragStart={ ( e ) =>
					rowRef.current === 'row_move' && handleDragStart( e, index )
				}
				onDragOver={ ( e ) => e.preventDefault() }
				onDragEnter={ ( e ) =>
					rowRef.current === 'row_move' && handleDragEnter( e, index )
				}
				onDragEnd={ ( e ) => {
					e.preventDefault();
				} }
				onMouseEnter={ () =>
					! settingRef.current && setActiveRow( index )
				}
				onMouseLeave={ () =>
					! settingRef.current && setActiveRow( -1 )
				}
			>
				{ activeRow === index && rowSetting() }
				<div
					className={ `wsx-reg-form-row ${
						row?.isMultiColumn ? 'double-column' : ''
					}` }
				>
					{ row.columns.length < 2 &&
						! isLeftColumnExist() &&
						row?.isMultiColumn && (
							<div className="wholesalex-registration-form-column">
								<FieldInserter
									type={ 'insertFieldOnLeftColumn' }
									index={ index }
								/>
							</div>
						) }
					{ row.columns.map( ( column, idx ) => {
						return (
							<ColumnField
								key={ idx }
								showDelete={ row.columns.length === 2 }
								column={ column }
								handleColumnDragEnter={ handleColumnDragEnter }
								handleColumnDragStart={ handleColumnDragStart }
								parentIndex={ index }
								columnIndex={ idx }
								colRef={ colRef }
								isRowActive={ activeRow === index }
								settingRef={ settingRef }
								setUpgradeProPopupStatus={
									setUpgradeProPopupStatus
								}
							/>
						);
					} ) }

					{ row.columns.length < 2 &&
						! isRightColumnExist() &&
						row?.isMultiColumn && (
							<>
								<span className="wholesalex-registration-form-column-spacer"></span>{ ' ' }
								<div className="wholesalex-registration-form-column wsx-empty-column">
									<FieldInserter
										type={ 'insertFieldOnRightColumn' }
										index={ index }
									/>
								</div>{ ' ' }
							</>
						) }
				</div>
			</div>
		)
	);
};

export default RowField;
