import React from 'react';
import Editor from './Editor';
import '../../assets/scss/FormBuilder.scss';
import { RegistrationFormContextProvider } from '../../context/RegistrationFormContext';

const RegistrationEditorBuilder = () => {
	return (
		<RegistrationFormContextProvider>
			<Editor />
		</RegistrationFormContextProvider>
	);
};

export default RegistrationEditorBuilder;
