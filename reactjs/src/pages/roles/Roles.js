import React, {
	Fragment,
	useState,
	useEffect,
	useContext,
	useRef,
} from 'react';
import { __ } from '@wordpress/i18n';
import Modal from '../../components/Modal';
import Tooltip from '../../components/Tooltip';
import Toast from '../../components/Toast';
import Import from './Import';
import Pagination from '../../components/Pagination';
import Search from '../../components/Search';
import { Link, useNavigate } from '../../contexts/RouterContext';
import { DataContext } from '../../contexts/DataProvider';
import Button from '../../components/Button';
import Select from '../../components/Select';
import Icons from '../../utils/Icons';
import Alert from '../../components/Alert';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';

const Roles = () => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	const [ fields, setFields ] = useState(
		wholesalex_overview.whx_roles_fields
	);
	const [ appState, setAppState ] = useState( {
		loading: false,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: {},
	} );
	const showRolesPerPage = 7;
	const [ roles, setRoles ] = useState( wholesalex_overview.whx_roles_data );
	const [ modalStatus, setModalStatus ] = useState( false );
	const [ overlayWindow, setOverlayWindow ] = useState( {
		status: false,
		type: '',
	} );

	const [ pageSize, setPageSize ] = useState( showRolesPerPage ); // Default page size
	const [ currentPage, setCurrentPage ] = useState( 0 ); // Current page index
	const [ paginatedRoles, setPaginatedRoles ] = useState( [] );

	//Bulk Action State
	const [ selectedRows, setSelectedRows ] = useState( [] );
	const [ bulkAction, setBulkAction ] = useState( '' );
	const [ alert, setAlert ] = useState( false );

	const [ isAlertVisible, setIsAlertVisible ] = useState( false );
	const [ pendingAction, setPendingAction ] = useState( null );

	//State for Searching Dynamic Rules Item Bse on Title
	const [ searchQuery, setSearchQuery ] = useState( '' );
	const [ filteredRules, setFilteredRules ] = useState( roles );

	const { dispatch } = useContext( ToastContexts );
	const { contextDataRole, setContextDataRole, contextData, setContextData } =
		useContext( DataContext );
	const { savedRole } = contextDataRole;
	const { globalCurrentPage } = contextData;
	const navigate = useNavigate();

	useEffect( () => {
		let filtered = roles;
		// Check if the search query is at least 3 characters long
		if ( searchQuery.length >= 3 ) {
			filtered = roles.filter(
				( role ) =>
					role._role_title &&
					role._role_title
						.toLowerCase()
						.includes( searchQuery.toLowerCase() )
			);
		}
		setFilteredRules( filtered );
	}, [ searchQuery, roles ] );

	useEffect( () => {
		const newRoles = roles;
		setContextDataRole( ( prevData ) => ( {
			...prevData,
			...prevData,
			newRoles,
		} ) );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ roles ] );

	useEffect( () => {
		if ( savedRole && savedRole.length > 0 ) {
			setRoles( savedRole );
		} else {
			setRoles( roles );
		}
	}, [ savedRole, roles ] );

	const fetchData = async (
		type = 'get',
		_id = '',
		_role = '',
		_key = ''
	) => {
		const attr = {
			type,
			action: 'role_action',
			nonce: wholesalex.nonce,
		};
		switch ( _key ) {
			case '_role_status':
				attr.check = _key;
				break;
			case 'delete':
				attr.delete = true;
				break;
		}

		const roleId = _role.id;

		if ( _key !== 'delete' && type === 'post' ) {
			if ( ! _role._role_title || _role._role_title === '' ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: __(
							'Please Fill Role Name Field',
							'wholesalex'
						),
					},
				} );
				return;
			}
		}
		if ( _id !== '' && _role !== '' && type === 'post' ) {
			attr.id = _id;
			attr.role = JSON.stringify( _role );

			const _state = { ...appState };
			_state.loadingOnSave[ _id ] = true;
			setAppState( { ...appState, ..._state } );
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/role_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( type === 'post' ) {
				if ( _id !== '' ) {
					const _state = { ...appState };
					_state.loadingOnSave[ _id ] = false;
					setAppState( { ...appState, ..._state } );
				}
			}
			if ( res.success ) {
				if ( type === 'get' ) {
					setFields( res.data.default );
					setAppState( {
						...appState,
						loading: false,
					} );
					setRoles( res.data.value );
				} else {
					switch ( _key ) {
						case 'delete':
							dispatch( {
								type: 'ADD_MESSAGE',
								payload: {
									id: Date.now().toString(),
									type: 'success',
									message: __(
										'Succesfully Deleted.',
										'wholesalex'
									),
								},
							} );
							if ( globalCurrentPage > 1 ) {
								setContextData( ( prevContext ) => ( {
									...prevContext,
									globalCurrentPage:
										prevContext.globalCurrentPage - 1,
								} ) );
							}
							setContextDataRole( ( prevData ) => {
								const updatedRoles = prevData.newRoles.filter(
									( role ) => role.id !== roleId
								);
								return { ...prevData, newRules: updatedRoles };
							} );
							break;
						default:
							dispatch( {
								type: 'ADD_MESSAGE',
								payload: {
									id: Date.now().toString(),
									type: 'success',
									message: __(
										'Succesfully Saved.',
										'wholesalex'
									),
								},
							} );
					}
				}
			}
		} );
	};

	const fetchSaveDuplicateRule = async ( dupRule ) => {
		// Prepare request attributes for saving rule
		const attr = {
			type: 'post',
			action: 'role_action',
			nonce: wholesalex.nonce,
			id: dupRule.id, // ID of the duplicated rule
			role: JSON.stringify( dupRule ), // Send duplicated rule as a JSON string
		};

		try {
			// Send the request to save the duplicated rule
			const response = await wp.apiFetch( {
				path: '/wholesalex/v1/role_action',
				method: 'POST',
				data: attr,
			} );

			if ( response.success ) {
				// Handle success and display success message
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message:
							__( 'Succesfully Saved.', 'wholesalex' ) ||
							'Duplicated rule saved successfully!',
					},
				} );
				return { success: true, data: dupRule };
			}
			// Handle failure and display error message from the response
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'error',
					message:
						response?.data?.message ||
						'Failed to save the duplicated rule',
				},
			} );
			return { success: false };
		} catch ( error ) {
			// Handle any network or unexpected errors
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'error',
					message: 'Error occurred while saving the duplicated rule',
				},
			} );
			return { success: false };
		}
	};

	const buttonHandler = ( type ) => {
		switch ( type ) {
			case 'save':
				// fetchData('post','__wholesalex_dynamic_rules',roles);
				break;
			case 'create':
				{
					const copy = [ ...roles ];
					const _newRule = {
						id: 'wholesalex_b2b_role_' + Date.now().toString(),
						label: 'Rule Title',
					};
					copy.push( _newRule );
					setRoles( copy );
					navigate( `/user-role/edit/${ _newRule.id }` );
					setContextData( ( prevContext ) => ( {
						...prevContext,
						globalNewItem: Array.isArray(
							prevContext.globalNewItem
						)
							? [ ...prevContext.globalNewItem, _newRule ]
							: [ _newRule ], // Fallback if it's not an array
					} ) );
				}

				break;
			default:
				break;
		}
	};

	// const buttonData = ( fieldsData, field ) => {
	// 	return (
	// 		<button
	// 			key={ field }
	// 			className={ `wholesalex-btn wholesalex-primary-btn wholesalex-btn-xlg ` }
	// 			onClick={ ( e ) => {
	// 				e.preventDefault();
	// 				buttonHandler( field );
	// 			} }
	// 		>
	// 			{ fieldsData.label }
	// 		</button>
	// 	);
	// };
	useEffect( () => {
		const start = currentPage * pageSize;
		const end = start + pageSize;
		setPaginatedRoles( filteredRules.slice( start, end ) );
	}, [ filteredRules, currentPage, pageSize ] );

	const handleGotoPage = ( pageIndex ) => {
		setCurrentPage( pageIndex );
	};

	const getExportUrl = () => {
		const url = new URL( window.location.href );

		const addParams = {
			nonce: wholesalex_overview?.whx_roles_nonce,
			action: 'export-roles-csv',
			exported_ids: selectedRows.join( ',' ),
		};

		const params = new URLSearchParams( url.search );
		if ( params.has( 'nonce' ) ) {
			params.delete( 'nonce' );
		}
		if ( params.has( 'action' ) ) {
			params.delete( 'action' );
		}

		const newParams = new URLSearchParams( [
			...Array.from( params.entries() ),
			...Object.entries( addParams ),
		] ).toString();

		const newUrl = `${ url.pathname }?${ newParams }`;

		return newUrl;
	};

	const buttonsData = ( fieldsData, field ) => {
		return (
			<div key={ field } className="wsx-justify-wrapper wsx-mb-40">
				<Button
					onClick={ ( e ) => {
						e.preventDefault();
						buttonHandler( 'create' );
					} }
					label={ __( 'Add New B2B Role', 'wholesalex' ) }
					background="primary"
					iconName="plus"
					customClass="wsx-font-16"
				/>
			</div>
		);
	};

	// Handle row selection (single or all rows)
	const handleRowSelection = ( rowId ) => {
		setSelectedRows( ( prevSelected ) =>
			prevSelected.includes( rowId )
				? prevSelected.filter( ( id ) => id !== rowId )
				: [ ...prevSelected, rowId ]
		);
	};

	// Handle selecting all rows
	const handleSelectAll = () => {
		if ( selectedRows.length === roles.length ) {
			setSelectedRows( [] );
		} else {
			setSelectedRows( roles.map( ( role ) => role.id ) );
		}
	};

	// Handle bulk action selection
	const handleBulkActionChange = ( e ) => {
		setBulkAction( e.target.value );
	};

	// Handle applying bulk action
	const handleApplyBulkAction = () => {
		if ( ! bulkAction || selectedRows.length === 0 ) {
			setAlert( true );
			return;
		}

		setPendingAction( bulkAction );
		setIsAlertVisible( true );
	};

	const handleDeleteRoles = ( selectedRows ) => {
		if ( selectedRows.length === 0 ) {
			alert( 'No roles selected' );
			return;
		}

		const attr = {
			roles: selectedRows,
			action: 'delete',
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/delete_roles',
			method: 'POST',
			data: attr,
		} )
			.then( ( res ) => {
				if ( res.success ) {
					// Optimistically update the front-end by removing selected roles
					const updatedFilteredRoles = roles.filter(
						( role ) => ! selectedRows.includes( role.id )
					);

					// Update the main rules state
					setRoles( updatedFilteredRoles );

					// Update paginated roles and handle pagination
					updatePaginatedRolesAfterDelete( updatedFilteredRoles );

					// Handle success actions
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: 'Roles successfully deleted',
						},
					} );

					setAppState( {
						...appState,
						loading: false,
					} );

					if ( globalCurrentPage > 1 ) {
						setContextData( ( prevContext ) => ( {
							...prevContext,
							globalCurrentPage:
								prevContext.globalCurrentPage - 1,
						} ) );
					}
				} else {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'error',
							message: 'Failed to delete roles',
						},
					} );
				}
			} )
			.catch( () => {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: 'Error deleting roles',
					},
				} );
			} );
	};

	const confirmBulkAction = () => {
		switch ( bulkAction ) {
			case 'delete':
				handleDeleteRoles( selectedRows );
				break;

			case 'export':
				window.location.href = getExportUrl();
				break;

			default:
		}

		setIsAlertVisible( false );
		setSelectedRows( [] );
	};

	const handleAlertClose = () => {
		setIsAlertVisible( false );
		setPendingAction( null );
		setAlert( false );
	};

	// Function to update paginated roles after deletion
	const updatePaginatedRolesAfterDelete = ( updatedFilteredRoles ) => {
		const start = currentPage * pageSize;
		const end = start + pageSize;
		const updatedPaginatedRoles = updatedFilteredRoles.slice( start, end );

		if ( updatedPaginatedRoles.length === 0 && currentPage > 0 ) {
			// If the current page is empty and there's a previous page, go back
			setCurrentPage( currentPage - 1 );

			// Calculate new start and end for the previous page
			const newStart = ( currentPage - 1 ) * pageSize;
			const newEnd = newStart + pageSize;
			setPaginatedRoles( updatedFilteredRoles.slice( newStart, newEnd ) );
		} else {
			setPaginatedRoles( updatedPaginatedRoles );
		}
	};
	const getStatusMessage = ( type ) => {
		switch ( type ) {
			case 'admin_approve':
				return 'Admin Approval Required';
			case 'global_setting':
				return 'Use Global Setting';
			case 'email_confirmation_require':
				return 'Email Confirmation Required';
			case 'auto_approve':
				return 'Automatically Approve Account';
			default:
				return 'Use Global Setting';
		}
	};

	const optionsData = [
		{ value: 'default', label: 'Select Bulk Action' },
		{ value: 'delete', label: 'Delete' },
		{ value: 'export', label: 'Export' },
	];

	const roleData = ( rolesData, section ) => {
		return (
			<>
				<div className="wsx-d-flex wsx-item-center wsx-gap-12 wsx-mb-24 wsx-justify-space wsx-sm-flex-wrap wsx-sm-justify-center">
					<div className="wsx-d-flex wsx-gap-8 wsx-item-center">
						<Select
							options={ optionsData }
							inputBackground="base1"
							borderColor="border-secondary"
							onChange={ handleBulkActionChange }
							minWidth="184px"
						/>
						<Button
							label={ __( 'Apply', 'wholesalex' ) }
							onClick={ handleApplyBulkAction }
							background="base2"
							borderColor="primary"
							color="primary"
							customClass="wsx-font-14"
							padding="11px 20px"
						/>
					</div>
					<div className="wsx-d-flex wsx-item-center wsx-gap-8">
						<Button
							label={ __( 'Import', 'wholesalex' ) }
							background="base2"
							iconName="import"
							onClick={ onImportClick }
							customClass="wsx-font-regular"
						/>

						<Search
							type="text"
							value={ searchQuery }
							onChange={ ( e ) =>
								setSearchQuery( e.target.value )
							}
							iconName="search"
							iconColor="#070707"
							background="transparent"
							borderColor="#868393"
							maxHeight="38px"
							placeholder="Search Role"
						/>
					</div>
				</div>
				<div className="wsx-row-wrapper wsx-roles-wrapper">
					<div className="wsx-row-container wsx-scrollbar">
						{ paginatedRoles.length === 0 ? (
							<div
								className="wsx-bg-base1 wsx-br-md wsx-d-flex wsx-item-center wsx-justify-center"
								style={ { height: '40vh', maxHeight: '370px' } }
							>
								{ Icons.noData }
							</div>
						) : (
							<div
								className={ `${
									wholesalex?.is_pro_active ? 'active' : ''
								} wsx-rules-header wsx-color-tertiary wsx-font-medium` }
							>
								<span
									className="wsx-rule-checkbox-with-title"
									style={ { minWidth: '320px' } }
								>
									<label
										className="wsx-label wsx-checkbox-option-wrapper"
										htmlFor="wsx"
									>
										{ ' ' }
										<input
											type="checkbox"
											checked={
												selectedRows.length ===
												roles.length
											}
											onChange={ handleSelectAll }
										/>
										<span className="wsx-checkbox-mark"></span>
									</label>
									{ __( 'Role Name', 'wholesalex' ) }
								</span>
								{ wholesalex?.is_pro_active && (
									<span
										className="wsx-role-limit"
										style={ { minWidth: '240px' } }
									>
										{ __( 'Credit Limit', 'wholesalex' ) }
									</span>
								) }
								<span
									className="wsx-role-registration"
									style={ { minWidth: '240px' } }
								>
									{ __( 'Approval Method', 'wholesalex' ) }
								</span>
								<span className="wsx-role-actions wsx-text-end">
									{ __( 'Action', 'wholesalex' ) }
								</span>
							</div>
						) }

						{ paginatedRoles.map( ( role, index ) => {
							let roleTitle;

							if ( role._role_title === 'B2C Users' ) {
								roleTitle = __( 'B2C Users', 'wholesalex' );
							} else if ( role._role_title === 'Guest Users' ) {
								roleTitle = __( 'Guest Users', 'wholesalex' );
							} else if ( role._role_title ) {
								roleTitle = role._role_title;
							} else {
								roleTitle = __( 'Untitled Role', 'wholesalex' );
							}
							const actualIndex = currentPage * pageSize + index;
							const isDeletable =
								role.id !== 'wholesalex_b2c_users' &&
								role.id !== 'wholesalex_guest';
							return (
								<div
									className="wsx-roles-row-wrapper"
									key={ index }
								>
									<div
										key={ `${ section }_${ actualIndex }` }
										id={ `dynamic_rule_${ role.id }` }
										className={ `${
											wholesalex?.is_pro_active
												? 'active'
												: ''
										} wsx-rules-row` }
									>
										<div
											className="wsx-rule-checkbox-with-title wsx-rule-item"
											style={ { minWidth: '320px' } }
										>
											{ isDeletable && (
												<label
													className="wsx-label wsx-checkbox-option-wrapper"
													htmlFor="wsx"
												>
													{ ' ' }
													<input
														type="checkbox"
														checked={ selectedRows.includes(
															role.id
														) }
														onChange={ () =>
															handleRowSelection(
																role.id
															)
														}
													/>
													<span className="wsx-checkbox-mark"></span>
												</label>
											) }

											<span
												className="wsx-ellipsis"
												style={ {
													paddingLeft:
														! isDeletable && '51px',
												} }
											>
												<Link
													to={ `/user-role/edit/${ role.id }` }
												>
													{ role.id !==
														'wholesalex_b2c_users' &&
														role.id !==
															'wholesalex_guest' && (
															<strong>
																{ __(
																	'B2B Role:',
																	'wholesalex'
																) }
															</strong>
														) }

													{ roleTitle }
												</Link>
											</span>
										</div>
										{ wholesalex?.is_pro_active && (
											<div
												className="wsx-role-limit wsx-rule-item"
												style={ { minWidth: '240px' } }
											>
												<span className="wsx-ellipsis">
													{ wholesalex_overview.whx_dr_currency +
														( role.credit_limit !==
															undefined &&
														role.credit_limit !==
															null
															? parseFloat(
																	role.credit_limit
															  ).toFixed( 2 )
															: '0.00' ) }
												</span>
											</div>
										) }
										<div
											className="wsx-role-registration wsx-rule-item"
											style={ { minWidth: '240px' } }
										>
											<span className="wsx-ellipsis">
												{ getStatusMessage(
													role.user_status
												) }
											</span>
										</div>
										<div className="wsx-rule-actions wsx-btn-group wsx-gap-8 wsx-justify-end">
											{ isDeletable && (
												<Tooltip
													content={ __(
														'Delete',
														'wholesalex'
													) }
												>
													<span
														className="wsx-btn-action"
														onClick={ ( e ) => {
															e.stopPropagation();
															setModalStatus( {
																status: true,
																actualIndex,
															} );
														} }
														onKeyDown={ ( e ) => {
															if (
																e.key ===
																	'Enter' ||
																e.key === ' '
															) {
																e.stopPropagation();
																setModalStatus(
																	{
																		status: true,
																		actualIndex,
																	}
																);
															}
														} }
														role="button"
														tabIndex="0"
													>
														{ Icons.delete }
													</span>
												</Tooltip>
											) }
											{ isDeletable && (
												<Tooltip
													content={ __(
														'Duplicate',
														'wholesalex'
													) }
												>
													<span
														className="wsx-btn-action"
														onClick={ async (
															e
														) => {
															e.stopPropagation();
															const copy = [
																...roles,
															];
															const dupRule = {
																...copy[
																	actualIndex
																],
															};
															let __roleTitle =
																dupRule._role_title;

															if (
																! __roleTitle
															) {
																__roleTitle =
																	__(
																		'Untitled',
																		'wholesalex'
																	);
															}

															( dupRule.id =
																'wholesalex_b2b_role_' +
																Date.now().toString() ),
																( dupRule._role_title =
																	__(
																		'Duplicate of',
																		'wholesalex'
																	) +
																	__roleTitle );

															const result =
																await fetchSaveDuplicateRule(
																	dupRule
																);
															if (
																result.success
															) {
																copy.splice(
																	actualIndex +
																		1,
																	0,
																	dupRule
																);
																setRoles(
																	copy
																);
															}
														} }
														onKeyDown={ async (
															e
														) => {
															if (
																e.key ===
																	'Enter' ||
																e.key === ' '
															) {
																e.stopPropagation();
																const copy = [
																	...roles,
																];
																const dupRule =
																	{
																		...copy[
																			actualIndex
																		],
																	};
																let __roleTitle =
																	dupRule._role_title;

																if (
																	! __roleTitle
																) {
																	__roleTitle =
																		__(
																			'Untitled',
																			'wholesalex'
																		);
																}

																( dupRule.id =
																	'wholesalex_b2b_role_' +
																	Date.now().toString() ),
																	( dupRule._role_title =
																		__(
																			'Duplicate of',
																			'wholesalex'
																		) +
																		__roleTitle );

																const result =
																	await fetchSaveDuplicateRule(
																		dupRule
																	);
																if (
																	result.success
																) {
																	copy.splice(
																		actualIndex +
																			1,
																		0,
																		dupRule
																	);
																	setRoles(
																		copy
																	);
																}
															}
														} }
														role="button"
														tabIndex="0"
													>
														{ Icons.copy }
													</span>
												</Tooltip>
											) }
											<Tooltip
												content={ __(
													'Edit',
													'wholesalex'
												) }
											>
												<Link
													className="wsx-lh-0"
													to={ `/user-role/edit/${ role.id }` }
												>
													<span className="wsx-btn-action">
														{ Icons.edit }
													</span>
												</Link>
											</Tooltip>
										</div>
									</div>
									{ modalStatus &&
										modalStatus.actualIndex ===
											actualIndex && (
											<Modal
												title={
													role._role_title
														? role._role_title
														: __(
																'Untitled Role',
																'wholesalex'
														  )
												}
												status={ modalStatus }
												setStatus={ setModalStatus }
												onDelete={ () => {
													let copy = [ ...roles ];
													copy = copy.filter(
														( row, r ) => {
															return (
																actualIndex !==
																r
															);
														}
													);
													setRoles( copy );
													setModalStatus( false );
													fetchData(
														'post',
														role.id,
														copy,
														'delete'
													);
												} }
												smallModal={ true }
											/>
										) }
								</div>
							);
						} ) }
					</div>
					{
						<Pagination
							gotoPage={ handleGotoPage }
							length={ filteredRules.length }
							pageSize={ pageSize }
							setPageSize={ setPageSize }
							items={ roles }
							showItemsPerPage={ showRolesPerPage }
						/>
					}
				</div>
				{ alert && (
					<Alert
						title="Please Select Properly!"
						description="Please select role from the list and an action to perform bulk action."
						onClose={ handleAlertClose }
					/>
				) }
				{ isAlertVisible && (
					<Alert
						title="Confirm Bulk Action"
						description={ `Are you sure you want to ${ pendingAction } the selected ${
							selectedRows.length === 1 ? 'role' : 'roles'
						}?` }
						onClose={ handleAlertClose }
						onConfirm={ confirmBulkAction }
					/>
				) }
			</>
		);
	};
	const ref = useRef( null );

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const toggleOverlayWindow = ( e, type ) => {
		if ( overlayWindow.status ) {
			const style = ref?.current?.style;
			if ( ! style ) {
				return;
			}
			sleep( 200 ).then( () => {
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = `translateX(${ isRTL ? '-50%' : '50%' })`;
				style.opacity = '0';

				sleep( 300 ).then( () => {
					setOverlayWindow( { ...overlayWindow, status: false } );
				} );
			} );
		} else {
			setOverlayWindow( { ...overlayWindow, type, status: true } );
			setTimeout( () => {
				const style = ref?.current?.style;
				if ( ! style ) {
					return;
				}
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = 'translateX(0%)';
				style.opacity = '1';
			}, 10 );
		}
	};

	const onImportClick = ( e ) => {
		toggleOverlayWindow( e, 'import' );
	};

	const renderOverlayWindow = () => {
		if ( ! overlayWindow.status ) {
			return;
		}
		return (
			<>
				{ overlayWindow.type === 'import' && (
					<Import
						toggleOverlayWindow={ toggleOverlayWindow }
						overlayWindowStatus={ overlayWindow.status }
						windowRef={ ref }
						setRoles={ setRoles }
					/>
				) }{ ' ' }
			</>
		);
	};
	return (
		<Fragment>
			<div className="wsx-wrapper">
				<div className="wsx-container">
					{ appState.loading && <LoadingGif /> }
					{ ! appState.loading &&
						Object.keys( fields ).map( ( sections ) => {
							switch ( fields[ sections ].type ) {
								case 'buttons':
									return buttonsData(
										fields[ sections ],
										sections
									);
								case 'role':
									return roleData(
										fields[ sections ],
										sections
									);
								default:
									return null;
							}
						} ) }

					<div className="wholesalex-role-wrapper wholesalex_user_roles">
						{ <Toast delay={ 5000 } position="top_right" /> }
					</div>
					{ renderOverlayWindow() }
				</div>
			</div>
		</Fragment>
	);
};
export default Roles;
