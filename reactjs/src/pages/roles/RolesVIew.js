import React from 'react';
import ReactDOM from 'react-dom';
import Roles from './Roles';

const RolesVIew = () => (
	<React.StrictMode>
		<Roles />
	</React.StrictMode>
);

export default RolesVIew;

// If you still want to render it on DOMContentLoaded as well
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains( document.getElementById( '_wholesalex_role' ) )
	) {
		ReactDOM.render(
			<WholesaleXRoleComponent />,
			document.getElementById( '_wholesalex_role' )
		);
	}
} );
