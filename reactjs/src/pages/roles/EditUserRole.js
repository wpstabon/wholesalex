import React, { useContext, useEffect, useState } from 'react';
import Role from './Role';
import Toast from '../../components/Toast';
import { __ } from '@wordpress/i18n';
import { DataContext } from '../../contexts/DataProvider';
import { useNavigate } from '../../contexts/RouterContext';
import Button from '../../components/Button';
import './Role.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const EditUserRole = ( { id } ) => {
	const [ fields ] = useState( wholesalex_overview.whx_roles_fields );
	const [ roles, setRoles ] = useState( wholesalex_overview.whx_roles_data );
	const [ fieldsData, setFieldsData ] = useState( null );
	const [ tempRole ] = useState( {} );

	const { contextData, contextDataRole, setContextDataRole } =
		useContext( DataContext );
	const { newRoles } = contextDataRole;
	const { dispatch } = useContext( ToastContexts );
	const { globalNewItem } = contextData;

	const navigate = useNavigate();

	const [ appState, setAppState ] = useState( {
		loading: false,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: {},
	} );

	const roleExists = roles.some( ( role ) => role.id === id );
	//If the role does not exist, redirect to the user-role page
	useEffect( () => {
		if (
			Array.isArray( globalNewItem ) &&
			globalNewItem.length === 0 &&
			! roleExists
		) {
			navigate( '/user-role' );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ globalNewItem ] );

	useEffect( () => {
		if ( newRoles && newRoles.length > 0 ) {
			setRoles( newRoles );
		} else {
			setRoles( roles );
		}
	}, [ newRoles, roles ] );

	useEffect( () => {
		const savedRole = roles;
		setContextDataRole( ( prevData ) => ( {
			...prevData,
			...prevData,
			savedRole,
		} ) );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ roles ] );

	useEffect( () => {
		Object.keys( fields ).forEach( ( sections ) => {
			if ( fields[ sections ].type === 'role' ) {
				setFieldsData( fields[ sections ] );
			}
		} );
	}, [ fields ] );

	const SaveFetchData = async ( type = 'post', _id = '', _role = '' ) => {
		const attr = {
			type,
			action: 'role_action',
			nonce: wholesalex.nonce,
		};

		if ( ! _role._role_title || _role._role_title === '' ) {
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'error',
					message: __( 'Please Fill Role Name Field', 'wholesalex' ),
				},
			} );
			return;
		}

		if ( _id !== '' && _role !== '' && type === 'post' ) {
			attr.id = _id;
			attr.role = JSON.stringify( _role );

			const _state = { ...appState };
			_state.loadingOnSave[ _id ] = true;
			setAppState( { ...appState, ..._state } );
		}

		wp.apiFetch( {
			path: '/wholesalex/v1/role_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( type === 'post' ) {
				if ( _id !== '' ) {
					const _state = { ...appState };
					_state.loadingOnSave[ _id ] = false;
					setAppState( { ...appState, ..._state } );
				}
			}

			if ( res.success ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: __( 'Succesfully Saved.', 'wholesalex' ),
					},
				} );
			}
		} );
	};

	const handleUserRoleBackBtn = () => {
		navigate( '/user-role' );
	};

	const onSaveAction = ( index ) => {
		const parent = [ ...roles ];
		parent[ index ] = { ...parent[ index ], ...tempRole };
		setRoles( parent );
		const _role = parent[ index ];
		if ( _role._role_title && _role._role_title.trim() !== '' ) {
			SaveFetchData( 'post', parent[ index ].id, parent[ index ] );
		}
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				{ roles.map(
					( role, index ) =>
						fieldsData &&
						role.id === id && (
							<>
								<div className="wsx-d-flex wsx-item-center wsx-justify-space wsx-mb-40">
									<Button
										iconName="arrowLeft_24"
										background="base2"
										padding="8px"
										onClick={ handleUserRoleBackBtn }
									/>
									<div className="wsx-d-flex wsx-item-center wsx-gap-32">
										<Button
											label={ __( 'Save', 'wholesalex' ) }
											onClick={ () =>
												onSaveAction( index )
											}
											background="positive"
											customClass="wsx-font-14"
										/>
									</div>
								</div>
								<div className="wsx-card wsx-edit-role-wrapper">
									<Role
										key={ id }
										fields={ fieldsData }
										roles={ roles }
										setRoles={ setRoles }
										index={ index }
										onSave={ SaveFetchData }
										appState={ appState }
										roleId={ id }
									/>
								</div>
							</>
						)
				) }
			</div>
			<Toast delay={ 3000 } position="top_right" />
		</div>
	);
};

export default EditUserRole;
