import React from 'react';
import ReactDOM from 'react-dom';
import Roles from './Roles';
import Header from '../../components/Header';
import { ToastContextProvider } from '../../context/ToastsContexts';
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains( document.getElementById( '_wholesalex_role' ) )
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ whx_roles.i18n.user_roles } />
					<Roles />
				</ToastContextProvider>
			</React.StrictMode>,
			document.getElementById( '_wholesalex_role' )
		);
	}
} );
