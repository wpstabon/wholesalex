import React, { useContext, useEffect, useState } from 'react';
import MultiSelect from '../../components/MultiSelect';
import { __ } from '@wordpress/i18n';
import Input from '../../components/Input';
import CheckboxContainer from '../../components/CheckboxContainer';
import RadioButtons from '../../components/RadioButtons';
import Switch from '../../components/Switch';
import { DataContext } from '../../contexts/DataProvider';

import Icons from '../../utils/Icons';
import Slider from '../../components/Slider';

const Role = ( { fields, roles, setRoles, index, roleId } ) => {
	const [ tempRole, setTempRole ] = useState( { ...roles[ index ] } );
	const [ shippingZoneStatus, setShippingZoneStatus ] = useState( false );
	const [ users, setUsers ] = useState( [] );
	const { setContextDataRole } = useContext( DataContext );

	useEffect( () => {
		const parent = [ ...roles ];
		parent[ index ] = { ...parent[ index ], ...tempRole };
		setRoles( parent );
		const savedRole = parent;
		setContextDataRole( savedRole );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ tempRole ] );

	const setDefaultValue = ( fieldName, sectionName, defaultValue ) => {
		const _roleStatus = { ...tempRole };
		let defVal = defaultValue;
		if ( sectionName ) {
			if ( ! _roleStatus[ sectionName ][ fieldName ] ) {
				_roleStatus[ sectionName ][ fieldName ] = defVal;
			}
			defVal = _roleStatus[ sectionName ][ fieldName ];
		} else {
			if ( ! _roleStatus[ fieldName ] ) {
				_roleStatus[ fieldName ] = defVal;
			}
			defVal = _roleStatus[ fieldName ];
		}
		return defVal;
	};

	/**
	 * B2C and Guest Users Role Name Change Disable and excludes field option added.
	 *
	 * @since 1.0.4
	 */

	const inputData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		let isExclude = false;
		if ( fieldData.excludes ) {
			fieldData.excludes.forEach( ( element ) => {
				if ( tempRole.id === element ) {
					isExclude = true;
				}
			} );
		}

		const onChangeHandler = ( e ) => {
			const copy = { ...tempRole };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = e.target.value;
			} else {
				copy[ fieldName ] = e.target.value;
			}
			if ( fieldName === '_role_title' ) {
				const parent = [ ...roles ];
				parent[ index ]._role_title = e.target.value;
				setRoles( parent );
			}
			setTempRole( copy );
		};

		return (
			! isExclude && (
				<Input
					inputClass="wsx-bg-base1"
					key={ `${ fieldName }` }
					label={ fieldData.label }
					type={ fieldData.type }
					id={ `${ fieldName }` }
					name={ fieldName }
					value={ fieldName === 'role_id' ? tempRole.id : defValue }
					onChange={ onChangeHandler }
					isDisable={ fieldName === 'role_id' }
				/>
			)
		);
	};

	const titleSectionData = ( sectionData, sectionName ) => {
		return (
			<div className="wsx-role-title-wrapper" key={ sectionName }>
				{ Object.keys( sectionData ).map( ( field ) => {
					switch ( sectionData[ field ].type ) {
						case 'text':
						case 'number':
						case 'url':
							return inputData( sectionData[ field ], field );
						default:
							return null;
					}
				} ) }
			</div>
		);
	};

	const creditLimitSection = ( sectionData, sectionName ) => {
		return (
			<div key={ sectionName } className="wsx-role-credit-wrapper">
				{ Object.keys( sectionData ).map( ( field ) => {
					const _data = sectionData[ field ];
					switch ( _data.type ) {
						case 'number':
						case 'text':
							return inputData( _data, field );
						default:
							return null;
					}
				} ) }
			</div>
		);
	};

	const checkboxData = ( fieldData, field ) => {
		const defValue = tempRole[ field ] || fieldData.default || [];
		let _final = [ ...defValue ];

		const onChangeHandler = ( e ) => {
			const copy = { ...tempRole };
			if ( e.target.checked ) {
				_final = _final.concat( e.target.value );
			} else {
				_final = _final.filter( ( item ) => item !== e.target.value );
			}
			copy[ field ] = _final;
			setTempRole( copy );
		};
		return (
			<CheckboxContainer
				label={ fieldData.label }
				options={ fieldData.options }
				name={ field }
				value={ defValue }
				className=""
				onChange={ onChangeHandler }
			/>
		);
	};

	const radioData = ( fieldData, field ) => {
		// const uniqueID = Math.floor( Math.random() * Date.now() );
		const defValue = tempRole[ field ] || fieldData.default || '';
		const onChangeHandler = ( e ) => {
			let _name = e.target.name.split( '#' );
			_name = _name[ _name.length - 1 ];
			setTempRole( { ...tempRole, [ _name ]: e.target.value } );
		};

		return (
			<RadioButtons
				className="wsx-user-role-radio-field"
				label={ fieldData.label }
				options={ fieldData.options }
				name={ `role_id_${ roles[ index ].id }#${ field }` }
				id={ `role_id_${ roles[ index ].id }#${ field }` }
				value={ defValue }
				onChange={ onChangeHandler }
				defaultValue={ defValue }
				flexView={ true }
			/>
		);
	};

	const switchData = ( fieldData, field ) => {
		let defValue;

		if ( tempRole[ field ] ) {
			if ( tempRole[ field ] === 'yes' ) {
				defValue = true;
			} else {
				defValue = false;
			}
		} else {
			defValue = fieldData.default;
		}
		let isExclude = false;
		if ( fieldData.excludes ) {
			fieldData.excludes.forEach( ( element ) => {
				if ( tempRole.id === element ) {
					isExclude = true;
				}
			} );
		}

		const onChangeHandler = ( e ) => {
			const _final = e.target.checked ? 'yes' : 'no';
			let _name = e.target.name.split( '#' );
			_name = _name[ _name.length - 1 ];
			setTempRole( { ...tempRole, [ _name ]: _final } );
		};
		return (
			! isExclude && (
				<Switch
					label={ fieldData.label }
					options={ fieldData.options }
					name={ `role_id_${ roles[ index ].id }#${ field }` }
					id={ `role_id_${ roles[ index ].id }#${ field }` }
					value={ defValue }
					onChange={ onChangeHandler }
					defaultValue={ defValue }
					desc={ fieldData.desc }
				/>
			)
		);
	};
	const checkboxDataPayment = ( fieldData, field ) => {
		field = '_payment_methods';
		fieldData = fieldData._payment_methods;
		const defValue = tempRole[ field ] || fieldData.default || [];
		const sliderClass =
			field === '_auto_role_migration' || field === '_subaccount_status'
				? 'wsx-slider-sm'
				: 'wsx-slider-md';
		let _final = [ ...defValue ];
		const onChangeHandler = ( e, option ) => {
			const copy = { ...tempRole };
			if ( e.target.checked ) {
				_final = _final.concat( option );
			} else {
				_final = _final.filter( ( item ) => item !== option );
			}
			copy[ field ] = _final;
			setTempRole( copy );
		};

		return (
			<div>
				{ fieldData.label && (
					<div className="wsx-radio-field-label wsx-mb-32 wsx-font-medium wsx-color-text-medium">
						{ fieldData.label }
					</div>
				) }
				<div className="wsx-slider-line-container">
					{ Object.keys( fieldData.options ).map( ( option ) => {
						return (
							<Slider
								name={ option }
								key={ option }
								label={ fieldData.options[ option ] }
								isLabelSide={ true }
								value={ defValue?.includes( option ) }
								onChange={ ( e ) =>
									onChangeHandler( e, option )
								}
								className={ sliderClass }
							/>
						);
					} ) }
				</div>
			</div>
		);
	};
	const sliderData = ( fieldData, field ) => {
		const sliderClass =
			field === '_auto_role_migration' || field === '_subaccount_status'
				? 'wsx-slider-sm'
				: 'wsx-slider-md';
		let isExclude = false;
		if ( fieldData.excludes ) {
			fieldData.excludes.forEach( ( element ) => {
				if ( tempRole.id === element ) {
					isExclude = true;
				}
			} );
		}

		const onChangeHandler = ( e ) => {
			const _final = e.target.checked ? 'yes' : 'no';
			let _name = e.target.name.split( '#' );
			_name = _name[ _name.length - 1 ];
			setTempRole( { ...tempRole, [ _name ]: _final } );
		};
		return (
			! isExclude && (
				<>
					{ fieldData.label && (
						<div className="wsx-radio-field-label wsx-mb-32 wsx-font-medium wsx-color-text-medium">
							{ fieldData.label }
						</div>
					) }
					<div className="wsx-slider-line-container">
						{ Object.keys( fieldData.options ).map(
							( option, i ) => {
								return (
									<Slider
										key={ i }
										name={ option }
										label={ fieldData.options[ option ] }
										isLabelSide={ true }
										value={ tempRole[ option ] === 'yes' }
										onChange={ ( e ) =>
											onChangeHandler( e )
										}
										className={ sliderClass }
									/>
								);
							}
						) }
					</div>
				</>
			)
		);
	};

	const getUserByRoleID = ( id ) => {
		const attr = {
			type: 'get_users_by_role_id',
			action: 'role_action',
			nonce: wholesalex.nonce,
			id,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/role_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.success ) {
				setUsers( res.data );
			}
		} );
	};

	const multiselectData = ( fieldData, fieldName ) => {
		const defValue = setDefaultValue( fieldName, '', fieldData.default );

		let isExclude = false;
		if ( fieldData.excludes ) {
			fieldData.excludes.forEach( ( element ) => {
				if ( tempRole.id === element ) {
					isExclude = true;
				}
			} );
		}
		return (
			! isExclude && (
				<div
					key={ `rule-field_${ fieldName }` }
					className="multiselect"
				>
					{ fieldData.label && (
						<label
							className="wsx-label wsx-input-label wsx-font-medium"
							htmlFor={ fieldName }
						>
							{ fieldData.label }
						</label>
					) }
					<MultiSelect
						name={ fieldName }
						value={ defValue }
						options={ users }
						placeholder={ fieldData.placeholder }
						isAjax={ true }
						onMultiSelectChangeHandler={ (
							name,
							selectedValues
						) => {
							const _temp = { ...tempRole };

							_temp[ name ] = [ ...selectedValues ];

							setTempRole( _temp );
						} }
					/>
				</div>
			)
		);
	};

	const dynamicSectionData = ( sectionData, sectionName ) => {
		const customClass = 'wsx-card-2 wsx-card-border';
		let content;

		if ( sectionName === 'regi_url_form_section' ) {
			content = (
				<div className="wsx-user-role-url-fields" key={ sectionName }>
					{ Object.keys( sectionData ).map( ( field ) => {
						if ( sectionData[ field ].type === 'radio' ) {
							return radioData( sectionData[ field ], field );
						} else if ( sectionData[ field ].type === 'url' ) {
							return inputData( sectionData[ field ], field );
						}
						return null;
					} ) }
				</div>
			);
		} else if (
			sectionName === 'settings_combined_subaccount_field' ||
			sectionName === 'settings_combined_migration_field'
		) {
			content = (
				<div className={ customClass } key={ sectionName }>
					{ Object.keys( sectionData ).map( ( field ) => {
						if ( sectionData[ field ].type === 'switch' ) {
							return switchData( sectionData[ field ], field );
						} else if ( sectionData[ field ].type === 'number' ) {
							return inputData( sectionData[ field ], field );
						} else if (
							sectionData[ field ].type === 'multiselect'
						) {
							return multiselectData(
								sectionData[ field ],
								field
							);
						} else if ( sectionData[ field ].type === 'slider' ) {
							return sliderData( sectionData[ field ], field );
						}
						return null;
					} ) }
				</div>
			);
		} else {
			content = (
				<div
					key={ sectionName }
					className={ `wsx-${ sectionName } ${
						sectionName === 'settings_section'
							? 'wsx-slider-line-container'
							: ''
					}` }
				>
					{ Object.keys( sectionData ).map( ( field ) => {
						const _data = sectionData[ field ];
						switch ( _data.type ) {
							case 'checkbox':
								return checkboxData( _data, field );
							case 'radio':
								return radioData( _data, field );
							case 'switch':
								return switchData( _data, field );
							case 'slider':
								return sliderData( _data, field );
							case 'number':
							case 'text':
								return inputData( _data, field );
							case 'multiselect':
								return multiselectData( _data, field );
							default:
								return null;
						}
					} ) }
				</div>
			);
		}
		return <>{ content }</>;
	};

	const shippingZoneData = ( sectionData, sectionName ) => {
		return (
			<div
				key={ `wholesalex_${ sectionName }` }
				className={ `wsx-shipping-zone-row` }
			>
				<div
					className="wsx-shipping-zone-title"
					key={ `wsx-role-header-${ sectionName }` }
				>
					{ sectionData.label }
				</div>
				{ Object.keys( sectionData.attr ).map( ( field ) => {
					const _data = sectionData.attr[ field ];
					switch ( _data.type ) {
						case 'checkbox':
							return checkboxData( _data, field );
						default:
							return null;
					}
				} ) }
			</div>
		);
	};

	const shippingSectionData = ( sectionsData, sectionsName ) => {
		return (
			<div
				key={ sectionsName }
				className={ `wsx-${ sectionsName } wsx-accordion-wrapper` }
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setShippingZoneStatus( ! shippingZoneStatus );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setShippingZoneStatus( ! shippingZoneStatus );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div className="wsx-accordion-title">
						{ sectionsData.label }
					</div>
					<span
						className={ `wsx-icon ${
							shippingZoneStatus ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>

				{ shippingZoneStatus &&
					Object.keys( sectionsData.attr ).length === 0 && (
						<div className="wsx-accordion-body">
							<div>
								{ __(
									'No Shipping Zones Found!',
									'wholesalex'
								) }
							</div>
						</div>
					) }

				{ shippingZoneStatus &&
					Object.keys( sectionsData.attr ).length > 0 &&
					Object.keys( sectionsData.attr ).map( ( sectionName ) => {
						const _data = sectionsData.attr[ sectionName ];
						switch ( _data.type ) {
							case 'shipping_zone':
								return shippingZoneData( _data, sectionName );
							default:
								return null;
						}
					} ) }
			</div>
		);
	};

	useEffect( () => {
		const _id = tempRole.id;

		if ( users.length === 0 ) {
			getUserByRoleID( _id );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const renderDynamicSections = () => {
		return (
			<div className="wsx-combined-field-wrapper">
				{ Object.keys( fields.attr ).map( ( section ) => {
					if (
						section === 'settings_combined_migration_field' ||
						section === 'settings_combined_subaccount_field'
					) {
						const _data = fields.attr[ section ];
						return dynamicSectionData( _data.attr, section );
					}
					return null;
				} ) }
			</div>
		);
	};

	return (
		<div className="wsx-edit-role-container">
			{ Object.keys( fields.attr ).map( ( section ) => {
				const _data = fields.attr[ section ];
				switch ( _data.type ) {
					case 'title_n_status':
						return titleSectionData( _data.attr, section );
					case 'credit_limit':
						return creditLimitSection( _data.attr, section );
					case 'role_id':
						return creditLimitSection( _data.attr, section );
					case 'display_price':
						return dynamicSectionData( _data.attr, section );
					case 'payment_method':
						return checkboxDataPayment( _data.attr, section );

					case 'shipping_method':
						return dynamicSectionData( _data.attr, section );
					case 'shipping_section':
						return shippingSectionData( _data, section );
					case 'role_setting':
						if (
							section !== 'settings_combined_migration_field' &&
							section !== 'settings_combined_subaccount_field'
						) {
							return dynamicSectionData( _data.attr, section );
						}
						break;
					default:
						return null;
				}
			} ) }
			{ roleId !== 'wholesalex_guest' &&
				roleId !== 'wholesalex_b2c_users' &&
				renderDynamicSections() }
		</div>
	);
};

export default Role;
