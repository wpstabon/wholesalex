const { __ } = wp.i18n;
import React, { useContext, useState } from 'react';
import Toast from '../../components/Toast';

import Button from '../../components/Button';
import Icons from '../../utils/Icons';
import LoadingGif from '../../components/LoadingGif';
import './Support.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const Support = () => {
	const [ name, setName ] = useState( '' );
	const [ email, setEmail ] = useState( '' );
	const [ subject, setSubject ] = useState( '' );
	const [ desc, setDesc ] = useState( '' );
	const [ loading, setLoading ] = useState( false );
	const [ type, setType ] = useState( '' );
	const [ showForm, setShowForm ] = useState( false );
	const [ contactLink, setContactLink ] = useState( false );
	const [ , setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const { dispatch } = useContext( ToastContexts );

	const fieldData = [
		{
			value: __( 'Technical Support', 'wholesalex' ),
			pro: true,
		},
		{
			value: __( 'Free Support (WordPress ORG)', 'wholesalex' ),
			external: true,
			only_free: true,
		},
		{
			value: __( 'Presale Questions', 'wholesalex' ),
			both: true,
		},
		{
			value: __( 'License Activation Issues', 'wholesalex' ),
			both: true,
		},
		{ value: __( 'Bug Report', 'wholesalex' ), both: true },
		{
			value: __( 'Compatibility Issues', 'wholesalex' ),
			both: true,
		},
		{ value: __( 'Feature Request', 'wholesalex' ), both: true },
	];
	const supportLink = [
		{
			label: __( 'Getting Started with WholesaleX', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/getting-started/?utm_source=quick_support&utm_medium=getting_started&utm_campaign=wholesalex-DB',
		},
		{
			label: __( 'Dynamic Pricing & Discount Rules', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/dynamic-rule/?utm_source=quick_support&utm_medium=dynamic_rule&utm_campaign=wholesalex-DB',
		},
		{
			label: __( 'Wholesale User Roles', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/user-roles/?utm_source=quick_support&utm_medium=user_roles&utm_campaign=wholesalex-DB',
		},
		{
			label: __( 'Registration Form Builder', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/registration-form-builder/?utm_source=quick_support&utm_medium=reg_form&utm_campaign=wholesalex-DB',
		},
		{
			label: __( 'WholeasaleX Addons', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/add-on/?utm_source=quick_support&utm_medium=addons&utm_campaign=wholesalex-DB',
		},
		{
			label: __( 'How to Create a Private Store', 'wholesalex' ),
			link: 'https://getwholesalex.com/docs/wholesalex/private-store/?utm_source=quick_support&utm_medium=private_store&utm_campaign=wholesalex-DB',
		},
	];

	const fetchData = ( action = 'support_data' ) => {
		if ( action === 'support_action' ) {
			let error = name && subject && desc ? false : true;
			error = error ? error : ! /\S+@\S+\.\S+/.test( email );

			if ( error || ! type ) {
				setToastMessages( {
					status: 'error',
					messages: [
						! type
							? __( 'Please Select Support type.', 'wholesalex' )
							: __(
									'Please Fill all the Input Field..',
									'wholesalex'
							  ),
					],
					state: true,
				} );
				return;
			}
			setLoading( true );
		}

		const actionObj =
			action === 'support_data'
				? { type: action, nonce: wholesalex.nonce }
				: {
						type: action,
						name,
						email,
						subject: '[WholesaleX- ' + type + '] ' + subject,
						desc,
						nonce: wholesalex.nonce,
				  };

		wp.apiFetch( {
			path: '/wholesalex/v1/support',
			method: 'POST',
			data: actionObj,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( action === 'support_data' ) {
					setName( res.data.name );
					setEmail( res.data.email );
				} else {
					setSubject( '' );
					setDesc( '' );
				}
			} else {
				setSubject( '' );
				setDesc( '' );
				setContactLink( true );
			}
			if ( res.message ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: res.success ? 'success' : 'error',
						message: res.message,
					},
				} );
			}
			setLoading( false );
		} );
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container-wrapper wsx-md-column-1">
				<div className="wsx-container-left">
					<div className="wsx-card">
						<div className="wsx-title wsx-font-20 wsx-mb-12">
							{ __(
								'Having Difficulties? We are here to help',
								'wholesalex'
							) }
						</div>
						<div className="wsx-color-text-light wsx-font-16 wsx-mb-32">
							{ __(
								'Let us know how we can help you.',
								'wholesalex'
							) }
						</div>
						<div className="wsx-card-2 wsx-card-border">
							<div className="wsx-radio-field-options wsx-radio-flex">
								{ fieldData.map( ( data, key ) => {
									return (
										( ( data.only_free &&
											! wholesalex.is_pro_active ) ||
											data.both ||
											( data.pro &&
												wholesalex.is_pro_active ) ) && (
											<div
												className="wsx-radio-field-option"
												key={ key }
											>
												<input
													type="radio"
													id={ data.value.replace(
														/[()\s]/g,
														''
													) }
													name="type"
													value={ data.value }
													onClick={ () => {
														let openState = false;
														if (
															data.value ===
																wholesalex_overview
																	.i18n
																	.whx_support_technical_support &&
															! wholesalex.is_pro_active
														) {
															window.open(
																'https://getwholesalex.com/pricing/?utm_source=quick_support&utm_medium=technical&utm_campaign=wholesalex-DB',
																'_blank'
															);
														} else if (
															data.only_free &&
															! wholesalex.is_pro_active
														) {
															window.open(
																'https://wordpress.org/support/plugin/wholesalex/',
																'_blank'
															);
														} else {
															openState = true;
														}

														setShowForm(
															openState
														);
														setContactLink( false );
														openState &&
															fetchData();
														openState &&
															setType(
																data.value
															);
													} }
												/>
												<label
													className="wsx-label wsx-option-desc"
													htmlFor={ data.value.replace(
														/[()\s]/g,
														''
													) }
												>
													{ data.value }
													{ data.pro &&
														! wholesalex.is_pro_active && (
															<>
																<a
																	className="wsx-link"
																	target="_blank"
																	href="https://getwholesalex.com/pricing/?utm_source=quick_support&utm_medium=technical&utm_campaign=wholesalex-DB"
																	rel="noreferrer"
																>
																	{ ' ' }
																	{ __(
																		'(Pro)',
																		'wholesalex'
																	) }
																</a>
																<span className="dashicons dashicons-external"></span>
															</>
														) }
													{ data?.external && (
														<span className="dashicons dashicons-external"></span>
													) }
												</label>
											</div>
										)
									);
								} ) }
							</div>
							{ ! showForm && (
								<Button
									label={ __(
										'Create a Ticket',
										'wholesalex'
									) }
									background="primary"
									customClass="wsx-mt-32"
									onClick={ ( e ) => {
										e.preventDefault();
										setShowForm( true );
										fetchData();
									} }
								/>
							) }
							{ showForm && (
								<div className="wsx-support-form">
									{ ! type && (
										<div className="wsx-font-medium wsx-color-negative wsx-mt-20">
											{ __(
												'Select Support Type from above',
												'wholesalex'
											) }
										</div>
									) }
									<div id="wsx-contact" className="wsx-mt-32">
										<div className="wsx-d-flex wsx-item-start wsx-gap-32 wsx-smd-column-1 wsx-mb-24">
											<div className="wsx-w-full">
												<label
													className="wsx-label wsx-input-label"
													htmlFor="wsx-support-user-name"
												>
													{ __(
														'Name',
														'wholesalex'
													) }
												</label>
												<input
													id="wsx-support-user-name"
													className="wsx-input wsx-bg-base1"
													type="text"
													placeholder={ __(
														'Name',
														'wholesalex'
													) }
													required
													defaultValue={ name }
													onChange={ ( e ) =>
														setName(
															e.target.value
														)
													}
												/>
											</div>
											<div className="wsx-w-full">
												<label
													className="wsx-label wsx-input-label"
													htmlFor="wsx-support-user-email"
												>
													{ __(
														'Email',
														'wholesalex'
													) }
												</label>
												<input
													id="wsx-support-user-email"
													className="wsx-input wsx-bg-base1"
													type="email"
													placeholder={ __(
														'Email',
														'wholesalex'
													) }
													required
													defaultValue={ email }
													onChange={ ( e ) =>
														setEmail(
															e.target.value
														)
													}
												/>
											</div>
										</div>
										<div className="wsx-w-full wsx-mb-24">
											<label
												className="wsx-label wsx-input-label"
												htmlFor="wsx-support-subject"
											>
												{ __(
													'Subject',
													'wholesalex'
												) }
											</label>
											<input
												id="wsx-support-subject"
												className="wsx-input wsx-bg-base1"
												type="text"
												placeholder={ __(
													'Subject',
													'wholesalex'
												) }
												required
												value={ subject }
												onChange={ ( e ) =>
													setSubject( e.target.value )
												}
											/>
										</div>
										<div className="wsx-w-full">
											<label
												className="wsx-label wsx-input-label"
												htmlFor="wsx-support-issue"
											>
												{ __(
													'Explain your Problem',
													'wholesalex'
												) }
											</label>
											<textarea
												id="wsx-support-issue"
												className="wsx-textarea wsx-w-full wsx-bg-base1"
												rows={ 8 }
												placeholder={ __(
													'Type here',
													'wholesalex'
												) }
												required
												value={ desc }
												onChange={ ( e ) =>
													setDesc( e.target.value )
												}
											/>
										</div>
									</div>
									{ contactLink ? (
										<div>
											{ __(
												'You can Contact in Support via our',
												'wholesalex'
											) }
											<a
												className="wsx-link"
												target="_blank"
												href="https://www.wpxpo.com/contact/?utm_source=quick_support&utm_medium=tech_support&utm_campaign=WholesaleX-dashboard"
												rel="noreferrer"
											>
												{ ' ' }
												{ __(
													'Contact Form',
													'wholesalex'
												) }
											</a>
										</div>
									) : (
										<Button
											label={ __(
												'Submit Ticket',
												'wholesalex'
											) }
											background="primary"
											customClass="wsx-mt-32"
											onClick={ ( e ) => {
												e.preventDefault();
												fetchData( 'support_action' );
											} }
										/>
									) }
									{ loading && <LoadingGif /> }
								</div>
							) }
						</div>
					</div>
				</div>
				<div className="wsx-container-right">
					<div className="wsx-card wsx-p-32 wsx-mb-32">
						<div className="wsx-title wsx-font-20 wsx-mb-12">
							{ __( 'Useful Guides', 'wholesalex' ) }
						</div>
						<div className="wsx-color-text-light wsx-font-16 wsx-mb-32">
							{ __(
								'Check out the in depth documentation',
								'wholesalex'
							) }
						</div>
						<ul className="wsx-list wsx-d-flex wsx-flex-column wsx-gap-16 wsx-mb-32">
							{ supportLink.map( ( data, key ) => {
								return (
									( ! data.pro ||
										( data.pro &&
											! wholesalex.is_pro_active ) ) && (
										<li
											className="wsx-list-item"
											key={ key }
										>
											<a
												className="wsx-link wsx-d-flex wsx-item-start wsx-gap-8 wsx-color-text-medium"
												target="_blank"
												href={ data.link }
												rel="noreferrer"
											>
												<span
													className="wsx-lh-0 wsx-rtl-arrow"
													style={ {
														marginTop: '2px',
													} }
												>
													{ Icons.arrowRight }
												</span>
												{ data.label }
											</a>
										</li>
									)
								);
							} ) }
						</ul>
						<Button
							label={ __(
								'Explore Documentation',
								'wholesalex'
							) }
							borderColor="primary"
							background="base2"
							iconName="doc"
							buttonLink="https://getwholesalex.com/documentation/?utm_source=quick_support&utm_medium=explore_docs&utm_campaign=wholesalex-DB"
						/>
					</div>
					<div className="wsx-card-2 wsx-p-32 wsx-bg-base2">
						<div className="wsx-title wsx-font-20 wsx-mb-12">
							{ __( 'WowCommerce Community', 'wholesalex' ) }
						</div>
						<div className="wsx-color-text-medium wsx-font-16 wsx-mb-40">
							{ __(
								'Join the Facebook community of WholesaleX to stay up-to-date and share your thoughts and feedback.',
								'wholesalex'
							) }
						</div>
						<Button
							label={ __( 'Join Community', 'wholesalex' ) }
							buttonLink="https://www.facebook.com/groups/438050567242124"
							borderColor="primary"
							background="base1"
						/>
					</div>
				</div>
			</div>
			<Toast delay={ 3000 } position="top_right" />
		</div>
	);
};

export default Support;
