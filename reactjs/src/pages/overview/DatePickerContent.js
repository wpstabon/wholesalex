/**
 * External dependencies
 */
import { __ } from '@wordpress/i18n';
import React, { createRef, useState, useRef, useLayoutEffect } from 'react';

/**
 * Internal dependencies
 */
import PresetPeriods from './preset-periods';
import TabContainer from '../../components/TabContainer';
import Calendar from '../../components/Calendar';
import Button from '../../components/Button';

const DatePickerContent = ( props ) => {
	const { period, onUpdate, onSelect, presetValues } = props;

	const calendarRef = useRef();
	const controlsRef = createRef();

	const wrapperRef = useRef();
	const [ wrapperWidth, setWrapperWidth ] = useState( 0 );

	useLayoutEffect( () => {
		const updateWidth = () =>
			wrapperRef.current &&
			setWrapperWidth( wrapperRef.current.offsetWidth );

		updateWidth();

		const resizeObserver = new ResizeObserver( () => updateWidth() );

		if ( wrapperRef.current ) {
			resizeObserver.observe( wrapperRef.current );
		}

		return () => {
			resizeObserver.disconnect();
		};
	}, [] );

	const onTabSelect = ( e ) => {
		/**
		 * If the period is `custom` and the user switches tabs to view the presets,
		 * then a preset should be selected. This logic selects the default, otherwise
		 * `custom` value for period will result in no selection.
		 */
		if ( e === 'period' && period === 'custom' ) {
			onUpdate( { period: 'today' } );
		}
	};

	return (
		<div className="wsx-date-range-width-selector" ref={ wrapperRef }>
			<div className="wsx-date-range-wrapper wsx-card">
				<TabContainer
					tabItems={ [
						{
							name: 'preset',
							title: __( 'Presets', 'wholesalex' ),
						},
						{
							name: 'custom',
							title: __( 'Custom', 'wholesalex' ),
						},
					] }
					initialTabItem="preset"
					onTabSelect={ onTabSelect }
					tabHeaderTitle={ __( 'Date Range', 'wholesalex' ) }
					tabHeaderClass="wsx-date-range-title"
					tabHeaderJustify="space"
					tabHeaderSpaceBottom="24"
				>
					{ ( selected ) => (
						<>
							{ selected.name === 'preset' && (
								<>
									<PresetPeriods
										onSelect={ onUpdate }
										selectedPreset={ period }
										presetValues={ presetValues }
									/>
									<div className="wsx-btn-group wsx-center">
										<Button
											label={ __(
												'Update',
												'wholesalex'
											) }
											background="primary"
											customClass="wsx-mt-16"
											ref={ controlsRef }
											onClick={ onSelect(
												selected.name
											) }
										/>
									</div>
								</>
							) }
							{ selected.name === 'custom' && (
								<>
									<Calendar
										ref={ calendarRef }
										onApply={ onUpdate }
										numberOfMonthView={
											wrapperWidth < 770 ? 1 : 2
										}
									/>
									<div className="wsx-btn-group wsx-center">
										<Button
											label={ __(
												'Update',
												'wholesalex'
											) }
											background="primary"
											customClass="wsx-mt-16"
											ref={ controlsRef }
											onClick={ onSelect(
												selected.name
											) }
										/>
									</div>
								</>
							) }
						</>
					) }
				</TabContainer>
			</div>
		</div>
	);
};

export default DatePickerContent;
