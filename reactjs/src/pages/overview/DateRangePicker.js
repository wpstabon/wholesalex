import { useState, useEffect, useRef } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import Icons from '../../utils/Icons';
import DatePickerContent from './DatePickerContent';
import { useContext } from 'react';
import { DataContext } from '../../contexts/DataProvider';
import Alert from '../../components/Alert';
import Select from '../../components/Select';
import '../../assets/scss/DateRangePicker.scss';

const shortDateFormat = __( 'MM/DD/YYYY', 'wholesalex' );
const presetValues = [
	{ value: 'today', label: __( 'Today', 'wholesalex' ) },
	{ value: 'yesterday', label: __( 'Yesterday', 'wholesalex' ) },
	{ value: 'week', label: __( 'Week to date', 'wholesalex' ) },
	{ value: 'last_week', label: __( 'Last week', 'wholesalex' ) },
	{ value: 'month', label: __( 'Month to date', 'wholesalex' ) },
	{ value: 'last_month', label: __( 'Last month', 'wholesalex' ) },
	{ value: 'quarter', label: __( 'Quarter to date', 'wholesalex' ) },
	{ value: 'last_quarter', label: __( 'Last quarter', 'wholesalex' ) },
	{ value: 'year', label: __( 'Year to date', 'wholesalex' ) },
	{ value: 'last_year', label: __( 'Last year', 'wholesalex' ) },
];

const DateRangePicker = ( { dateQuery, onRangeSelect } ) => {
	const [ alert, setAlert ] = useState( false );
	const wrapperRef = useRef( null );
	const { contextData } = useContext( DataContext );
	const formatDate = ( date, format ) => {
		if (
			date &&
			date._isAMomentObject &&
			typeof date.format === 'function'
		) {
			return date.format( format );
		}
		return '';
	};
	const handleAlertClose = () => {
		setAlert( false );
	};
	const getResetState = () => {
		const { period, before, after } = dateQuery;

		return {
			period,
			before,
			after,
			focusedInput: 'startDate',
			afterText: formatDate( after, shortDateFormat ),
			beforeText: formatDate( before, shortDateFormat ),
			afterError: null,
			beforeError: null,
		};
	};

	const [ state, setState ] = useState( {
		...getResetState(),
		isActive: false,
	} );

	const update = ( newUpdate ) => {
		setState( ( prevState ) => ( { ...prevState, ...newUpdate } ) );
	};

	const formatDateRange = ( date ) => {
		if ( ! date ) {
			return '';
		}
		const d = new Date( date );
		if ( isNaN( d.getTime() ) ) {
			return '';
		}
		const year = d.getFullYear();
		const month = String( d.getMonth() + 1 ).padStart( 2, '0' );
		const day = String( d.getDate() ).padStart( 2, '0' );
		return `${ year }-${ month }-${ day }`;
	};
	const onSelect = ( selectedTab ) => () => {
		const { period } = state;
		const data = {
			period: selectedTab === 'custom' ? 'custom' : period,
		};

		if ( selectedTab === 'custom' ) {
			if (
				formatDateRange( contextData.startDateRange ) === '' ||
				formatDateRange( contextData.endDateRange ) === ''
			) {
				setAlert( true );
				return;
			}
			data.after = formatDateRange( contextData.startDateRange );
			data.before = formatDateRange( contextData.endDateRange );
		} else {
			data.after = undefined;
			data.before = undefined;
		}
		if ( data.period === undefined ) {
			data.period = 'today';
		}
		onRangeSelect( data );
		setState( ( prevState ) => ( {
			...prevState,
			isActive: ! prevState.isActive,
		} ) );
	};

	const getButtonLabel = () => {
		const { primaryDate } = dateQuery;
		return [ `${ primaryDate.label } (${ primaryDate.range })` ];
	};

	const isValidSelection = ( selectedTab ) => {
		const { after, before } = state;
		if ( selectedTab === 'custom' ) {
			return after && before;
		}
		return true;
	};

	const resetCustomValues = () => {
		setState( {
			after: null,
			before: null,
			focusedInput: 'startDate',
			afterText: '',
			beforeText: '',
			afterError: null,
			beforeError: null,
		} );
	};

	const handleClick = () => {
		setState( ( prevState ) => ( {
			...prevState,
			isActive: ! prevState.isActive,
		} ) );
	};

	const handleClickOutside = ( event ) => {
		if (
			wrapperRef.current &&
			! wrapperRef.current.contains( event.target )
		) {
			resetCustomValues();
			setState( ( prevState ) => ( { ...prevState, isActive: false } ) );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () => {
			document.removeEventListener( 'mousedown', handleClickOutside );
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const {
		period,
		after,
		before,
		focusedInput,
		afterText,
		beforeText,
		afterError,
		beforeError,
		isActive,
	} = state;

	return (
		<div
			ref={ wrapperRef }
			className={ `wsx-date-range-picker-wrapper ${
				isActive ? ' active' : ''
			}` }
		>
			<div
				className={ `wsx-input-wrapper-with-icon ${
					isActive ? ' active' : ''
				}` }
				onClick={ handleClick }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						handleClick();
					}
				} }
				role="button"
				tabIndex="0"
			>
				<span className="wsx-icon wsx-icon-left">
					{ Icons.calendar }
				</span>
				<Select
					label={ getButtonLabel() }
					customClass="wsx-font-medium"
					noValue={ true }
					iconRotation="none"
				/>
			</div>
			<DatePickerContent
				period={ period }
				after={ after }
				before={ before }
				onUpdate={ update }
				onSelect={ onSelect }
				isValidSelection={ isValidSelection }
				resetCustomValues={ resetCustomValues }
				focusedInput={ focusedInput }
				afterText={ afterText }
				beforeText={ beforeText }
				afterError={ afterError }
				beforeError={ beforeError }
				shortDateFormat={ shortDateFormat }
				presetValues={ presetValues }
			/>
			{ alert && (
				<Alert
					title="Please Select Valid Date Range!"
					description="Please select both a start date and an end date."
					onClose={ handleAlertClose }
				/>
			) }
		</div>
	);
};

// DateRangePicker.propTypes = {
// 	onRangeSelect: PropTypes.func.isRequired,
// 	dateQuery: PropTypes.shape( {
// 		period: PropTypes.string.isRequired,
// 		before: PropTypes.object,
// 		after: PropTypes.object,
// 		primaryDate: PropTypes.shape( {
// 			label: PropTypes.string.isRequired,
// 			range: PropTypes.string.isRequired,
// 		} ).isRequired,
// 		secondaryDate: PropTypes.shape( {
// 			label: PropTypes.string.isRequired,
// 			range: PropTypes.string.isRequired,
// 		} ).isRequired,
// 	} ).isRequired,
// };

export default DateRangePicker;
