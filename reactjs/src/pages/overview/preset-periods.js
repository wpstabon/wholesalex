/**
 * External dependencies
 */
/**
 * Internal dependencies
 */
import { SegmentedSelection } from '@woocommerce/components';

const PresetPeriods = ( props ) => {
	const { onSelect, selectedPreset, presetValues } = props;
	return (
		<SegmentedSelection
			options={ presetValues }
			selected={ selectedPreset }
			onSelect={ onSelect }
			name="period"
		/>
	);
};
export default PresetPeriods;
