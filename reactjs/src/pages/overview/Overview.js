import React, { useState, useEffect, useRef } from 'react';
import { __ } from '@wordpress/i18n';
import DateRangePicker from './DateRangePicker';
import { getCurrentDates, isoDateFormat } from '@woocommerce/date';
import Icons from '../../utils/Icons';
import TabContainer from '../../components/TabContainer';
import Table from '../../components/Table';
import CounterMessageCard from '../../components/CounterMessageCard';
import Button from '../../components/Button';
import SalesChart from '../../components/SalesChart';
import LoadingGif from '../../components/LoadingGif';
import './Dashboard.scss';

const Overview = () => {
	const [ data, setData ] = useState( {} );
	const [ dateQuery, setDateQuery ] = useState( {
		period: 'month',
		after: null,
		before: null,
	} );
	const [ graphLoader, setGraphLoader ] = useState( false );
	const [ salesData, setSalesData ] = useState( {} );

	const [ graphData, setGraphData ] = useState( [] );
	const [ previewsSales, setPreviewsSales ] = useState( {} );

	// Custom formatter for tooltip
	const tooltipFormatter = ( value, name ) => {
		switch ( name ) {
			case 'totalSales':
				return [
					`${ wholesalex_overview.whx_dr_currency }${ value }`,
					'Total Sale (B2B)',
				];
			case 'netRevenue':
				return [
					`${ wholesalex_overview.whx_dr_currency }${ value }`,
					'Net Revenue (B2B)',
				];
			case 'grossSales':
				return [
					`${ wholesalex_overview.whx_dr_currency }${ value }`,
					'Gross Sale (B2B)',
				];
			default:
				return [ value, name ];
		}
	};

	const [ dateQueryData, setDateQueryData ] = useState( {
		...dateQuery,
		primaryDate: getCurrentDates(
			dateQuery,
			`period=${ dateQuery.period }&compare=previous_year&after=${ dateQuery.after }&before=${ dateQuery.before }`
		).primary,
	} );

	const handleLinkAction = ( link ) => {
		window.open( link, '_blank' );
	};

	const [ currentTab, setActive ] = useState( 'customer' );
	const [ status, setStatus ] = useState( false );

	const myRef = useRef();

	const onTabSelect = ( e ) => {
		setActive( e );
	};

	const handleClickOutside = ( e ) => {
		if ( myRef.current !== null ) {
			if ( ! myRef.current.contains( e.target ) ) {
				setStatus( false );
			}
		}
	};

	useEffect( () => {
		document.addEventListener( 'click', handleClickOutside );
		return () =>
			document.removeEventListener( 'click', handleClickOutside );
	}, [] );

	const normalizeValue = ( value, min, max ) => {
		return ( ( value - min ) / ( max - min ) ).toFixed( 3 );
	};

	// Function to format the data
	const formatData = ( value ) => {
		const dates = Object.keys( value.sales_graph );
		const formattedData = dates.map( ( date ) => ( {
			date,
			totalSales: parseFloat(
				normalizeValue(
					parseFloat( value.sales_graph[ date ] ),
					0,
					200
				)
			),
			netRevenue: parseFloat(
				normalizeValue(
					parseFloat( value.revenue_graph[ date ] ),
					0,
					200
				)
			),
			grossSales: parseFloat(
				normalizeValue(
					parseFloat( value.gross_graph[ date ] ),
					0,
					200
				)
			),
		} ) );
		return formattedData;
	};

	const fetchData = async (
		type = 'get',
		request_for = '',
		start_date = '',
		end_date = '',
		period = ''
	) => {
		if ( request_for === 'b2b_order_data' ) {
			setGraphLoader( true );
		}
		const attr = {
			type,
			action: 'overview_action',
			request_for,
			nonce: wholesalex.nonce,
		};

		if ( start_date && end_date ) {
			attr.start_date = start_date;
			attr.end_date = end_date;
			attr.period = period;
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/overview_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				if ( request_for === 'b2b_order_data' ) {
					setSalesData( ( prevData ) => ( {
						...prevData,
						[ request_for ]: res.data,
					} ) );
					const datasets = formatData( res.data );
					const {
						prev_average_order,
						prev_total_sales,
						prev_gross_sales,
						prev_net_revenue,
						prev_total_order,
						period,
					} = res.data;
					setPreviewsSales( {
						prev_average_order,
						prev_total_sales,
						prev_gross_sales,
						prev_net_revenue,
						prev_total_order,
						period,
					} );
					setGraphData( datasets );
					setGraphLoader( false );
				} else {
					setData( ( prevData ) => ( {
						...prevData,
						[ request_for ]: res.data,
					} ) );
				}
			}
		} );
	};

	// const skeletonLoader = ( columnCount, rowCount = 5 ) => {
	// 	return (
	// 		<>
	// 			{ [ ...Array( rowCount ) ].map( ( row ) => {
	// 				return (
	// 					<tr key={ row }>
	// 						{ [ ...Array( columnCount ) ].map( ( colum ) => {
	// 							return (
	// 								<td
	// 									key={ colum }
	// 									className="wholesalex_lists__column"
	// 								>
	// 									<div className=" wholesalex_skeleton_loader"></div>
	// 								</td>
	// 							);
	// 						} ) }{ ' ' }
	// 					</tr>
	// 				);
	// 			} ) }
	// 		</>
	// 	);
	// };

	// const newMessagesCard = () => {
	// 	// if(!data.new_messages_count){
	// 	// 	fetchData('get','new_messages_count');
	// 	// }
	// 	const messagesLink = wholesalex_overview.wholesalex_conversation;
	// 	return (
	// 		<div className="wholesalex_new_messages_card">
	// 			<div className="wholesalex_new_messages_card__content">
	// 				<div className="wholesalex_new_messages_text">
	// 					<span>{ __( 'New Messages', 'wholesalex' ) }</span>{ ' ' }
	// 					{ ! data.new_messages_count && (
	// 						<span
	// 							className={ `${
	// 								! data.new_messages_count &&
	// 								data.new_messages_count !== 0
	// 									? 'wholesalex_skeleton_loader'
	// 									: ''
	// 							}` }
	// 						></span>
	// 					) }{ ' ' }
	// 					{ ( data.new_messages_count ||
	// 						data.new_messages_count === 0 ) && (
	// 						<span className={ `wholesalex_new_messages_count` }>
	// 							({ data.new_messages_count })
	// 						</span>
	// 					) }
	// 				</div>
	// 				<a
	// 					href={ messagesLink }
	// 					className="wsx-link wholesalex_new_message_view_link"
	// 					target="_blank"
	// 					rel="noreferrer"
	// 				>
	// 					<span>{ __( 'View All', 'wholesalex' ) }</span>{ ' ' }
	// 					<span className="dashicons dashicons-arrow-right-alt wholesalex_dashboard_right_arrow_icon wholesalex_icon"></span>
	// 				</a>
	// 			</div>
	// 			<div className="wholesalex_new_messages_card__icon">
	// 				<span className="dashicons dashicons-email wholesalex_message_icon wholesalex_large_icon"></span>
	// 			</div>
	// 		</div>
	// 	);
	// };

	// const newRegistrationCard = () => {
	// 	// if(!data.new_registrations_count){
	// 	// 	fetchData('get','new_registrations_count');
	// 	// }
	// 	const viewRegistrationsLink = wholesalex_overview.wholesalex_users;
	// 	return (
	// 		<div className="wholesalex_new_registrations_card">
	// 			<div className="wholesalex_new_registrations_card__icon">
	// 				<span className="dashicons dashicons-id wholesalex_message_icon wholesalex_large_icon"></span>
	// 			</div>
	// 			<div className="wholesalex_new_registrations_card__content">
	// 				<div className="wholesalex_new_registrations_text">
	// 					<span>
	// 						{ __( 'New Registrations', 'wholesalex' ) }{ ' ' }
	// 					</span>
	// 					{ ! data.new_registrations_count && (
	// 						<span
	// 							className={ `${
	// 								! data.new_registrations_count &&
	// 								data.new_registrations_count != 0
	// 									? 'wholesalex_skeleton_loader'
	// 									: ''
	// 							}` }
	// 						></span>
	// 					) }
	// 					{ ( data.new_registrations_count ||
	// 						data.new_registrations_count == 0 ) && (
	// 						<span
	// 							className={ `wholesalex_new_registrations_count` }
	// 						>
	// 							({ data.new_registrations_count })
	// 						</span>
	// 					) }
	// 				</div>
	// 				<a
	// 					href={ viewRegistrationsLink }
	// 					className="wsx-link wholesalex_new_message_view_link"
	// 					target="_blank"
	// 					rel="noreferrer"
	// 				>
	// 					<span>{ __( 'Approve', 'wholesalex' ) }</span>{ ' ' }
	// 					<span className="dashicons dashicons-arrow-right-alt wholesalex_dashboard_right_arrow_icon wholesalex_icon"></span>
	// 				</a>
	// 			</div>
	// 		</div>
	// 	);
	// };

	// const renderNewCounterCards = () => {
	// 	return (
	// 		<div className="wholesalex_dashboard_card wholesalex_dashboard_counter_cards">
	// 			{ wholesalex?.is_pro_active &&
	// 				wholesalex?.settings?.wsx_addon_conversation === 'yes' &&
	// 				newMessagesCard() }
	// 			{ newRegistrationCard() }
	// 		</div>
	// 	);
	// };

	// const renderRecentOrders = () => {
	// 	const headings = wholesalex_overview.recent_order_heading;
	// 	return (
	// 		<div className="wholesalex_lists wholesalex_dashboard_top_customer_lists">
	// 			<table className="wsx-table wholesalex_lists__table">
	// 				<thead className="wholesalex_lists__header">
	// 					<tr className="wholesalex_lists__heading_row">
	// 						{ Object.keys( headings ).map(
	// 							( heading, index ) => (
	// 								<th
	// 									key={ index }
	// 									className={ `wholesalex_lists__column_${ headings[ heading ].name } wholesalex_lists__column` }
	// 								>
	// 									{ headings[ heading ].title }
	// 								</th>
	// 							)
	// 						) }
	// 					</tr>
	// 				</thead>

	// 				<tbody className="wholesalex_lists__body wsx-relative">
	// 					{ ! data.recent_orders && (
	// 						<tr className="wsx-lists-table-row wsx-lists-empty">
	// 							<td
	// 								colSpan={ 100 }
	// 								className={ `wsx-lists-table-column` }
	// 								style={ { height: '213px' } }
	// 							>
	// 								{ <LoadingGif insideContainer={ true } /> }
	// 							</td>
	// 						</tr>
	// 					) }
	// 					{ data?.recent_orders?.length === 0 && (
	// 						<tr>
	// 							{ ' ' }
	// 							<td
	// 								colSpan={ 100 }
	// 								className="wholesalex_lists__empty"
	// 							>
	// 								{ __(
	// 									'No New Orders Found!',
	// 									'wholesalex'
	// 								) }
	// 							</td>{ ' ' }
	// 						</tr>
	// 					) }

	// 					{ data.recent_orders &&
	// 						data.recent_orders.map( ( order, index ) => (
	// 							<tr
	// 								key={ index }
	// 								className="wholesalex_lists__body_row"
	// 							>
	// 								{ Object.keys( headings ).map(
	// 									( heading ) =>
	// 										renderTableCell(
	// 											order[
	// 												headings[ heading ].name
	// 											],
	// 											order,
	// 											headings[ heading ].type,
	// 											headings[ heading ].name
	// 										)
	// 								) }
	// 							</tr>
	// 						) ) }
	// 				</tbody>
	// 			</table>
	// 		</div>
	// 	);
	// };

	// const renderTableCell = ( data, item, fieldType, fieldName ) => {
	// 	switch ( fieldType ) {
	// 		case 'name_n_email':
	// 			return (
	// 				<td
	// 					className={ `wholesalex_lists__column_${ fieldName } wholesalex_lists__column` }
	// 				>
	// 					<div className="wholesalex_text_with_avatar_field">
	// 						<img
	// 							className="user_image"
	// 							src={ item.avatar_url }
	// 							alt={ `${ item.display_name }'s avatar` }
	// 						/>
	// 						<div className="wholesalex_name_email_field">
	// 							<div className="wholesalex_name_email_field__name">
	// 								{ item.display_name }
	// 							</div>
	// 							<div className="wholesalex_name_email_field__email">
	// 								{ item.user_email }
	// 							</div>
	// 						</div>
	// 					</div>
	// 				</td>
	// 			);
	// 		case 'html':
	// 			return (
	// 				<td
	// 					className={ `wholesalex_lists__column_${ fieldName } wholesalex_lists__column` }
	// 					dangerouslySetInnerHTML={ { __html: data } }
	// 				></td>
	// 			);
	// 		case 'view_order':
	// 			return (
	// 				<td
	// 					className={ `wholesalex_lists__column_${ fieldName } wholesalex_lists__column` }
	// 				>
	// 					<a
	// 						href={ data }
	// 						className="wholesalex_dashboard_link"
	// 						target="_blank"
	// 						rel="noreferrer"
	// 					>
	// 						{ __( 'View Order', 'wholesalex' ) }
	// 					</a>
	// 				</td>
	// 			);
	// 		case 'edit_user':
	// 			return (
	// 				<td
	// 					className={ `wholesalex_lists__column_${ fieldName } wholesalex_lists__column` }
	// 				>
	// 					<a
	// 						href={ data }
	// 						className="wsx-link wsx-link wholesalex_dashboard_link"
	// 						target="_blank"
	// 						rel="noreferrer"
	// 					>
	// 						{ __( 'Review', 'wholesalex' ) }
	// 					</a>
	// 				</td>
	// 			);

	// 		default:
	// 			return (
	// 				<td
	// 					className={ `wholesalex_lists__column_${ fieldName } wholesalex_lists__column` }
	// 				>
	// 					{ data }
	// 				</td>
	// 			);
	// 	}
	// };

	const saleSummaryCard = (
		name,
		title,
		icon,
		value = '0',
		valueType,
		previousValue,
		growthColor,
		growth
	) => {
		const Icon = icon ? true : false;
		const valueData = value ? value : '0';
		const valueHtml = valueType && valueType === 'html' ? true : false;
		let content;

		if ( valueHtml ) {
			content = (
				<span
					className="wsx-title wsx-color-text-dark"
					dangerouslySetInnerHTML={ { __html: value } }
				></span>
			);
		} else if ( valueData ) {
			content = (
				<span className="wsx-title wsx-color-text-dark">
					{ valueData }
				</span>
			);
		} else {
			content = '';
		}

		return (
			<div className="wsx-card wsx-sales-card">
				<div className="wsx-title-wrap">
					<span className="wsx-font-16 wsx-font-medium">
						{ title }
					</span>
				</div>
				<div className="wsx-d-flex wsx-item-center wsx-gap-12">
					{ Icon ? (
						<img className="wsx-icon" src={ icon } alt="icon" />
					) : (
						''
					) }
					{ content }
				</div>
				{ growth !== 'no' && (
					<div className="wsx-sales-growth">
						<span
							className={ `wsx-growth-wrapper wsx-color-${ growthColor }` }
						>
							<span className="wsx-lh-0">{ Icons.growUp }</span>
							<span>{ growth } % </span>
						</span>
						<span>
							{ __( 'than last', 'wholesalex' ) }{ ' ' }
							{ previewsSales?.period }
						</span>
					</div>
				) }
			</div>
		);
	};

	const renderSaleSummaryCards = () => {
		const saleSummaryCards = [
			{
				name: 'b2b_customer',
				icon: wholesalex.url + '/assets/icons/dashboard-customer.svg',
				growthColor: 'sky',
				title: __( 'Customer No. (B2B)', 'wholesalex' ),
				valueType: 'number',
				value: data?.b2b_customer_count,
				previousValue: '2',
				growth: 'no',
			},
			{
				name: 'b2b_total_order',
				icon: wholesalex.url + '/assets/icons/dashboard-order.svg',
				growthColor: 'orchid',
				title: __( 'Total Order (B2B)', 'wholesalex' ),
				valueType: 'number',
				value: salesData?.b2b_order_data?.total_orders,
				previousValue: '1',
				growth: previewsSales?.prev_total_order,
			},
			{
				name: 'b2b_total_sale',
				icon: wholesalex.url + '/assets/icons/dashboard-sale.svg',
				growthColor: 'primary',
				title: __( 'Total Sale (B2B)', 'wholesalex' ),
				valueType: 'html',
				value: salesData?.b2b_order_data?.total_sales,
				previousValue: '1',
				growth: previewsSales?.prev_total_sales,
			},
			{
				name: 'b2b_net_earning',
				icon: wholesalex.url + '/assets/icons/dashboard-revenue.svg',
				growthColor: 'mint',
				title: __( 'Net Revenue (B2B)', 'wholesalex' ),
				valueType: 'html',
				value: salesData?.b2b_order_data?.net_revenue,
				previousValue: '1',
				growth: previewsSales?.prev_net_revenue,
			},
			{
				name: 'b2b_gross_sale',
				icon: wholesalex.url + '/assets/icons/dashboard-gross.svg',
				growthColor: 'secondary',
				title: __( 'Gross Sale (B2B)', 'wholesalex' ),
				valueType: 'html',
				value: salesData?.b2b_order_data?.gross_sales,
				previousValue: '1',
				growth: previewsSales?.prev_gross_sales,
			},
			{
				name: 'average_order_value',
				icon:
					wholesalex.url + '/assets/icons/dashboard-order-value.svg',
				growthColor: 'lime',
				title: __( 'Average Order (B2B)', 'wholesalex' ),
				valueType: 'html',
				value: salesData?.b2b_order_data?.average_order_value,
				previousValue: '1',
				growth: previewsSales?.prev_average_order,
			},
		];

		return (
			<div className="wsx-card-container wsx-mt-40 wsx-mb-40">
				{ saleSummaryCards.map( ( card ) => {
					return saleSummaryCard(
						card.name,
						card.title,
						card.icon,
						card.value,
						card.valueType,
						card.previousValue,
						card.growthColor,
						card.growth
					);
				} ) }
			</div>
		);
	};

	const renderCounterCard = ( cardName = 'message' ) => {
		let title = __( 'New Messages', 'wholesalex' );
		let newDataCount = 0;
		let isActive = false;
		let buttonLink = '#';
		let buttonText = __( 'View All', 'wholesalex' );
		if ( cardName === 'message' ) {
			title = __( 'New Messages', 'wholesalex' );
			buttonText = __( 'View All', 'wholesalex' );
			buttonLink = '/conversation';
			{
				( data.new_messages_count || data.new_messages_count !== 0 ) &&
					( ( newDataCount = data.new_messages_count ),
					( isActive = true ) );
			}
		} else {
			title = __( 'New Registrations', 'wholesalex' );
			buttonText = __( 'Approve', 'wholesalex' );
			buttonLink = '/users';
			{
				( data.new_registrations_count ||
					data.new_registrations_count !== 0 ) &&
					( ( newDataCount = data.new_registrations_count ),
					( isActive = true ) );
			}
		}
		return (
			<CounterMessageCard
				isActive={ isActive }
				value={ newDataCount }
				title={ title }
				buttonText={ buttonText }
				buttonLink={ buttonLink }
			/>
		);
	};

	const dashboardCard = (
		title = '',
		renderContent = '',
		renderButton = '',
		className = ''
	) => {
		return (
			<div className={ `wholesalex_dashboard_card ${ className }` }>
				<div className="wsx-font-18 wsx-font-bold wsx-color-tertiary wsx-mb-16">
					{ title }
				</div>
				<div className="wsx-color-text-light wsx-mb-32">
					{ renderContent }
				</div>
				{ renderButton && (
					<div className="wholesalex_dashboard_card__button">
						{ renderButton }
					</div>
				) }
			</div>
		);
	};

	const renderDashboardCards = () => {
		const hotFeatureCardContent = () => {
			const _hotFeatures = [
				__( 'Full Access to Dynamic Rules', 'wholesalex' ),
				__( 'Multiple Pricing Tiers', 'wholesalex' ),
				__( 'Bulk Order Form', 'wholesalex' ),
				__( 'Request A Quote', 'wholesalex' ),
				__( 'Subaccounts', 'wholesalex' ),
				__( 'Conversation', 'wholesalex' ),
				__( 'WholesaleX Wallet', 'wholesalex' ),
				__( 'And Much More!', 'wholesalex' ),
			];
			return (
				<>
					<div className="wsx-color-text-light wsx-mb-26">
						{ __(
							'Unlock the full potential of WholesaleX to create and manage WooCommerce B2B or B2B+B2C stores with ease!',
							'wholesalex'
						) }
					</div>
					<ul className="wsx-list wsx-d-flex wsx-flex-column wsx-gap-20">
						{ _hotFeatures &&
							_hotFeatures.map( ( feature, idx ) => {
								return (
									<li
										key={ idx }
										className="wsx-list-item wsx-d-flex wsx-item-center wsx-gap-8"
									>
										<span className="wsx-lh-0">
											{ Icons.listCheckMark }
										</span>
										{ feature }
									</li>
								);
							} ) }
					</ul>
				</>
			);
		};
		const communityContent = __(
			'Join the Facebook community of WholesaleX to stay up-to-date and share your thoughts and feedback.',
			'wholesalex'
		);
		const featureRequestContent = __(
			'Can’t find your desired feature? Let us know your requirements. We will definitely take them into our consideration.',
			'wholesalex'
		);

		const newsCardContent = () => {
			const links = [
				{
					url: 'https://getwholesalex.com/docs/wholesalex/getting-started/?utm_source=wholesalex-menu&utm_medium=sales_summary-news_tips&utm_campaign=wholesalex-DB',
					title: __( 'Getting Started Guides', 'wholesalex' ),
				},
				{
					url: 'https://getwholesalex.com/docs/wholesalex/dynamic-rule/?utm_source=wholesalex-menu&utm_medium=sales_summary-news_tips&utm_campaign=wholesalex-DB',
					title: __(
						'How to Create the Dynamic Rules',
						'wholesalex'
					),
				},
				{
					url: 'https://getwholesalex.com/docs/wholesalex/user-roles/?utm_source=wholesalex-menu&utm_medium=sales_summary-news_tips&utm_campaign=wholesalex-DB',
					title: __( 'How to Create User Roles', 'wholesalex' ),
				},
				{
					url: 'https://getwholesalex.com/docs/wholesalex/registration-form-builder/?utm_source=wholesalex-menu&utm_medium=sales_summary-news_tips&utm_campaign=wholesalex-DB',
					title: __(
						'How to Create a Registration Form',
						'wholesalex'
					),
				},
				{
					url: 'https://getwholesalex.com/blog/?utm_source=wholesalex-menu&utm_medium=sales_summary-news_tips&utm_campaign=wholesalex-DB',
					title: __( 'WholesaleX Blog', 'wholesalex' ),
				},
			];

			return (
				<ul className="wsx-list wsx-d-flex wsx-flex-column wsx-gap-16">
					{ links.map( ( link ) => {
						return (
							<li className="wsx-list-item" key={ link.title }>
								<a
									className="wsx-link wsx-d-flex wsx-item-start wsx-gap-8 wsx-color-text-medium"
									href={ link.url }
								>
									<span
										className="wsx-lh-0 wsx-rtl-arrow"
										style={ { marginTop: '2px' } }
									>
										{ Icons.arrowRight }
									</span>
									{ link.title }
								</a>
							</li>
						);
					} ) }
				</ul>
			);
		};

		const getContent = ( name ) => {
			switch ( name ) {
				case 'hot_feature_card':
					return hotFeatureCardContent();
				case 'community_card':
					return communityContent;
				case 'feature_request_card':
					return featureRequestContent;
				case 'news_card':
					return newsCardContent();

				default:
					break;
			}
		};
		const getButton = ( name ) => {
			switch ( name ) {
				case 'hot_feature_card':
					return (
						<Button
							label={ __( 'Upgrade to Pro', 'wholesalex' ) }
							iconName="growUp"
							background="secondary"
							onClick={ () =>
								handleLinkAction(
									'https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=sales_summary-pro_features-upgrade_to_pro&utm_campaign=wholesalex-DB'
								)
							}
						/>
					);
				case 'community_card':
					return (
						<Button
							label={ __(
								'Join WholesaleX Community',
								'wholesalex'
							) }
							background="base2"
							onClick={ () =>
								handleLinkAction(
									'https://www.facebook.com/groups/438050567242124/'
								)
							}
							borderColor="primary"
						/>
					);
				case 'feature_request_card':
					return (
						<Button
							label={ __( 'Request a Feature', 'wholesalex' ) }
							background="primary"
							onClick={ () =>
								handleLinkAction(
									'https://getwholesalex.com/roadmap/?utm_source=wholesalex-menu&utm_medium=sales_summary-feature_request&utm_campaign=wholesalex-DB'
								)
							}
						/>
					);
				case 'news_card':
					return '';

				default:
					break;
			}
		};
		let cardContent = [
			{
				name: 'hot_feature_card',
				title: __( 'Pro Features', 'wholesalex' ),
				content: getContent( 'hot_feature_card' ),
				button: getButton( 'hot_feature_card' ),
				className: 'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
			},
			{
				name: 'community_card',
				title: __( 'WholesaleX Community', 'wholesalex' ),
				content: getContent( 'community_card' ),
				button: getButton( 'community_card' ),
				className: 'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
			},
			{
				name: 'feature_request_card',
				title: __( 'Feature Request', 'wholesalex' ),
				content: getContent( 'feature_request_card' ),
				button: getButton( 'feature_request_card' ),
				className: 'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
			},
			{
				name: 'news_card',
				title: __( 'News, Tips & Updates', 'wholesalex' ),
				content: getContent( 'news_card' ),
				button: getButton( 'news_card' ),
				className: 'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
			},
		];

		if ( wholesalex?.is_pro_active ) {
			delete cardContent[ 0 ];
		}

		if ( ! wholesalex?.is_pro_active ) {
			cardContent = [
				{
					name: 'hot_feature_card',
					title: __( 'Pro Features', 'wholesalex' ),
					content: getContent( 'hot_feature_card' ),
					button: getButton( 'hot_feature_card' ),
					className:
						'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
				},
				{
					name: 'community_card',
					title: __( 'WholesaleX Community', 'wholesalex' ),
					content: getContent( 'community_card' ),
					button: getButton( 'community_card' ),
					className:
						'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
				},
				{
					name: 'feature_request_card',
					title: __( 'Feature Request', 'wholesalex' ),
					content: getContent( 'feature_request_card' ),
					button: getButton( 'feature_request_card' ),
					className:
						'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
				},
				{
					name: 'news_card',
					title: __( 'News, Tips & Updates', 'wholesalex' ),
					content: getContent( 'news_card' ),
					button: getButton( 'news_card' ),
					className:
						'wsx-card wsx-card-border wsx-shadow-none wsx-mt-40',
				},
			];
		}

		return (
			<>
				{ wholesalex?.is_pro_active && renderCounterCard( 'message' ) }
				{ renderCounterCard( 'registration' ) }

				{ ! wholesalex?.whitelabel_enabled &&
					cardContent.map( ( card ) => {
						return dashboardCard(
							card.title,
							card.content,
							card.button,
							card.className
						);
					} ) }
			</>
		);
	};

	useEffect( () => {
		setDateQueryData( {
			...dateQuery,
			primaryDate: getCurrentDates(
				dateQuery,
				`period=${ dateQuery.period }&compare=previous_year&after=${ dateQuery.after }&before=${ dateQuery.before }`
			).primary,
		} );
	}, [ dateQuery ] );

	useEffect( () => {
		const _startDate =
			dateQueryData.primaryDate.after.format( isoDateFormat );
		const _endDate =
			dateQueryData.primaryDate.before.format( isoDateFormat );
		fetchData(
			'get',
			'b2b_order_data',
			_startDate,
			_endDate,
			dateQueryData.period
		);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ dateQueryData ] );

	useEffect( () => {
		fetchData( 'get', 'new_messages_count' );

		fetchData( 'get', 'new_registrations_count' );

		fetchData( 'get', 'recent_orders' );

		fetchData( 'get', 'pending_registrations' );

		fetchData( 'get', 'top_customers' );

		fetchData( 'get', 'b2b_customer_count' );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const renderTopCustomers = () => {
		const headings = wholesalex_overview.top_customer_heading;
		const topCustomerHeadingList =
			Object.keys( headings ).map( ( heading ) => ( {
				title: headings[ heading ].title,
				minWidth: heading === 'name_n_email' ? '228px' : undefined,
			} ) ) || [];

		const topCustomerList =
			data?.top_customers?.map( ( customer ) => {
				const isEmail = !! customer.user_email;
				const commonFields = [
					{
						content: customer.display_name,
						minWidth: '228px',
						type: isEmail ? 'name_n_email' : undefined,
						userEmail: isEmail ? customer.user_email : undefined,
						avatar: customer.avatar_url || undefined,
					},
					{
						content: `${ customer.total_purchase }${ wholesalex_overview.whx_dr_currency }`,
					},
					{
						content: customer.wholesalex_role,
					},
				];
				if (
					wholesalex?.is_pro_active &&
					wholesalex?.settings?.wsx_addon_wallet === 'yes'
				) {
					commonFields.splice( 1, 0, {
						content: `${ customer.wallet_balance }`,
					} );
				}

				return commonFields;
			} ) || [];

		return (
			<Table
				headerItems={ topCustomerHeadingList }
				tableItems={ topCustomerList }
			/>
		);
	};
	const renderPendingRegister = () => {
		const headings = wholesalex_overview.pending_registration_heading;
		const topCustomerHeadingList =
			Object.keys( headings ).map( ( heading ) => {
				return heading === 'name_n_email'
					? { title: headings[ heading ].title, minWidth: '228px' }
					: { title: headings[ heading ].title };
			} ) || [];

		const topCustomerList =
			data?.pending_registrations?.map( ( customer ) => {
				const isEmail = customer.user_email ? true : false;

				return [
					{
						content: customer.display_name,
						minWidth: '228px',
						type: isEmail ? 'name_n_email' : undefined,
						userEmail: isEmail ? customer.user_email : undefined,
						avatar: customer.avatar_url
							? customer.avatar_url
							: undefined,
					},
					{
						content: customer.user_registered
							.split( ' ' )
							.map( ( part, index ) => {
								return index === 0 ? part + ' /' : part;
							} )
							.join( ' ' ),
					},
					{
						content: customer.registration_role,
					},
					{
						content: __( 'Review', 'wholesalex' ),
						link: customer.edit_user,
						type: 'button',
					},
				];
			} ) || [];

		return (
			<Table
				headerItems={ topCustomerHeadingList }
				tableItems={ topCustomerList }
			/>
		);
	};
	const renderRecentOrder = () => {
		const headings = wholesalex_overview.recent_order_heading;
		const topCustomerHeadingList =
			Object.keys( headings ).map( ( heading ) => {
				return { title: headings[ heading ].title };
			} ) || [];

		const topCustomerList =
			data?.recent_orders?.map( ( customer ) => {
				return [
					{
						content: customer.ID,
					},
					{
						content: customer.customer_name,
					},
					{
						content: customer.order_date,
					},
					{
						content: customer.order_status,
					},
					{
						content: __( 'View Order', 'wholesalex' ),
						link: customer.view_order,
						type: 'button',
					},
				];
			} ) || [];

		return (
			<Table
				headerItems={ topCustomerHeadingList }
				tableItems={ topCustomerList }
			/>
		);
	};

	return (
		<>
			<div className="wsx-wrapper">
				<div className="wsx-dashboard wsx-container">
					<div className="wsx-container-left">
						<div className="wsx-dashboard-sale-summary">
							<div className="wsx-dashboard-header wsx-justify-wrapper">
								<div className="wsx-title wsx-color-text-dark">
									{ __(
										'Sales Summary (B2B)',
										'wholesalex'
									) }
								</div>
								<DateRangePicker
									key="daterange"
									onRangeSelect={ ( e ) => {
										setDateQuery( e );
									} }
									dateQuery={ dateQueryData }
									isoDateFormat={ isoDateFormat }
								/>
							</div>

							{ graphLoader && <LoadingGif /> }
							{ renderSaleSummaryCards() }

							{ /* New Custom Chart */ }
							<div className="wsx-mb-40">
								{ ' ' }
								<SalesChart data={ graphData } />{ ' ' }
							</div>
						</div>

						<div
							className="wsx-table-wrapper"
							style={ {
								overflow: 'hidden',
								borderRadius: 'var(--border-radius-md)',
								boxShadow:
									'0 2px 4px 0 var(--color-shadow-primary)',
							} }
						>
							<TabContainer
								tabItems={ [
									{
										name: 'customer',
										title: __(
											'Top Customers',
											'wholesalex'
										),
									},
									{
										name: 'recent_order',
										title: __(
											'Recent Orders',
											'wholesalex'
										),
									},
									{
										name: 'pending_user',
										title: __(
											'Pending Registrations',
											'wholesalex'
										),
									},
								] }
								initialTabItem="customer"
								type="secondary"
								onTabSelect={ onTabSelect }
							/>

							{ currentTab === 'customer' &&
								renderTopCustomers() }
							{ currentTab === 'recent_order' &&
								renderRecentOrder() }
							{ currentTab === 'pending_user' &&
								renderPendingRegister() }
						</div>
					</div>

					<div className="wsx-container-right">
						{ renderDashboardCards() }
					</div>
				</div>
			</div>
		</>
	);
};
export default Overview;
