import React from 'react';
import { __ } from '@wordpress/i18n';

const SaleSummaryCards = () => {
	const saleSummaryCard = (
		iconClass,
		title,
		valueType = '',
		value = '',
		name = ''
	) => {
		return (
			<div
				className={ `wholesalex_dashboard_sale_summary_card wholesalex_dashboard_sale_summary_card__${ name }` }
			>
				<div className="wholesalex_dashboard_sale_summary_card__image">
					<span
						className={ `dashicons ${ iconClass } wholesalex_sale_summary_card_icon ` }
					></span>
				</div>
				<div className="wholesalex_dashboard_sale_summary_card__content">
					<div className="wholesalex_dashboard_sale_summary_card__title">
						{ title }
					</div>
					{ value === '' && (
						<div className="wholesalex_skeleton_loader"></div>
					) }
					{ valueType === 'html' && (
						<div
							className="wholesalex_dashboard_sale_summary_card__count"
							dangerouslySetInnerHTML={ { __html: value } }
						></div>
					) }
					{ valueType === 'number' && (
						<div className="wholesalex_dashboard_sale_summary_card__count">
							{ value }
						</div>
					) }
				</div>
			</div>
		);
	};
	const saleSummaryCards = [
		{
			name: 'b2b_customer',
			iconClass: 'dashicons-admin-users',
			title: __( 'Customer Nr.(B2B)', 'wholesalex' ),
			valueType: 'number',
			value: data?.b2b_customer_count,
		},
		{
			name: 'b2b_total_order',
			iconClass: 'dashicons-screenoptions',
			title: __( 'Total Order(B2B)', 'wholesalex' ),
			valueType: 'number',
			value: '100',
		},
		{
			name: 'b2b_total_sale',
			iconClass: 'dashicons-money-alt',
			title: __( 'Total Sale(B2B)', 'wholesalex' ),
			valueType: 'html',
			value: '100',
		},
		{
			name: 'b2b_net_earning',
			iconClass: 'dashicons-cart',
			title: __( 'Net Earning(B2B)', 'wholesalex' ),
			valueType: 'number',
			value: '100',
		},
		{
			name: 'b2b_gross_sale',
			iconClass: 'dashicons-cart',
			title: __( 'Gross Sale(B2B)', 'wholesalex' ),
			valueType: 'number',
			value: '100',
		},
	];

	return (
		<div className="wholesalex_dashboard_sale_summary_cards">
			{ saleSummaryCards.map( ( card ) => {
				return saleSummaryCard(
					card.iconClass,
					card.title,
					card.valueType,
					card.value,
					card.name
				);
			} ) }
		</div>
	);
};
