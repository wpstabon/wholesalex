import React from 'react';
import Wizard from '../installation-wizard/Wizard';
import Overview from './Overview';
const OverviewIndex = () => {
	return (
		<>
			{ ! wholesalex_overview.wsx_wizard___wholesalex_initial_setup ? (
				<div className="wsx_wizard_container">
					<Wizard />
				</div>
			) : (
				<Overview />
			) }
		</>
	);
};

export default OverviewIndex;
