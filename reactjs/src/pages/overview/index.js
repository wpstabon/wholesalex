import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider, Route, useRouter } from '../../contexts/RouterContext';
import OverviewIndex from './OverviewIndex';
import RolesVIew from '../roles/RolesVIew';
import Addons from '../addons/Addons';
import Header from '../../components/Header';
import Users from '../users/Users';
import EmailLists from '../EmailTemplates/EmailLists';
import Conversation from '../conversation/Conversation';
import RegistrationEditorBuilder from '../registration_form_builder';
import Setting from '../settings/Setting';
import DynamicRulesView from '../dynamic_rules';
import Features from '../features/Features';
import License from '../license/License';
import Support from '../support/Support';
import EditDynamicRule from '../dynamic_rules/EditDynamicRule';
import { DataProvider } from '../../contexts/DataProvider';
import EditUserRole from '../roles/EditUserRole';
import '../../assets/scss/Common.scss';
import { ToastContextProvider } from '../../context/ToastsContexts';

const App = () => {
	const { currentHash } = useRouter();

	const showHeader = currentHash !== '#/registration';

	return (
		<>
			{ showHeader && <Header title={ '' } /> }
			<Route path={ '' } component={ OverviewIndex } />
			<Route path={ '/' } component={ OverviewIndex } />
			<Route path={ '/user-role' } component={ RolesVIew } />
			<Route path={ '/user-role/edit/:id' } component={ EditUserRole } />
			<Route path={ '/dynamic-rules' } component={ DynamicRulesView } />
			<Route
				path={ '/dynamic-rules/edit/:id' }
				component={ EditDynamicRule }
			/>
			<Route path={ '/addons' } component={ Addons } />
			<Route
				path={ '/registration' }
				component={ RegistrationEditorBuilder }
			/>
			<Route path={ '/conversation' } component={ Conversation } />
			<Route path={ '/users' } component={ Users } />
			<Route path={ '/emails' } component={ EmailLists } />
			<Route path={ '/settings' } component={ Setting } />
			<Route path={ '/features' } component={ Features } />
			<Route path={ '/license' } component={ License } />
			<Route path={ '/quick-support' } component={ Support } />
		</>
	);
};

document.addEventListener( 'DOMContentLoaded', function () {
	const rootElement = document.getElementById( 'wholesalex-overview' );
	if ( rootElement ) {
		const root = ReactDOM.createRoot( rootElement );
		root.render(
			<React.StrictMode>
				<DataProvider>
					<RouterProvider>
						<ToastContextProvider>
							<App />
						</ToastContextProvider>
					</RouterProvider>
				</DataProvider>
			</React.StrictMode>
		);
	}
} );
