import React, { useRef, useState } from 'react';
import OverlayWindow from '../../components/OverlayWindow';
import DragDropFileUpload from '../../components/DragDropFileUpload';
import { __ } from '@wordpress/i18n';
import Slider from '../../components/Slider';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';

const Import = ( { windowRef, toggleOverlayWindow, setRules } ) => {
	const [ selectedFile, setSelectedFile ] = useState( null );
	const [ loader, setLoader ] = useState( false );
	const [ isUpdateExisting, setIsUpdateExisting ] = useState( false );

	const [ step, setStep ] = useState( 'upload' );
	const [ importData, setImportData ] = useState( {} );

	const mappingFormRef = useRef( null );

	const uploadFile = () => {
		setLoader( true );

		const formData = new FormData();

		formData.append( 'import', selectedFile );
		formData.append(
			'action',
			'wholesalex_dynamic_rule_import_upload_file'
		);
		formData.append( 'update_existing', isUpdateExisting ? 'yes' : 'no' );
		formData.append( 'nonce', wholesalex.nonce );

		fetch( wholesalex.ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.status ) {
					setStep( 'column_mapping' );
					setImportData( {
						headers: res.headers,
						mapped_items: res.mapped_items,
						args: res.args,
						sample: res.sample,
						mapping_options: res.mapping_options,
						file: res.file,
					} );
				} else {
					//Error Occurred!
				}
				setLoader( false );
			} );
	};

	const runImporter = ( e ) => {
		e.preventDefault();
		const formData = new FormData( mappingFormRef.current );

		formData.append( 'import', selectedFile );
		formData.append( 'action', 'wholesalex_dynamic_rule_run_importer' );
		formData.append( 'file', importData.file );
		formData.append( 'nonce', wholesalex.nonce );
		formData.append( 'update_existing', isUpdateExisting ? 'yes' : 'no' );

		fetch( wholesalex.ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res.status ) {
					performImport( res );
				} else {
					window.alert( __( 'Error Occured!', 'wholesalex' ) );
				}
			} );
	};

	const columnMappingForm = () => {
		return (
			<div className="column_mapping  wholesalex-importer">
				<div className="column_mapping__heading">
					<h2>
						{ __(
							'Map CSV Fields to Dynamic Rules',
							'wholesalex'
						) }
					</h2>
					<div>
						{ __(
							'Select fields from your CSV file to map against role fields, or to ignore during import.',
							'wholesalex'
						) }
					</div>
				</div>
				<form
					ref={ mappingFormRef }
					className="wc-progress-from-content woocommerce-importer"
					onSubmit={ ( e ) => {
						e.preventDefault();
					} }
				>
					<section className="wholesalex-importer-mapping-table-wrapper wc-importer-mapping-table-wrappe">
						<table className="wsx-table widefat wc-importer-mapping-table">
							<thead>
								<tr>
									<th>
										{ __( 'Column name', 'wholesalex' ) }
									</th>
									<th>
										{ __( 'Map to field', 'wholesalex' ) }
									</th>
								</tr>
							</thead>
							<tbody>
								{ importData.headers &&
									importData.headers.map( ( header, idx ) => {
										const mappedValue =
											importData.mapped_items[ idx ];
										return (
											<tr key={ idx }>
												<td className="wc-importer-mapping-table-name">
													{ header }
												</td>
												<td className="wc-importer-mapping-table-field">
													<input
														type="hidden"
														name={ `map_from[ ${ idx }]` }
														value={ header }
													/>
													<select
														className="wsx-select"
														name={ `map_to[ ${ idx }]` }
													>
														<option value="">
															{ __(
																'Do not import',
																'wholesalex'
															) }
														</option>
														{ importData.mapping_options &&
															Object.keys(
																importData.mapping_options
															).map( ( key ) => {
																return (
																	<option
																		key={
																			key
																		}
																		value={
																			key
																		}
																		selected={
																			mappedValue ===
																			key
																		}
																	>
																		{
																			importData
																				.mapping_options[
																				key
																			]
																		}
																	</option>
																);
															} ) }
													</select>
												</td>
											</tr>
										);
									} ) }
							</tbody>
						</table>
					</section>
				</form>

				<Button
					label={ __( 'Run the importer', 'wholesalex' ) }
					onClick={ runImporter }
					customClass="wsx-mt-32"
					background="tertiary"
					smallButton={ true }
				/>
			</div>
		);
	};

	const progress = () => {
		return (
			<div className="wholesalex_importer__importing  wholesalex-importer wsx-relative">
				<header>
					<h2>{ __( 'Importing', 'wholesalex' ) }</h2>
					<div>
						{ __(
							'Your Dynamic Rules are now being imported..',
							'wholesalex'
						) }
						.
					</div>
				</header>
				<LoadingGif insideContainer={ true } />
			</div>
		);
	};

	const uploadForm = () => {
		return (
			<div className="wsx-d-flex wsx-flex-column wsx-gap-20 wsx-relative">
				{ loader && <LoadingGif insideContainer={ true } /> }

				<DragDropFileUpload
					name={ 'import' }
					label={ __( 'Upload CSV', 'wholesalex' ) }
					help={ __(
						'You can upload only csv file format',
						'wholesalex'
					) }
					onChange={ ( file ) => {
						setSelectedFile( file );
					} }
					allowedTypes={ [ 'text/csv' ] }
				/>
				<Slider
					label={ __( 'Update Existing Rules', 'wholesalex' ) }
					help={ __(
						'Selecting"Update Existing Rules" will only update existing rules. No new rules will be added.',
						'wholesalex'
					) }
					value={ isUpdateExisting }
					className="wsx-slider-sm"
					isLabelSide={ true }
					onChange={ () => setIsUpdateExisting( ! isUpdateExisting ) }
				/>
				<Button
					label={ __( 'Continue', 'wholesalex' ) }
					background="primary"
					customClass="wsx-center-hz wsx-w-half wsx-text-center wsx-mt-20"
					onClick={ uploadFile }
				/>
			</div>
		);
	};

	const content = () => {
		switch ( step ) {
			case 'upload':
				return uploadForm();
			case 'column_mapping':
				return columnMappingForm();
			case 'importing':
				return progress();
			case 'done':
				return progressComplete();

			default:
				break;
		}
	};

	const [ , setProgressValue ] = useState( 0 );
	const [ viewErrorLog, setViewErrorLog ] = useState( false );
	const performImport = ( res ) => {
		// Number of import successes/failures.
		let imported = 0;
		let failed = 0;
		let updated = 0;
		let skipped = 0;
		let position = 0;

		const runImport = () => {
			setStep( 'importing' );
			const formData = new FormData();

			formData.append(
				'action',
				'wholesalex_do_ajax_dynamic_rule_import'
			);
			formData.append( 'position', position );
			formData.append( 'file', importData.file );
			formData.append( 'delimiter', res.delimiter );
			formData.append( 'character_encoding', res.character_encoding );
			formData.append(
				'update_existing',
				isUpdateExisting ? 'yes' : 'no'
			);
			formData.append( 'nonce', wholesalex.nonce );

			res.mapping.from.map( ( item, idx ) => {
				formData.append( `mapping[from][${ idx }]`, item );
				return null;
			} );
			res.mapping.to.map( ( item, idx ) => {
				formData.append( `mapping[to][${ idx }]`, item );
				return null;
			} );
			fetch( wholesalex.ajax, {
				method: 'POST',
				body: formData,
			} )
				.then( ( res1 ) => res1.json() )
				.then( ( result ) => {
					if ( res.status ) {
						position = result.position;
						imported += result.imported;
						failed += result.failed;
						updated += result.updated;
						skipped += result.skipped;
						setProgressValue( result.percentage );

						if ( 'done' === result.position ) {
							setStep( 'done' );
							setImportData( {
								...importData,
								imported,
								failed,
								skipped,
								updated,
								errors: result.errors,
							} );
							if ( result.updated_rules ) {
								setRules( result.updated_rules );
							}
						} else {
							runImport();
						}
					} else {
						window.alert( __( 'Eror Occured!', 'wholesalex' ) );
					}
				} );
		};

		runImport();
	};

	const progressComplete = () => {
		return (
			<div className="wholesalex-progress-form-content  wholesalex-importer ">
				<section className="wholesalex-importer-done success-popup-content">
					<span className="success-icon">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 52 52"
							className="wholesalex-animation"
						>
							<circle
								className="wholesalex__circle"
								cx="26"
								cy="26"
								r="25"
								fill="none"
							/>
							<path
								className="wholesalex__check"
								fill="none"
								d="M14.1 27.2l7.1 7.2 16.7-16.8"
							/>
						</svg>
					</span>
					<div className="import-complete-text message">
						{ __( 'Import Complete!', 'wholesalex' ) }
					</div>
					{ importData.imported > 0 && (
						<div
							className="wholesalex-imported wsx-font-18 wsx-font-medium"
							style={ { color: 'var(--color-positive)' } }
						>
							{ importData.imported }
							{ __( 'Dynamic Rules Imported.', 'wholesalex' ) }
						</div>
					) }
					{ importData.updated > 0 && (
						<div
							className="wholesalex-updated wsx-mt-8"
							style={ { color: 'var(--color-lime)' } }
						>
							{ importData.updated }
							{ __( 'Dynamic Rules Updated.', 'wholesalex' ) }
						</div>
					) }
					{ importData.skipped > 0 && (
						<div
							className="wholesalex-skipped wsx-mt-8"
							style={ { color: 'var(--color-negative)' } }
						>
							{ importData.skipped }
							{ __( 'Dynamic Rules Skipped.', 'wholesalex' ) }
						</div>
					) }
					{ importData.failed > 0 && (
						<div
							className="wholesalex-failed wsx-mt-8"
							style={ { color: 'var(--color-text-body)' } }
						>
							{ importData.failed }
							{ __( 'Dynamic Rules Failed.', 'wholesalex' ) }
						</div>
					) }

					{ importData?.errors && importData?.errors?.length > 0 && (
						<Button
							label={ __( 'View Error Logs', 'wholesalex' ) }
							onClick={ () => setViewErrorLog( ! viewErrorLog ) }
							smallButton={ true }
							customClass="wsx-mt-16"
							background="secondary"
						/>
					) }
				</section>
				{ viewErrorLog && (
					<section className="wholesalex-importer-log wsx-mt-20">
						<table className="wsx-table widefat wholesalex-importer-log-table">
							<tr>
								<th>{ __( 'Dynamic Rule', 'wholesalex' ) }</th>
								<th>
									{ __( 'Reason for failure', 'wholesalex' ) }
								</th>
							</tr>
							<tbody>
								{ importData.errors &&
									importData.errors.map( ( err ) => {
										return (
											<tr key={ err.id }>
												<th>
													<code>{ err.id }</code>
												</th>
												<td>
													<code>{ err.message }</code>
												</td>
											</tr>
										);
									} ) }
							</tbody>
						</table>
					</section>
				) }
			</div>
		);
	};

	return (
		<OverlayWindow
			windowRef={ windowRef }
			heading={ __( 'Import Dynamic Rules', 'wholesalex' ) }
			onClose={ toggleOverlayWindow }
			content={ content }
		/>
	);
};

export default Import;
