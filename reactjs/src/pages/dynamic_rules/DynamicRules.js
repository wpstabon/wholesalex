import React, {
	createRef,
	Fragment,
	useContext,
	useEffect,
	useRef,
	useState,
} from 'react';
import { __ } from '@wordpress/i18n';
import Slider from '../../components/Slider';
import Modal from '../../components/Modal';
import Toast from '../../components/Toast';
import Import from './Import';
import { Link, useNavigate } from '../../contexts/RouterContext';
import { DataContext } from '../../contexts/DataProvider';
import Search from '../../components/Search';
import Pagination from '../../components/Pagination';
import Button from '../../components/Button';
import TabContainer from '../../components/TabContainer';
import Icons from '../../utils/Icons';
import Tooltip from '../../components/Tooltip';
import Alert from '../../components/Alert';
import Select from '../../components/Select';
import { ToastContexts } from '../../context/ToastsContexts';

const DynamicRules = ( props ) => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	const [ fields, setFields ] = useState( wholesalex_overview.whx_dr_fields );
	const [ appState, setAppState ] = useState( {
		loading: false,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: {},
	} );

	const { contextDataRule, setContextDataRule, contextData, setContextData } =
		useContext( DataContext );
	const { savedRule } = contextDataRule;
	const { globalCurrentPage } = contextData;

	const [ modalStatus, setModalStatus ] = useState( false );

	const [ rules, setRules ] = useState(
		savedRule ? savedRule : wholesalex_overview.whx_dr_rule
	);
	const [ overlayWindow, setOverlayWindow ] = useState( {
		status: false,
		type: '',
	} );

	const { dispatch } = useContext( ToastContexts );

	const initialTab = 'latest';
	const [ alert, setAlert ] = useState( false );

	const [ isAlertVisible, setIsAlertVisible ] = useState( false );
	const [ pendingAction, setPendingAction ] = useState( null );

	const [ selectedRows, setSelectedRows ] = useState( [] );
	const [ bulkAction, setBulkAction ] = useState( '' );

	const navigate = useNavigate();
	const showRulesPerPage = 8;

	const [ pageSize, setPageSize ] = useState( showRulesPerPage );
	const [ currentPage, setCurrentPage ] = useState( 0 );
	const [ paginatedRoles, setPaginatedRoles ] = useState( [] );

	const [ searchQuery, setSearchQuery ] = useState( '' );
	const [ filteredRules, setFilteredRules ] = useState( rules );
	const [ isAscending, setIsAscending ] = useState( true );

	useEffect( () => {
		let filtered = rules;
		if ( searchQuery.length >= 3 ) {
			filtered = rules.filter(
				( rule ) =>
					rule._rule_title &&
					rule._rule_title
						.toLowerCase()
						.includes( searchQuery.toLowerCase() )
			);
		}
		if ( ! isAscending ) {
			filtered = [ ...filtered ].reverse();
		}
		setFilteredRules( filtered );
	}, [ searchQuery, isAscending, rules ] );

	const handleSortToggle = () => {
		setIsAscending( ! isAscending );
	};

	useEffect( () => {
		const startPage = currentPage * pageSize;
		const endPage = startPage + pageSize;
		setPaginatedRoles( filteredRules.slice( startPage, endPage ) );
	}, [ filteredRules, currentPage, pageSize ] );

	const handleGotoPage = ( pageIndex ) => {
		setCurrentPage( pageIndex );
	};

	useEffect( () => {
		if ( savedRule && savedRule.length >= rules.length ) {
			setRules( savedRule );
		} else {
			setRules( rules );
		}
	}, [ savedRule, rules ] );

	const fetchData = async (
		type = 'get',
		index = '',
		_rule = '',
		_key = ''
	) => {
		const attr = {
			type,
			action: 'dynamic_rule_action',
			nonce: wholesalex.nonce,
			isFrontend: props?.isFrontend,
		};

		const rulesBeforeSave = [ ...rules ];

		let readyToSave = true;
		if ( _key !== 'delete' && type === 'post' ) {
			const ruleToBeSave = { ..._rule };
			ruleToBeSave.limit = {
				_usage_limit: ruleToBeSave?.limit?._usage_limit
					? ruleToBeSave.limit._usage_limit
					: '',
				_start_date: ruleToBeSave?.limit?._start_date
					? ruleToBeSave.limit._start_date
					: '',
				_end_date: ruleToBeSave?.limit?._end_date
					? ruleToBeSave.limit._end_date
					: '',
			};

			if (
				empty( ruleToBeSave._rule_type ) ||
				( ruleToBeSave._rule_type !== 'restrict_checkout' &&
					ruleToBeSave._rule_type !== 'restrict_product_visibility' &&
					ruleToBeSave._rule_type !== 'hidden_price' &&
					ruleToBeSave._rule_type !== 'non_purchasable' &&
					empty( ruleToBeSave[ ruleToBeSave._rule_type ] ) )
			) {
				readyToSave = false;
			}
			if ( empty( ruleToBeSave._rule_for ) ) {
				readyToSave = false;
			}
			if (
				! (
					ruleToBeSave._rule_for === 'all_users' ||
					ruleToBeSave._rule_for === 'all' ||
					ruleToBeSave._rule_for === 'all_roles'
				) &&
				empty( ruleToBeSave[ ruleToBeSave._rule_for ] )
			) {
				readyToSave = false;
			}
			if (
				empty( ruleToBeSave._product_filter ) ||
				( ruleToBeSave._product_filter !== 'all_products' &&
					empty( ruleToBeSave[ ruleToBeSave._product_filter ] ) )
			) {
				readyToSave = false;
			}

			if ( ! readyToSave ) {
				setTimeout( () => {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'error',
							message: __(
								'Please Fill All Fields.',
								'wholesalex'
							),
						},
					} );
					const parent = [ ...rules ];
					parent[ index ] = {
						...parent[ index ],
						_rule_status: false,
					};
					setRules( parent );
				}, 700 );
				return;
			}
		}

		switch ( _rule._rule_type ) {
			case 'buy_x_get_one':
				{
					const minPurchaseCount =
						_rule.buy_x_get_one._minimum_purchase_count;
					if ( ! ( minPurchaseCount && minPurchaseCount > 1 ) ) {
						dispatch( {
							type: 'ADD_MESSAGE',
							payload: {
								id: Date.now().toString(),
								type: 'error',
								message: __(
									'Minimum Product Quantity Should Greater then Free Product Quantity.',
									'wholesalex'
								),
							},
						} );
						return;
					}
				}
				break;
			default:
				break;
		}

		let requestFor = 'fetch';
		switch ( _key ) {
			case '_rule_status':
				attr.check = _key;
				break;
			case 'delete':
				attr.delete = true;
				break;
		}

		const ruleId = _rule.id;
		if ( _rule !== '' && type === 'post' && readyToSave ) {
			attr.id = ruleId;
			attr.rule = JSON.stringify( _rule );

			const _state = { ...appState };
			_state.loadingOnSave[ ruleId ] = true;
			setAppState( { ...appState, ..._state } );
			requestFor = 'save_rule';
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/dynamic_rule_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( type === 'post' ) {
				if ( ruleId !== '' ) {
					const _state = { ...appState };
					_state.loadingOnSave[ ruleId ] = false;
					setAppState( { ...appState, ..._state } );
				}
			}
			if ( res.success ) {
				if ( type === 'get' ) {
					setFields( res.data.default );
					setAppState( {
						...appState,
						loading: false,
					} );
					setRules( res.data.value );
				} else {
					if ( requestFor === 'save_rule' ) {
						const parent = [ ...rules ];
						parent[ index ] = { ...parent[ index ], ..._rule };
						setContextDataRule( ( prevData ) => ( {
							...prevData,
							...prevData,
							savedRule: parent,
						} ) );
						setRules( parent );
					}
					if ( attr.delete ) {
						let parent = [ ...rulesBeforeSave ];
						parent = parent.filter( ( row, r ) => {
							return index !== r;
						} );
						setRules( parent );
						if ( globalCurrentPage > 1 ) {
							setContextData( ( prevContext ) => ( {
								...prevContext,
								globalCurrentPage:
									prevContext.globalCurrentPage - 1,
							} ) );
						}
						setContextDataRule( ( prevData ) => {
							const updatedRules = prevData.newRules.filter(
								( rule ) => rule.id !== ruleId
							);
							return { ...prevData, savedRule: updatedRules };
						} );
					}

					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data.message,
						},
					} );
				}
			} else {
				setRules( [ ...rulesBeforeSave ] );
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: res.data.message,
					},
				} );
			}
		} );
	};
	const fetchSaveDuplicateRule = async ( dupRule ) => {
		const attr = {
			type: 'post',
			action: 'dynamic_rule_action',
			nonce: wholesalex.nonce,
			id: dupRule.id,
			rule: JSON.stringify( dupRule ),
			isFrontend: props?.isFrontend,
		};

		try {
			const response = await wp.apiFetch( {
				path: '/wholesalex/v1/dynamic_rule_action',
				method: 'POST',
				data: attr,
			} );

			if ( response.success ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: response.data.message,
					},
				} );
				return { success: true, data: dupRule };
			}
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'error',
					message: response.data.message,
				},
			} );
			return { success: false };
		} catch ( error ) {
			dispatch( {
				type: 'ADD_MESSAGE',
				payload: {
					id: Date.now().toString(),
					type: 'error',
					message: 'Error saving duplicated rule',
				},
			} );
			return { success: false };
		}
	};

	const empty = ( ele ) => {
		if (
			ele === '' ||
			( typeof ele === 'object' && Object.keys( ele ).length === 0 ) ||
			ele === undefined
		) {
			return true;
		}
		return false;
	};

	useEffect( () => {
		const newRules = rules;
		setContextDataRule( ( prevData ) => ( {
			...prevData,
			...prevData,
			newRules,
		} ) );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ rules ] );

	const buttonHandler = ( type ) => {
		switch ( type ) {
			case 'create':
				{
					const copy = [ ...rules ];
					const _newRule = {
						id: Date.now().toString(),
						label: __( 'Rule Title', 'wholesalex' ),
					};
					copy.push( _newRule );
					setRules( copy );
					navigate(
						`/dynamic-rules/edit/${ Date.now().toString() }`
					);

					setContextData( ( prevContext ) => ( {
						...prevContext,
						globalNewDynamicRules: Array.isArray(
							prevContext.globalNewDynamicRules
						)
							? [ ...prevContext.globalNewDynamicRules, _newRule ]
							: [ _newRule ],
					} ) );
				}

				break;
			default:
				break;
		}
	};
	const buttonsData = ( fieldsData, field ) => {
		return (
			<div key={ field } className="wsx-justify-wrapper wsx-mb-40">
				<Button
					onClick={ ( e ) => {
						e.preventDefault();
						buttonHandler( 'create' );
					} }
					label={ __( 'Create Dynamic Rule', 'wholesalex' ) }
					background="primary"
					iconName="plus"
				/>
				{ wholesalex?.is_admin_interface && (
					<div className="wsx-btn-group wsx-gap-8">
						<Button
							label={ __( 'Doc', 'wholesalex' ) }
							iconName="doc"
							background="base1"
							buttonLink="https://getwholesalex.com/docs/wholesalex/dynamic-rule/"
						/>
						<Button
							label={ __( 'Tutorial', 'wholesalex' ) }
							iconName="play"
							background="base1"
							buttonLink="https://www.youtube.com/watch?v=Wme7rtG6dXc&list=PLgcWNgnvK8Cwtaq_TwzYDb2M8jBuYIivy&ab_channel=WholesaleX"
						/>
					</div>
				) }
			</div>
		);
	};
	const ruleRef = useRef( [] );
	const ruleStatusRef = useRef( [] );
	ruleRef.current = rules.map(
		( element, i ) => ruleRef.current[ i ] ?? createRef()
	);
	ruleStatusRef.current = rules.map(
		( element, i ) => ruleStatusRef.current[ i ] ?? createRef()
	);

	const deleteRule = ( e, index ) => {
		e.stopPropagation();
		setModalStatus( { status: true, index } );
	};
	const duplicateRule = async ( e, index ) => {
		e.stopPropagation();
		const copy = [ ...rules ];
		const dupRule = { ...copy[ index ] };
		let _ruleTitle = dupRule._rule_title;
		if ( ! _ruleTitle ) {
			_ruleTitle = __( 'Untitled', 'wholesalex' );
		}

		switch ( dupRule._rule_type ) {
			case 'quantity_based':
				if ( dupRule.quantity_based.tiers ) {
					const tiers = dupRule.quantity_based.tiers;
					dupRule.quantity_based.tiers = tiers.map( ( tier ) => {
						tier._id = Date.now().toString();
						return tier;
					} );
				}
				break;

			default:
				break;
		}

		dupRule.id = Date.now().toString();
		dupRule._rule_title = __( 'Duplicate of', 'wholesalex' ) + _ruleTitle;
		dupRule._rule_status = false;

		const result = await fetchSaveDuplicateRule( dupRule );

		if ( result.success ) {
			copy.splice( index + 1, 0, dupRule );
			setRules( copy );
		}
	};

	const deleteModal = ( index, rule ) => {
		return (
			modalStatus &&
			modalStatus.index === index && (
				<Modal
					smallModal={ true }
					title={
						rule._rule_title
							? rule._rule_title
							: __( 'Untitled Rule', 'wholesalex' )
					}
					status={ modalStatus }
					setStatus={ setModalStatus }
					onDelete={ () => {
						setModalStatus( false );
						fetchData( 'post', index, rules[ index ], 'delete' );
					} }
				/>
			)
		);
	};

	const getExportUrl = () => {
		const url = new URL( window.location.href );
		const addParams = {
			nonce: wholesalex_overview?.whx_dr_nonce,
			action: 'export-dynamic-rule-csv',
			exported_ids: selectedRows.join( ',' ),
		};

		const params = new URLSearchParams( url.search );
		if ( params.has( 'nonce' ) ) {
			params.delete( 'nonce' );
		}
		if ( params.has( 'action' ) ) {
			params.delete( 'action' );
		}

		const newParams = new URLSearchParams( [
			...Array.from( params.entries() ),
			...Object.entries( addParams ),
		] ).toString();
		const newUrl = `${ url.pathname }?${ newParams }`;
		return newUrl;
	};

	const handleRowSelection = ( rowId ) => {
		setSelectedRows( ( prevSelected ) =>
			prevSelected.includes( rowId )
				? prevSelected.filter( ( id ) => id !== rowId )
				: [ ...prevSelected, rowId ]
		);
	};

	const handleSelectAll = () => {
		if ( selectedRows.length === rules.length ) {
			setSelectedRows( [] );
		} else {
			const rulesIds = rules.map( ( rule ) => rule.id );
			setSelectedRows( rulesIds );
		}
	};

	const handleBulkActionChange = ( e ) => {
		setBulkAction( e.target.value );
	};

	const handleApplyBulkAction = () => {
		if (
			! bulkAction ||
			bulkAction === 'default' ||
			selectedRows.length === 0
		) {
			setAlert( true );
			return;
		}

		setPendingAction( bulkAction );
		setIsAlertVisible( true );
	};

	const confirmBulkAction = () => {
		let updatedFilteredRules = [ ...filteredRules ];

		switch ( pendingAction ) {
			case 'enable':
				selectedRows.forEach( ( id ) => {
					const index = updatedFilteredRules.findIndex(
						( rule ) => rule.id === id
					);
					if ( index !== -1 ) {
						updatedFilteredRules[ index ]._rule_status = 1;
					}
				} );
				break;

			case 'disable':
				selectedRows.forEach( ( id ) => {
					const index = updatedFilteredRules.findIndex(
						( rule ) => rule.id === id
					);
					if ( index !== -1 ) {
						updatedFilteredRules[ index ]._rule_status = 0;
					}
				} );
				break;

			case 'delete':
				updatedFilteredRules = updatedFilteredRules.filter(
					( rule ) => ! selectedRows.includes( rule.id )
				);
				setFilteredRules( updatedFilteredRules );
				setRules( updatedFilteredRules );
				updatePaginatedRoles( updatedFilteredRules );
				if ( globalCurrentPage > 1 ) {
					setContextData( ( prevContext ) => ( {
						...prevContext,
						globalCurrentPage: prevContext.globalCurrentPage - 1,
					} ) );
				}
				break;

			case 'export':
				window.location.href = getExportUrl();
				break;

			default:
				break;
		}

		wp.apiFetch( {
			path: '/wholesalex/v1/dynamic_bulk_action',
			method: 'POST',
			data: { ruleIds: selectedRows, action: pendingAction },
		} )
			.then( ( response ) => {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: response.message,
					},
				} );

				updatePaginatedRolesAfterDelete( updatedFilteredRules );
			} )
			.catch( () => {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message:
							'Failed to process bulk action. Please try again.',
					},
				} );
			} );

		setIsAlertVisible( false );
		setSelectedRows( [] );
	};

	const handleAlertClose = () => {
		setIsAlertVisible( false );
		setPendingAction( null );
		setAlert( false );
	};

	const updatePaginatedRoles = ( updatedFilteredRules ) => {
		const start = currentPage * pageSize;
		const end = start + pageSize;
		setPaginatedRoles( updatedFilteredRules.slice( start, end ) );
	};

	const updatePaginatedRolesAfterDelete = ( updatedFilteredRules ) => {
		const start = currentPage * pageSize;
		const end = start + pageSize;
		const updatedPaginatedRoles = updatedFilteredRules.slice( start, end );

		if ( updatedPaginatedRoles.length === 0 && currentPage > 0 ) {
			setCurrentPage( currentPage - 1 );
		} else {
			setPaginatedRoles( updatedPaginatedRoles );
		}
	};

	const getAppliedForRule = ( type, userData ) => {
		const typeMessages = {
			all: 'All Registered & Guest Users...',
			all_users: 'All Registered Users',
			all_roles: 'All Registered Roles',
		};

		if ( typeMessages[ type ] ) {
			return <div className="wsx-ellipsis">{ typeMessages[ type ] }</div>;
		}

		if ( type === 'specific_users' || type === 'specific_roles' ) {
			const entity = type === 'specific_users' ? 'Users' : 'Roles';
			return (
				<div className="wsx-d-flex wsx-item-center wsx-gap-8">
					<div className="wsx-d-flex wsx-item-center">
						{ userData &&
							userData.map( ( user, index ) => {
								return (
									index < 4 &&
									( user.image ? (
										<img
											className="wsx-profile-list"
											style={ {
												marginLeft: `${
													index === 0 ? 0 : -12
												}px`,
												border: `${
													index === 0 && 'none'
												}`,
											} }
											src={ user.image }
											alt={ `${ user.name }` }
										/>
									) : (
										<div
											className="wsx-profile-list"
											style={ {
												marginLeft: `${
													index === 0 ? 0 : -12
												}px`,
												border: `${
													index === 0 && 'none'
												}`,
											} }
										>{ `${ user.name.slice(
											0,
											2
										) }` }</div>
									) )
								);
							} ) }
					</div>
					{ userData.length === 1 && (
						<div
							className="wsx-ellipsis"
							style={ { maxWidth: '10rem' } }
						>
							{ ' ' }
							{ `${ userData[ 0 ].name }` }
						</div>
					) }
					{ userData.length !== 1 && userData.length < 5 && (
						<div> { `${ userData.length } ${ entity }` }</div>
					) }
					{ userData.length > 4 && (
						<div> { `+${ userData.length } ${ entity }` }</div>
					) }
				</div>
			);
		}

		return null;
	};

	const getProductFilter = ( type, filterData ) => {
		const filterTypes = {
			all_products: 'All Products',
			products_in_list: `Products`,
			products_not_in_list: `Products`,
			cat_in_list: `Categories`,
			cat_not_in_list: `Categories`,
			attribute_in_list: `Variations`,
			attribute_not_in_list: `Variations`,
		};

		if ( type === 'all_products' ) {
			return <div className="wsx-ellipsis">{ filterTypes[ type ] }</div>;
		}

		const entity = filterTypes[ type ];
		return (
			<div className="wsx-d-flex wsx-item-center wsx-gap-8">
				<div className="wsx-d-flex wsx-item-center">
					{ filterData &&
						filterData.map( ( filter, index ) => {
							return (
								index < 4 &&
								( filter.image ? (
									<img
										className="wsx-profile-list"
										style={ {
											marginLeft: `${
												index === 0 ? 0 : -12
											}px`,
											border: `${
												index === 0 && 'none'
											}`,
										} }
										src={ filter.image }
										alt={ `${ filter.name }` }
									/>
								) : (
									<div
										className="wsx-profile-list"
										style={ {
											marginLeft: `${
												index === 0 ? 0 : -12
											}px`,
											border: `${
												index === 0 && 'none'
											}`,
										} }
									>{ `${ filter.name.slice( 0, 2 ) }` }</div>
								) )
							);
						} ) }
				</div>
				{ filterData.length === 1 && (
					<div
						className="wsx-ellipsis"
						style={ { maxWidth: '8rem' } }
					>
						{ ' ' }
						{ `${ filterData[ 0 ].name }` }
					</div>
				) }
				{ filterData.length !== 1 && filterData.length < 5 && (
					<div>
						{ ' ' }
						{ `${
							filterData.length > 0 ? filterData.length : ''
						} ${ entity ?? 'Filter Not Found' }` }
					</div>
				) }
				{ filterData.length > 4 && (
					<div> { `+${ filterData.length } ${ entity }` }</div>
				) }
			</div>
		);
	};

	const getFilterData = ( rule ) => {
		if ( rule.products_in_list?.length ) {
			return rule.products_in_list;
		} else if ( rule.products_not_in_list?.length ) {
			return rule.products_not_in_list;
		} else if ( rule.cat_in_list?.length ) {
			return rule.cat_in_list;
		} else if ( rule.cat_not_in_list?.length ) {
			return rule.cat_not_in_list;
		} else if ( rule.attribute_in_list?.length ) {
			return rule.attribute_in_list;
		} else if ( rule.attribute_not_in_list?.length ) {
			return rule.attribute_not_in_list;
		}
		return '';
	};

	const getUserFilterData = ( rule ) => {
		if ( rule.specific_users?.length ) {
			return rule.specific_users;
		} else if ( rule.specific_roles?.length ) {
			return rule.specific_roles;
		}
		return '';
	};

	const getRuleTypeName = ( key ) => {
		const dynamicRulesName = {
			product_discount: 'Product Discount',
			cart_discount: 'Cart Discount',
			payment_discount: 'Payment Method Discount',
			payment_order_qty: 'Required Quantity for Payment Method',
			buy_x_get_one: 'BOGO Discounts (Buy X Get One Free)',
			shipping_rule: 'Shipping Rule',
			min_order_qty: 'Minimum Order Quantity',
			tax_rule: 'Tax Rule',
			restrict_checkout: 'Checkout Restriction',
			quantity_based: 'Quantity Based Discount',
			extra_charge: 'Extra Charge',
			buy_x_get_y: 'Buy X Get Y Free',
			max_order_qty: 'Maximum Order Quantity',
			restrict_product_visibility: 'Restrict Product Visibility',
			hidden_price: 'Hidden Price',
			non_purchasable: 'Non Purchasable',
		};
		return dynamicRulesName[ key ] || 'Type Not Found';
	};

	const optionsData = [
		{ value: 'default', label: 'Select Bulk Action' },
		{ value: 'enable', label: 'Enable' },
		{ value: 'disable', label: 'Disable' },
		{ value: 'delete', label: 'Delete' },
		{ value: 'export', label: 'Export' },
	];

	const ruleData = ( rulesData, section ) => {
		return (
			<>
				<div className="wsx-justify-wrapper wsx-slg-justify-wrapper wsx-mb-24">
					<div className="wsx-d-flex wsx-item-center wsx-gap-12 wsx-sm-flex-wrap wsx-sm-justify-center">
						<div className="wsx-d-flex wsx-gap-8 wsx-item-center">
							<Select
								options={ optionsData }
								onChange={ handleBulkActionChange }
								inputBackground="base1"
								borderColor="border-secondary"
								minWidth="182px"
							/>
							<Button
								label={ __( 'Apply', 'wholesalex' ) }
								onClick={ handleApplyBulkAction }
								background="base2"
								borderColor="primary"
								color="primary"
								customClass="wsx-font-14"
								padding="11px 20px"
							/>
						</div>
						<TabContainer
							tabItems={ [
								{
									name: 'latest',
									title: __( 'Latest', 'wholesalex' ),
								},
								{
									name: 'oldest',
									title: __( 'Oldest', 'wholesalex' ),
								},
							] }
							initialTabItem={ initialTab }
							wrapperClass="wsx-sm-justify-center"
							customClass="wsx-m-0"
							onTabSelect={ handleSortToggle }
						/>
					</div>

					<div className="wsx-d-flex wsx-item-center wsx-gap-8">
						<Button
							label={ __( 'Import', 'wholesalex' ) }
							background="base2"
							iconName="import"
							onClick={ onImportClick }
							customClass="wsx-font-regular"
						/>

						<Search
							type="text"
							name={ 'wsx-lists-search-user' }
							value={ searchQuery }
							onChange={ ( e ) =>
								setSearchQuery( e.target.value )
							}
							iconName="search"
							iconColor="#070707"
							background="transparent"
							borderColor="#868393"
							maxHeight="38px"
							placeholder="Search Rule"
						/>
					</div>
				</div>
				<Toast delay={ 3000 } position="top_right" />
				<div className="wsx-row-wrapper">
					<div className="wsx-row-container wsx-scrollbar">
						{ paginatedRoles.length === 0 ? (
							<div
								className="wsx-bg-base1 wsx-br-md wsx-d-flex wsx-item-center wsx-justify-center"
								style={ { height: '40vh', maxHeight: '370px' } }
							>
								{ Icons.noData }
							</div>
						) : (
							<div className="wsx-rules-header wsx-color-tertiary wsx-font-medium">
								<div className="wsx-rule-checkbox-with-title">
									<label
										className="wsx-label wsx-checkbox-option-wrapper"
										htmlFor="checkbox"
									>
										{ ' ' }
										<input
											type="checkbox"
											checked={
												selectedRows.length ===
												rules.length
											}
											onChange={ handleSelectAll }
										/>
										<span className="wsx-checkbox-mark"></span>
									</label>
									{ __( 'Rule Title', 'wholesalex' ) }
								</div>
								<div className="wsx-rule-status">
									{ __( 'Status', 'wholesalex' ) }
								</div>
								<div className="wsx-rule-types">
									{ __( 'Rule Type', 'wholesalex' ) }
								</div>
								<div className="wsx-rule-applied-users">
									{ __( 'Applied For', 'wholesalex' ) }
								</div>
								<div className="wsx-rule-product-filter">
									{ __( 'Product Filter', 'wholesalex' ) }
								</div>
								<div className="wsx-rule-actions wsx-text-end">
									{ __( 'Action', 'wholesalex' ) }
								</div>
							</div>
						) }
						{ paginatedRoles.map( ( rule, index ) => {
							//const actualIndex = currentPage * pageSize + index;
							const totalItems = paginatedRoles.length; // Assuming `items` is the array of all items
							const actualIndex = isAscending
								? currentPage * pageSize + index
								: totalItems -
								  1 -
								  ( currentPage * pageSize + index );
							return (
								<div
									key={ `${ section }_${ actualIndex }` }
									id={ `dynamic_rule_${ rule.id }` }
									className="wsx-rules-row"
								>
									<div className="wsx-rule-checkbox-with-title wsx-rule-item">
										<label
											className="wsx-label wsx-checkbox-option-wrapper"
											htmlFor="wsx-checkbox-label"
										>
											{ ' ' }
											<input
												type="checkbox"
												checked={ selectedRows.includes(
													rule.id
												) }
												onChange={ () =>
													handleRowSelection(
														rule.id
													)
												}
											/>
											<div className="wsx-checkbox-mark"></div>
										</label>
										<span className="wsx-ellipsis">
											{ rule.created_from ===
												'dokan_vendor_dashboard' ||
											rule.created_from ===
												'vendor_dashboard' ? (
												<span className="wholesalex_rule__dokan wholesalex_rule__vendor">
													{ __(
														'Vendor #',
														'wholesalex'
													) }
													{ rule.created_by }:{ ' ' }
												</span>
											) : (
												''
											) }
											{ rule._rule_title ? (
												<span>
													<Link
														to={ `/dynamic-rules/edit/${ rule.id }` }
													>
														{ rule._rule_title }
													</Link>
												</span>
											) : (
												__(
													'Untitled Rule',
													'wholesalex'
												)
											) }
										</span>
									</div>

									<Slider
										className="wsx-rule-status wsx-rule-item"
										name={ `${ rule.id }-rule-status-slider` }
										value={
											rule._rule_status ? true : false
										}
										onChange={ ( e ) => {
											e.stopPropagation();
											const parent = [ ...rules ];
											const copy = { ...rule };
											copy._rule_status =
												e.target.checked;
											parent[ actualIndex ] = copy;
											setRules( [ ...parent ] );
											fetchData(
												'post',
												actualIndex,
												copy,
												'_rule_status'
											);
										} }
									/>

									<div className="wsx-rule-types wsx-rule-item">
										<span className="wsx-ellipsis">
											{ getRuleTypeName(
												rule._rule_type
											) }
										</span>
									</div>
									<div className="wsx-rule-applied-users wsx-rule-item">
										{ getAppliedForRule(
											rule._rule_for,
											getUserFilterData( rule )
										) ?? 'Not Applied' }
									</div>
									<div className="wsx-rule-product-filter wsx-rule-item">
										{ getProductFilter(
											rule._product_filter,
											getFilterData( rule )
										) ?? '' }
									</div>

									<div className="wsx-rule-actions wsx-btn-group wsx-gap-8 wsx-justify-end">
										<Tooltip
											content={ __(
												'Delete',
												'wholesalex'
											) }
											direction="top"
										>
											<div
												className="wsx-btn-action"
												onClick={ ( e ) => {
													deleteRule(
														e,
														actualIndex
													);
												} }
												onKeyDown={ ( e ) => {
													if (
														e.key === 'Enter' ||
														e.key === ' '
													) {
														deleteRule(
															e,
															actualIndex
														);
													}
												} }
												role="button"
												tabIndex="0"
											>
												{ Icons.delete }
											</div>
										</Tooltip>
										<Tooltip
											content={ __(
												'Duplicate',
												'wholesalex'
											) }
											direction="top"
										>
											<div
												className="wsx-btn-action"
												onClick={ ( e ) => {
													duplicateRule(
														e,
														actualIndex
													);
												} }
												onKeyDown={ ( e ) => {
													if (
														e.key === 'Enter' ||
														e.key === ' '
													) {
														duplicateRule(
															e,
															actualIndex
														);
													}
												} }
												role="button"
												tabIndex="0"
											>
												{ Icons.copy }
											</div>
										</Tooltip>
										<Tooltip
											content={ __(
												'Edit',
												'wholesalex'
											) }
										>
											<Link
												className="wsx-lh-0"
												to={ `/dynamic-rules/edit/${ rule.id }` }
											>
												<div className="wsx-btn-action">
													{ Icons.edit }
												</div>
											</Link>
										</Tooltip>
									</div>

									{ deleteModal( actualIndex, rule ) }
								</div>
							);
						} ) }
					</div>
					{
						<Pagination
							gotoPage={ handleGotoPage }
							length={ filteredRules.length }
							pageSize={ pageSize }
							setPageSize={ setPageSize }
							items={ rules }
							showItemsPerPage={ showRulesPerPage }
						/>
					}
				</div>

				{ alert && (
					<Alert
						title="Please Select Properly!"
						description="Please select rule from the list and an action to perform bulk action."
						onClose={ handleAlertClose }
					/>
				) }
				{ isAlertVisible && (
					<Alert
						title="Confirm Bulk Action"
						description={ `Are you sure you want to ${ pendingAction } the selected ${
							selectedRows.length === 1 ? 'rule' : 'rules'
						}?` }
						onClose={ handleAlertClose }
						onConfirm={ confirmBulkAction }
					/>
				) }
			</>
		);
	};

	const ref = useRef( null );

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const toggleOverlayWindow = ( e, type ) => {
		if ( overlayWindow.status ) {
			const style = ref?.current?.style;
			if ( ! style ) {
				return;
			}
			sleep( 200 ).then( () => {
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = `translateX(${ isRTL ? '-50%' : '50%' })`;
				style.opacity = '0';

				sleep( 300 ).then( () => {
					setOverlayWindow( { ...overlayWindow, status: false } );
				} );
			} );
		} else {
			setOverlayWindow( { ...overlayWindow, type, status: true } );
			setTimeout( () => {
				const style = ref?.current?.style;
				if ( ! style ) {
					return;
				}
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = 'translateX(0%)';
				style.opacity = '1';
			}, 10 );
		}
	};

	const onImportClick = ( e ) => {
		toggleOverlayWindow( e, 'import' );
	};

	const renderOverlayWindow = () => {
		if ( ! overlayWindow.status ) {
			return;
		}
		return (
			<>
				{ overlayWindow.type === 'import' && (
					<Import
						toggleOverlayWindow={ toggleOverlayWindow }
						overlayWindowStatus={ overlayWindow.status }
						windowRef={ ref }
						setRules={ setRules }
					/>
				) }{ ' ' }
			</>
		);
	};
	return (
		<Fragment>
			<div className="wsx-wrapper">
				<div className="wsx-container">
					{ ! appState.loading &&
						Object.keys( fields ).map( ( sections ) => {
							switch ( fields[ sections ].type ) {
								case 'buttons':
									return buttonsData(
										fields[ sections ],
										sections
									);
								case 'rule':
									return ruleData(
										fields[ sections ],
										sections
									);
								default:
									return null;
							}
						} ) }
					{ renderOverlayWindow() }
				</div>
			</div>
		</Fragment>
	);
};

export default DynamicRules;
