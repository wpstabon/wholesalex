import React, { useState, useEffect, useContext } from 'react';
import MultiSelect from './MultiSelect';
import Tier from '../../components/Tier';
import Input from '../../components/Input';
import Slider from '../../components/Slider';
import { __ } from '@wordpress/i18n';
import Switch from '../../components/Switch';
import Choosebox from '../../components/Choosebox';
import { DataContext } from '../../contexts/DataProvider';

import Icons from '../../utils/Icons';
import Select from '../../components/Select';
import ColorPicker from '../../components/ColorPicker';
import DateSelector from '../../components/DateSelector';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';
import './Badge.scss';

const Rule = ( { fields, rules, setRules, index } ) => {
	const { setContextDataRule } = useContext( DataContext );
	const [ tempRule, setTempRule ] = useState( { ...rules[ index ] } );
	const [ sectionStatus, setSectionStatus ] = useState( {} );
	const [ popupStatus, setPopupStatus ] = useState( false );

	useEffect( () => {
		const parent = [ ...rules ];
		parent[ index ] = { ...parent[ index ], ...tempRule };
		setRules( parent );
		setContextDataRule( [] );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ tempRule ] );

	const setDefaultValue = ( fieldName, sectionName, defaultValue ) => {
		const _rule = { ...tempRule };
		let defVal = defaultValue;

		if ( sectionName ) {
			_rule[ sectionName ] = _rule[ sectionName ] || {};

			if ( ! _rule[ sectionName ][ fieldName ] ) {
				_rule[ sectionName ][ fieldName ] = defVal;
			}
			defVal = _rule[ sectionName ][ fieldName ];
		} else {
			if ( ! _rule[ fieldName ] ) {
				_rule[ fieldName ] = defVal;
			}
			defVal = _rule[ fieldName ];
		}
		return defVal;
	};

	const inputData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const onChangeHandler = ( e ) => {
			let inputValue = e.target.value;
			//Check Undefine for shipping zone
			if (
				fieldData.type === 'number' &&
				inputValue !== '' &&
				inputValue < 0
			) {
				inputValue = 1;
			}
			const copy = { ...tempRule };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = inputValue;
			} else {
				copy[ fieldName ] = inputValue;
			}

			if ( fieldName === '_rule_title' ) {
				const parent = [ ...rules ];
				parent[ index ]._rule_title = inputValue;
				setRules( parent );
			}
			setTempRule( copy );
		};
		return (
			<Input
				key={ `${ fieldName }_${ index }` }
				label={ fieldData.label }
				type={ fieldData.type }
				id={ fieldName }
				name={ fieldName }
				value={ defValue }
				onChange={ onChangeHandler }
			/>
		);
	};

	const colorData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const onChangeHandler = ( e ) => {
			const inputValue = e.target.value;

			const copy = { ...tempRule };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = inputValue;
			} else {
				copy[ fieldName ] = inputValue;
			}

			setTempRule( copy );
		};
		return (
			<div>
				<div className="wsx-input-label wsx-font-medium">
					{ fieldData.label }
				</div>
				<ColorPicker
					type={ fieldData.type }
					defaultValue={ defValue }
					name={ fieldName }
					onChange={ onChangeHandler }
				/>
			</div>
		);
	};
	const choseBoxes = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const onChangeHandler = ( e ) => {
			const checked = e.target.checked;
			let inputValue = '';
			if ( checked ) {
				inputValue = e.target.value;
			}
			const copy = { ...tempRule };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = inputValue;
			} else {
				copy[ fieldName ] = inputValue;
			}

			setTempRule( copy );
		};

		return (
			<Choosebox
				label={ fieldData.label }
				id={ fieldName }
				className={ 'wsx-badge-style' }
				name={ fieldName }
				defaultValue={ defValue }
				value={ defValue }
				onChange={ onChangeHandler }
				options={ fieldData.options }
				tooltip={ fieldData.tooltip }
			/>
		);
	};

	const switchData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const onChangeHandler = ( e ) => {
			const inputValue = e.target.checked ? 'yes' : 'no';

			const copy = { ...tempRule };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = inputValue;
			} else {
				copy[ fieldName ] = inputValue;
			}

			setTempRule( copy );
		};
		return (
			<Switch
				key={ `${ fieldName }_${ index }` }
				className="wholesalex_dynamic_rule_field"
				label={ fieldData.label }
				id={ fieldName }
				name={ fieldName }
				value={ defValue }
				onChange={ onChangeHandler }
				desc={ fieldData?.desc }
				descTooltip={ fieldData?.descTooltip }
			/>
		);
	};

	const sliderData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const onChangeHandler = ( e ) => {
			const inputValue = e.target.checked ? 'yes' : 'no';

			const copy = { ...tempRule };
			if ( sectionName ) {
				copy[ sectionName ][ fieldName ] = inputValue;
			} else {
				copy[ fieldName ] = inputValue;
			}

			setTempRule( copy );
		};
		return (
			<div className="wsx-dynamic-rules-slider-container">
				<div className="wsx-input-label">{ fieldData.label }</div>
				<Slider
					key={ `${ fieldName }_${ index }` }
					className="wsx-slider-sm"
					label={ fieldData?.desc }
					isLabelSide={ true }
					name={ fieldName }
					value={ defValue }
					onChange={ onChangeHandler }
					tooltip={ fieldData?.descTooltip }
				/>
			</div>
		);
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	const selectData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const optionsArray = getOptionsArray( fieldData.options );
		return (
			<Select
				label={ fieldData.label }
				options={ optionsArray }
				value={ defValue }
				onChange={ ( e ) => {
					const isLock = e.target.value.startsWith( 'pro_' )
						? true
						: false;
					if ( isLock ) {
						setPopupStatus( true );
					} else {
						const copy = { ...tempRule };
						if ( sectionName ) {
							copy[ sectionName ][ fieldName ] = e.target.value;
						} else {
							copy[ fieldName ] = e.target.value;
						}
						setTempRule( copy );
					}
				} }
			/>
		);
	};

	const multiselectData = ( fieldData, fieldName, sectionName ) => {
		const defValue = setDefaultValue(
			fieldName,
			sectionName,
			fieldData.default
		);
		const depsFullfilled = fieldData.depends_on
			? dependencyCheck( fieldData.depends_on, sectionName )
			: true;
		const optionsDependend = fieldData.options_dependent_on
			? fieldData.options_dependent_on
			: false;

		const _options = fieldData.options;
		let flag = true;
		let dependsValue = '';

		if ( optionsDependend ) {
			if ( tempRule[ optionsDependend ] ) {
				dependsValue = tempRule[ optionsDependend ];
			} else if (
				tempRule[ sectionName ] &&
				tempRule[ sectionName ][ optionsDependend ]
			) {
				dependsValue = tempRule[ sectionName ][ optionsDependend ];
			} else {
				flag = false;
			}
		}
		if ( dependsValue !== '' ) {
			flag = true;
		}
		return (
			depsFullfilled &&
			flag && (
				<div
					key={ `rule-field_${ fieldName }` }
					className="wsx-input-wrapper "
				>
					{ fieldData.label && (
						<label
							className="wsx-label wsx-input-label wsx-font-medium"
							htmlFor={ fieldName }
						>
							{ fieldData.label }
						</label>
					) }
					<MultiSelect
						name={ fieldName }
						value={ defValue }
						options={ _options }
						placeholder={ fieldData.placeholder }
						onMultiSelectChangeHandler={ (
							fieldName,
							selectedValues
						) => {
							const copy = { ...tempRule };
							if ( sectionName ) {
								copy[ sectionName ][ fieldName ] = [
									...selectedValues,
								];
							} else {
								copy[ fieldName ] = [ ...selectedValues ];
							}
							setTempRule( copy );
						} }
						isAjax={ fieldData?.is_ajax }
						ajaxAction={ fieldData?.ajax_action }
						ajaxSearch={ fieldData?.ajax_search }
						dependsValue={ optionsDependend ? dependsValue : false }
					/>
				</div>
			)
		);
	};

	const dependencyCheck = ( deps, sectionName ) => {
		if ( ! deps ) {
			return true;
		}

		let _flag = true;
		deps.forEach( ( dep ) => {
			if ( tempRule[ dep.key ] !== dep.value ) {
				_flag = false;
			}
		} );

		let _sectionFlag = false;
		if ( tempRule[ sectionName ] && ! _flag ) {
			deps.forEach( ( dep ) => {
				if ( tempRule[ sectionName ][ dep.key ] === dep.value ) {
					_sectionFlag = true;
				}
			} );
		}
		if ( ! _flag && _sectionFlag ) {
			return true;
		}
		return _flag;
	};

	const visibilityCheck = ( deps ) => {
		if ( ! deps ) {
			return true;
		}

		const flag = deps._rule_type.includes( tempRule._rule_type );

		return ! flag;
	};

	const titleSectionData = ( sectionData, sectionName ) => {
		return (
			<div
				className="wsx-card wsx-column-2 wsx-smd-column-1 wsx-gap-32"
				key={ sectionName }
			>
				{ Object.keys( sectionData ).map( ( field ) => {
					return (
						<>
							{ ( () => {
								const _data = sectionData[ field ];
								switch ( sectionData[ field ].type ) {
									case 'text':
									case 'number':
										return inputData(
											sectionData[ field ],
											field
										);
									case 'select':
										return selectData(
											sectionData[ field ],
											field
										);
									case 'multiselect':
										return (
											dependencyCheck(
												_data.depends_on,
												sectionName
											) && multiselectData( _data, field )
										);

									default:
										break;
								}
							} )() }
						</>
					);
				} ) }
			</div>
		);
	};

	const rulesSectionData = ( sectionData, sectionName ) => {
		const newFields = Object.keys( sectionData );

		const shouldRenderDiv = newFields.some( ( field ) => {
			const _data = sectionData[ field ];
			return (
				_data.type === 'multiselect' &&
				dependencyCheck( _data.depends_on, sectionName )
			);
		} );

		const renderSelectFields = () => {
			return newFields.map( ( field ) => {
				const _data = sectionData[ field ];
				if ( _data.type === 'select' ) {
					return selectData( _data, field );
				}
				return null;
			} );
		};

		const renderMultiSelectFields = () => {
			return newFields.map( ( field ) => {
				const _data = sectionData[ field ];
				if (
					_data.type === 'multiselect' &&
					dependencyCheck( _data.depends_on, sectionName )
				) {
					return multiselectData( _data, field );
				}
				return null;
			} );
		};

		return (
			<section
				key={ sectionName }
				className="wsx-card wsx-d-flex wsx-item-center wsx-gap-12"
			>
				<div
					className={ `wsx-selected-field-wrapper ${
						shouldRenderDiv ? 'wsx-multiple-field' : ''
					}` }
				>
					{ renderSelectFields() }
				</div>

				{ shouldRenderDiv && (
					<div className="wsx-selected-field-wrapper">
						{ renderMultiSelectFields() }
					</div>
				) }
			</section>
		);
	};

	const manageDiscountSectionData = ( sectionData, sectionName ) => {
		if ( ! tempRule[ sectionName ] ) {
			const copy = { ...tempRule };
			copy[ sectionName ] = {};
			setTempRule( copy );
		}
		return (
			<section key={ sectionName } className="wsx-accordion-wrapper">
				<div className="wsx-accordion-header">
					<div
						className="wsx-accordion-title"
						key={ `wsx-role-header-${ sectionName }` }
					>
						{ sectionData.label }
					</div>
				</div>

				<div className="wsx-accordion-body">
					<div
						className={ `wsx-accordion-wrapper-dynamic wsx-column-${
							Object.keys( sectionData.attr ).length
						} wsx-md-column-1` }
					>
						{ ' ' }
						{ Object.keys( sectionData.attr ).map( ( field ) => {
							const _data = sectionData.attr[ field ];
							switch ( _data.type ) {
								case 'number':
								case 'text':
									return inputData(
										sectionData.attr[ field ],
										field,
										sectionName
									);
								case 'select':
									return selectData(
										sectionData.attr[ field ],
										field,
										sectionName
									);

								default:
									return null;
							}
						} ) }
					</div>
				</div>
			</section>
		);
	};

	const tierData = ( fieldData, tierName ) => {
		/**
		 * For Deep Copy, Used JSON Stringfy and JSON Parse
		 */
		let defaultTier = JSON.stringify( fieldData._tiers.data );
		defaultTier = JSON.parse( defaultTier );
		/**
		 * For Restrict Product Visibility Some Conditions option removed
		 */
		if ( tempRule._rule_type === 'restrict_product_visibility' ) {
			const options = {
				'': 'Choose Conditions...',
				order_count: 'User Order Count ',
				total_purchase: 'Total Purchase Amount ',
			};
			defaultTier._conditions_for.options = options;
		}
		return (
			<div key={ tierName } className="wsx-accordion-body">
				<div
					className={ `wsx-accordion-container ${
						tierName !== 'quantity_based' &&
						'wsx-condition-container'
					}` }
				>
					{ tierName !== 'quantity_based' && (
						<div className="wsx-tier-header wsx-mb-8">
							<div className="wsx-tier-header-item wsx-font-medium">
								{ __( 'Condition', 'wholesalex' ) }
							</div>
							<div className="wsx-tier-header-item wsx-font-medium">
								{ __( 'Operator', 'wholesalex' ) }
							</div>
							<div className="wsx-tier-header-item wsx-font-medium">
								{ __( 'Amount', 'wholesalex' ) }
							</div>
						</div>
					) }
					{ tempRule[ tierName ] &&
						tempRule[ tierName ].tiers &&
						tempRule[ tierName ].tiers.map( ( t, idx ) => (
							<Tier
								key={ `wholesalex_${ tierName }_tier_${ idx }` }
								fields={ defaultTier }
								tier={ tempRule }
								setTier={ ( r ) => {
									let copy = { ...tempRule };
									copy = { ...copy, ...r };
									setTempRule( copy );
								} }
								index={ idx }
								tierName={ tierName }
								setPopupStatus={ setPopupStatus }
							/>
						) ) }
				</div>
			</div>
		);
	};

	const tiersData = ( sectionData, sectionName ) => {
		return (
			<section
				key={ `wholesalex_${ sectionName }` }
				className="wsx-accordion-wrapper"
				onClick={ () => {
					const copy = { ...tempRule };
					if ( ! copy[ sectionName ] ) {
						copy[ sectionName ] = {
							tiers: [
								{
									_id: Date.now().toString(),
									src: 'dynamic_rule',
								},
							],
						};
					}
					setTempRule( copy );
				} }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						const copy = { ...tempRule };
						if ( ! copy[ sectionName ] ) {
							copy[ sectionName ] = {
								tiers: [
									{
										_id: Date.now().toString(),
										src: 'dynamic_rule',
									},
								],
							};
						}
						setTempRule( copy );
					}
				} }
				role="button"
				tabIndex="0"
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setSectionStatus( {
							...sectionStatus,
							[ sectionName ]: ! sectionStatus[ sectionName ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setSectionStatus( {
								...sectionStatus,
								[ sectionName ]: ! sectionStatus[ sectionName ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx_role_header` }
					>
						{ sectionData.label }
					</div>
					<span
						className={ `wsx-icon ${
							sectionStatus[ sectionName ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ sectionStatus[ sectionName ] &&
					sectionData.attr &&
					Object.keys( sectionData.attr ).map( ( fieldData ) => {
						const _data = sectionData.attr[ fieldData ];
						switch ( _data.type ) {
							case 'tier':
								return tierData( _data, sectionName );
							default:
								return null;
						}
					} ) }
			</section>
		);
	};

	const dateData = ( fieldData, fieldName, sectionName ) => {
		let defValue;

		if ( sectionName ) {
			if (
				tempRule[ sectionName ] &&
				tempRule[ sectionName ][ fieldName ]
			) {
				defValue = new Date( tempRule[ sectionName ][ fieldName ] );
			} else {
				defValue = '';
			}
		} else if ( tempRule[ fieldName ] ) {
			defValue = new Date( tempRule[ fieldName ] );
		} else {
			defValue = '';
		}
		const copy = { ...tempRule };
		return (
			<div key={ fieldName } className="">
				{ fieldData.label && (
					<label
						className="wsx-label wsx-input-label"
						htmlFor={ fieldName }
					>
						{ fieldData.label }
					</label>
				) }
				<DateSelector
					onChange={ ( val ) => {
						if ( sectionName ) {
							copy[ sectionName ][ fieldName ] = val ? val : '';
						} else {
							copy[ fieldName ] = val ? val : '';
						}
						setTempRule( copy );
					} }
					value={ defValue }
					id={ fieldName }
					name={ fieldName }
				/>
				{ fieldData.description && (
					<span className="wholesalex-field__description">
						{ fieldData.description }
					</span>
				) }
			</div>
		);
	};

	const dynamicSectionData = ( sectionData, sectionName ) => {
		if (
			! tempRule[ sectionName ] ||
			( typeof tempRule[ sectionName ] !== 'undefined' &&
				Array.isArray( tempRule[ sectionName ] ) &&
				Object.keys( tempRule[ sectionName ] ).length === 0 )
		) {
			const copy = { ...tempRule };
			copy[ sectionName ] = {};
			setTempRule( copy );
		}

		return (
			<section
				key={ `wholesalex_${ sectionName }` }
				className={ `wsx-rule-${ sectionName } wsx-accordion-wrapper` }
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setSectionStatus( {
							...sectionStatus,
							[ sectionName ]: ! sectionStatus[ sectionName ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setSectionStatus( {
								...sectionStatus,
								[ sectionName ]: ! sectionStatus[ sectionName ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx-role-header-${ sectionName }` }
					>
						{ sectionData.label }
					</div>
					<span
						className={ `wsx-icon ${
							sectionStatus[ sectionName ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>

				{ sectionStatus[ sectionName ] && (
					<div className="wsx-accordion-body">
						<div
							className={ `wsx-accordion-wrapper-dynamic wsx-column-${
								Object.keys( sectionData.attr ).length > 4
									? 2
									: Object.keys( sectionData.attr ).length
							} wsx-md-column-1` }
						>
							{ ' ' }
							{ Object.keys( sectionData.attr ).map(
								( field ) => {
									const _data = sectionData.attr[ field ];
									switch ( _data.type ) {
										case 'number':
										case 'text':
											return (
												visibilityCheck(
													_data.not_visible_on
												) &&
												inputData(
													_data,
													field,
													sectionName
												)
											);
										case 'select':
											return (
												dependencyCheck(
													_data.depends_on,
													sectionName
												) &&
												selectData(
													_data,
													field,
													sectionName
												)
											);
										case 'multiselect':
											return multiselectData(
												_data,
												field,
												sectionName
											);
										case 'date':
											return (
												sectionStatus[ sectionName ] &&
												dateData(
													_data,
													field,
													sectionName
												)
											);
										case 'switch':
											return switchData(
												_data,
												field,
												sectionName
											);
										case 'slider':
											return sliderData(
												_data,
												field,
												sectionName
											);
										case 'color':
											return colorData(
												_data,
												field,
												sectionName
											);
										case 'choosebox':
											return choseBoxes(
												_data,
												field,
												sectionName
											);
										default:
											return null;
									}
								}
							) }
						</div>
					</div>
				) }
			</section>
		);
	};
	const dynamicRuleDependentCondition = () => {
		return (
			<div className="wsx-card wsx-p-32 wsx-d-flex wsx-flex-column wsx-gap-32">
				{ Object.keys( fields.attr ).map( ( section ) => {
					const _data = fields.attr[ section ];
					switch ( _data.type ) {
						case 'manage_discount':
							return (
								dependencyCheck( _data.depends_on, section ) &&
								manageDiscountSectionData( _data, section )
							);
						case 'tiers':
							return (
								dependencyCheck( _data.depends_on, section ) &&
								tiersData( _data, section )
							);
						case 'payment_discount':
						case 'payment_qty_discount':
						case 'payment_extra_charge':
						case 'tax_rule':
						case 'shipping_rule':
						case 'buy_x_get_y':
						case 'buy_x_get_one':
						case 'max_order_qty':
						case 'min_order_qty':
						case 'hidden_price':
							return (
								dependencyCheck( _data.depends_on, section ) &&
								dynamicSectionData( _data, section )
							);
						case 'date_n_usages_limit':
							return (
								visibilityCheck( _data.not_visible_on ) &&
								dependencyCheck( _data.depends_on, section ) &&
								dynamicSectionData( _data, section )
							);
						default:
							return null;
					}
				} ) }
			</div>
		);
	};

	return (
		<div className="wsx-mt-40">
			{ Object.keys( fields.attr ).map( ( section ) => {
				const _data = fields.attr[ section ];
				switch ( _data.type ) {
					case 'title_n_status':
						return titleSectionData( _data.attr, section );
					case 'rules':
						return rulesSectionData( _data.attr, section );
					default:
						return null;
				}
			} ) }
			{ dynamicRuleDependentCondition() }
			{ popupStatus && (
				<UpgradeProPopUp
					title={ __( 'Unlock all Features with', 'wholesalex' ) }
					onClose={ () => setPopupStatus( false ) }
				/>
			) }
		</div>
	);
};

export default Rule;
