import React from 'react';
import DynamicRules from './DynamicRules';

const DynamicRulesView = () => (
	<React.StrictMode>
		<DynamicRules />
	</React.StrictMode>
);

export default DynamicRulesView;
