import React, { useState, useEffect, useContext } from 'react';
import { __ } from '@wordpress/i18n';
import Rule from './Rule';
import Toast from '../../components/Toast';
import { DataContext } from '../../contexts/DataProvider';
import { useNavigate } from '../../contexts/RouterContext';
import Button from '../../components/Button';
import Tooltip from '../../components/Tooltip';
import { ToastContexts } from '../../context/ToastsContexts';

const EditDynamicRule = ( { id } ) => {
	const { contextData, contextDataRule, setContextDataRule } =
		useContext( DataContext );
	const { newRules } = contextDataRule;
	const { globalNewDynamicRules } = contextData;
	const [ fields ] = useState( wholesalex_overview.whx_dr_fields );
	const [ fieldsData, setFieldsData ] = useState( null );
	const [ rules, setRules ] = useState( wholesalex_overview.whx_dr_rule );
	const { dispatch } = useContext( ToastContexts );
	const [ isActive, setIsActive ] = useState( false );
	const [ tempRule, setTempRule ] = useState( {} );

	const navigate = useNavigate();

	const [ appState, setAppState ] = useState( {
		loading: false,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: {},
	} );

	const ruleExists = rules.some( ( rule ) => rule.id === id );
	//If the dynamic rules does not exist, redirect to the dynamic-rules page
	useEffect( () => {
		if (
			Array.isArray( globalNewDynamicRules ) &&
			globalNewDynamicRules.length === 0 &&
			! ruleExists
		) {
			navigate( '/dynamic-rules' );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ globalNewDynamicRules ] );

	useEffect( () => {
		Object.keys( fields ).forEach( ( sections ) => {
			if ( fields[ sections ].type === 'rule' ) {
				setFieldsData( fields[ sections ] );
			}
		} );
	}, [ fields ] );

	useEffect( () => {
		const savedRule = rules;
		setContextDataRule( ( prevData ) => ( {
			...prevData,
			...prevData,
			savedRule,
		} ) );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ rules ] );

	useEffect( () => {
		if ( newRules && newRules.length > 0 ) {
			setRules( newRules );
		} else {
			setRules( rules );
		}
	}, [ newRules, rules ] );

	const updateRule = ( index, updatedRule ) => {
		const updatedRules = [ ...rules ];
		updatedRules[ index ] = updatedRule;
		setRules( updatedRules );
	};

	const fetchData = async (
		type = 'post',
		index = '',
		_rule = '',
		_key = ''
	) => {
		const attr = {
			type,
			action: 'dynamic_rule_action',
			nonce: wholesalex.nonce,
		};

		const rulesBeforeSave = [ ...rules ];
		let readyToSave = true;

		if ( _key !== 'delete' && type === 'post' ) {
			const ruleToBeSave = { ..._rule };
			ruleToBeSave.limit = {
				_usage_limit: ruleToBeSave?.limit?._usage_limit
					? ruleToBeSave.limit._usage_limit
					: '',
				_start_date: ruleToBeSave?.limit?._start_date
					? ruleToBeSave.limit._start_date
					: '',
				_end_date: ruleToBeSave?.limit?._end_date
					? ruleToBeSave.limit._end_date
					: '',
			};

			if (
				! ruleToBeSave._rule_type ||
				( ruleToBeSave._rule_type !== 'restrict_checkout' &&
					ruleToBeSave._rule_type !== 'restrict_product_visibility' &&
					ruleToBeSave._rule_type !== 'hidden_price' &&
					ruleToBeSave._rule_type !== 'non_purchasable' &&
					! ruleToBeSave[ ruleToBeSave._rule_type ] )
			) {
				readyToSave = false;
			}
			if ( ! ruleToBeSave._rule_for ) {
				readyToSave = false;
			}
			if (
				! (
					ruleToBeSave._rule_for === 'all_users' ||
					ruleToBeSave._rule_for === 'all' ||
					ruleToBeSave._rule_for === 'all_roles'
				) &&
				! ruleToBeSave[ ruleToBeSave._rule_for ]
			) {
				readyToSave = false;
			}
			if (
				! ruleToBeSave._product_filter ||
				( ruleToBeSave._product_filter !== 'all_products' &&
					! ruleToBeSave[ ruleToBeSave._product_filter ] )
			) {
				readyToSave = false;
			}

			if ( ! readyToSave ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: __( 'Please Fill All Fields.', 'wholesalex' ),
					},
				} );
				return;
			}
		}

		attr.id = _rule.id;
		attr.rule = JSON.stringify( _rule );

		if ( _key === 'delete' ) {
			attr.delete = true;
		}

		const _state = { ...appState };
		_state.loadingOnSave[ _rule.id ] = true;
		setAppState( _state );

		wp.apiFetch( {
			path: '/wholesalex/v1/dynamic_rule_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			_state.loadingOnSave[ _rule.id ] = false;
			setAppState( _state );

			if ( res.success ) {
				if ( type === 'post' ) {
					updateRule( index, { ...rules[ index ], ..._rule } );
					const updatedRules = [ ...rules ];
					updatedRules[ index ] = {
						...updatedRules[ index ],
						..._rule,
					};
					setRules( updatedRules );
					setContextDataRule( ( prevData ) => ( {
						...prevData,
						...prevData,
						savedRule: updatedRules,
					} ) );

					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message: res.data.message,
						},
					} );
				}
			} else {
				setRules( [ ...rulesBeforeSave ] );
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'error',
						message: res.data.message,
					},
				} );
			}
		} );
	};

	const handleDynamicRuleBackBtn = () => {
		navigate( '/dynamic-rules' );
	};

	const onSaveAction = ( index ) => {
		const parent = [ ...rules ];
		parent[ index ] = { ...parent[ index ], ...tempRule };
		setRules( parent );

		const savedRule = parent;
		setContextDataRule( ( prevData ) => ( {
			...prevData,
			...prevData,
			savedRule,
		} ) );

		const _rule = parent[ index ];
		if ( _rule._rule_title && _rule._rule_title.trim() !== '' ) {
			fetchData( 'post', index, parent[ index ] );
		}
	};

	useEffect( () => {
		rules.map( ( rule ) => {
			if ( rule.id === id ) {
				rule._rule_status === '1' ||
				rule._rule_status === true ||
				rule._rule_status
					? setIsActive( true )
					: setIsActive( false );
			}
			return null;
		} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ rules ] );

	const onPublish = ( e, index ) => {
		const parent = [ ...rules ];
		parent[ index ] = { ...parent[ index ], ...tempRule };
		const _rule = parent[ index ];
		_rule._rule_status = true;
		setIsActive( _rule._rule_status );
		setTempRule( _rule );
	};

	const onDraft = ( e, index ) => {
		const parent = [ ...rules ];
		parent[ index ] = { ...parent[ index ], ...tempRule };
		const _rule = parent[ index ];
		_rule._rule_status = false;
		setIsActive( _rule._rule_status );
		setTempRule( _rule );
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				{ rules.map(
					( rule, index ) =>
						fieldsData &&
						rule.id === id && (
							<>
								<div className="wsx-d-flex wsx-item-center wsx-justify-space">
									<Button
										iconName="arrowLeft_24"
										background="base2"
										padding="8px"
										onClick={ handleDynamicRuleBackBtn }
									/>
									<div className="wsx-d-flex wsx-item-center wsx-gap-32">
										<div
											className={ `wsx-btn-toggle-wrapper ${
												isActive && 'active'
											}` }
										>
											<div
												className={ `wsx-btn-toggle-container ${
													isActive && 'active'
												}` }
											>
												<Tooltip
													content={ __(
														'Publish',
														'wholesalex'
													) }
													position="top"
													onlyText={ true }
												>
													<div
														className={ `wsx-btn-toggle-item ${
															isActive && 'active'
														}` }
														onClick={ ( e ) =>
															onPublish(
																e,
																index
															)
														}
														onKeyDown={ ( e ) => {
															if (
																e.key ===
																	'Enter' ||
																e.key === ' '
															) {
																onPublish(
																	e,
																	index
																);
															}
														} }
														role="button"
														tabIndex="0"
													>
														{ __(
															'Publish',
															'wholesalex'
														) }
													</div>
												</Tooltip>
												<Tooltip
													content={ __(
														'Draft',
														'wholesalex'
													) }
													position="top"
													onlyText={ true }
												>
													<div
														className={ `wsx-btn-toggle-item ${
															! isActive &&
															'active'
														}` }
														onClick={ ( e ) =>
															onDraft( e, index )
														}
														onKeyDown={ ( e ) => {
															if (
																e.key ===
																	'Enter' ||
																e.key === ' '
															) {
																onDraft(
																	e,
																	index
																);
															}
														} }
														role="button"
														tabIndex="0"
													>
														{ __(
															'Draft',
															'wholesalex'
														) }
													</div>
												</Tooltip>
											</div>
										</div>
										<Button
											label={ __( 'Save', 'wholesalex' ) }
											onClick={ () =>
												onSaveAction( index )
											}
											background="positive"
											customClass="wsx-font-14"
										/>
									</div>
								</div>
								<Rule
									key={ index }
									fields={ fieldsData }
									rules={ rules }
									setRules={ setRules }
									index={ index }
									onSave={ fetchData }
									appState={ appState }
								/>
							</>
						)
				) }
				<Toast delay={ 3000 } position="top_right" />
			</div>
		</div>
	);
};

export default EditDynamicRule;
