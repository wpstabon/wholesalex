import React, { useContext, useEffect, useState } from 'react';
import Search from '../../components/Search';
import Slider from '../../components/Slider';
import Dropdown from '../../components/Dropdown';
import Modal from '../../components/Modal';
import Toast from '../../components/Toast';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import Select from '../../components/Select';
import Icons from '../../utils/Icons';
import UserPagination from '../../components/UserPagination';
import Tooltip from '../../components/Tooltip';
import LoadingGif from '../../components/LoadingGif';
import Alert from '../../components/Alert';
import { ToastContexts } from '../../context/ToastsContexts';

const Conversation = ( props ) => {
	const [ initialLoader, setInitialLoader ] = useState( false );
	const [ users, setUsers ] = useState( [] );
	const [ paginationData, setPaginationData ] = useState( {
		totalUsers: 0,
		currentPage: 1,
		loader: false,
	} );
	const [ bulkAction, setBulkAction ] = useState( '' );
	const [ selectedUsers, setSelectedUsers ] = useState( [] );
	const [ filterStatus, setFilterStatus ] = useState( '' );
	const [ postStatus, setPostStatus ] = useState( '' );
	const [ filterType, setFilterType ] = useState( '' );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ tempSearchValue, setTempSearchValue ] = useState( searchValue );
	const [ isInitialRender, setIsInitialRender ] = useState( false );
	const [ columns, setColumns ] = useState( wholesalex_overview?.heading );
	const [ current, setCurrent ] = useState( '' );
	const [ deleteModalStatus, setDeleteModalStatus ] = useState( false );
	const [ isBulkAction, setIsBulkAction ] = useState( false );
	const { state, dispatch } = useContext( ToastContexts );
	const [ alert, setAlert ] = useState( false );
	const [ isAlertVisible, setIsAlertVisible ] = useState( false );

	const fetchData = async (
		type = 'get',
		page = 1,
		isInitial = true,
		message = '',
		itemsPerPage = 10
	) => {
		if ( isInitial ) {
			setInitialLoader( true );
		} else {
			setPaginationData( { ...paginationData, loader: true } );
		}
		const attr = {
			type,
			action: 'conversation',
			nonce: wholesalex.nonce,
			status: filterStatus,
			conversation_type: filterType,
			post_status: postStatus,
			page: page ? page : 1,
			search: searchValue,
			isFrontend: props?.isFrontend,
			itemsPerPage,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/conversation',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				setUsers( res.data.conversations );
				setPaginationData( {
					...paginationData,
					totalUsers: res.data.total,
					loader: false,
					currentPage: page,
				} );

				setSelectedUsers( [] );
				if ( message ) {
					dispatch( {
						type: 'ADD_MESSAGE',
						payload: {
							id: Date.now().toString(),
							type: 'success',
							message,
						},
					} );
				}
			}

			if ( isInitial ) {
				setInitialLoader( false );
			}
		} );
	};

	const hasPagination = () => {
		return (
			wholesalex_overview.conversation_per_page <=
			paginationData.totalUsers
		);
	};

	useEffect( () => {
		// fetchData();
		setIsInitialRender( true );
	}, [] );

	const renderDropDownContent = () => {
		return rowActions();
	};

	const renderTableCell = ( data, fieldType, name, conversation = '' ) => {
		if ( fieldType === '3dot' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<Dropdown
						className="wsx-3dot-wrapper"
						contentClass="wsx-p-4"
						renderContent={ renderDropDownContent }
						onClickCallback={ () => {
							setCurrent( conversation );
						} }
						iconName="dot3"
						iconRotation="none"
					/>
				</td>
			);
		} else if ( fieldType === 'html' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
					dangerouslySetInnerHTML={ { __html: data } }
				></td>
			);
		}
		if ( name === 'title' ) {
			if ( props?.isFrontend ) {
				const url = new URL( wholesalex_overview?.frontend_url );
				url.searchParams.append( 'conv', conversation.ID );
				return (
					<td
						className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
					>
						{ props?.isFrontend && (
							<a href={ url }>
								<span className="wsx-link wsx-ellipsis">
									{ data }
								</span>
							</a>
						) }
					</td>
				);
			}
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					{ ! props?.isFrontend && (
						<Tooltip content={ data } onlyText={ true }>
							<a
								className="wsx-link"
								target="_blank"
								href={ conversation?.edit_conversation }
								rel="noreferrer"
							>
								<span className="wsx-ellipsis">{ data }</span>
							</a>
						</Tooltip>
					) }
				</td>
			);
		}
		return (
			<td
				className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
			>
				<span
					className={ `${
						Number.isInteger( data ) ? '' : 'wsx-ellipsis'
					}` }
				>
					{ data }
				</span>
			</td>
		);
	};

	const updateAllUserSelection = () => {
		if ( selectedUsers.length === users.length ) {
			setSelectedUsers( [] );
		} else {
			const tempUsers = [ ...users ];
			const _selectedUsers = [];
			tempUsers.forEach( ( user ) => {
				_selectedUsers.push( user.ID );
			} );
			setSelectedUsers( _selectedUsers );
		}
	};

	const selectUser = ( e ) => {
		const userId = parseInt( e.target.getAttribute( 'data-id' ) );
		if ( selectedUsers.includes( userId ) ) {
			const _selectedUsers = selectedUsers.filter( ( id ) => {
				return id !== userId;
			} );
			setSelectedUsers( _selectedUsers );
		} else {
			setSelectedUsers( [ ...selectedUsers, userId ] );
		}
	};

	const onBulkActionChange = ( e ) => {
		setBulkAction( e.target.value );
	};

	const searchUser = () => {
		fetchData( 'get', 1, false );
	};

	//Fetch data on component load to get the initial column status for Filters
	useEffect( () => {
		wp.apiFetch( { path: '/wholesalex/v1/get-conversation-filters' } )
			.then( ( response ) => {
				const updatedColumns = { ...columns };
				Object.keys( response.filters ).forEach( ( key ) => {
					const columnKey = key.replace( 'users_filter_', '' );
					if ( updatedColumns[ columnKey ] ) {
						updatedColumns[ columnKey ].status = response.filters[
							key
						]
							? true
							: false;
					}
				} );
				setColumns( updatedColumns );
			} )
			.catch( () => {
				// error
			} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const setColumnStatus = ( columnName, status ) => {
		const _columns = { ...columns };
		const _column = { ..._columns[ columnName ], status };
		_columns[ columnName ] = _column;
		setColumns( _columns );
	};

	// Function to save the updated column status to the backend for Conversation Filters
	const handleSaveData = ( filterKey, column ) => {
		wp.apiFetch( {
			path: '/wholesalex/v1/save-conversation-filters',
			method: 'POST',
			data: {
				filters: {
					[ filterKey ]: ! columns[ column ].status,
				},
			},
		} )
			.then( () => {
				// res
			} )
			.catch( () => {
				//error
			} );
	};

	const columnsSelectionContent = () => {
		return Object.keys( columns ).map( ( column ) => {
			const filterKey = `users_filter_${ column }`;
			return (
				<div
					className="wsx-dropdown-actions-list"
					key={ filterKey }
					onClick={ ( e ) => e.stopPropagation() }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							e.stopPropagation();
						}
					} }
					role="button"
					tabIndex="0"
				>
					<Slider
						label={ columns[ column ].title }
						name={ `wsx-overview-column-${ column }` }
						key={ `wsx-overview-column-${ column }` }
						value={ columns[ column ].status }
						isLabelSide={ true }
						className="wsx-slider-md"
						onChange={ ( e ) => {
							e.stopPropagation();
							setColumnStatus(
								column,
								! columns[ column ].status
							);
							handleSaveData( filterKey, column );
						} }
					/>
				</div>
			);
		} );
	};

	const onFilterStatusChange = ( e ) => {
		setFilterStatus( e.target.value );
	};

	useEffect( () => {
		if ( filterStatus !== '' ) {
			fetchData( 'get', 1, false );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ filterStatus ] );

	useEffect( () => {
		if ( filterType !== '' ) {
			fetchData( 'get', 1, false );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ filterType ] );

	useEffect( () => {
		if ( postStatus !== '' ) {
			fetchData( 'get', 1, false );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ postStatus ] );

	useEffect( () => {
		if ( users.length === 0 && searchValue === '' && ! isInitialRender ) {
			fetchData();
		} else {
			searchUser( searchValue );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ searchValue ] );

	useEffect( () => {
		const delay = setTimeout(
			() => setSearchValue( tempSearchValue ),
			500
		);
		return () => clearTimeout( delay );
	}, [ tempSearchValue ] );

	const updateStatus = async (
		type = 'get',
		page = 1,
		conversationAction = '',
		ids = ''
	) => {
		setPaginationData( { ...paginationData, loader: true } );
		const attr = {
			type,
			action: 'conversation',
			nonce: wholesalex.nonce,
			status: filterStatus,
			conversation_type: filterType,
			post_status: postStatus,
			page: page ? page : 1,
			search: searchValue,
			isFrontend: props?.isFrontend,
		};

		switch ( type ) {
			case 'update_status':
				attr.conversation_action = conversationAction;
				attr.id = ids;
				break;
			case 'bulk_action':
				attr.conversation_action = conversationAction;
				attr.ids = ids;
				break;

			default:
				break;
		}

		wp.apiFetch( {
			path: '/wholesalex/v1/conversation',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				fetchData( 'get', paginationData.currentPage, false, res.data );
			}
		} );
	};

	const rowActionHandler = ( action ) => {
		switch ( action ) {
			case 'edit':
				if ( props?.isFrontend ) {
					const url = new URL( wholesalex_overview?.frontend_url );
					url.searchParams.append( 'conv', current.ID );
					window.open( url );
				} else {
					window.open( current.edit_conversation, '_blank' );
				}
				break;
			case 'delete':
				setDeleteModalStatus( true );
				break;
			case 'resolved':
				updateStatus(
					'update_status',
					paginationData.currentPage,
					'resolved',
					current.ID
				);
				break;

			default:
				break;
		}
	};

	const rowActions = () => {
		let options = [];
		if ( props?.isFrontend ) {
			options = [
				{
					label: __( 'Edit', 'wholesalex' ),
					iconClass: 'edit',
					action: 'edit',
					url: '',
				},
				{
					label: __( 'Resolved', 'wholesalex' ),
					iconClass: 'tick',
					action: 'resolved',
					url: '',
				},
			];
		} else {
			options = [
				{
					label: __( 'Edit', 'wholesalex' ),
					iconClass: 'edit',
					action: 'edit',
					url: '',
				},
				{
					label: __( 'Resolved', 'wholesalex' ),
					iconClass: 'tick',
					action: 'resolved',
					url: '',
				},
				{
					label: __( 'Force Delete', 'wholesalex' ),
					iconClass: 'delete',
					action: 'delete',
					url: '',
				},
			];
		}

		return options.map( ( option, idx ) => {
			return (
				<div
					key={ idx }
					className="wsx-row-actions-list"
					onClick={ () => {
						rowActionHandler( option.action, current.ID );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							rowActionHandler( option.action, current.ID );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<span className="wsx-icon wsx-lh-0">
						{ Icons[ option.iconClass ] }
					</span>
					<span className="wsx-row-actions-list-link">
						<span className="wsx-row-actions-list-link-label">
							{ option.label }
						</span>
					</span>
				</div>
			);
		} );
	};

	const deleteModal = () => {
		return (
			deleteModalStatus && (
				<Modal
					title={
						isBulkAction
							? __( 'Selected Conversations', 'wholesalex' )
							: current.title
					}
					status={ deleteModalStatus }
					setStatus={ setDeleteModalStatus }
					onDelete={ () => {
						setDeleteModalStatus( false );
						updateStatus(
							isBulkAction ? 'bulk_action' : 'update_status',
							paginationData.currentPage,
							'delete',
							isBulkAction ? selectedUsers : current.ID
						);
					} }
				/>
			)
		);
	};

	const handleBulkAction = () => {
		setIsBulkAction( true );
		if (
			! bulkAction ||
			bulkAction === 'default' ||
			selectedUsers.length === 0
		) {
			setAlert( true );
			return;
		}
		if ( 'delete' === bulkAction || 'move_to_trash' === bulkAction ) {
			setDeleteModalStatus( true );
		} else {
			setIsAlertVisible( true );
		}
	};

	const confirmBulkAction = () => {
		setIsAlertVisible( false );
		updateStatus(
			'bulk_action',
			paginationData.currentPage,
			bulkAction,
			selectedUsers
		);
	};

	const handleAlertClose = () => {
		setIsAlertVisible( false );
		setAlert( false );
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				<div className="wsx-user-list-wrapper">
					{ ! props?.isFrontend && (
						<Button
							label={ __(
								'Add New Conversation',
								'wholesalex'
							) }
							background="primary"
							iconName="plus"
							buttonLink={
								wholesalex_overview.new_conversation_url
							}
							customClass="wsx-mb-40 wsx-slg-center-hz"
						/>
					) }

					<div className="wsx-justify-wrapper wsx-slg-justify-wrapper wsx-gap-12 wsx-mb-24 wsx-flex-wrap">
						<div className="wsx-d-flex wsx-gap-8 wsx-item-center wsx-slg-justify-wrapper">
							{ ! props?.isFrontend && (
								<div className="wsx-d-flex wsx-gap-4 wsx-item-center">
									<Select
										options={ getOptionsArray(
											wholesalex_overview.bulk_actions
										) }
										onChange={ onBulkActionChange }
										inputBackground="base1"
										minWidth="205px"
									/>
									<Button
										label={ __(
											'Apply',
											'wholesalex'
										) }
										onClick={ handleBulkAction }
										background="base2"
										borderColor="primary"
										color="primary"
										customClass="wsx-font-14"
										padding="11px 20px"
									/>
								</div>
							) }
							<Select
								options={ getOptionsArray(
									wholesalex_overview.statuses
								) }
								value={ filterStatus }
								onChange={ onFilterStatusChange }
								inputBackground="base1"
								minWidth="172px"
								selectionText={ __(
									'Select Status',
									'wholesalex'
								) }
							/>
							{ ! props?.isFrontend && (
								<Select
									options={ getOptionsArray(
										wholesalex_overview.post_statuses
									) }
									value={ postStatus }
									onChange={ ( e ) =>
										setPostStatus( e.target.value )
									}
									inputBackground="base1"
									selectionText={ __(
										'Select Post Status',
										'wholesalex'
									) }
									minWidth="185px"
								/>
							) }
							<Select
								options={ getOptionsArray(
									wholesalex_overview.types
								) }
								value={ filterType }
								onChange={ ( e ) =>
									setFilterType( e.target.value )
								}
								inputBackground="base1"
								selectionText={ __(
									'Select Type',
									'wholesalex'
								) }
								minWidth="142px"
							/>
						</div>
						<div className="wsx-d-flex wsx-gap-8 wsx-item-center">
							<Search
								type="text"
								name={ 'wsx-lists-search-user' }
								value={ tempSearchValue }
								onChange={ ( e ) =>
									setTempSearchValue( e.target.value )
								}
								iconName="search"
								iconColor="#070707"
								background="transparent"
								borderColor="#868393"
								maxHeight="38px"
								placeholder={ __( 'Search…', 'wholesalex' ) }
							/>
							<Dropdown
								className="wsx-relative wsx-bg-base2 wsx-border-default wsx-bc-primary wsx-p-10 wsx-mb-0 wsx-btn"
								iconName="menu"
								iconRotation="half"
								renderContent={ columnsSelectionContent }
							/>
						</div>
					</div>

					<div className="wsx-lists-table-wrapper wsx-scrollbar">
						<table className="wsx-table wsx-lists-table wsx-w-full">
							<thead className="wsx-lists-table-header">
								<tr className="wsx-lists-table-header-row">
									<th className="wsx-checkbox-column wsx-lists-table-column">
										<label
											className="wsx-label wsx-checkbox-option-wrapper"
											style={ { paddingRight: '16px' } }
											htmlFor="checkbox"
										>
											{ ' ' }
											<input
												type="checkbox"
												checked={
													selectedUsers.length ===
														users.length &&
													users.length > 0
												}
												onChange={
													updateAllUserSelection
												}
											/>
											<span className="wsx-checkbox-mark"></span>
										</label>
									</th>
									{ Object.keys( columns ).map(
										( heading, index ) => {
											return (
												columns[ heading ].status && (
													<th
														key={ index }
														className={ `wsx-lists-table-column-${ columns[ heading ].name } wsx-lists-table-column` }
													>
														{
															columns[ heading ]
																.title
														}
													</th>
												)
											);
										}
									) }
								</tr>
							</thead>

							<tbody className="wsx-lists-table-body wsx-relative">
								{ initialLoader && (
									<tr className="wsx-lists-table-row wsx-lists-empty">
										<td
											colSpan={ 100 }
											className={ `wsx-lists-table-column` }
											style={ { height: '213px' } }
										>
											{ <LoadingGif overlay={ false } /> }
										</td>
									</tr>
								) }
								{ paginationData.loader && (
									<LoadingGif insideContainer={ true } />
								) }
								{ ! initialLoader &&
									Object.keys( users ).map(
										( user, index ) => (
											<tr key={ index }>
												<td className="wsx-checkbox-column wsx-lists-table-column">
													<label
														className="wsx-label wsx-checkbox-option-wrapper"
														style={ {
															paddingRight:
																'16px',
														} }
														htmlFor="checkbox"
													>
														{ ' ' }
														<input
															type="checkbox"
															data-id={
																users[ user ].ID
															}
															checked={ selectedUsers.includes(
																users[ user ].ID
															) }
															onChange={
																selectUser
															}
														/>
														<span className="wsx-checkbox-mark"></span>
													</label>
												</td>
												{ columns &&
													Object.keys( columns ).map(
														( heading ) =>
															columns[ heading ]
																.status && (
																<>
																	{ renderTableCell(
																		users[
																			user
																		][
																			columns[
																				heading
																			]
																				.name
																		],
																		columns[
																			heading
																		].type,
																		columns[
																			heading
																		].name,
																		users[
																			user
																		],
																		index
																	) }
																</>
															)
													) }
											</tr>
										)
									) }
								{ ! initialLoader &&
									Object.keys( users ).length === 0 && (
										<tr className="wsx-lists-table-row wsx-lists-empty">
											<td
												colSpan={ 100 }
												className={ `wsx-lists-table-column` }
											>
												{ Icons.noData }
											</td>
										</tr>
									) }
							</tbody>
						</table>
					</div>
					{ hasPagination() && (
						<UserPagination
							paginationData={ paginationData }
							fetchData={ fetchData }
							spaceTop={ 48 }
						/>
					) }
				</div>
			</div>
			{ deleteModal() }
			{ state.length > 0 && (
				<Toast position={ 'top_right' } delay={ 5000 } />
			) }
			{ alert && (
				<Alert
					title="Please Select Properly!"
					description="Please select conversation from the list and an action to perform bulk action."
					onClose={ handleAlertClose }
				/>
			) }
			{ isAlertVisible && (
				<Alert
					title="Confirm Bulk Action"
					description={ `Are you sure you want to ${ bulkAction } the selected ${
						selectedUsers.length === 1
							? 'conversation'
							: 'conversations'
					} ?` }
					onClose={ handleAlertClose }
					onConfirm={ confirmBulkAction }
				/>
			) }
		</div>
	);
};

export default Conversation;
