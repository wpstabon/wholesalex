import React from 'react';
import Conversation from './Conversation';
import Header from '../../components/Header';
import { ToastContextProvider } from '../../context/ToastsContexts';

document.addEventListener( 'DOMContentLoaded', function () {
	const rootElement = document.getElementById(
		'wholesalex_conversation_root'
	);

	if ( rootElement ) {
		const root = createRoot( rootElement ); // Create a root using the element
		root.render(
			<React.StrictMode>
				<Header title={ 'Conversation' } />
				<Conversation />
			</React.StrictMode>,
			document.getElementById( 'wholesalex_conversation_root' )
		);
	}
} );
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_conversation_root_frontend' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<Header title={ 'Conversation' } isFrontend={ true } />
				<Conversation isFrontend={ true } />
			</React.StrictMode>,
			document.getElementById( 'wholesalex_conversation_root_frontend' )
		);
	}
} );

document.addEventListener( 'DOMContentLoaded', function () {
	const frontendElement = document.getElementById(
		'wholesalex_conversation_root_frontend'
	);

	if ( frontendElement ) {
		const root = createRoot( frontendElement ); // Create a root using the frontend element
		root.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ 'Conversation' } />
					<Conversation />
				</ToastContextProvider>
			</React.StrictMode>
		);
	}
} );

document.addEventListener( 'DOMContentLoaded', function () {
	const headerElement = document.getElementById(
		'wholesalex_coversation_header'
	);

	if ( headerElement ) {
		const root = createRoot( headerElement ); // Create a root using the element
		root.render(
			<React.StrictMode>
				<Header title={ 'Conversations' } />
			</React.StrictMode>
		);
	}
} );
