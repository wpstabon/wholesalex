import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';
import ProgressBar from './ProgressBar';
import WelcomePage from './WelcomePage';
import GeneralFieldsPage from './GeneralFieldsPage';
import ReadyPage from './ReadyPage';
import CoreFeaturesPage from './CoreFeatures';
import '../../assets/scss/InstallationWizard.scss';
import Button from '../../components/Button';

const Wizard = () => {
	const [ activeStep, setActiveStep ] = useState( 1 );

	const nextStep = () => {
		if ( activeStep < totalSteps ) {
			setActiveStep( activeStep + 1 );
		}
	};

	const prevStep = () => {
		if ( activeStep ) {
			setActiveStep( activeStep - 1 );
		}
	};

	const steps = [
		{
			label: __( 'WELCOME', 'wholesalex' ),
			step: 1,
		},
		{
			label: __( 'Business Info', 'wholesalex' ),
			step: 2,
		},
		// {
		//   label:__('Addons', 'wholesalex'),
		//   step: 3,
		// },
		{
			label: __( 'Exclusive Features', 'wholesalex' ),
			step: 3,
		},
		{
			label: __( 'READY', 'wholesalex' ),
			step: 4,
		},
	];

	const totalSteps = steps.length;

	const handleFooterClick = () => {
		sendAjaxRequests( 'wsx_wizard_update' );
	};
	const sendAjaxRequests = ( action = 'wsx_wizard_update' ) => {
		const formData = new FormData();
		formData.append( 'action', action );
		formData.append( 'nonce', wholesalex_overview.wsx_wizard_nonce );
		if ( action === 'wsx_wizard_update' ) {
			formData.append( '__wholesalex_initial_setup', true );
		}
		fetch( wholesalex_overview.wsx_wizard_ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( () => {
				location.replace(
					wholesalex_overview.wsx_wizard_dashboard_url
				);
			} )
			.catch( () => {
				// handle error
			} );
	};

	return (
		<div className="wsx-container">
			<ProgressBar activeStep={ activeStep } steps={ steps } />
			{ activeStep === 1 && <WelcomePage nextStep={ nextStep } /> }
			{ activeStep === 2 && (
				<GeneralFieldsPage
					nextStep={ nextStep }
					prevStep={ prevStep }
				/>
			) }
			{ /* { activeStep == 3 && < AddonsPaage nextStep={nextStep} prevStep={prevStep}/>} */ }
			{ activeStep === 3 && (
				<CoreFeaturesPage nextStep={ nextStep } prevStep={ prevStep } />
			) }
			{ activeStep === 4 && <ReadyPage /> }
			{ activeStep !== 4 && (
				<div className="wsx-mt-48 wsx-pb-100">
					<Button
						label={ __( 'Back to dashboard', 'wholesalex' ) }
						onClick={ handleFooterClick }
						onlyText={ true }
						color="text-body"
						customClass="wsx-text-center wsx-font-18 wsx-font-regular"
					/>
				</div>
			) }
		</div>
	);
};
export default Wizard;
