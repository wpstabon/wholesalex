import React from 'react';
import { __ } from '@wordpress/i18n';
const Header = () => {
	return (
		<div className="wholesalex-wizard-header">
			<div className="wholesalex-logo">
				<img
					className="logo center-image"
					src={
						wholesalex_overview.wsx_wizard_url +
						'assets/img/logo-option.svg'
					}
					alt={ __( 'WholesaleX Logo', 'wholesalex' ) }
				/>
			</div>
		</div>
	);
};

export default Header;
