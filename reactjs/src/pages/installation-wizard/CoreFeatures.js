import { __ } from '@wordpress/i18n';

import Button from '../../components/Button';

const CoreFeaturesPage = ( { nextStep, prevStep } ) => {
	const coreFeaturs = [
		{ icon: '', title: __( 'Dynamic Rules', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'User Roles', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'WholesaleX Wallet', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Bulk Order Form', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Form Builder (New)', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Conversation', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Request a Quote', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Sub-accounts', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Private Store', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Product visibility', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Tiered Pricing', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Min & Max Discount', 'wholesalex' ), url: '' },
		{ icon: '', title: __( 'Bogo Discount (New)', 'wholesalex' ), url: '' },
	];
	return (
		<div className="wsx-card wsx-p-0">
			<div className="wsx-bc-border-primary wsx-border-bottom wsx-p-20 wsx-text-center">
				<div className="wsx-font-24 wsx-color-text-medium wsx-font-bold wsx-mb-12">
					{ __( 'WholesaleX Exclusive', 'wholesalex' ) }
				</div>
				<div className="wsx-font-16 wsx-color-text-light wsx-width-580 wsx-center-hz">
					{ __(
						'Take advantage of the Core Features of the Plugin to ensure your store’s success!',
						'wholesalex'
					) }
				</div>
			</div>
			<div className="wsx-p-50 wsx-d-flex wsx-flex-column wsx-gap-32">
				<div className="wsx-d-flex wsx-flex-wrap wsx-item-center wsx-justify-center wsx-gap-20 wsx-center-hz">
					{ coreFeaturs.map( ( feature ) => {
						return (
							<div
								key={ feature.title }
								className="wsx-bg-base2 wsx-p-10 wsx-plr-16 wsx-br-md"
							>
								{ feature.url && (
									<a
										href={ feature.url }
										className="wsx-link wsx-font-16 wsx-font-medium wsx-color-text-medium"
									>
										{ feature.title }
									</a>
								) }
								{ ! feature.url && (
									<div className="wsx-font-16 wsx-font-medium wsx-color-text-medium">
										{ feature.title }
									</div>
								) }
							</div>
						);
					} ) }
				</div>
				<div
					className={ `wsx-d-flex wsx-justify-space wsx-item-center wsx-gap-16` }
				>
					<Button
						label={ __( 'Skip the step', 'wholesalex' ) }
						onlyText={ true }
						color="text-body"
						customClass="wsx-text-center wsx-font-18 wsx-font-regular"
						onClick={ nextStep }
					/>

					<div className="wsx-btn-group">
						<Button
							label={ __( 'Previous', 'wholesalex' ) }
							onClick={ prevStep }
							background="base2"
							iconName="angleLeft"
							padding="10px 20px 10px 12px"
						/>
						<Button
							label={ __( 'Next', 'wholesalex' ) }
							onClick={ nextStep }
							background="primary"
							iconName="angleRight"
							iconPosition="after"
							padding="10px 12px 10px 20px"
						/>
					</div>
				</div>
			</div>
		</div>
	);
};
export default CoreFeaturesPage;
