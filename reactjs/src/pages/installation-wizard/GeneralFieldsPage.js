import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Select from '../../components/Select';
import Checkbox from '../../components/Checkbox';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';

const GeneralFieldsPage = ( { nextStep, prevStep } ) => {
	const [ , setInitialState ] = useState( {
		tier_table_layout: 'layout_one',
		install_woocommerce: 'yes',
		install_productx: 'yes',
	} );
	const [ value, setValue ] = useState( {
		tier_table_layout: 'layout_one',
		install_woocommerce: 'yes',
		install_productx: 'yes',
		website_name: wholesalex_overview.wsx_wizard_site_name,
		website_type: '',
	} );

	const [ popUpStatus, setPopUpStatus ] = useState( false );

	const [ pageLoading ] = useState( false );

	const generalFields = [
		{
			label: __( 'Store Type', 'wholesalex' ),
			type: 'select',
			placeholder: __( 'Personal blog', 'wholesalex' ),
			name: 'website_type',
		},
		// { 'label': __('Store Name', 'wholesalex'), 'type': 'text', 'placeholder': __('Your Website Name', 'wholesalex'), 'name': 'website_name' },
		{
			label: __( 'Upload Site Icon', 'wholesalex' ),
			type: 'file',
			placeholder: __( 'Add or Upload File', 'wholesalex' ),
			name: 'site_icon',
		},
		// { 'label': __('Tier table layout for Single Product Page', 'wholesalex'), 'type': 'choosebox', 'placeholder': '', 'name': 'tier_table_layout' }
	];

	const checkboxFields = [
		{
			label: wholesalex_overview.wsx_wizard_woocommer_installed
				? 'WooCommerce (Already Installed)'
				: 'WooCommerce',
			type: 'checkbox',
			placeholder: __( 'Add or Upload File', 'wholesalex' ),
			name: 'install_woocommerce',
		},
		{
			label: wholesalex_overview.wsx_wizard_productx_installed
				? 'WowStore (Already Installed)'
				: 'WowStore',
			type: 'checkbox',
			placeholder: '',
			name: 'install_productx',
		},
	];

	const tierLayouts = {
		layout_one: 'layout_one.png',
		layout_two: 'layout_two.png',
		layout_three: 'layout_three.png',
		pro_layout_four: 'layout_four.png',
		pro_layout_five: 'layout_five.png',
		pro_layout_six: 'layout_six.png',
		pro_layout_seven: 'layout_seven.png',
		pro_layout_eight: 'layout_eight.png',
	};

	const installationNotice = () => {
		const _woocommerce =
			value.install_woocommerce === 'yes' ||
			value.activate_woocommerce === 'yes';
		const _productx =
			value.install_productx === 'yes' ||
			value.activate_productx === 'yes';
		let _message = '';
		if ( _woocommerce && _productx ) {
			_message = `<span class='installable-plugin'>${ __(
				'WooCommerce',
				'wholesalex'
			) }</span> & <span class='installable-plugin'><a class="wsx-link" href="https://www.wpxpo.com/productx/?utm_source=wholesalex-setup&utm_medium=general-productx&utm_campaign=wholesalex-wizard">${ __(
				'WowStore',
				'wholesalex'
			) }</a></span>`;
		} else if ( _woocommerce ) {
			_message = `<span class='installable-plugin'>${ __(
				'WooCommerce',
				'wholesalex'
			) }</span>`;
		} else if ( _productx ) {
			_message = `<span class='installable-plugin'> <a class="wsx-link" href="https://www.wpxpo.com/productx/?utm_source=wholesalex-setup&utm_medium=general-productx&utm_campaign=wholesalex-wizard"> ${ __(
				'WowStore',
				'wholesalex'
			) }</a></span>`;
		}
		if ( _woocommerce || _productx ) {
			return (
				__(
					'The following plugins will be installed and activated for you:',
					'wholesalex'
				) + _message
			);
		}
	};

	const saveGeneralData = () => {
		sendAjaxRequest( 'save_general_data' );
	};

	const sendAjaxRequest = ( action = 'get_general_data' ) => {
		const formData = new FormData();
		formData.append( 'action', action );
		formData.append( 'nonce', wholesalex_overview.wsx_wizard_nonce );

		if ( action === 'save_general_data' ) {
			if ( value.website_type ) {
				formData.append( 'website_type', value.website_type );
			}
			if ( value.website_name ) {
				formData.append( 'website_name', value.website_name );
			}
			if ( value.site_icon ) {
				formData.append( 'site_icon', value.site_icon );
			}
			if ( value.tier_table_layout ) {
				formData.append( 'tier_table_layout', value.tier_table_layout );
			}
			if ( value.install_woocommerce ) {
				formData.append(
					'install_woocommerce',
					value.install_woocommerce
				);
			}
			if ( value.install_productx ) {
				formData.append( 'install_productx', value.install_productx );
			}
		}

		fetch( wholesalex_overview.wsx_wizard_ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if ( res ) {
					if (
						res.success &&
						res.data &&
						action === 'get_general_data'
					) {
						setInitialState( res.data );
						setValue( res.data );
					}
				}
			} );
		if ( action === 'save_general_data' ) {
			nextStep();
		}
	};

	const optionsData = [
		{ value: 'default', label: 'Select Store Type' },
		{ value: 'Fashion', label: 'Fashion' },
		{ value: 'Electronics', label: 'Electronics' },
		{ value: 'Clothing', label: 'Clothing' },
		{
			value: 'Beauty and Personal Care',
			label: 'Beauty and Personal Care',
		},
		{ value: 'Food and Beverage', label: 'Food and Beverage' },
		{ value: 'Apparel and Accessories', label: 'Apparel and Accessories' },
		{ value: 'Medical Equipment', label: 'Medical Equipment' },
		{ value: 'Automotive Parts', label: 'Automotive Parts' },
		{ value: 'Others', label: 'Others' },
	];

	return (
		<>
			{ ! pageLoading && (
				<div
					key={ 'wizard_general_fields' }
					className="wsx-card wsx-p-0"
				>
					<div className="wsx-bc-border-primary wsx-border-bottom wsx-p-20 wsx-text-center">
						<div className="wsx-font-24 wsx-color-text-medium wsx-font-bold wsx-mb-12">
							{ __( 'Store Details', 'wholesalex' ) }
						</div>
						<div className="wsx-font-16 wsx-color-text-light wsx-width-580 wsx-center-hz">
							{ __(
								'Fill out the following information on the type of store you want to build.',
								'wholesalex'
							) }
						</div>
					</div>
					<div className="wsx-p-50 wsx-d-flex wsx-flex-column wsx-gap-32">
						{ generalFields.map( ( field ) => {
							const type = field.type;
							const name = field.name;
							const placeholder = field.placeholder;
							const defaultValue = value[ name ];
							switch ( type ) {
								case 'text':
									return (
										<Input
											key={ name }
											type="text"
											name={ name }
											className="wholesalex_wizard_field"
											label={ field.label }
											placeholder={ placeholder }
											value={ defaultValue }
											onChange={ ( e ) => {
												setValue( {
													...value,
													[ name ]: e.target.value,
												} );
											} }
										/>
									);
								case 'choosebox':
									return (
										<div
											key={
												'wholesalex_wizard_choosebox'
											}
											className="wizard-field choosebox wholesalex_wizard_field"
										>
											<label
												className="wsx-label"
												htmlFor={ name }
											>
												{ ' ' }
												{ field.label }
											</label>
											<div className="tier-layouts">
												{ Object.keys(
													tierLayouts
												).map( ( option, k ) => {
													const _isProActive =
														wholesalex_wizard?.is_pro_active;
													const imagePath =
														tierLayouts[ option ];
													if ( _isProActive ) {
														if (
															option.startsWith(
																'pro_'
															)
														) {
															option =
																option.replace(
																	'pro_',
																	''
																);
														}
													}

													const _lockField =
														option.startsWith(
															'pro_'
														);

													return (
														<>
															<div
																key={ name }
																className={ `tier-layout ${
																	value[
																		name
																	] === option
																		? 'active-tier'
																		: ''
																}` }
															>
																<input
																	className="wsx-radio"
																	disabled={
																		_lockField
																	}
																	id={
																		option
																	}
																	name={
																		name
																	}
																	type="radio"
																	value={
																		option
																	}
																	defaultChecked={
																		value[
																			name
																		] &&
																		value[
																			name
																		] ===
																			option
																			? true
																			: false
																	}
																	onChange={ (
																		e
																	) => {
																		setValue(
																			{
																				...value,
																				[ name ]:
																					e
																						.target
																						.value,
																			}
																		);
																	} }
																/>
																<div className="tier_image">
																	<label
																		className={ `wsx-label center-image ${
																			value[
																				name
																			] ===
																			option
																				? ''
																				: 'bor-transparent-2px'
																		}` }
																		htmlFor={
																			option
																		}
																		key={ `wholesalex-choosebox_${ k }` }
																		onClick={ () => {
																			_lockField &&
																				setPopUpStatus(
																					true
																				);
																		} }
																		onKeyDown={ (
																			e
																		) => {
																			if (
																				e.key ===
																					'Enter' ||
																				e.key ===
																					' '
																			) {
																				_lockField &&
																					setPopUpStatus(
																						true
																					);
																			}
																		} }
																		role="none"
																		tabIndex="0"
																	>
																		{ ' ' }
																		<img
																			className={
																				_lockField
																					? 'locked'
																					: ''
																			}
																			src={
																				wholesalex_overview.wsx_wizard_url +
																				'assets/img/' +
																				imagePath
																			}
																			alt={
																				option
																			}
																		/>
																		{ _lockField && (
																			<span className="get-pro dashicons dashicons-lock"></span>
																		) }
																	</label>
																</div>
															</div>
														</>
													);
												} ) }
											</div>
										</div>
									);
								case 'select':
									return (
										<Select
											label={ __(
												'Store Type',
												'wholesalex'
											) }
											options={ optionsData }
											onChange={ ( e ) => {
												setValue( {
													...value,
													[ name ]: e.target.value,
												} );
											} }
											inputBackground="base1"
											borderColor="border-secondary"
											inputPadding="9px 16px"
											minWidth="182px"
										/>
									);
							}
						} ) }
						<div className="wizard-field-checkboxes">
							{ checkboxFields.map( ( field ) => {
								const type = field.type;
								const name = field.name;
								let label = field.label;
								const _isWcActivated = value.wc_activated
									? true
									: false;

								const _isWC = name === 'install_woocommerce';
								const _PX = name === 'install_productx';

								const _isProductXActivated =
									value.productx_activated ? true : false;

								if (
									( _isWC && _isWcActivated ) ||
									( _PX && _isProductXActivated )
								) {
									label += __( '(Activated)', 'wholesalex' );
									// isDisable = true;
								}

								switch ( type ) {
									case 'checkbox':
										return (
											<Checkbox
												className="wsx-mb-12 wsx-font-16"
												option={ label }
												name={ name }
												value={
													value[ name ] &&
													value[ name ] === 'yes'
														? true
														: false
												}
												onChange={ ( e ) => {
													const _final = e.target
														.checked
														? 'yes'
														: 'no';
													setValue( {
														...value,
														[ name ]: _final,
													} );
												} }
											/>
										);
								}
							} ) }
							<div
								className="wsx-font-14"
								dangerouslySetInnerHTML={ {
									__html: installationNotice(),
								} }
							></div>
						</div>
						{ pageLoading && <LoadingGif /> }
						{ ! pageLoading && (
							<div
								className={ `wsx-d-flex wsx-justify-space wsx-item-center wsx-gap-16` }
							>
								<Button
									label={ __(
										'Skip the step',
										'wholesalex'
									) }
									onlyText={ true }
									color="text-body"
									customClass="wsx-text-center wsx-font-18 wsx-font-regular"
									onClick={ nextStep }
								/>
								<div className="wsx-btn-group">
									<Button
										label={ __( 'Previous', 'wholesalex' ) }
										onClick={ prevStep }
										background="base2"
										iconName="angleLeft"
										padding="10px 20px 10px 12px"
									/>
									<Button
										label={ __( 'Next', 'wholesalex' ) }
										onClick={ () => {
											saveGeneralData();
										} }
										background="primary"
										iconName="angleRight"
										iconPosition="after"
										padding="10px 12px 10px 20px"
									/>
								</div>
								{ popUpStatus && (
									<UpgradeProPopUp
										title={ __(
											'Unlock all Features with',
											'wholesalex'
										) }
										onClose={ () =>
											setPopUpStatus( false )
										}
									/>
								) }
							</div>
						) }
					</div>
				</div>
			) }
		</>
	);
};
export default GeneralFieldsPage;
