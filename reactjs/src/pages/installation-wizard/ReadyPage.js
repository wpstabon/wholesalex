import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import Checkbox from '../../components/Checkbox';
import LoadingGif from '../../components/LoadingGif';
import PopupModal from '../../components/PopupModal';

const ReadyPage = () => {
	const [ value, setValue ] = useState( {
		get_updates_consent: 'yes',
		share_data_consent: 'yes',
	} );
	const [ loading, setLoading ] = useState( false );
	const [ playVideo, setPlayVideo ] = useState( false );

	const helpInfo = [
		{
			icon: 'explore_pro.svg',
			label: __( 'Explore All Features', 'wholesalex' ),
			link: 'https://getwholesalex.com/features/?utm_source=wholesalex-setup&utm_medium=ready-all_features&utm_campaign=wholesalex-wizard',
		},
		{
			icon: 'knowledge_base.svg',
			label: __( 'WholesaleX Documentation', 'wholesalex' ),
			link: 'https://getwholesalex.com/documentation/?utm_source=wholesalex-setup&utm_medium=ready-documentation&utm_campaign=wholesalex-wizard',
		},
		{
			icon: 'forum_support.svg',
			label: __( 'Forum Support', 'wholesalex' ),
			link: 'https://wordpress.org/support/plugin/wholesalex/',
		},
		{
			icon: 'ticket_support.svg',
			label: __( 'Pre-Sale & Pro Support', 'wholesalex' ),
			link: 'https://getwholesalex.com/contact/?utm_source=wholesalex-setup&utm_medium=ready-pro_support&utm_campaign=wholesalex-wizard',
		},
	];

	const socialMediaHandles = [
		{
			icon: 'facebook-icon.svg',
			label: __( 'Join the community to stay up to date', 'wholesalex' ),
			link: 'https://www.facebook.com/groups/438050567242124/',
		},
	];

	const readyToGo = () => {
		sendAjaxRequest();
	};

	const sendAjaxRequest = ( action = 'data_collection' ) => {
		setLoading( true );
		const formData = new FormData();
		formData.append( 'action', action );
		formData.append( 'nonce', wholesalex_overview.wsx_wizard_nonce );

		if ( action === 'data_collection' ) {
			formData.append( 'share_data_consent', value.share_data_consent );
			formData.append( 'get_updates_consent', value.get_updates_consent );
		}
		fetch( wholesalex_overview.wsx_wizard_ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( () => {
				setLoading( false );
			} );

		location.replace( wholesalex_overview.wsx_wizard_dashboard_url );
	};

	const addonVideoModalContent = () => {
		return (
			<div className="wsx-addon-video-container">
				<iframe
					className="wsx-addon-modal-video"
					src={ `https://www.youtube.com/embed/Zvfxpu8Gtd0?autoplay=1` }
					allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
					title="Embedded YouTube"
					loading="lazy"
				/>
			</div>
		);
	};

	return (
		<div className="wsx-card wsx-p-0">
			<div className="wsx-bc-border-primary wsx-border-bottom wsx-p-20 wsx-text-center">
				<div className="wsx-font-24 wsx-color-text-medium wsx-font-bold wsx-mb-12">
					{ __( 'You’re Done…', 'wholesalex' ) }
				</div>
				<div className="wsx-font-16 wsx-color-text-light wsx-width-580 wsx-center-hz wsx-mb-24">
					{ __(
						'You are ready to embark on your Wholesale journey with WholesaleX',
						'wholesalex'
					) }
				</div>
				<Button
					label={ __( 'Ready To Go', 'wholesalex' ) }
					onClick={ readyToGo }
					background="primary"
					customClass="wsx-center-hz wsx-font-20"
				/>
			</div>
			<div className="wsx-p-50 wsx-d-flex wsx-flex-wrap wsx-gap-48">
				<div className="wsx-flex-1">
					<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-lh-0 wsx-relative wsx-mb-32">
						<img
							src={
								wholesalex_overview.wsx_wizard_url +
								'/assets/img/setup-wizard/intro.png'
							}
							alt={ __( 'WholesaleX Intro Video', 'wholesalex' ) }
						/>
						<div
							className="wsx-absolute"
							style={ { maxWidth: '25%' } }
							onClick={ () => {
								setPlayVideo( true );
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									setPlayVideo( true );
								}
							} }
							role="button"
							tabIndex="0"
						>
							<img
								src={
									wholesalex_overview.wsx_wizard_url +
									'/assets/img/setup-wizard/play.png'
								}
								alt={ __( 'Play', 'wholesalex' ) }
							/>
							<span className="wsx-btn-play-animate"></span>
						</div>
						{ playVideo && (
							<PopupModal
								className="wsx-video-modal"
								renderContent={ addonVideoModalContent }
								onClose={ () => setPlayVideo( false ) }
							/>
						) }
					</div>
					<Checkbox
						className="wsx-mb-32 wsx-font-16"
						help={ __(
							'We will send important tips & tricks for effective usage of WholesaleX.',
							'wholesalex'
						) }
						helpClass="wsx-font-12"
						option={ __( 'Get Updates', 'wholesalex' ) }
						name="get_updates_consent"
						value={
							value.get_updates_consent &&
							value.get_updates_consent === 'yes'
								? true
								: false
						}
						onChange={ ( e ) => {
							const _final = e.target.checked ? 'yes' : 'no';
							setValue( { ...value, [ e.target.name ]: _final } );
						} }
					/>
					<Checkbox
						className="wsx-mb-32 wsx-font-16"
						help={ __(
							'Let us collect non-sensitive diagnosis data and usage information.',
							'wholesalex'
						) }
						helpClass="wsx-font-12"
						option={ __( 'Share Essentials', 'wholesalex' ) }
						name="wsx_wizard_field"
						value={
							value.share_data_consent &&
							value.share_data_consent === 'yes'
								? true
								: false
						}
						onChange={ ( e ) => {
							const _final = e.target.checked ? 'yes' : 'no';
							setValue( { ...value, [ e.target.name ]: _final } );
						} }
					/>
				</div>
				<div className="wsx-flex-1">
					<div className="wsx-font-24 wsx-font-medium wsx-color-text-medium wsx-mb-40">
						{ __( 'Learn More', 'wholesalex' ) }
					</div>
					<div
						className="wsx-d-flex wsx-flex-column wsx-gap-24"
						style={ {
							marginBottom:
								socialMediaHandles.length === 0 ? '0' : '50px',
						} }
					>
						{ helpInfo.map( ( option, index ) => {
							return (
								<div key={ index }>
									<span className="wsx-d-flex wsx-gap-16 wsx-item-center">
										<img
											className="wsx-learn-more-option-icon"
											src={
												wholesalex_overview.wsx_wizard_url +
												'assets/img/setup-wizard/' +
												option.icon
											}
											alt={ option.label }
										/>
										<span
											onClick={ () => {
												window.open(
													option.link,
													'_blank'
												);
											} }
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													window.open(
														option.link,
														'_blank'
													);
												}
											} }
											role="button"
											tabIndex="0"
											className="wsx-font-18 wsx-font-medium wsx-learn-more-option-label"
										>
											{ option.label }
										</span>
									</span>
								</div>
							);
						} ) }
					</div>
					{ socialMediaHandles.length &&
						socialMediaHandles.map( ( socialMedia, index ) => {
							return (
								<button
									key={ index }
									className="wsx-btn wsx-bg-transparent wsx-btn-icon wsx-border-default wsx-color-primary wsx-btn-border-primary wsx-social"
									onClick={ () => {
										window.open(
											socialMedia.link,
											'_blank'
										);
									} }
								>
									<img
										src={
											wholesalex_overview.wsx_wizard_url +
											'/assets/img/setup-wizard/' +
											socialMedia.icon
										}
										alt="wizard"
									/>
									<span>{ socialMedia.label }</span>
								</button>
							);
						} ) }
				</div>
			</div>
			{ loading && <LoadingGif /> }
		</div>
	);
};
export default ReadyPage;
