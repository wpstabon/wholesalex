import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';
import Wizard from './Wizard';
import { ToastContextProvider } from '../../context/ToastsContexts';

const element = document.getElementById( 'wholesalex_initial_setup_wizard' );

if ( document.body.contains( element ) ) {
	ReactDOM.render(
		<React.StrictMode>
			<ToastContextProvider>
				<div className="wholesalex-wizard wsx-wrapper">
					<Header />
					<Wizard />
				</div>
			</ToastContextProvider>
		</React.StrictMode>,
		element
	);
}
