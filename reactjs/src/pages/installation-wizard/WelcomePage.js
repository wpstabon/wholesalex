import React from 'react';
import { __ } from '@wordpress/i18n';

import Button from '../../components/Button';

const WelcomePage = ( { nextStep } ) => {
	return (
		<div className="wsx-card wsx-text-center wsx-plr-20 wsx-p-50 wsx-family-body">
			<div className="wsx-font-36 wsx-font-bold wsx-color-text-medium">
				{ __( 'Welcome to the', 'wholesalex' ) }{ ' ' }
				<span className="wsx-color-primary">
					{ __( 'WholesaleX', 'wholesalex' ) }
				</span>{ ' ' }
				{ __( 'Installation Wizard!', 'wholesalex' ) }
			</div>
			<div className="wsx-mt-32 wsx-mb-40">
				<div className="wsx-font-18 wsx-color-text-light wsx-mb-32">
					{ __( 'To start using the WholesaleX', 'wholesalex' ) }
					<span className="wsx-font-bold">
						{ __( 'B2B+B2C', 'wholesalex' ) }
					</span>
					{ __( 'Hybrid Plugin, we need your help…', 'wholesalex' ) }
				</div>
				<div className="wsx-font-16 wsx-color-text-body">
					{ __(
						'Please fill out the following information to build your dream wholesale store!',
						'wholesalex'
					) }
				</div>
			</div>
			<Button
				label={ __( "Let's Start", 'wholesalex' ) }
				onClick={ nextStep }
				iconName="angleRight"
				iconPosition="after"
				background="primary"
				customClass="wsx-font-20 wsx-font-medium wsx-center-hz"
				padding="10px 12px 10px 20px"
			/>
		</div>
	);
};
export default WelcomePage;
