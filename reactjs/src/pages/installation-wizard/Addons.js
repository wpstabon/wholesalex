import React, { useState, useContext } from 'react';
import { __ } from '@wordpress/i18n';
import AddonCard from '../addons/AddonCard';
import PopupModal from '../../components/PopupModal';
import { ToastContexts } from '../../context/ToastsContexts';
import Button from '../../components/Button';

const AddonsPage = ( { nextStep, prevStep } ) => {
	const [ addonOptions, setAddonOptions ] = useState(
		wholesalex_overview.wsx_wizard_addons
	);
	const [ popUpStatus, setPopUpStatus ] = useState( false );
	const [ whitelabelPopup, setWhitelabelPopup ] = useState( false );
	const { dispatch } = useContext( ToastContexts );
	const [ installText, setInstallText ] = useState( {
		wsx_addon_dokan_integration: 'Install & Activate',
		wsx_addon_wcfm_integration: 'Install & Activate',
	} );
	const [ loading, setLoading ] = useState( {
		wsx_addon_dokan_integration: false,
		wsx_addon_wcfm_integration: false,
	} );

	const updateAddonStatus = ( addonName, status ) => {
		const _addonData = { ...addonOptions };
		_addonData[ addonName ] = { ..._addonData[ addonName ], status };
		setAddonOptions( _addonData );
		fetchData( 'post', 'update_status', addonName, status );
	};

	const fetchData = async (
		type = 'post',
		addonName = '',
		updatedStatus = ''
	) => {
		const attr = {
			type,
			action: 'addons',
			request_for: 'update_status',
			requestFor: 'update_status',
			status: updatedStatus ? 'yes' : 'no',
			nonce: wholesalex.nonce,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/addons',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				// res
			} else {
				const _addonData = { ...addonOptions };
				_addonData[ addonName ] = {
					..._addonData[ addonName ],
					status: updatedStatus,
				};
				setAddonOptions( _addonData );
			}
		} );
	};

	const proPopupContent = () => {
		return (
			<>
				{ /* <img src={wholesalex.url+'/assets/img/unlock.svg'} alt="Unlock Icon"/>
                <div className='unlock_text'>{__('UNLOCK', 'wholesalex')}</div>
                <div className='unlock_heading'>{__('Unlock This Addon', 'wholesalex')}</div>
                <div className='with_premium_text'>{__('With WholesaleX Pro', 'wholesalex')}</div>
                <div className='desc'>{__('We are sorry, but unfortunately, this addon is unavailable in the free version. Please upgrade to a pro plan.', 'wholesalex')}</div>
                <a className='wsx-link wholesalex-btn wholesalex-upgrade-pro-btn wholesalex-btn-lg' target="_blank" href='https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_features-upgrade_to_pro&utm_campaign=wholesalex-DB' >Upgrade to Pro  ➤</a> */ }

				<img
					className="wsx-addon-popup-image"
					src={ wholesalex.url + '/assets/img/unlock.svg' }
					alt="Unlock Icon"
				/>
				<div className="wsx-title wsx-font-18 wsx-font-medium wsx-mt-4">
					{ __( 'Unlock This Addon With', 'wholesalex' ) }
				</div>
				<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mb-16">
					<div className="wsx-title wsx-font-18 wsx-font-medium">
						{ __( 'WholesaleX', 'wholesalex' ) }
					</div>
					<div className="wsx-title wsx-font-18 wsx-font-medium wsx-color-primary">
						{ __( 'Pro', 'wholesalex' ) }
					</div>
				</div>
				{ /* <div className='wsx-mb-20'>{__('We are sorry, but unfortunately, this addon is unavailable in the free version. Please upgrade to a pro plan.', 'wholesalex')}</div> */ }
				<Button
					buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_features-upgrade_to_pro&utm_campaign=wholesalex-DB"
					label={ __( 'Upgrade to Pro ➤', 'wholesalex' ) }
					background="secondary"
					iconName="growUp"
					customClass="wsx-w-auto wsx-br-lg wsx-justify-center wsx-font-16"
				/>
			</>
		);
	};
	const onLockIconClickHandler = ( id ) => {
		if ( 'wsx_addon_whitelabel' === id ) {
			setWhitelabelPopup( true );
		} else {
			setPopUpStatus( true );
		}
	};

	const installPlugin = ( addonName = '' ) => {
		setInstallText( { ...installText, [ addonName ]: 'Installing...' } );
		setLoading( { ...loading, [ addonName ]: true } );
		const attr = {
			type: 'post',
			action: 'addons',
			request_for: 'install_plugin',
			addon: addonName,
			nonce: wholesalex.nonce,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/addons',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: res.data,
					},
				} );
				setInstallText( {
					...installText,
					[ addonName ]: 'Installed',
				} );
				window.location.reload();
			} else {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Error Occure! Please try again',
					},
				} );
			}

			setLoading( { ...loading, [ addonName ]: false } );
		} );
	};

	const getWhiteLabelPopupContent = () => {
		return (
			<>
				{ /* <img src={wholesalex.url+'/assets/img/unlock.svg'} alt="Unlock Icon"/>
                <div className='unlock_text'>{__('UNLOCK', 'wholesalex')}</div>
                <div className='unlock_heading'>{__('Unlock This Addon', 'wholesalex')}</div>
                <div className='with_premium_text'>{__('With WholesaleX Pro', 'wholesalex')}</div>
                <div className='desc'>{__('We are sorry, but unfortunately, this addon is unavailable in your current plan. Please upgrade to an agency plan.', 'wholesalex')}</div>
                <a className='wsx-link wholesalex-btn wholesalex-upgrade-pro-btn wholesalex-btn-lg' href='https://getwholesalex.com/pricing/?utm_source=wholesalex-setup&utm_medium=WL_addons-upgrade_to_pro&utm_campaign=wholesalex-wizard' target='_blank' >{__('Upgrade Now', 'wholesalex')}</a> */ }

				<img
					className="wsx-addon-popup-image"
					src={ wholesalex.url + '/assets/img/unlock.svg' }
					alt="Unlock Icon"
				/>
				<div className="wsx-title wsx-font-18 wsx-font-medium wsx-mt-4">
					{ __( 'Unlock This Addon With', 'wholesalex' ) }
				</div>
				<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mb-16">
					<div className="wsx-title wsx-font-18 wsx-font-medium">
						{ __( 'WholesaleX', 'wholesalex' ) }
					</div>
					<div className="wsx-title wsx-font-18 wsx-font-medium wsx-color-primary">
						{ __( 'Pro', 'wholesalex' ) }
					</div>
				</div>
				{ /* <div className='wsx-mb-20'>{__('We are sorry, but unfortunately, this addon is unavailable in your current plan. Please upgrade to an agency plan.', 'wholesalex')}</div> */ }
				<Button
					buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-setup&utm_medium=WL_addons-upgrade_to_pro&utm_campaign=wholesalex-wizard"
					label={ __( 'Upgrade Now', 'wholesalex' ) }
					background="secondary"
					iconName="growUp"
					customClass="wsx-w-auto wsx-br-lg wsx-justify-center wsx-font-16"
				/>
			</>
		);
	};

	return (
		<>
			{
				<div className="wizard-container wholesalex_setup_wizard__addons_page setup_wizard_container wholesalex-wizard general">
					<div className="wizard-container__header">
						<div className="wizard-container__heading">
							<span className="heading_text">
								{ __( 'WholesaleX Addons', 'wholesalex' ) }
							</span>
						</div>
						<div className="wizard-container__subheading">
							{ __(
								'Use these conversion-focused addons to grow your B2B business and earn more profit',
								'wholesalex'
							) }
						</div>
					</div>
					<div className="setup_wizard__content">
						<div className="wholesalex_addon__cards">
							{ Object.keys( addonOptions ).map( ( addon ) => {
								const _addons = { ...addonOptions[ addon ] };
								delete _addons.features;
								return (
									<AddonCard
										key={ addon }
										{ ..._addons }
										id={ addon }
										alt={ addonOptions[ addon ].name }
										icon={ addonOptions[ addon ].img }
										onUpdateStatus={ updateAddonStatus }
										onLockClick={ onLockIconClickHandler }
										installPlugin={ installPlugin }
										installText={ installText }
										loading={ loading }
									/>
								);
							} ) }
						</div>

						{
							<div
								className={ `progress-buttons-container justify-space-between` }
							>
								<div className="left-buttons">
									<span
										className="wizard_step_skip_button"
										onClick={ nextStep }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												nextStep;
											}
										} }
										role="button"
										tabIndex="0"
									>
										{ __( 'Skip the step', 'wholesalex' ) }
									</span>
								</div>

								<div className="right-buttons">
									{ /* <div className="skip-button" onClick={nextStep}>
                                Skip This Step
                            </div> */ }
									<div
										className="wholesalex-btn wholesalex-secondary-btn wizard_previous_btn"
										onClick={ prevStep }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												prevStep;
											}
										} }
										role="button"
										tabIndex="0"
									>
										<span className="dashicons dashicons-arrow-left-alt2"></span>
										{ __( 'Previous', 'wholesalex' ) }
									</div>
									<div
										className="wholesalex-btn wholesalex-primary-btn wizard_next_btn"
										onClick={ nextStep }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												nextStep;
											}
										} }
										role="button"
										tabIndex="0"
									>
										{ __( 'Next', 'wholesalex' ) }
										<span
											className={
												'dashicons dashicons-arrow-right-alt2'
											}
										></span>
									</div>
								</div>
							</div>
						}
					</div>

					{ popUpStatus && (
						<PopupModal
							className="wsx-pro-modal"
							renderContent={ proPopupContent }
							onClose={ () => setPopUpStatus( false ) }
						/>
					) }
					{ whitelabelPopup && (
						<PopupModal
							className="wsx-pro-modal"
							renderContent={ getWhiteLabelPopupContent }
							onClose={ () => setWhitelabelPopup( false ) }
						/>
					) }
				</div>
			}
		</>
	);
};
export default AddonsPage;
