import React from 'react';

const ProgressBar = ( { activeStep, steps } ) => {
	const getClassName = ( label ) => {
		let className = 'wsx-progress-step-label ';
		if ( label === 'WELCOME' ) {
			className += 'wsx-progress-step-label-left';
		} else if ( label === 'READY' ) {
			className += 'wsx-progress-step-label-right';
		}
		return className;
	};
	return (
		<div className="wsx-progressbar-container">
			<div
				className={ `wsx-progress-step-container wsx-step-${ activeStep }` }
			>
				{ steps.map( ( { step, label } ) => (
					<div
						className={ `wsx-progress-step-wrapper ${
							activeStep >= step ? 'completed' : ''
						}` }
						key={ step }
					>
						<div className="wsx-progress-step-style"></div>
						<div className="wsx-progress-step-label-container">
							<div
								className={ getClassName( label ) }
								key={ step }
							>
								{ label }
							</div>
						</div>
					</div>
				) ) }
			</div>
		</div>
	);
};

export default ProgressBar;
