import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';

import LoadingGif from '../../components/LoadingGif';

const SiteStatusPage = ( { nextStep, prevStep } ) => {
	const [ pluginStatus, setPluginStatus ] = useState( 'b2b' );
	const [ loading, setLoading ] = useState( false );
	const [ pageLoading, setPageLoading ] = useState( false );
	const pluginStatusData = [
		{
			icon: 'b2b_business.svg',
			value: 'b2b',
			label: __( 'B2B Business', 'wholesalex' ),
			desc: __(
				'Add wholesale pricing & discounts based on multiple criteria for the B2B customers.',
				'wholesalex'
			),
		},
		{
			icon: 'b2c_business.svg',
			value: 'b2c',
			label: __( 'B2C Business', 'wholesalex' ),
			desc: __(
				'Offer discounts based on multiple criteria only for B2C customers.',
				'wholesalex'
			),
		},
		{
			icon: 'hybrid_business.svg',
			value: 'b2b_n_b2c',
			label: __( 'B2B & B2C Hybrid', 'wholesalex' ),
			desc: __(
				'Add wholesale pricing & discounts for both B2B and B2C customers.',
				'wholesalex'
			),
		},
	];

	const savePluginStatus = () => {
		sendAjaxRequest( 'set_plugin_status', pluginStatus );
	};

	const sendAjaxRequest = ( action = 'get_plugin_status', status = '' ) => {
		if ( action !== 'get_plugin_status' ) {
			setLoading( true );
		}
		const formData = new FormData();
		formData.append( 'action', action );
		formData.append( 'nonce', wholesalex_overview.wsx_wizard_nonce );

		if ( action === 'set_plugin_status' ) {
			formData.append( 'plugin_status', status );
		}
		fetch( wholesalex_overview.wsx_wizard_ajax, {
			method: 'POST',
			body: formData,
		} )
			.then( ( res ) => res.json() )
			.then( ( res ) => {
				if (
					res.success &&
					res.data &&
					action === 'get_plugin_status'
				) {
					setPluginStatus( res.data );
					setPageLoading( false );
				}
				setLoading( false );
				if ( action === 'set_plugin_status' ) {
					nextStep();
				}
			} );
	};

	return (
		<>
			{ ! pageLoading && (
				<div className="wizard-container site-status">
					{ pluginStatusData.map( ( status ) => {
						return (
							<div
								key={ status.icon }
								className={ `wholesalex-status-card ${
									pluginStatus === status.value
										? 'active-card'
										: ''
								}` }
							>
								<input
									id={ status.value }
									name={ 'plugin_status' }
									type="radio"
									className="wsx-radio"
									value={ status.value }
									defaultChecked={
										pluginStatus &&
										pluginStatus === status.value
											? true
											: false
									}
									onChange={ ( e ) => {
										setPluginStatus( e.target.value );
									} }
								/>
								<label
									htmlFor={ status.value }
									className="wsx-label"
								>
									<img
										className="wholesalex-status-image"
										src={
											wholesalex_overview.wsx_wizard_url +
											'assets/img/setup-wizard/' +
											status.icon
										}
										alt={ status.title }
									/>
									<div className="card-meta flex-column">
										<div className="card-title">
											{ status.label }
										</div>
										<div className="card-desc">
											{ status.desc }
										</div>
									</div>
								</label>
							</div>
						);
					} ) }
				</div>
			) }
			{ pageLoading && <LoadingGif /> }
			{ ! pageLoading && (
				<div
					className={ `progress-buttons-container justify-space-between` }
				>
					<div className="left-buttons">
						<div
							className="progress-button previous-button"
							onClick={ prevStep }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									prevStep;
								}
							} }
							role="button"
							tabIndex="0"
						>
							<span className="dashicons dashicons-arrow-left-alt2"></span>
							{ __( 'Previous', 'wholesalex' ) }
						</div>
					</div>
					<div className="right-buttons">
						<div
							className="progress-button next-button"
							onClick={ () => {
								! loading && savePluginStatus();
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									! loading && savePluginStatus();
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ __( 'Next', 'wholesalex' ) }
							<span
								className={ ` ${
									loading
										? 'loading'
										: 'dashicons dashicons-arrow-right-alt2'
								}` }
							></span>
						</div>
					</div>
				</div>
			) }
		</>
	);
};
export default SiteStatusPage;
