import React from 'react';
import { __ } from '@wordpress/i18n';
import CardBox from '../../components/CardBox';
import Button from '../../components/Button';
import './Features.scss';

const Features = () => {
	const majorFeatureCards = [
		{
			iconSVG: `${ wholesalex.url }assets/img/restrict.svg`,
			title: __( 'Restrict Guest Access', 'wholesalex' ),
			desc: __(
				'Make your wholesale area private for registered users and restrict guest users.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/private-store/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/addons/bulkorder.svg`,
			title: __( 'Bulk Order', 'wholesalex' ),
			desc: __(
				'Allow your customers to order products in bulk or create purchase lists to order later.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/bulk-order/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/addons/raq.svg`,
			title: __( 'Request A Quote', 'wholesalex' ),
			desc: __(
				'Let the potential buyers send quote requests to you directly from the cart page.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/request-a-quote/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/wholesale_pricing.svg`,
			title: __( 'Wholesale Pricing', 'wholesalex' ),
			desc: __(
				'Effortlessly manage wholesale pricing based on multiple wholesale/b2b user roles.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/how-to-set-wholesale-prices-in-woocommerce/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/registration_form.svg`,
			title: __( 'Registration Form', 'wholesalex' ),
			desc: __(
				'Create a custom registration form with custom fields for effective customer acquisition.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/registration-form-builder/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/addons/subaccount.svg`,
			title: __( 'Subaccounts Management', 'wholesalex' ),
			desc: __(
				'Let your registered B2B customers create subaccounts with necessary user access.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/subaccounts-in-woocommerce-b2b-stores/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/addons/wallet.svg`,
			title: __( 'Wallet Management', 'wholesalex' ),
			desc: __(
				'Let B2B customers add funds to their digital wallets and use it as a payment method.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/wallet/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/dynamic_rule.svg`,
			title: __( 'Dynamic Discount Rules', 'wholesalex' ),
			desc: __(
				'Effectively manage discounted wholesale pricing using the dynamic discount rules.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/dynamic-rules/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
		{
			iconSVG: `${ wholesalex.url }assets/img/addons/conversation.svg`,
			title: __( 'Conversations Built-in Messaging', 'wholesalex' ),
			desc: __(
				'Let your registered customers communicate with you with the in-built conversation system.',
				'wholesalex'
			),
			support_url:
				'https://getwholesalex.com/conversation/?utm_source=wholesalex-menu&utm_medium=features_page-explore_more&utm_campaign=wholesalex-DB',
		},
	];

	const coreFeatures = [
		{
			icon: '',
			title: __( 'Create Unlimited User Roles', 'wholesalex' ),
		},
		{ icon: '', title: __( 'Tax Control', 'wholesalex' ) },
		{
			icon: '',
			title: __( 'Import and Export Role Base/Sale Price', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Import Export Customer', 'wholesalex' ),
		},
		{
			icon: '',
			title: __(
				'Automatic Approval For B2B Registration',
				'wholesalex'
			),
		},
		{
			icon: '',
			title: __( 'Manual Approval For B2B Registration', 'wholesalex' ),
		},
		{
			icon: '',
			title: __(
				'Email Notifications For Different Actions',
				'wholesalex'
			),
		},
		{
			icon: '',
			title: __( 'Control Redirect URLs', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Visibility Control', 'wholesalex' ),
		},
		{ icon: '', title: __( 'Shipping Control', 'wholesalex' ) },
		{
			icon: '',
			title: __( 'Force Free Shipping', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Payment Gateway Control', 'wholesalex' ),
		},
		{ icon: '', title: __( 'Extra Charge', 'wholesalex' ) },
		{ icon: '', title: __( 'BOGO Discounts', 'wholesalex' ) },
		{
			icon: '',
			title: __( 'Show Login to view prices', 'wholesalex' ),
		},
		{ icon: '', title: __( 'Buy X Get Y', 'wholesalex' ) },
		{
			icon: '',
			title: __( 'Control Order Quantity', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Google RreCAPTCHA V3 Integration', 'wholesalex' ),
		},
		{ icon: '', title: __( 'Quote Request', 'wholesalex' ) },
		{
			icon: '',
			title: __( 'Conversation With Store Owner', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Auto Role Migration', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Rolewise Credit Limit', 'wholesalex' ),
		},
		{
			icon: '',
			title: __( 'Conditions and Limits', 'wholesalex' ),
		},
	];

	return (
		<div className="wsx-wrapper">
			<div className="wsx-container">
				<div className="wsx-text-center wsx-mb-48">
					<div className="wsx-title wsx-mb-16">
						{ __( 'The Most Complete WooCommerce', 'wholesalex' ) }{ ' ' }
						<span className="wsx-color-primary">
							{ __( 'B2B + B2C', 'wholesalex' ) }
						</span>{ ' ' }
						{ __( 'Hybrid Solution', 'wholesalex' ) }
					</div>
					<div className="wsx-color-text-medium">
						{ __(
							'Use these conversion-focused features and earn more profit',
							'wholesalex'
						) }
					</div>
				</div>

				<div className="wsx-column-3 wsx-gap-24 wsx-row-gap-30 wsx-lg-column-2 wsx-sm-column-1">
					{ majorFeatureCards.map( ( feature, index ) => (
						<CardBox
							key={ index }
							icon={ feature.iconSVG }
							title={ feature.title }
							description={ feature.desc }
							linkText={ __( 'Explore More', 'wholesalex' ) }
							linkUrl={ feature.support_url }
						/>
					) ) }
				</div>

				<div className="wsx-features-section">
					<div className="wsx-features-container">
						<div className="wsx-title wsx-mb-40 wsx-text-center">
							{ __( 'WholesaleX Core', 'wholesalex' ) }{ ' ' }
							{ __( 'Components', 'wholesalex' ) }
						</div>
						<ul className="wsx-color-text-medium wsx-column-3 wsx-font-16 wsx-gap-40 wsx-row-gap-20 wsx-lg-column-2 wsx-mb-40 wsx-md-column-1 wsx-list-default">
							{ coreFeatures.map( ( feature ) => {
								return (
									<li key={ feature.title } className="">
										{ feature.title }
									</li>
								);
							} ) }
						</ul>
						<Button
							label={ __(
								'Explore More Features',
								'wholesalex'
							) }
							buttonLink="https://getwholesalex.com/features/?utm_source=wholesalex-menu&utm_medium=features_page-explore_all_features&utm_campaign=wholesalex-DB"
							background="primary"
							customClass="wsx-center-hz"
						/>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Features;
