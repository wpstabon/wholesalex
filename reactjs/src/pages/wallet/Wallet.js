import React, { useState, useEffect } from 'react';
import { __ } from '@wordpress/i18n';
import PendingRequest from './PendingRequest';
import styleGenerate from '../../components/Helper';

import LoadingGif from '../../components/LoadingGif';
import Button from '../../components/Button';

const Wallet = () => {
	const [ appState, setAppState ] = useState( { loading: false } );
	const [ topupAmount, setTopUpAmount ] = useState( '' );
	const [ fields, setFields ] = useState( {} );
	const [ currentPage, setCurrentPage ] = useState( 1 );
	const [ activeTab, setActiveTab ] = useState( '_add_balance_section' );
	const [ isLoading, setLoading ] = useState( false );
	const [ requestLoader, setRequestLoader ] = useState( false );

	const styles = styleGenerate( wholesalex );

	const fetchData = async (
		type = 'get',
		_amount = '',
		_redirect = '',
		isRequest = false
	) => {
		if ( type === 'post' ) {
			setRequestLoader( true );
		}
		const attr = {
			type,
			action: 'wallet_action',
			nonce: wholesalex.nonce,
		};
		if ( isRequest ) {
			attr.is_request = true;
		}
		if ( type === 'post' ) {
			attr.amount = _amount;
		}
		wp.apiFetch( {
			path: '/wholesalex/v1/wallet_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			setLoading( false );
			setRequestLoader( false );
			if ( res.success ) {
				if ( type === 'get' ) {
					if (
						! Object.prototype.hasOwnProperty.call(
							res.data.default,
							'_add_balance_section'
						)
					) {
						setActiveTab( '_transaction_history_section' );
					}
					setFields( res.data.default );
					setAppState( { ...appState, loading: false } );
				} else if ( type === 'post' ) {
					if ( ! isRequest ) {
						window.location.href = _redirect;
					} else {
						window.alert(
							__( 'Request Send Successful.', 'wholesalex-pro' )
						);
					}
				}
			}
		} );
	};

	useEffect( () => {
		// fetchData();
		if (
			! Object.prototype.hasOwnProperty.call(
				wholesalex_wallet.fields,
				'_add_balance_section'
			)
		) {
			setActiveTab( '_transaction_history_section' );
		}
		setFields( wholesalex_wallet.fields );
	}, [] );

	const balanceSectionData = ( sectionData ) => {
		return Object.keys( sectionData ).map(
			( fieldName ) =>
				sectionData[ fieldName ].type === 'balance' &&
				balanceData( sectionData[ fieldName ], fieldName )
		);
	};

	const balanceData = ( fieldData, fieldName ) => {
		return (
			<div
				key={ fieldName }
				className="wsx-d-flex wsx-w-full wsx-justify-space wsx-item-center wsx-mb-26 wsx-font-bold wholesalex-wallet__balance-field"
				style={ styles.border_color }
			>
				<div
					style={ styles.text }
					className="wallet_label wsx-font-24"
					htmlFor={ fieldName }
				>
					{ fieldData.label }
				</div>
				<div
					style={ styles.primary_color }
					className="wallet_balance wsx-font-32"
					id={ fieldName }
					dangerouslySetInnerHTML={ {
						__html: fieldData.formated_balance,
					} }
				></div>
			</div>
		);
	};

	const buttonSectionData = ( sectionData, sectionName ) => {
		return (
			<div
				style={ { overflow: 'auto hidden' } }
				className="wsx-scrollbar"
			>
				<div
					key={ sectionName }
					className={ `wsx-d-flex wsx-gap-24 wsx-mb-24 wsx-font-20 wsx-font-medium wholesalex-wallet__button-section ${
						wholesalex_wallet?.settings?.wsx_addon_subaccount ===
							'yes' && 'wholesalex_master_wallet'
					}` }
				>
					{ Object.keys( sectionData ).map(
						( fieldName ) =>
							sectionData[ fieldName ].type === 'button' &&
							buttonData( sectionData[ fieldName ], fieldName )
					) }
				</div>
			</div>
		);
	};

	const buttonData = ( fieldData, fieldName ) => {
		const _tabName = fieldName + '_section';
		return (
			<div
				key={ fieldName }
				style={
					fieldName === '_submit_btn' || fieldName === '_request_btn'
						? styles.primary_button
						: {}
				}
				className={ `${
					activeTab === _tabName ? 'active' : ''
				} wallet${ fieldName } wsx-btn-wallet` }
				id={ fieldName }
				onClick={ ( e ) => {
					e.preventDefault();
					let _amount = 0;
					switch ( fieldName ) {
						case '_submit_btn':
							_amount = parseFloat( topupAmount );
							if ( _amount > 0 ) {
								fetchData(
									'post',
									_amount,
									fieldData.redirect_to
								);
							}
							break;
						case '_request_btn':
							_amount = parseFloat( topupAmount );
							if ( _amount > 0 ) {
								fetchData( 'post', _amount, '', true );
							}
							break;

						default:
							setActiveTab( _tabName );
							break;
					}
				} }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						e.preventDefault();
						let _amount = 0;
						switch ( fieldName ) {
							case '_submit_btn':
								_amount = parseFloat( topupAmount );
								if ( _amount > 0 ) {
									fetchData(
										'post',
										_amount,
										fieldData.redirect_to
									);
								}
								break;
							case '_request_btn':
								_amount = parseFloat( topupAmount );
								if ( _amount > 0 ) {
									fetchData( 'post', _amount, '', true );
								}
								break;

							default:
								setActiveTab( _tabName );
								break;
						}
					}
				} }
				role="button"
				tabIndex="0"
			>
				{ fieldData.icon_class && (
					<span className={ `icon ${ fieldData.icon_class }` }></span>
				) }
				<div className={ `label${ fieldName }` }>
					{ fieldData.label }{ ' ' }
					{ isLoading && <span className="whx_loading"></span> }{ ' ' }
				</div>
			</div>
		);
	};

	const addBalanceSectionData = ( sectionData, sectionName ) => {
		return (
			<div
				key={ sectionName }
				className="wsx-wallet-add-balance wallet_add_balance_section"
			>
				{ Object.keys( sectionData ).map( ( fieldName ) => {
					const _data = sectionData[ fieldName ];
					switch ( _data.type ) {
						case 'number':
							return numberData( _data, fieldName );
						case 'button':
							return buttonData( _data, fieldName );
						default:
							return [];
					}
				} ) }
			</div>
		);
	};

	const numberData = ( fieldData, fieldName ) => {
		return (
			<div
				key={ fieldName }
				className={ `wsx-mb-24 wallet${ fieldName }` }
			>
				<div
					className={ `wsx-label wsx-font-18 wsx-font-bold wsx-color-text-medium wsx-mb-12 wsx-d-block label${ fieldName }` }
					htmlFor={ fieldName }
					dangerouslySetInnerHTML={ { __html: fieldData.label } }
				/>
				<input
					className="wsx-input wsx-w-full wsx-plr-10"
					type="number"
					placeholder={ __( 'Enter here', 'wholesalex-pro' ) }
					id={ fieldName }
					value={ topupAmount }
					onChange={ ( e ) => {
						setTopUpAmount( e.target.value );
					} }
				></input>
			</div>
		);
	};

	const transactionsData = ( sectionData, sectionName ) => {
		return (
			<div
				key={ sectionName }
				className="wallet_transaction_history_section wsx-scrollbar"
				style={ { overflowX: 'auto' } }
			>
				{ Object.keys( sectionData ).map( ( fieldName ) => {
					const _data = sectionData[ fieldName ];
					switch ( _data.type ) {
						case 'table':
							return tableData( _data, fieldName );

						default:
							return [];
					}
				} ) }
			</div>
		);
	};

	const tableData = ( fieldData, fieldName ) => {
		// const itemPerPage = parseInt(fieldData['item_per_page']);
		const itemPerPage = 50;

		const transactionData = fieldData.data;
		const totalPageCount = fieldData.data.length / itemPerPage;

		const getPaginatedData = () => {
			const startIndex = currentPage * itemPerPage - itemPerPage;
			const endIndex = startIndex + itemPerPage;
			return transactionData.slice( startIndex, endIndex );
		};

		return (
			<div
				key={ fieldName }
				className="wallet_transaction_history_table_section"
			>
				<div className="wsx-account-table-container wsx-font-14">
					<table className="wsx-table">
						<thead>
							<tr>
								{ Object.keys( fieldData.columns ).map(
									( column, i ) => (
										<th
											key={ `wallet_transaction_header_${ i }` }
											className={ column }
										>
											{ fieldData.columns[ column ] }
										</th>
									)
								) }
							</tr>
						</thead>
						<tbody>
							{ getPaginatedData().map( ( transaction, i ) => (
								<tr key={ `wallet_transaction_${ i }` }>
									<td className="transaction_id">
										{ transaction._id }
									</td>
									<td className="transction_type">
										{ transaction.type }
									</td>
									<td
										className="amount"
										dangerouslySetInnerHTML={ {
											__html: transaction.formated_amount,
										} }
									></td>
									<td className="date">
										{ new Date(
											parseInt( transaction.timestamp ) *
												1000
										)
											.toDateString()
											.split( ' ' )
											.slice( 1 )
											.join( ' ' ) }
									</td>
									<td className="details">
										{ transaction.reason }
									</td>
								</tr>
							) ) }
						</tbody>
					</table>
				</div>
				<div className="wsx-btn-group">
					{ currentPage > 1 && (
						<Button
							onClick={ () => {
								setCurrentPage( ( page ) => page - 1 );
							} }
							label={ fieldData.pagination_label.prev }
							iconName="angleLeft_24"
							background="primary"
							padding="8px 16px 8px 10px"
						/>
					) }
					{ currentPage < totalPageCount && (
						<Button
							onClick={ () => {
								setCurrentPage( ( page ) => page + 1 );
							} }
							label={ fieldData.pagination_label.next }
							iconName="angleRight_24"
							iconPosition="after"
							background="primary"
							padding="8px 10px 8px 16px"
						/>
					) }
				</div>
			</div>
		);
	};
	return (
		<div className="wholesalex_wallet_page">
			{ appState.loading && (
				<div className="wholesalex-editor__loading-overlay">
					<img
						alt="Loading..."
						className="wholesalex-editor__loading-overlay__inner"
						src={ wholesalex.url + 'assets/img/spinner.gif' }
					/>
				</div>
			) }
			{ requestLoader && <LoadingGif /> }
			{ Object.keys( fields ).map( ( section ) => {
				const _sectionData = fields[ section ].attr;
				switch ( section ) {
					case '_add_balance_section':
						return (
							activeTab === '_add_balance_section' &&
							addBalanceSectionData( _sectionData, section )
						);
					case '_balance_section':
						return balanceSectionData( _sectionData, section );
					case '_button_section':
						return buttonSectionData( _sectionData, section );
					case '_transaction_history_section':
						return (
							activeTab === '_transaction_history_section' &&
							transactionsData( _sectionData, section )
						);
					case '_subaccount_pending_request_section':
						return (
							activeTab ===
								'_subaccount_pending_request_section' && (
								<PendingRequest
									key={ 'pending_requests' }
									setFields={ setFields }
									setRequestLoader={ setRequestLoader }
									requestLoader={ requestLoader }
									columns={
										_sectionData._pending_requested_table
											.columns
									}
									requests={
										_sectionData._pending_requested_table
											.data
									}
								/>
							)
						);
					default:
						return [];
				}
			} ) }
		</div>
	);
};

export default Wallet;
