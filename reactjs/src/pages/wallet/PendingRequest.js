import React, { useState } from 'react';
import LoadingGif from '../../components/LoadingGif';
import { __ } from '@wordpress/i18n';

const PendingRequest = ( {
	setFields,
	columns,
	requests,
	setRequestLoader,
} ) => {
	const [ isLoading, setIsLoading ] = useState( false );
	const [ isDeclinePopupVisible, setIsDeclinePopupVisible ] =
		useState( false );
	const [ , setIsRequestDeclined ] = useState( false );

	const handleApproveClick = ( id ) => {
		sendRequest( 'wallet_request', 'approve', id );
	};

	const handleDeclineClick = ( id ) => {
		// setIsDeclinePopupVisible(true);
		sendRequest( 'wallet_request', 'reject', id );
	};

	const handlePopupBackClick = () => {
		setIsDeclinePopupVisible( false );
	};

	const handlePopupDeclineClick = () => {
		setIsDeclinePopupVisible( false );
		setIsRequestDeclined( true );
	};

	const handleOverlayClick = ( event ) => {
		if ( event.target.classList.contains( 'popup-overlay' ) ) {
			setIsDeclinePopupVisible( false );
		}
	};

	const sendRequest = async (
		type = 'wallet_request',
		requestType = 'approve',
		requestId = ''
	) => {
		setRequestLoader( true );
		setIsLoading( true );
		const attr = {
			type,
			action: 'wallet_action',
			nonce: wholesalex.nonce,
			requestType,
			requestId,
		};

		wp.apiFetch( {
			path: '/wholesalex/v1/wallet_action',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			setRequestLoader( false );
			setIsLoading( false );
			if ( res.success ) {
				if ( requestType === 'approve' ) {
					window.alert( res.data.data );
					setFields( res.data.fields );
				} else if ( requestType === 'reject' ) {
					window.alert( res.data.data );
					setFields( res.data.fields );
				}
			}
		} );
	};

	const renderRequests = () => {
		let pendingRequests;
		if ( requests ) {
			pendingRequests = requests.filter( ( request ) => {
				return request.status === 'pending';
			} );
		}

		if ( pendingRequests && pendingRequests.length > 0 ) {
			return pendingRequests.map( ( request ) => {
				return (
					<tr key={ request.id }>
						<td>{ request.subaccount_name }</td>
						<td
							dangerouslySetInnerHTML={ {
								__html: request.amount_html,
							} }
						></td>
						<td className="wsx-d-flex wsx-justify-center">
							{ request.status === 'pending' && (
								<>
									<button
										className="approve-button"
										onClick={ () => {
											handleApproveClick( request.id );
										} }
									>
										{ __( 'Approve', 'wholesalex-pro' ) }
									</button>
									<button
										className="decline-button"
										onClick={ () => {
											handleDeclineClick( request.id );
										} }
									>
										{ __( 'Decline', 'wholesalex-pro' ) }
									</button>
								</>
							) }
							{ request.status === 'rejected' && (
								<>
									<button className="decline-button" disabled>
										{ __( 'Rejected', 'wholesalex-pro' ) }
									</button>
								</>
							) }
							{ request.status === 'accepted' && (
								<>
									<button className="approve-button" disabled>
										{ __( 'Approved', 'wholesalex-pro' ) }
									</button>
								</>
							) }
						</td>
					</tr>
				);
			} );
		}
		return (
			<tr key={ 'no_req_found' }>
				<td colSpan={ 100 } className="no_wallet_requests">
					{ __( 'No Requests Found!', 'wholesalex-pro' ) }
				</td>
			</tr>
		);
	};

	return (
		<div className="table-container wsx-account-table-container wsx-font-14">
			<table className="wsx-table">
				<thead>
					<tr key={ 'subaccount_credit_cols' }>
						{ Object.keys( columns ).map( ( column, i ) => (
							<th key={ `wallet_request_header_${ i }` }>
								{ columns[ column ] }
							</th>
						) ) }
					</tr>
				</thead>
				<tbody className="wsx-relative">
					{ isLoading && (
						<tr className="wsx-lists-table-row wsx-lists-empty">
							<td
								colSpan={ 100 }
								className={ `wsx-lists-table-column` }
								style={ { height: '213px' } }
							>
								{ <LoadingGif insideContainer={ false } /> }
							</td>
						</tr>
					) }
					{ ! isLoading && renderRequests() }
				</tbody>
			</table>
			{ isDeclinePopupVisible && (
				<div
					className="popup-overlay"
					onClick={ handleOverlayClick }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							handleOverlayClick;
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div className="popup">
						<div className="popup-message">
							{ __(
								'Do you really want to decline this request?',
								'wholesalex-pro'
							) }
						</div>
						<div className="popup-buttons">
							<button
								className="popup-back-button"
								onClick={ handlePopupBackClick }
							>
								{ __( 'Back', 'wholesalex-pro' ) }
							</button>
							<button
								className="popup-decline-button"
								onClick={ handlePopupDeclineClick }
							>
								{ __( 'Decline', 'wholesalex-pro' ) }
							</button>
						</div>
					</div>
				</div>
			) }
		</div>
	);
};

export default PendingRequest;
