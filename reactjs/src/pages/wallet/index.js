import React from 'react';
import ReactDOM from 'react-dom';
import Wallet from './Wallet';
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( '_wholesalex_wallet_page' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<Wallet />
			</React.StrictMode>,
			document.getElementById( '_wholesalex_wallet_page' )
		);
	}
} );
