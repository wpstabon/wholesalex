import React from 'react';
import ReactDOM from 'react-dom';
import EmailLists from './EmailLists';
import Header from '../../components/Header';
import { ToastContextProvider } from '../../context/ToastsContexts';

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_email_templates_root' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ whx_email_templates.i18n.emails } />
					<EmailLists />
				</ToastContextProvider>
			</React.StrictMode>,
			document.getElementById( 'wholesalex_email_templates_root' )
		);
	}
} );
