import React, { useContext, useState } from 'react';
import Input from '../../components/Input';
import Slider from '../../components/Slider';
import OverlayWindow from '../../components/OverlayWindow';
import TextArea from '../../components/TextArea';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import Select from '../../components/Select';
import Icons from '../../utils/Icons';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';

const EmailEdit = ( props ) => {
	const { templateId, template, onChange, editWindowHandler, windowRef } =
		props;

	const [ tagStatus, setTagStatus ] = useState( false );
	const { dispatch } = useContext( ToastContexts );
	const [ loader, setLoader ] = useState( false );

	const fields = [
		{
			type: 'email',
			name: 'recipient',
			label: __( 'Admin Email Recipient', 'wholesalex' ),
			value: template.recipient,
		},
		{
			type: 'text',
			name: 'subject',
			label: __( 'Subject', 'wholesalex' ),
			value: template.subject,
		},
		{
			type: 'text',
			name: 'heading',
			label: __( 'Heading', 'wholesalex' ),
			value: template.heading,
		},
		{
			type: 'textarea',
			name: 'additional_content',
			label: __( 'Additional Content', 'wholesalex' ),
			value: template.additional_content,
		},
		// {type:'preview',name:'email_preview',label:'Email Preview',value: template.content},
		{
			type: 'smart_tags',
			name: 'smart_tags',
			label: __( 'Smart Tag Used', 'wholesalex' ),
			value: template.smart_tags,
		},
		{
			type: 'select',
			name: 'email_type',
			label: __( 'Email Type', 'wholesalex' ),
			value: template.email_type,
			options: { html: 'HTML', multipart: 'Multipart' },
		},
	];

	const onSave = () => {
		setLoader( true );

		const _template = {
			subject: template.subject,
			heading: template.heading,
			type: template.type,
			email_type: template.email_type,
			additional_content: template.additional_content,
		};
		if ( template.recipient ) {
			_template.recipient = template.recipient;
		}

		const attr = {
			type: 'save_template',
			template_name: templateId,
			template: _template,
			action: 'email_templates',
			nonce: wholesalex.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/email_templates',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: res.data,
					},
				} );
				setLoader( false );
			}
		} );
	};

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	const overlayContent = () => {
		return (
			<div className="wsx-pb-100 wsx-mb-100">
				{ loader && <LoadingGif /> }
				<div className="wsx-d-grid wsx-gap-24">
					{ fields.map( ( field ) => {
						switch ( field.type ) {
							case 'email':
								return (
									templateId ===
										'wholesalex_new_user_approval_required' && (
										<Input
											key={ field.name }
											label={ field.label }
											type={ 'text' }
											name={ field.name }
											value={ field.value }
											onChange={ onChange }
										/>
									)
								);
							case 'text':
								return (
									<Input
										key={ field.name }
										label={ field.label }
										type={ field.type }
										name={ field.name }
										value={ field.value }
										onChange={ onChange }
									/>
								);
							case 'slider':
								return (
									<Slider
										key={ field.name }
										name={ field.name }
										label={ field.label }
										value={ field.value }
										onChange={ onChange }
									/>
								);
							case 'select':
								return (
									<Select
										key={ field.name }
										label={ field.label }
										options={ getOptionsArray(
											field.options
										) }
										name={ field.name }
										value={ field.value }
										onChange={ onChange }
										minWidth="124px"
									/>
								);
							case 'textarea':
								return (
									<TextArea
										key={ field.name }
										label={ field?.label }
										name={ field.name }
										value={ field?.value }
										onChange={ onChange }
										desc={ field?.desc }
										help={ field?.help }
									/>
								);
							case 'preview':
								return (
									<div className="wsx-html-preview">
										<div
											dangerouslySetInnerHTML={ {
												__html: field.value,
											} }
										></div>
									</div>
								);
							case 'smart_tags':
								return (
									<div className="wsx-accordion-wrapper">
										<div
											className="wsx-accordion-header"
											onClick={ () =>
												setTagStatus( ! tagStatus )
											}
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													setTagStatus( ! tagStatus );
												}
											} }
											role="button"
											tabIndex="0"
										>
											<div className="wsx-accordion-title">
												{ __(
													'Smart Tags',
													'wholesalex'
												) }{ ' ' }
											</div>
											<span
												className={ `wsx-icon ${
													tagStatus && 'active'
												}` }
											>
												{ Icons.angleDown }
											</span>
										</div>
										{ tagStatus && (
											<div className="wsx-accordion-body">
												<div className="wsx-column-2 wsx-sm-column-1 wsx-gap-24">
													{ field.value &&
														Object.keys(
															field.value
														).map( ( tag, i ) => {
															return (
																<div
																	key={ i }
																	className="wsx-d-flex wsx-item-center wsx-gap-12"
																>
																	<code className="wsx-m-0">{ `${ tag }` }</code>{ ' ' }
																	:{ ' ' }
																	{
																		field
																			.value[
																			tag
																		]
																	}{ ' ' }
																</div>
															);
														} ) }
												</div>
											</div>
										) }
									</div>
								);

							default:
								return <div></div>;
						}
					} ) }
					<Button
						label={ __( 'Save Changes', 'wholesalex' ) }
						onClick={ onSave }
						background="primary"
						customClass="wsx-center-hz wsx-w-half wsx-text-center wsx-mt-20"
					/>
				</div>
			</div>
		);
	};

	return (
		<OverlayWindow
			windowRef={ windowRef }
			heading={ template.title }
			content={ overlayContent }
			onClose={ editWindowHandler }
		/>
	);
};

export default EmailEdit;
