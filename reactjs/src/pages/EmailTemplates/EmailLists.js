import React, { useContext, useEffect, useRef, useState } from 'react';
import Slider from '../../components/Slider';
import EmailEdit from './EmailEdit';
import Toast from '../../components/Toast';
import { __ } from '@wordpress/i18n';
import Button from '../../components/Button';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';
import './EmailList.scss';
import { ToastContexts } from '../../context/ToastsContexts';

const EmailLists = () => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	const [ templates, setTemplates ] = useState( {} );
	const [ editWindow, setEditWindow ] = useState( false );
	const [ loader, setLoader ] = useState( false );

	const { dispatch } = useContext( ToastContexts );

	const [ popupStatus, setPopUpStatus ] = useState( false );

	const [ editTemplate, setEditTemplate ] = useState( '' );

	const updateEmailStatus = ( template_name, status ) => {
		const _templates = { ...templates };
		_templates[ template_name ][ 'enabled' ] =
			status && status !== 'no' ? true : false;
		setTemplates( _templates );
		const attr = {
			type: 'update_status',
			template_name,
			enabled: status && status !== 'no' ? 'yes' : 'no',
			action: 'email_templates',
			nonce: wholesalex.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/email_templates',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: res.data,
					},
				} );
			}
		} );
	};

	const templateStatusChangeHandler = ( templateName, status ) => {
		if ( templates[ templateName ].is_lock ) {
			setPopUpStatus( true );
		} else {
			updateEmailStatus( templateName, ! status );
		}
	};

	const headings = [
		{
			name: 'enabled',
			title: __( 'Status', 'wholesalex' ),
			type: 'slider',
			className: '',
		},
		{
			name: 'title',
			title: __( 'Email Template', 'wholesalex' ),
			type: 'text',
			className: '',
		},
		{
			name: 'email_type',
			title: __( 'Content Type', 'wholesalex' ),
			type: 'text',
			className: '',
		},
		{
			name: 'action',
			title: __( 'Action', 'wholesalex' ),
			type: 'button',
			className: '',
		},
	];
	const renderTableCell = ( templateName, data, fieldType, name ) => {
		if ( fieldType === 'slider' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<Slider
						className="wsx-email-lists-status"
						value={
							data && data !== 'no'
								? ! templates[ templateName ].is_lock
								: false
						}
						name={ templateName + '-status' }
						onChange={ () => {
							templateStatusChangeHandler( templateName, data );
						} }
						isLock={ templates[ templateName ].is_lock }
					/>
				</td>
			);
		} else if ( fieldType === 'button' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<Button
						label={ __( 'Edit', 'wholesalex' ) }
						iconName="edit"
						background="base3"
						borderColor="primary"
						customClass="wsx-btn-sm"
						onClick={ ( e ) => {
							editWindowHandler( e, templateName );
						} }
					/>
				</td>
			);
		}
		if ( name === 'email_type' ) {
			if ( ! data ) {
				return (
					<td
						className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
					>
						{ 'text/html' }
					</td>
				);
			}
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					{ data }
				</td>
			);
		} else if ( name === 'title' ) {
			return (
				<td
					className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
				>
					<span
						className={ `wsx-email-template-title wsx-ellipsis ${
							templates[ templateName ].is_pro &&
							! wholesalex.is_pro_active
								? 'wholesalex_locked_title'
								: ''
						}` }
						onClick={ () => {
							if ( templates[ templateName ].is_lock ) {
								setPopUpStatus( true );
							} else {
								setEditTemplate( templateName );
								setEditWindow( true );
							}
						} }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								if ( templates[ templateName ].is_lock ) {
									setPopUpStatus( true );
								} else {
									setEditTemplate( templateName );
									setEditWindow( true );
								}
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ data.substring( data.indexOf( ':' ) + 2 ) }
					</span>
				</td>
			);
		}
		return (
			<td
				className={ `wsx-lists-table-column-${ name } wsx-lists-table-column` }
			>
				{ data }
			</td>
		);
	};

	const ref = useRef( null );

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const editWindowHandler = ( e, templateNname ) => {
		if ( templates[ templateNname ]?.is_lock ) {
			setPopUpStatus( true );
		} else if ( editWindow ) {
			const style = ref?.current?.style;
			if ( ! style ) {
				return;
			}
			sleep( 200 ).then( () => {
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = `translateX(${ isRTL ? '-50%' : '50%' })`;
				style.opacity = '0';

				sleep( 300 ).then( () => {
					setEditWindow( false );
					setEditTemplate( '' );
				} );
			} );
		} else {
			setEditTemplate( templateNname );
			setEditWindow( true );
			setTimeout( () => {
				const style = ref?.current?.style;
				if ( ! style ) {
					return;
				}
				style.transition = 'all var(--transition-md) ease-in-out';
				style.transform = 'translateX(0%)';
				style.opacity = '1';
			}, 10 );
		}
	};

	const fetchData = async ( type = 'get' ) => {
		setLoader( true );
		const attr = {
			type,
			action: 'email_templates',
			nonce: wholesalex.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/email_templates',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				setTemplates( res.data );
			}
			setLoader( false );
		} );
	};

	useEffect( () => {
		fetchData();
	}, [] );

	const onEmailTemplateChangeHandler = ( templateNname, field, value ) => {
		const _templates = { ...templates };
		_templates[ templateNname ][ field ] = value;
		setTemplates( _templates );
	};

	const onTemplateChange = ( e ) => {
		if ( e.target.type === 'checkbox' ) {
			onEmailTemplateChangeHandler(
				editTemplate,
				e.target.name,
				! templates[ editTemplate ][ e.target.name ]
			);
		} else {
			onEmailTemplateChangeHandler(
				editTemplate,
				e.target.name,
				e.target.value
			);
		}
	};

	return (
		//List
		<div className="wsx-wrapper">
			<div className="wsx-container">
				<div className="wsx-lists-table-wrapper wsx-scrollbar wsx-email-lists">
					<table className="wsx-table wsx-lists-table wsx-w-full">
						<thead className="wsx-lists-table-header">
							<tr className="wsx-lists-table-header-row">
								{ headings.map( ( heading, index ) => (
									<th
										key={ index }
										className={ `wsx-lists-table-column-${ heading.name } wsx-lists-table-column` }
									>
										{ heading.title }
									</th>
								) ) }
							</tr>
						</thead>

						<tbody className="wsx-lists-table-body wsx-relative">
							{ /* Loading Placeholder Content-- Start */ }
							{ loader && (
								<tr className="wsx-lists-table-row wsx-lists-empty">
									<td
										colSpan={ 100 }
										className={ `wsx-lists-table-column` }
										style={ { height: '213px' } }
									>
										{ <LoadingGif overlay={ false } /> }
									</td>
								</tr>
							) }

							{ /* Loading Placeholder Content-- End */ }

							{ ! loader &&
								Object.keys( templates ).map(
									( templateName, index ) => (
										<tr
											key={ index }
											className="wsx-lists-table-row"
										>
											{ headings.map( ( heading ) => (
												<>
													{ renderTableCell(
														templateName,
														templates[
															templateName
														][ heading.name ],
														heading.type,
														heading.name
													) }
												</>
											) ) }
										</tr>
									)
								) }
						</tbody>
					</table>
				</div>
			</div>
			{ editWindow && (
				<EmailEdit
					onChange={ onTemplateChange }
					templateId={ editTemplate }
					template={ { ...templates[ editTemplate ] } }
					editWindowHandler={ editWindowHandler }
					windowRef={ ref }
				/>
			) }
			{ popupStatus && (
				<UpgradeProPopUp
					title={ __(
						'Unlock Full Email Access with',
						'wholesalex'
					) }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
			{ <Toast delay={ 3000 } position="top_right" /> }
		</div>
	);
};

export default EmailLists;
