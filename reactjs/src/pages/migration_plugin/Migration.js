import React, { useContext, useEffect, useState } from 'react';
import Toast from '../../components/Toast';
import LoadingGif from '../../components/LoadingGif';
import { ToastContexts } from '../../context/ToastsContexts';

const fields = wholesalex_migration.fields;
const Migration = () => {
	const [ activeTab, setActiveTab ] = useState(
		Object.keys( fields )?.[ 0 ]
	);
	const { dispatch } = useContext( ToastContexts );

	const [ migrationStatus, setMigrationStatus ] = useState(
		wholesalex_migration?.migration_status
	);

	const [ migrationStats, setMigrationStats ] = useState(
		wholesalex_migration?.stats
	);

	useEffect( () => {
		const hash = window.location.hash.slice( 1 );
		setActiveTab(
			hash && fields[ hash ] ? hash : Object.keys( fields )?.[ 0 ]
		);
		setAllowB2BKingMigration(
			wholesalex_migration?.allow_b2bking_migration
		);
		setAllowWSMigration(
			wholesalex_migration?.allow_wholesale_suite_migration
		);
	}, [] );

	useEffect( () => {
		const handleHashChange = () => {
			const hash = window.location.hash.slice( 1 );
			setActiveTab(
				hash && fields[ hash ] ? hash : Object.keys( fields )?.[ 0 ]
			);
		};
		window.addEventListener( 'hashchange', handleHashChange );
		return () => {
			window.removeEventListener( 'hashchange', handleHashChange );
		};
	}, [] );

	const tabClickHandler = ( tabID ) => {
		setActiveTab( tabID );
		window.location.hash = tabID;
	};

	const startB2BKingMigration = () => {
		setMigrationStatus( ( prev ) => {
			return { ...prev, b2bking_migration: 'running' };
		} );
		const attr = {
			type: 'start_b2bking_migration',
			action: 'migration',
			nonce: wholesalex_migration.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/migration',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Migration Started',
					},
				} );
				checkB2BKingMigrationStatus();
			}
		} );
	};

	const startWSMigration = () => {
		setMigrationStatus( ( prev ) => {
			return { ...prev, wholesale_suite_migration: 'running' };
		} );
		const attr = {
			type: 'start_ws_migration',
			action: 'migration',
			nonce: wholesalex_migration.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/migration',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res.status ) {
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Migration Started',
					},
				} );
				checkWSMigrationStatus();
			}
		} );
	};

	const sleep = async ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const checkB2BKingMigrationStatus = () => {
		const attr = {
			type: 'b2bking_migration_status',
			action: 'migration',
			nonce: wholesalex_migration.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/migration',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res?.is_migrating ) {
				setMigrationStatus( ( prev ) => {
					return { ...prev, b2bking_migration: 'running' };
				} );
				sleep( 5000 ).then( () => {
					checkB2BKingMigrationStatus();
				} );
			} else {
				setMigrationStatus( ( prev ) => {
					return { ...prev, b2bking_migration: false };
				} );

				setMigrationStats( ( prevData ) => {
					return {
						...prevData,
						b2bking_migration: res.migration_stats,
					};
				} );

				setAllowB2BKingMigration( false );
				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Migration Successful.',
					},
				} );
			}
		} );
	};

	const checkWSMigrationStatus = () => {
		const attr = {
			type: 'ws_migration_status',
			action: 'migration',
			nonce: wholesalex_migration.nonce,
		};
		wp.apiFetch( {
			path: '/wholesalex/v1/migration',
			method: 'POST',
			data: attr,
		} ).then( ( res ) => {
			if ( res?.is_migrating ) {
				setMigrationStatus( ( prev ) => {
					return { ...prev, wholesale_suite_migration: 'running' };
				} );

				sleep( 5000 ).then( () => {
					checkWSMigrationStatus();
				} );
			} else {
				setMigrationStats( ( prevData ) => {
					return {
						...prevData,
						wholesale_suite_migration: res.migration_stats,
					};
				} );

				setMigrationStatus( ( prev ) => {
					return { ...prev, wholesale_suite_migration: false };
				} );

				setAllowWSMigration( false );

				dispatch( {
					type: 'ADD_MESSAGE',
					payload: {
						id: Date.now().toString(),
						type: 'success',
						message: 'Migration Successful.',
					},
				} );
			}
		} );
	};

	const [ allowB2BKingMigration, setAllowB2BKingMigration ] =
		useState( true );
	const [ allowWSMigration, setAllowWSMigration ] = useState( true );

	const renderMigrationButton = () => {
		switch ( activeTab ) {
			case 'b2bking_migration':
				return (
					<button
						disabled={ ! allowB2BKingMigration }
						className="wholesalex-btn wholesalex-migrate-button wholesalex-primary-btn wholesalex-btn-lg"
						onClick={ () => startB2BKingMigration() }
					>
						{ migrationStatus?.b2bking_migration === 'running' &&
							'Migrating..' }
						{ ! migrationStatus?.b2bking_migration &&
							'Migrate Now' }
						{ migrationStatus?.b2bking_migration === 'complete' &&
							'Migrated' }
					</button>
				);

			case 'wholesale_suite_migration':
				return (
					<button
						disabled={ ! allowWSMigration }
						className="wholesalex-btn wholesalex-migrate-button wholesalex-primary-btn wholesalex-btn-lg"
						onClick={ () => startWSMigration() }
					>
						{ migrationStatus?.wholesale_suite_migration ===
							'running' && 'Migrating..' }
						{ ! migrationStatus?.wholesale_suite_migration &&
							'Migrate Now' }
						{ migrationStatus?.wholesale_suite_migration ===
							'complete' && 'Migrated' }
					</button>
				);
			default:
				break;
		}
	};

	const renderTabData = () => {
		const tabData = fields[ activeTab ];

		return (
			<div className="wholesalex_settings_tab wholesalex_migration_tab">
				<div className="wholesalex_settings__tab_header">
					<span className="wholesalex_settings__tab_heading">
						{ tabData?.label }
					</span>
				</div>

				{
					<ul className="wholesalex_settings__tab_content">
						{ migrationStatus[ activeTab ] === 'running' && (
							<LoadingGif />
						) }
						{ tabData &&
							Object.keys( tabData.attr ).map( ( field, k ) => {
								const _status =
									migrationStats?.[ activeTab ][ field ] ===
									100;

								return (
									<li
										key={ `settings_field_content_${ k }` }
										className={
											'wholesalex_settings__fields'
										}
									>
										<div className="wholesalex_migration_field">
											<span
												className={
													_status
														? 'wholesalex_migration_status dashicons dashicons-yes-alt wholesalex_migrated'
														: 'wholesalex_migration_status dashicons dashicons-yes-alt wholesalex_not_migrated'
												}
											></span>
											<div className="wholesalex_migration_content">
												<div className="wholesalex_migration_field__label">
													{
														tabData.attr[ field ]
															.label
													}
												</div>
												<div className="wholesalex_migration_desc">
													{
														tabData.attr[ field ]
															.desc
													}
												</div>
											</div>
										</div>
									</li>
								);
							} ) }
					</ul>
				}
				<div className="wholesalex_migration_tab__footer">
					{ renderMigrationButton() }
				</div>
			</div>
		);
	};

	return (
		<>
			<div className="wsx-wrapper">
				<div className="wholesalex_settings">
					<ul className="wholesalex_settings_tab_lists">
						{ Object.keys( fields ).map(
							( section, k ) =>
								fields[ section ].attr &&
								Object.keys( fields[ section ].attr ).length >
									0 && (
									<li
										key={ k }
										onClick={ () => {
											tabClickHandler( section );
										} }
										className={ `wholesalex_settings_tab_list ${
											activeTab === section
												? 'wholesalex_active_tab'
												: ''
										}` }
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												tabClickHandler( section );
											}
										} }
										role="none"
										tabIndex="0"
									>
										<span className="wholesalex_settings_tab__title">
											{ fields[ section ].label }
										</span>
									</li>
								)
						) }
					</ul>
					{ renderTabData() }
				</div>

				<Toast position={ 'top_right' } delay={ 7000 } />
			</div>
		</>
	);
};

export default Migration;
