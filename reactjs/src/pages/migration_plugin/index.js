import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../../components/Header';
import Migration from './Migration';
import { ToastContextProvider } from '../../context/ToastsContexts';
document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_tools_root' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ 'Tools' } />
					<Migration />
				</ToastContextProvider>
			</React.StrictMode>,
			document.getElementById( 'wholesalex_tools_root' )
		);
	}
} );

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_migration_tools_root' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ToastContextProvider>
					<Header title={ 'Migration Tools' } />
					<Migration />
				</ToastContextProvider>
			</React.StrictMode>,
			document.getElementById( 'wholesalex_migration_tools_root' )
		);
	}
} );
