import React, { useEffect, useRef, useState } from 'react';
import Icons from '../../utils/Icons';
import { __, sprintf } from '@wordpress/i18n';
import Button from '../../components/Button';

const DeleteModal = ( {
	title,
	status,
	setStatus,
	onDelete,
	customClass,
	smallModal = true,
} ) => {
	const [ isOpen, setIsOpen ] = useState( false );

	const myRef = useRef();

	useEffect( () => {
		setIsOpen( true );
	}, [] );

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setStatus( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	return (
		status && (
			<div
				className={ `wsx-modal-wrapper ${
					customClass ? customClass : ''
				} ${ isOpen ? 'open' : '' }` }
			>
				<div
					className={ `wsx-modal ${
						smallModal ? 'wsx-modal-sm' : ''
					}` }
					tabIndex="-1"
					aria-hidden="true"
				>
					<div ref={ myRef } className="wsx-card">
						<div className="wsx-modal-header">
							<div className="wsx-modal-title">
								{ __( 'Confirm Delete', 'wholesalex-pro' ) }
							</div>
							<span
								className="wsx-modal-close"
								onClick={ () => {
									setStatus( false );
									setIsOpen( false );
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										setStatus( false );
										setIsOpen( false );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.cross }
							</span>
						</div>

						<div className="wsx-modal-body">
							{ sprintf(
								// translators: %s Allowed File Types.
								__(
									'Do you want to delete %s? Be careful, this procedure is irreversible. Do you want to proceed?',
									'wholesalex-pro'
								),
								'##'
							).replace( '##', title ) }
						</div>

						<div className="wsx-modal-footer">
							<Button
								smallButton={ true }
								borderColor="border-primary"
								color="text-light"
								background="base1"
								customClass="wsx-font-14"
								onClick={ () => {
									setStatus( false );
								} }
								label={ __( 'Cancel', 'wholesalex-pro' ) }
							/>
							<Button
								smallButton={ true }
								borderColor="border-primary"
								background="negative"
								customClass="wsx-font-14"
								onClick={ ( e ) => {
									e.preventDefault();
									onDelete();
								} }
								label={ __( 'Delete', 'wholesalex-pro' ) }
							/>
						</div>
					</div>
				</div>
			</div>
		)
	);
};
export default DeleteModal;
