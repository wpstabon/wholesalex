import React, { useRef, useState } from 'react';
import { __ } from '@wordpress/i18n';
import styleGenerate from '../../components/Helper';
import Button from '../../components/Button';
import Alert from '../../components/Alert';
import LoadingGif from '../../components/LoadingGif';

const Account = ( { state, setState } ) => {
	const isEdit = state.curPage === 'update';
	const styles = styleGenerate( wholesalex );

	const initialAccount = {
		username: '',
		full_name: '',
		avatar_url: '',
		email: '',
		job_title: '',
		phone: '',
		permissions: [],
		created_at: '',
		status: '',
		password: '',
		credit_balance: '',
		nonce: wholesalex_subaccount.nonce,
	};

	const [ accountData, setAccountData ] = useState(
		isEdit ? state.data : initialAccount
	);
	const [ loader, setLoader ] = useState( false );

	const [ isEditable, setIsEditable ] = useState( false );
	const [ showWarning, setShowWarning ] = useState( false );
	const [ alertData, setAlertData ] = useState( null );
	const inputRef = useRef( null );

	const handleFieldClick = () => {
		if ( ! isEditable ) {
			setShowWarning( true );
		}
	};

	const handleBackClick = () => {
		setShowWarning( false );
	};

	const handleProceedClick = () => {
		setShowWarning( false );
		setIsEditable( true );
		inputRef.current.focus();
	};

	const handleFieldBlur = () => {
		setIsEditable( false );
	};

	const sendRequest = ( type = 'create_account' ) => {
		const attr = {
			type,
			action: 'subaccount',
			nonce: wholesalex_subaccount.nonce,
			data: JSON.stringify( accountData ),
		};

		setLoader( true );
		wp.apiFetch( {
			path: '/wholesalex/v1/subaccount',
			method: 'POST',
			data: attr,
		} )
			.then( ( res ) => {
				setLoader( false );
				if ( res.success ) {
					if ( res.data ) {
						const statusType = res.data.status
							? 'success'
							: 'error';
						setAlertData( {
							title:
								statusType === 'success'
									? __( 'Success', 'wholesalex-pro' )
									: __( 'Data Invalid', 'wholesalex-pro' ),
							description: res.data.message,
							onClose: handleAlertClick,
							cancelText:
								statusType === 'success'
									? __( 'OK', 'wholesalex-pro' )
									: __( 'Cancel', 'wholesalex-pro' ),
						} );
					}
					switch ( type ) {
						case 'create_account':
							if ( res.data.status ) {
								setState( { curPage: '' } );
								window.location.hash = '';
								location.reload();
							}
							break;
						case 'update':
							if ( res.data.status ) {
								setState( { curPage: '' } );
								window.location.hash = '';
								location.reload();
							}
							break;
						default:
							break;
					}
				} else {
					window.alert( __( 'Error Occured!', 'wholesalex-pro' ) );
				}
			} )
			.catch( () => {} );
	};

	const handleAlertClick = () => {
		setAlertData( null );
	};

	const createSubAccount = ( e ) => {
		e.preventDefault();
		sendRequest();
	};
	const updateSubaccount = ( e ) => {
		e.preventDefault();
		sendRequest( 'update' );
	};

	const onChangeHandler = ( e ) => {
		let permissions = [ ...accountData.permissions ];
		switch ( e.target.type ) {
			case 'text':
			case 'number':
			case 'tel':
			case 'email':
			case 'password':
				setAccountData( {
					...accountData,
					[ e.target.name ]: e.target.value,
				} );
				break;
			case 'checkbox':
				if ( e.target.checked ) {
					permissions = permissions.concat( e.target.value );
				} else {
					permissions = permissions.filter(
						( item ) => item !== e.target.value
					);
				}
				setAccountData( {
					...accountData,
					[ e.target.name ]: permissions,
				} );
				break;

			default:
				break;
		}
	};

	return (
		<div className="wsx-new-sub-account">
			<div className="wsx-sub-account-heading">
				<div
					className="wsx-font-32 wsx-font-bold wsx-color-tertiary"
					style={ { lineHeight: '1.2em' } }
				>
					{ isEdit
						? __( 'Update Subaccount', 'wholesalex-pro' )
						: __( 'Create Subaccount', 'wholesalex-pro' ) }
				</div>
				<Button
					style={ styles.primary_button }
					label={ __( 'Go Back', 'wholesalex-pro' ) }
					background="primary"
					padding="8px 20px"
					iconName="arrowLeft_24"
					onClick={ ( e ) => {
						e.preventDefault();
						setState( { curPage: '' } );
						window.location.hash = '';
					} }
				/>
			</div>

			<form
				className={ `wsx-sub-account-form ${
					loader ? 'has_loader' : ''
				}` }
				action=""
				method="post"
			>
				{ loader && <LoadingGif /> }
				<div className="wsx-font-28 wsx-font-medium wsx-mb-24 wsx-color-text-medium">
					{ __( 'Sub Account Information', 'wholesalex-pro' ) }
				</div>
				<div className=" wsx-d-flex wsx-flex-column wsx-gap-20 wsx-mb-32">
					<div className="wsx-d-flex wsx-gap-30">
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Name', 'wholesalex-pro' ) }{ ' ' }
								<span
									className="wsx-required"
									style={ { color: 'var(--color-negative)' } }
								>
									*
								</span>
							</div>
							<div className="wsx-input-container">
								<input
									type="text"
									name="full_name"
									id="full_name"
									value={ accountData.full_name }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter Account holder name here',
										'wholesalex-pro'
									) }
									className="wsx-input"
								/>
							</div>
						</div>
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Username', 'wholesalex-pro' ) }{ ' ' }
								<span
									className="wsx-required"
									style={ { color: 'var(--color-negative)' } }
								>
									*
								</span>
							</div>
							<div className="wsx-input-container">
								<input
									type="text"
									name="username"
									id="username"
									value={ accountData.username }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter the subaccount username',
										'wholesalex-pro'
									) }
									disabled={ isEdit }
									className="wsx-input"
								/>
							</div>
						</div>
					</div>
					<div className="wsx-d-flex wsx-gap-30">
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Email', 'wholesalex-pro' ) }{ ' ' }
								<span
									className="wsx-required"
									style={ { color: 'var(--color-negative)' } }
								>
									*
								</span>
							</div>
							<div className="wsx-input-container">
								<input
									type="email"
									name="email"
									id="email"
									value={ accountData.email }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter email',
										'wholesalex-pro'
									) }
									className="wsx-input"
								/>
							</div>
						</div>
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Password', 'wholesalex-pro' ) }{ ' ' }
								<span
									className="wsx-required"
									style={ { color: 'var(--color-negative)' } }
								>
									*
								</span>
							</div>
							<div className="wsx-input-container">
								<input
									type="password"
									name="password"
									id="password"
									value={ accountData.password }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter password',
										'wholesalex-pro'
									) }
									className="wsx-input"
								/>
							</div>
						</div>
					</div>
					<div className="wsx-d-flex wsx-gap-30">
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Job Title', 'wholesalex-pro' ) }
							</div>
							<div className="wsx-input-container">
								<input
									type="text"
									name="job_title"
									id="job_title"
									value={ accountData.job_title }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter Job Title',
										'wholesalex-pro'
									) }
									className="wsx-input"
								/>
							</div>
						</div>
						<div className="wsx-input-wrapper wsx-flex-1">
							<div className="wsx-input-label wsx-font-medium wsx-mb-8">
								{ __( 'Phone Number', 'wholesalex-pro' ) }
							</div>
							<div className="wsx-input-container">
								<input
									type="tel"
									name="phone"
									id="phone"
									value={ accountData.phone }
									onChange={ onChangeHandler }
									placeholder={ __(
										'Enter Phone Number',
										'wholesalex-pro'
									) }
									className="wsx-input"
								/>
							</div>
						</div>
					</div>
					{ wholesalex_subaccount?.settings?.wsx_addon_wallet ===
						'yes' && (
						<div className="wsx-d-flex wsx-gap-30">
							<div className="wsx-input-wrapper wsx-flex-1">
								<div className="wsx-input-label wsx-font-medium wsx-mb-8">
									{ __( 'Credit Balance', 'wholesalex-pro' ) }
								</div>
								<div className="wsx-input-container">
									<input
										type="number"
										min={ 0 }
										name="wallet_balance"
										value={ accountData.wallet_balance }
										readOnly={ ! isEditable }
										onClick={ handleFieldClick }
										onChange={ onChangeHandler }
										onBlur={ handleFieldBlur }
										placeholder={ __(
											'Enter Credit Balance',
											'wholesalex-pro'
										) }
										ref={ inputRef }
										className="wsx-input"
									/>
								</div>
							</div>
						</div>
					) }
				</div>
				<div className="wsx-font-28 wsx-font-medium wsx-mb-24 wsx-color-text-medium">
					{ __( 'Sub Account Permissions', 'wholesalex-pro' ) }
				</div>
				<div className="wsx-d-flex wsx-flex-column wsx-gap20 wsx-mb-32">
					<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
						<div className="wsx-checkbox-option-wrapper wsx-item-center">
							<label className="wsx-label" htmlFor="permissions">
								{ ' ' }
								<input
									type="checkbox"
									id="place_order"
									name="permissions"
									defaultChecked={ accountData.permissions.includes(
										'place_order'
									) }
									value="place_order"
									onChange={ onChangeHandler }
								/>
								<div className="wsx-checkbox-mark"></div>
							</label>
							<div className="wsx-checkbox-option wsx-mb-0">
								{ __( 'Place an Order', 'wholesalex-pro' ) }
							</div>
						</div>
					</div>
					<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
						<div className="wsx-checkbox-option-wrapper wsx-item-center">
							<label className="wsx-label" htmlFor="permissions">
								{ ' ' }
								<input
									type="checkbox"
									id="view_all_orders"
									name="permissions"
									defaultChecked={ accountData.permissions.includes(
										'view_all_orders'
									) }
									value="view_all_orders"
									onChange={ onChangeHandler }
								/>
								<div className="wsx-checkbox-mark"></div>
							</label>
							<div className="wsx-checkbox-option wsx-mb-0">
								{ __(
									'View all account orders',
									'wholesalex-pro'
								) }
							</div>
						</div>
					</div>
					<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
						<div className="wsx-checkbox-option-wrapper wsx-item-center">
							<label className="wsx-label" htmlFor="permissions">
								{ ' ' }
								<input
									type="checkbox"
									id="view_all_conversations"
									name="permissions"
									defaultChecked={ accountData.permissions.includes(
										'view_all_conversations'
									) }
									value="view_all_conversations"
									onChange={ onChangeHandler }
								/>
								<div className="wsx-checkbox-mark"></div>
							</label>
							<div className="wsx-checkbox-option wsx-mb-0">
								{ __(
									'View all account conversations',
									'wholesalex-pro'
								) }
							</div>
						</div>
					</div>
					<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
						<div className="wsx-checkbox-option-wrapper wsx-item-center">
							<label className="wsx-label" htmlFor="permissions">
								{ ' ' }
								<input
									type="checkbox"
									id="view_all_purchase_lists"
									name="permissions"
									defaultChecked={ accountData.permissions.includes(
										'view_all_purchase_lists'
									) }
									value="view_all_purchase_lists"
									onChange={ onChangeHandler }
								/>
								<div className="wsx-checkbox-mark"></div>
							</label>
							<div className="wsx-checkbox-option wsx-mb-0">
								{ __(
									'View all account purchase lists',
									'wholesalex-pro'
								) }
							</div>
						</div>
					</div>
					<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
						<div className="wsx-checkbox-option-wrapper wsx-item-center">
							<label className="wsx-label" htmlFor="permissions">
								{ ' ' }
								<input
									type="checkbox"
									id="order_approval_required"
									name="permissions"
									defaultChecked={ accountData.permissions.includes(
										'order_approval_required'
									) }
									value="order_approval_required"
									onChange={ onChangeHandler }
								/>
								<div className="wsx-checkbox-mark"></div>
							</label>
							<div className="wsx-checkbox-option wsx-mb-0">
								{ __(
									'Order Approval Required',
									'wholesalex-pro'
								) }
							</div>
						</div>
					</div>

					{ wholesalex_subaccount?.settings?.wsx_addon_wallet ===
						'yes' && (
						<>
							<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
								<div className="wsx-checkbox-option-wrapper wsx-item-center">
									<label
										className="wsx-label"
										htmlFor="permissions"
									>
										{ ' ' }
										<input
											type="checkbox"
											id="wallet_access"
											name="permissions"
											defaultChecked={ accountData.permissions.includes(
												'wallet_access'
											) }
											value="wallet_access"
											onChange={ onChangeHandler }
										/>
										<div className="wsx-checkbox-mark"></div>
									</label>
									<div className="wsx-checkbox-option wsx-mb-0">
										{ __(
											'Can Access Wallet',
											'wholesalex-pro'
										) }
									</div>
								</div>
							</div>
							<div className="wsx-checkbox-wrapper wsx-mb-12 wsx-font-16">
								<div className="wsx-checkbox-option-wrapper wsx-item-center">
									<label
										className="wsx-label"
										htmlFor="permissions"
									>
										{ ' ' }
										<input
											type="checkbox"
											id="wallet_topup_access"
											name="permissions"
											defaultChecked={ accountData.permissions.includes(
												'wallet_topup_access'
											) }
											value="wallet_topup_access"
											onChange={ onChangeHandler }
										/>
										<div className="wsx-checkbox-mark"></div>
									</label>
									<div className="wsx-checkbox-option wsx-mb-0">
										{ __(
											'Can TopUp Wallet',
											'wholesalex-pro'
										) }
									</div>
								</div>
							</div>
						</>
					) }
				</div>
				<Button
					style={ styles.primary_button }
					label={
						isEdit
							? __( 'Update Subaccount', 'wholesalex-pro' )
							: __( 'Create Subaccount', 'wholesalex-pro' )
					}
					background="primary"
					padding="8px 20px"
					iconName={ ! isEdit ? 'profile' : 'profileUpdate' }
					onClick={ isEdit ? updateSubaccount : createSubAccount }
				/>
			</form>
			{ showWarning && (
				<Alert
					title={ __( 'Please be Careful!', 'wholesalex-pro' ) }
					description={ __(
						'Be careful when setting the credit balance.',
						'wholesalex-pro'
					) }
					onClose={ handleBackClick }
					onConfirm={ handleProceedClick }
					confirmText={ __( 'Confirm', 'wholesalex-pro' ) }
				/>
			) }
			{ alertData && (
				<Alert
					title={ alertData.title }
					description={ alertData.description }
					onClose={ alertData.onClose }
					cancelText={ alertData.cancelText }
				/>
			) }
		</div>
	);
};
export default Account;
