import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import Account from './Account';
import SubAccounts from './SubAccounts';
import '../../assets/scss/SubAccount.scss';

const ManageSubaccounts = () => {
	const [ state, setState ] = useState( { curPage: '' } );
	const hash = window.location.hash;
	return (
		<>
			{ hash === '' && (
				<SubAccounts state={ state } setState={ setState } />
			) }
			{ hash === '#create' && (
				<Account state={ state } setState={ setState } />
			) }
			{ hash === '#update' && (
				<Account state={ state } setState={ setState } />
			) }
		</>
	);
};

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( 'wholesalex_subaccounts' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<ManageSubaccounts />
			</React.StrictMode>,
			document.getElementById( 'wholesalex_subaccounts' )
		);
	}
} );
