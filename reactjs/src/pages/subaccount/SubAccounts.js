import React, { useEffect, useRef, useState } from 'react';
import DeleteModal from './DeleteModal';
import styleGenerate from '../../components/Helper';
import { __ } from '@wordpress/i18n';
import Slider from '../../components/Slider';
import Dropdown from '../../components/Dropdown';

import Button from '../../components/Button';
import Alert from '../../components/Alert';

import Tooltip from '../../components/Tooltip';
import Icons from '../../utils/Icons';
import LoadingGif from '../../components/LoadingGif';

const SubAccounts = ( { setState } ) => {
	let allColumns = {
		account_name: {
			name: 'account_name',
			title: __( 'Account Name', 'wholesalex-pro' ),
			status: true,
			class: 'account-info',
		},
		job_title: {
			name: 'job_title',
			title: __( 'Job Title', 'wholesalex-pro' ),
			status: true,
			class: 'job-title',
		},
		phone: {
			name: 'phone',
			title: __( 'Phone', 'wholesalex-pro' ),
			status: true,
			class: 'phone',
		},
		permission: {
			name: 'permission',
			title: __( 'Permissions', 'wholesalex-pro' ),
			status: true,
			class: 'permissions',
		},
		actions: {
			name: 'actions',
			title: __( 'Actions', 'wholesalex-pro' ),
			status: true,
			class: 'actions-column justify-self-end',
		},
	};
	if ( wholesalex_subaccount?.settings?.wsx_addon_wallet === true ) {
		allColumns = {
			account_name: {
				name: 'account_name',
				title: __( 'Account Name', 'wholesalex-pro' ),
				status: true,
				class: 'account-info',
			},
			job_title: {
				name: 'job_title',
				title: __( 'Job Title', 'wholesalex-pro' ),
				status: true,
				class: 'job-title',
			},
			phone: {
				name: 'phone',
				title: __( 'Phone', 'wholesalex-pro' ),
				status: true,
				class: 'phone',
			},
			permission: {
				name: 'permission',
				title: __( 'Permissions', 'wholesalex-pro' ),
				status: true,
				class: 'permissions',
			},
			credit_balance: {
				name: 'credit_balance',
				title: __( 'Wallet Balance', 'wholesalex-pro' ),
				status: true,
				class: 'credit_balance',
			},
			actions: {
				name: 'actions',
				title: __( 'Actions', 'wholesalex-pro' ),
				status: true,
				class: 'actions-column justify-self-end',
			},
		};
	}

	const permissions = {
		place_order: __( 'Place an Order', 'wholesalex-pro' ),
		view_all_orders: __( 'View all account orders', 'wholesalex-pro' ),
		wallet_access: __( 'Can Access Wallet', 'wholesalex-pro' ),
		order_approval_required: __(
			'Order Approval Required',
			'wholesalex-pro'
		),
		view_all_conversations: __(
			'View all account conversations',
			'wholesalex-pro'
		),
		view_all_purchase_lists: __(
			'View all account purchase lists',
			'wholesalex-pro'
		),
		wallet_topup_access: __( 'Can TopUp Wallet', 'wholesalex-pro' ),
	};
	const styles = styleGenerate( wholesalex );
	const [ subAccounts, setSubAccounts ] = useState( [] );
	const [ loader, setLoader ] = useState( false );
	const [ deleteAccount, setDeleteAccount ] = useState( false );
	const [ currentPage, setCurrentPage ] = useState( 1 );
	const [ columns, setColumns ] = useState( allColumns );
	const [ alert, setAlert ] = useState( false );

	const subscribedRef = useRef();

	const itemPerPage = 50;

	const renderSubaccountLists = () => {
		const _subaccounts = [ ...subAccounts ];
		const getPaginatedData = () => {
			const startIndex = currentPage * itemPerPage - itemPerPage;
			const endIndex = startIndex + itemPerPage;
			return _subaccounts.slice( startIndex, endIndex );
		};

		return getPaginatedData().map( ( subaccount ) => {
			return rowData( subaccount );
		} );
	};

	const columnVisibleStatus = ( columnName ) => {
		return columns[ columnName ]?.status;
	};

	const rowData = ( subaccount ) => {
		return (
			<tr key={ subaccount.id }>
				{ columnVisibleStatus( 'account_name' ) && (
					<td>
						<div className="account-info flex-row wsx-d-flex wsx-item-center wsx-gap-8 wsx-account-info">
							<div className="account-avatar">
								<img
									className="account-avatar"
									src={ subaccount.avatar_url }
									alt="account"
								></img>
							</div>
							<div className="account-meta">
								<div className="account-name wsx-color-primary wsx-font-bold">
									{ subaccount.full_name }
								</div>
								<div className="account-email">
									{ subaccount.email }
								</div>
							</div>
						</div>
					</td>
				) }
				{ columnVisibleStatus( 'job_title' ) && (
					<td className="job-title">{ subaccount.job_title }</td>
				) }
				{ columnVisibleStatus( 'phone' ) && (
					<td className="phone">{ subaccount.phone }</td>
				) }
				{ columnVisibleStatus( 'permission' ) && (
					<td>
						<div className="permissions">
							<ul style={ { margin: '0' } }>
								{ getPermissions( subaccount.permissions ).map(
									( permission, pidx ) => {
										return (
											<li
												key={ `permission_${ pidx }` }
												className={
													permission.status
														? 'permission'
														: 'no_permission'
												}
											>
												{ ' ' }
												{ permission.title }
											</li>
										);
									}
								) }
							</ul>
						</div>
					</td>
				) }
				{ wholesalex_subaccount?.settings?.wsx_addon_wallet === 'yes' &&
					columnVisibleStatus( 'credit_balance' ) && (
						<td
							className="wallet_balance"
							dangerouslySetInnerHTML={ {
								__html: subaccount.wallet_balance,
							} }
						></td>
					) }
				{ columnVisibleStatus( 'actions' ) && (
					<td>
						<div className="wsx-rule-actions wsx-btn-group wsx-gap-8 wsx-justify-end">
							<Tooltip
								content={ __( 'Edit this user', 'wholesalex-pro' ) }
								direction="top"
							>
								<span
									className="wsx-btn-action"
									onClick={ ( e ) => {
										e.preventDefault();
										setState( {
											curPage: 'update',
											id: subaccount.id,
											data: subaccount,
										} );
										window.location.hash = 'update';
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											e.preventDefault();
											setState( {
												curPage: 'update',
												id: subaccount.id,
												data: subaccount,
											} );
											window.location.hash = 'update';
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.edit }
								</span>
							</Tooltip>
							<Tooltip
								content={ __(
									'Delete this user',
									'wholesalex-pro'
								) }
								direction="top"
							>
								<span
									className="wsx-btn-action"
									onClick={ ( e ) => {
										e.preventDefault();
										setDeleteAccount( {
											full_name: subaccount.full_name,
											id: subaccount.id,
										} );
									} }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											e.preventDefault();
											setDeleteAccount( {
												full_name: subaccount.full_name,
												id: subaccount.id,
											} );
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.delete }
								</span>
							</Tooltip>
						</div>
					</td>
				) }
			</tr>
		);
	};
	const paginations = () => {
		const length = subAccounts.length;
		const totalPageCount = parseInt(
			( length + itemPerPage - 1 ) / itemPerPage
		);

		const getPages = () => {
			let pages = [];
			if ( totalPageCount <= 5 ) {
				for ( let i = 1; i <= totalPageCount; i++ ) {
					pages.push( i );
				}
			} else if ( currentPage <= 3 ) {
				pages = [ 1, 2, 3, 4, '...', totalPageCount ];
			} else if ( currentPage >= totalPageCount - 2 ) {
				pages = [
					1,
					'...',
					totalPageCount - 3,
					totalPageCount - 2,
					totalPageCount - 1,
					totalPageCount,
				];
			} else {
				pages = [
					1,
					'...',
					currentPage - 1,
					currentPage,
					currentPage + 1,
					'...',
					totalPageCount,
				];
			}
			return pages;
		};

		return (
			<div className="wsx-pagination-wrapper wsx-justify-end">
				<div className="wsx-btn-group">
					{ totalPageCount > 1 && (
						<Button
							onClick={ () => {
								currentPage > 1
									? setCurrentPage( ( page ) => page - 1 )
									: '';
							} }
							iconName="angleLeft_24"
							borderColor="tertiary"
							iconColor="tertiary"
							disable={ currentPage < 2 }
							key={ `pagination_no_prev}` }
						/>
					) }

					{ totalPageCount > 1 &&
						getPages().map( ( page, i ) => (
							<Button
								key={ `pagination_no_${ i }` }
								borderColor="border-secondary"
								background="base1"
								label={ page }
								onClick={ () => {
									setCurrentPage( i + 1 );
								} }
								customClass={
									currentPage === page ? 'active' : ''
								}
								disable={ page === '...' }
							/>
						) ) }

					{ totalPageCount > 1 && (
						<Button
							onClick={ () => {
								currentPage < totalPageCount
									? setCurrentPage( ( page ) => page + 1 )
									: '';
							} }
							iconName="angleRight_24"
							borderColor="tertiary"
							iconColor="tertiary"
							disable={ currentPage === totalPageCount }
							key={ `pagination_no_next` }
						/>
					) }
				</div>
			</div>
		);
	};

	const getPermissions = ( subaccountPermissions ) => {
		const preparePermissionData = [];
		for ( const [ key, value ] of Object.entries( permissions ) ) {
			preparePermissionData.push( {
				status: subaccountPermissions.includes( key ),
				title: value,
			} );
		}
		return preparePermissionData;
	};

	const sendRequest = ( type = 'get_accounts', id = '' ) => {
		const attr = {
			type,
			action: 'subaccount',
			nonce: wholesalex_subaccount.nonce,
			id,
		};
		setLoader( true );

		wp.apiFetch( {
			path: '/wholesalex/v1/subaccount',
			method: 'POST',
			data: attr,
		} )
			.then( ( res ) => {
				if ( subscribedRef.current ) {
					if ( res.success ) {
						switch ( type ) {
							case 'create_account':
								setIsAccountCreated( true );
								break;
							case 'get_accounts':
								setSubAccounts( res.data.accounts );
								break;
							case 'delete_account':
								{
									let _subaccounts = [ ...subAccounts ];
									_subaccounts = _subaccounts.filter(
										( subaccount ) => {
											return subaccount.id !== id;
										}
									);
									setSubAccounts( _subaccounts );
									setAlert( true );
								}
								break;
							default:
								break;
						}
						setLoader( false );
					}
				}
			} )
			.catch( () => {} );
	};

	const handleAlertClose = () => {
		setAlert( false );
	};

	useEffect( () => {
		subscribedRef.current = true;
		// sendRequest();
		setSubAccounts( wholesalex_subaccount.accounts );

		return () => {
			subscribedRef.current = false;
		};
	}, [] );

	const columnsSelectionContent = () => {
		return Object.keys( columns ).map( ( column, idx ) => {
			return (
				<div
					key={ idx }
					className="wsx-dropdown-actions-list"
					onClick={ ( e ) => e.stopPropagation() }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							e.preventDefault();
							setState( { curPage: 'create' } );
							window.location.hash = 'create';
						}
					} }
					role="button"
					tabIndex="0"
				>
					<Slider
						label={ columns[ column ].title }
						name={ `whx_users_column_${ columns[ column ].name }` }
						key={ `whx_users_column_${ columns[ column ].name }` }
						value={ columns[ column ].status }
						isLabelSide={ true }
						className="wsx-slider-sm"
						onChange={ () => {
							const _columns = { ...columns };
							_columns[ column ].status =
								! _columns[ column ].status;
							setColumns( _columns );
						} }
					/>
				</div>
			);
		} );
	};

	return (
		<div className="wsx-sub-accounts">
			<div className="wsx-sub-account-heading wsx-mb-26">
				<div className="wsx-title" style={ styles.text }>
					{ __( 'Subaccounts', 'wholesalex-pro' ) }
				</div>
				<div className="wsx-sub-account-controller">
					<div
						onClick={ ( e ) => {
							e.preventDefault();
							setState( { curPage: 'create' } );
							window.location.hash = 'create';
						} }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								e.preventDefault();
								setState( { curPage: 'create' } );
								window.location.hash = 'create';
							}
						} }
						role="button"
						tabIndex="0"
						className="wsx-btn wsx-bg-primary wsx-d-flex wsx-gap-6 wsx-item-center wsx-justify-center wsx-font-regular wsx-font-16"
						style={ styles.primary_button }
					>
						<div className="wsx-icon">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								fill="none"
							>
								<path
									stroke="currentColor"
									strokeLinecap="round"
									strokeLinejoin="round"
									strokeWidth="1.5"
									d="M10 4.167v11.666M4.167 10h11.666"
								/>
							</svg>
						</div>
						<div>
							{ __( 'Create Subaccounts', 'wholesalex-pro' ) }
						</div>
					</div>
					<div className="wsx-search-wrapper">
						<input
							className="wsx-input wsx-m-0"
							id="wholesalex_subaccount_search"
							name={ 'wholesalex_subaccount_search' }
							type="text"
							placeholder={ __(
								'Search account name…',
								'wholesalex-pro'
							) }
							onChange={ ( e ) => {
								const key = e.target.value;
								const searchResult =
									wholesalex_subaccount.accounts.filter(
										( option ) => {
											const {
												username,
												full_name: fullName,
												email,
											} = option;
											return (
												username
													.toLowerCase()
													.includes(
														key.toLowerCase()
													) ||
												fullName
													.toLowerCase()
													.includes(
														key.toLowerCase()
													) ||
												email
													.toLowerCase()
													.includes(
														key.toLowerCase()
													)
											);
										}
									);
								setSubAccounts( searchResult );
							} }
						/>
						<div className="wsx-icon wsx-icon-right">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								fill="none"
							>
								<path
									stroke="currentColor"
									strokeLinecap="round"
									strokeLinejoin="round"
									strokeWidth="1.5"
									d="M9.167 15.833a6.667 6.667 0 1 0 0-13.333 6.667 6.667 0 0 0 0 13.333ZM17.5 17.5l-3.625-3.625"
								></path>
							</svg>
						</div>
					</div>
					<Dropdown
						className="wsx-relative wsx-bg-base2 wsx-border-default wsx-bc-primary wsx-p-10 wsx-mb-0 wsx-btn"
						contentClass="wsx-right wsx-down-4"
						iconName="menu"
						iconRotation="half"
						renderContent={ columnsSelectionContent }
					/>
				</div>
			</div>
			{ subAccounts.length > 0 && (
				<div className="wsx-account-table-container wsx-font-14">
					<table className="wsx-table">
						<thead>
							<tr>
								{ columns &&
									Object.keys( columns ).map( ( column ) => {
										return (
											columns[ column ].status && (
												<th
													key={
														columns[ column ].name
													}
													className={
														columns[ column ].class
													}
												>
													{ columns[ column ].title }
												</th>
											)
										);
									} ) }
							</tr>
						</thead>
						{
							<tbody
								className={ `${
									loader ? 'waiting_cursor' : ''
								}` }
							>
								{ renderSubaccountLists() }
							</tbody>
						}
					</table>
					{ paginations() }
				</div>
			) }
			{ subAccounts.length === 0 && (
				<div className="container flex-row">
					<div className="flex-column">
						<div className="message">
							{ __(
								'No subaccounts were found. To create one,',
								'wholesalex-pro'
							) }
							<span
								className="create_account_link"
								onClick={ ( e ) => {
									e.preventDefault();
									setState( { curPage: 'create' } );
									window.location.hash = 'create';
								} }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										e.preventDefault();
										setState( { curPage: 'create' } );
										window.location.hash = 'create';
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ __( 'Click Here', 'wholesalex-pro' ) }
							</span>
						</div>
					</div>
				</div>
			) }
			{ loader && <LoadingGif /> }
			{ deleteAccount && deleteAccount != {} && (
				<DeleteModal
					title={ deleteAccount.full_name }
					status={ deleteAccount }
					setStatus={ setDeleteAccount }
					onDelete={ () => {
						setDeleteAccount( false );
						sendRequest( 'delete_account', deleteAccount.id );
					} }
				/>
			) }
			{ alert && (
				<Alert
					title="Delete Completed"
					description="Account delete action is successful"
					onClose={ handleAlertClose }
					cancelText={ __( 'OK', 'wholesalex-pro' ) }
				/>
			) }
		</div>
	);
};

export default SubAccounts;
