import { __ } from '@wordpress/i18n';
import React, { useState, useEffect } from 'react';
import MultiSelect from '../../components/MultiSelect';
import Tier from '../../components/Tier';
import Switch from '../../components/Switch';
import Input from '../../components/Input';
import Icons from '../../utils/Icons';
import Slider from '../../components/Slider';
import Button from '../../components/Button';
import Select from '../../components/Select';
import LoadingGif from '../../components/LoadingGif';
import UpgradeProPopUp from '../../components/UpgradeProPopUp';

const Category = () => {
	const initialTier = {
		_id: Date.now().toString(),
		_discount_type: '',
		_discount_amount: '',
		_min_quantity: '',
		src: 'category',
	};

	const [ appState, setAppState ] = useState( {
		loading: true,
		currentWindow: 'new',
		index: -1,
		loadingOnSave: false,
	} );
	const [ fields, setFields ] = useState( {} );

	const [ tierStatus, setTierStatus ] = useState( {
		_settings_category_visibility: true,
	} );

	const [ visibilityValue, setVisibilityValue ] = useState( {} );

	const [ tier, setTier ] = useState( {} );

	const _isProActivate = wholesalex.is_pro_activated;
	const [ popUpStatus, setPopUpStatus ] = useState( false );

	useEffect( () => {
		const selector = document.querySelector( 'input[name="tag_ID"]' );
		const termID = selector ? selector.value : '';
		setFields( wholesalex_category.fields );
		const roles = Object.keys( wholesalex_category.fields._b2b_tiers.attr );
		const initialRoleData = {};
		roles.map( ( role ) => {
			initialRoleData[ role ] = {
				_base_price: '',
				_sale_price: '',
				tiers: [ { ...initialTier } ],
			};
			return null;
		} );
		if ( ! wholesalex_category.discounts[ termID ] ) {
			wholesalex_category.discounts[ termID ] = {};
		}
		const _temp = {
			...initialRoleData,
			...wholesalex_category.discounts[ termID ],
		};
		setTier( _temp );

		if ( wholesalex_category.visibility_settings[ termID ] ) {
			setVisibilityValue(
				wholesalex_category.visibility_settings[ termID ]
			);
		}
		setAppState( {
			...appState,
			loading: false,
		} );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	const buttonData = ( fieldData, tierName ) => {
		return (
			<Button
				label={ fieldData.label }
				background="tertiary"
				smallButton={ true }
				onClick={ ( e ) => {
					e.preventDefault();
					const copy = { ...tier };
					copy[ tierName ].tiers.push( initialTier );
					setTier( copy );
				} }
			/>
		);
	};
	const selectData = ( fieldData, field ) => {
		const defValue = visibilityValue[ field ] || fieldData.default || '';

		const getOptionsArray = ( options ) => {
			return Object.keys( options ).map( ( option ) => ( {
				value: option,
				label: options[ option ],
			} ) );
		};

		return (
			<Select
				label={ fieldData.label }
				options={ getOptionsArray( fieldData.options ) }
				value={ defValue }
				onChange={ ( e ) => {
					setVisibilityValue( {
						...visibilityValue,
						[ field ]: e.target.value,
					} );
				} }
				help={ fieldData.help }
				inputBackground="base1"
			/>
		);
	};

	const switchData = ( fieldData, field ) => {
		let defValue;

		if ( visibilityValue[ field ] ) {
			if ( visibilityValue[ field ] === 'yes' ) {
				defValue = true;
			} else {
				defValue = false;
			}
		} else {
			defValue = fieldData.default;
		}
		return (
			<Switch
				className={ 'wholesalex_category_field' }
				desc={ fieldData.label }
				name={ field }
				value={ defValue }
				onChange={ ( e ) => {
					const _final = e.target.checked ? 'yes' : 'no';
					setVisibilityValue( {
						...visibilityValue,
						[ field ]: _final,
					} );
				} }
				defaultValue={ fieldData.default }
				help={ fieldData?.help }
			/>
		);
	};

	const sliderData = ( fieldData, field ) => {
		let defValue;

		if ( visibilityValue[ field ] ) {
			if ( visibilityValue[ field ] === 'yes' ) {
				defValue = true;
			} else {
				defValue = false;
			}
		} else {
			defValue = fieldData.default;
		}
		return (
			<Slider
				className="wsx-slider-sm"
				label={ fieldData.label }
				isLabelSide={ true }
				labelClass="wsx-font-regular"
				name={ field }
				value={ defValue }
				onChange={ ( e ) => {
					const _final = e.target.checked ? 'yes' : 'no';
					setVisibilityValue( {
						...visibilityValue,
						[ field ]: _final,
					} );
				} }
			/>
		);
	};

	const multiselectData = ( fieldData, field ) => {
		const defValue = visibilityValue[ field ]
			? visibilityValue[ field ]
			: fieldData.default;
		return (
			<MultiSelect
				name={ field }
				value={ defValue }
				options={ fieldData.options }
				placeholder={ fieldData.placeholder }
				onMultiSelectChangeHandler={ ( fieldName, selectedValues ) => {
					const copy = { ...visibilityValue };
					copy[ fieldName ] = [ ...selectedValues ];
					setVisibilityValue( { ...visibilityValue, ...copy } );
				} }
			/>
		);
	};
	const visibilitySectionData = ( fieldData, section ) => {
		return (
			<div
				key={ section }
				className="wsx-category-edit-settings wsx-accordion-wrapper"
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setTierStatus( {
							...tierStatus,
							[ section ]: ! tierStatus[ section ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setTierStatus( {
								...tierStatus,
								[ section ]: ! tierStatus[ section ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx_role_header_${ section }` }
					>
						{ fieldData.label }
					</div>
					<span
						className={ `wsx-icon ${
							tierStatus[ section ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ tierStatus[ section ] && (
					<div className="wsx-accordion-body wsx-p-16">
						<div className="wsx-visibility-slider-fields">
							{ tierStatus[ section ] &&
								Object.keys( fieldData.attr ).map(
									( field ) => {
										switch (
											fieldData.attr[ field ].type
										) {
											case 'switch':
												return switchData(
													fieldData.attr[ field ],
													field
												);
											case 'slider':
												return sliderData(
													fieldData.attr[ field ],
													field
												);
											default:
												return null;
										}
									}
								) }
						</div>

						<div
							key={ `wholesalex_${ section }_group` }
							className="wsx-visibility-select-options"
						>
							{ tierStatus[ section ] &&
								Object.keys( fieldData.attr ).map(
									( field ) => {
										switch (
											fieldData.attr[ field ].type
										) {
											case 'select':
												return selectData(
													fieldData.attr[ field ],
													field
												);
											case 'multiselect':
												if (
													visibilityValue._hide_for_b2b_role_n_user ===
														'b2b_specific' &&
													field === '_hide_for_roles'
												) {
													return multiselectData(
														fieldData.attr[ field ],
														field
													);
												}
												if (
													visibilityValue._hide_for_b2b_role_n_user ===
														'user_specific' &&
													field === '_hide_for_users'
												) {
													return multiselectData(
														fieldData.attr[ field ],
														field
													);
												}
												break;
											default:
												return null;
										}
									}
								) }
						</div>
					</div>
				) }
			</div>
		);
	};

	const UpgradeButton = ( fieldData ) => {
		return (
			<Button
				label={ fieldData.label }
				onClick={ ( e ) => {
					e.preventDefault();
					setPopUpStatus( true );
				} }
				background="secondary"
				iconName="angleRight_24"
				iconPosition="after"
				iconAnimation="icon-left"
				smallButton={ true }
			/>
		);
	};

	const tierData = ( fieldData, tierName, lockStatus ) => {
		const defaultTier = fieldData._tiers.data;
		return (
			<div key={ tierName } className="wsx-settings-tiers-wrapper">
				<div className="wsx-settings-tiers-container">
					{ tier &&
						tier[ tierName ] &&
						tier[ tierName ].tiers.map( ( t, index ) => (
							<Tier
								key={ `wholesalex_${ tierName }_tier_${ index }` }
								fields={ defaultTier }
								tier={ tier }
								setTier={ setTier }
								index={ index }
								tierName={ tierName }
								deleteSpaceLeft="auto"
							/>
						) ) }
				</div>
				{ ! lockStatus &&
					fieldData._tiers.add.type === 'button' &&
					buttonData( fieldData._tiers.add, tierName ) }
				{ lockStatus &&
					fieldData._tiers.upgrade_pro.type === 'button' &&
					UpgradeButton( fieldData._tiers.upgrade_pro, tierName ) }
			</div>
		);
	};

	const inputData = ( fieldData, fieldName, tierName ) => {
		const flag = tier[ tierName ] && tier[ tierName ][ fieldName ];
		const defValue = flag
			? tier[ tierName ][ fieldName ]
			: fieldData.default;
		return (
			<Input
				className="wholesalex_category_field"
				label={ fieldData.label }
				type={ fieldData.type }
				name={ fieldName }
				value={ defValue }
				onChange={ ( e ) => {
					const parent = { ...tier };
					const copy = parent[ tierName ];
					copy[ fieldName ] = e.target.value;
					parent[ tierName ] = copy;
					setTier( { ...tier, ...parent } );
				} }
			/>
		);
	};

	const pricesData = ( fieldData, tierName ) => {
		return (
			<div key={ `wholesalex_${ tierName }_separate` }>
				<div
					key={ `wholesalex_prices_${ tierName }` }
					className="wholesalex_role_based_prices_section"
				>
					{ Object.keys( fieldData.attr ).map(
						( field ) =>
							fieldData.attr[ field ].type === 'number' &&
							inputData(
								fieldData.attr[ field ],
								field,
								tierName
							)
					) }
				</div>
			</div>
		);
	};

	const tiersData = ( fieldsData, section ) => {
		let _limit = 99999999999;
		const isPro = fieldsData.is_pro;
		if ( isPro ) {
			_limit = fieldsData.pro_data?.value;
		}
		if ( ! _limit ) {
			_limit = 99999999999;
		}

		const isLock =
			tier[ section ].tiers.length >= _limit && ! _isProActivate;
		return (
			<section
				key={ `wholesalex_${ section }` }
				className={
					section +
					' wsx-accordion-wrapper wsx-category-tiers-wrapper'
				}
			>
				<div
					className="wsx-accordion-header"
					onClick={ () => {
						setTierStatus( {
							...tierStatus,
							[ section ]: ! tierStatus[ section ],
						} );
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setTierStatus( {
								...tierStatus,
								[ section ]: ! tierStatus[ section ],
							} );
						}
					} }
					role="button"
					tabIndex="0"
				>
					<div
						className="wsx-accordion-title"
						key={ `whx_role_header` }
					>
						{ fieldsData.label }
					</div>
					<span
						className={ `wsx-icon ${
							tierStatus[ section ] ? 'active' : ''
						}` }
					>
						{ Icons.angleDown_24 }
					</span>
				</div>
				{ tierStatus[ section ] && (
					<div className="wsx-accordion-body">
						{ tierStatus[ section ] &&
							fieldsData.attr &&
							Object.keys( fieldsData.attr ).map(
								( fieldData ) => {
									switch (
										fieldsData.attr[ fieldData ].type
									) {
										case 'tier':
											return tierData(
												fieldsData.attr[ fieldData ],
												section,
												isLock
											);
										case 'prices':
											return pricesData(
												fieldsData.attr[ fieldData ],
												section
											);
										default:
											return null;
									}
								}
							) }
					</div>
				) }
			</section>
		);
	};

	return (
		<div className="wsx-category-wrapper">
			{ appState.loading && <LoadingGif /> }
			{ ! appState.loading && (
				<>
					{ Object.keys( fields ).map(
						( section ) =>
							Object.keys( fields[ section ].attr ).length >
								0 && (
								<div
									className="wsx-category-section"
									key={ section }
								>
									<div className="wsx-title wsx-font-18 wsx-mb-16">
										{ fields[ section ].label }
									</div>
									{ Object.keys( fields[ section ].attr ).map(
										( field ) => {
											switch (
												fields[ section ].attr[ field ]
													.type
											) {
												case 'visibility_section':
													return visibilitySectionData(
														fields[ section ].attr[
															field
														],
														field
													);
												case 'tiers':
													return tiersData(
														fields[ section ].attr[
															field
														],
														field
													);
												default:
													return null;
											}
										}
									) }
								</div>
							)
					) }
				</>
			) }
			<input
				type="hidden"
				value={ JSON.stringify( tier ) }
				name="wholesalex_category_tiers"
			/>
			<input
				type="hidden"
				value={ JSON.stringify( visibilityValue ) }
				name="wholesalex_category_visibility_settings"
			/>
			{ popUpStatus && (
				<UpgradeProPopUp
					title={ __(
						'Unlock all of these Features with',
						'wholesalex'
					) }
					onClose={ () => setPopUpStatus( false ) }
				/>
			) }
		</div>
	);
};

export default Category;
