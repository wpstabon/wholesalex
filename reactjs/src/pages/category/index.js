import React from 'react';
import ReactDOM from 'react-dom';
import Category from './Category';

document.addEventListener( 'DOMContentLoaded', function () {
	if (
		document.body.contains(
			document.getElementById( '_wholesalex_edit_category' )
		)
	) {
		ReactDOM.render(
			<React.StrictMode>
				<Category />
			</React.StrictMode>,
			document.getElementById( '_wholesalex_edit_category' )
		);
	}
} );
