import React, { createContext, useReducer } from 'react';

export const RegistrationFormContext = createContext();

export const RegistrationFormContextProvider = ( props ) => {
	const isWooUsername = wholesalex_overview.whx_form_builder_is_woo_username;
	const getFormHeader = (
		isShowFormTitle,
		isHideDescription,
		title,
		desc,
		styles
	) => {
		return {
			isShowFormTitle,
			isHideDescription,
			title,
			description: desc,
			styles,
		};
	};
	const createHeadingStyle = ( size, weight, transform, padding, color ) => {
		return {
			size,
			weight,
			transform,
			padding,
			color,
		};
	};
	const setHeaderStyles = ( titleColor, descColor ) => ( {
		title: createHeadingStyle( 24, 500, '', '', titleColor ),
		description: createHeadingStyle( 14, 400, '', '', descColor ),
	} );

	const initialState = {
		registrationFormHeader: getFormHeader(
			true,
			false,
			`Sign Up for New Account`,
			`Let's Get started with WholesaleX`,
			setHeaderStyles( '343A46', '#343A46' )
		),
		loginFormHeader: getFormHeader(
			true,
			false,
			`Log in with wholesalex`,
			`Let's Get started with WholesaleX`,
			setHeaderStyles( '343A46', '#343A46' )
		),
		settings: {
			inputVariation: 'variation_1',
			isShowLoginForm: false,
		},
		registrationFields: [
			...( isWooUsername === 'no'
				? [
						{
							id: 'regi_3',
							type: 'row',
							columns: [
								{
									status: true,
									type: 'text',
									label: 'Username',
									name: 'user_login',
									isLabelHide: false,
									placeholder: '',
									columnPosition: 'left',
									parent: 'regi_3',
									required: true,
									conditions: {
										status: 'show',
										relation: 'all',
										tiers: [
											{
												_id: Date.now().toString(),
												condition: '',
												field: '',
												value: '',
												src: 'registration_form',
											},
										],
									},
								},
							],
							isMultiColumn: false,
						},
				  ]
				: [] ),
			{
				id: 'regi_1',
				type: 'row',
				columns: [
					{
						status: true,
						type: 'email',
						label: 'Email',
						name: 'user_email',
						isLabelHide: false,
						placeholder: '',
						columnPosition: 'left',
						parent: 'regi_1',
						required: true,
						conditions: {
							status: 'show',
							relation: 'all',
							tiers: [
								{
									_id: Date.now().toString(),
									condition: '',
									field: '',
									value: '',
									src: 'registration_form',
								},
							],
						},
					},
				],
				isMultiColumn: false,
			},
			{
				id: 'regi_2',
				type: 'row',
				columns: [
					{
						status: true,
						type: 'password',
						label: 'Password',
						name: 'user_pass',
						isLabelHide: false,
						placeholder: '',
						columnPosition: 'left',
						parent: 'regi_2',
						required: true,
						conditions: {
							status: 'show',
							relation: 'all',
							tiers: [
								{
									_id: Date.now().toString(),
									condition: '',
									field: '',
									value: '',
									src: 'registration_form',
								},
							],
						},
					},
				],
				isMultiColumn: false,
			},
		],
		loginFields: [
			{
				id: 'login_row_1',
				type: 'row',
				columns: [
					{
						status: true,
						type: 'text',
						label: 'Username or email address',
						name: 'username',
						isLabelHide: false,
						placeholder: '',
						columnPosition: 'left',
						parent: 'login_row_1',
						required: true,
					},
				],
				isMultiColumn: false,
			},
			{
				id: 'login_row_2',
				type: 'row',
				columns: [
					{
						status: true,
						type: 'password',
						label: 'Password',
						name: 'password',
						isLabelHide: false,
						placeholder: '',
						columnPosition: 'left',
						parent: 'login_row_2',
						required: true,
					},
				],
				isMultiColumn: false,
			},
			{
				id: 'login_row_3',
				type: 'row',
				columns: [
					{
						type: 'checkbox',
						label: '',
						name: 'rememberme',
						isLabelHide: true,
						columnPosition: 'left',
						option: [
							{
								name: 'Remember me',
								value: 'rememberme',
							},
						],
						parent: 'row_3438998',
						excludeRoles: [],
					},
				],
				isMultiColumn: false,
			},
		],
		registrationFormButton: { title: 'Register' },
		loginFormButton: { title: 'Log in' },
		style: {},
	};

	const getVariationStates = ( _initialState ) => {
		return _initialState;
	};

	const setPremade = ( state, premade ) => {
		const setFieldStyles = (
			labelColor,
			textColor,
			bgColor,
			borderColor,
			placeholderColor
		) => ( {
			label: labelColor,
			text: textColor,
			background: bgColor,
			border: borderColor,
			placeholder: placeholderColor,
		} );

		const setButtonStyles = (
			textColor,
			bgColor,
			borderColor,
			hoverText,
			hoverBG,
			hoverBorder
		) => ( {
			normal: {
				text: textColor,
				background: bgColor,
				border: borderColor,
			},
			hover: {
				text: hoverText,
				background: hoverBG,
				border: hoverBorder,
			},
		} );

		const setContainerStyles = (
			mainBgColor,
			mainBorder,
			signInBgColor,
			signInBorder,
			signUpBgColor,
			signUpBorder
		) => ( {
			main: {
				background: mainBgColor,
				border: mainBorder,
			},
			signIn: {
				background: signInBgColor,
				border: signInBorder,
			},
			signUp: {
				background: signUpBgColor,
				border: signUpBorder,
			},
		} );

		const setTypographyStyles = (
			labelSize = 14,
			labelWeight = 500,
			labelTransform = '',
			inputSize = 14,
			inputWeight = 400,
			inputTransform = '',
			buttonSize = 14,
			buttonWeight = 50,
			buttonTransform = ''
		) => ( {
			field: {
				label: {
					size: labelSize,
					weight: labelWeight,
					transform: labelTransform,
				},
				input: {
					size: inputSize,
					weight: inputWeight,
					transform: inputTransform,
				},
			},
			button: {
				size: buttonSize,
				weight: buttonWeight,
				transform: buttonTransform,
			},
		} );

		const setSizeSpacingStyles = (
			width,
			border,
			borderRadius,
			padding,
			align,
			separator
		) => ( { width, border, borderRadius, padding, align, separator } );

		let _style = {};

		switch ( premade ) {
			case 'premade_1':
				state.settings.inputVariation = 'variation_1';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#343A46'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#343A46'
				);

				_style = {
					color: {
						field: {
							signIn: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#E9E9F0',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#6C6CFF',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#FFF',
									'#FF6C6C',
									'#6C6E77'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#E9E9F0',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#6C6CFF',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#FFF',
									'#FF6C6C',
									'#6C6E77'
								),
							},
						},
						button: {
							signIn: setButtonStyles(
								'#fff',
								'#6C6CFF',
								'',
								'#fff',
								'#1a1ac3',
								''
							),
							signUp: setButtonStyles(
								'#fff',
								'#6C6CFF',
								'',
								'#fff',
								'#1a1ac3',
								''
							),
						},
						container: setContainerStyles(
							'#FFF',
							'#E9E9F0',
							'#FFF',
							'',
							'#FFF',
							''
						),
					},
					// (labelSize, labelWeight, inputSize, inputWeight, buttonSize, buttonWeight)
					typography: setTypographyStyles(
						14,
						500,
						'',
						14,
						400,
						'',
						14,
						500,
						''
					),
					sizeSpacing: {
						input: setSizeSpacingStyles( 395, 1, 2, 16 ),
						button: setSizeSpacingStyles( 50, 0, 2, 13, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 1, 16, 0, '', 1 ),
							signIn: setSizeSpacingStyles(
								'',
								0,
								16,
								54,
								'',
								''
							),
							signUp: setSizeSpacingStyles(
								'',
								0,
								16,
								54,
								'',
								''
							),
						},
					},
				};
				state.style = _style;
				break;

			case 'premade_2':
				state.settings.inputVariation = 'variation_2';
				state.registrationFormHeader.isHideDescription = true;
				state.loginFormHeader.isHideDescription = true;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#343A46'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#fff',
					'#fff'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#fff',
									'#fff',
									'#343A46',
									'#C8C8D6',
									'#E9E9F0'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#343A46',
									'#6C6CFF',
									'#E9E9F0'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'transparent',
									'#FF6C6C',
									'#E9E9F0'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#C8C8D6',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#6C6CFF',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#FFF',
									'#FF6C6C',
									'#6C6E77'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signIn: setButtonStyles(
								'#343A46',
								'#E9E9F0',
								'#E9E9F0',
								'#fff',
								'#6C6CFF',
								'#6C6CFF'
							),
							signUp: setButtonStyles(
								'#fff',
								'#343A46',
								'#343A46',
								'#fff',
								'#6C6CFF',
								'#6C6CFF'
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'',
							'',
							'#343A46',
							'#343A46',
							'',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						14,
						500,
						'',
						14,
						400,
						'',
						14,
						500,
						''
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 395, 1, 2, 15, 'left' ),
						button: setSizeSpacingStyles( 100, 0, 2, 13, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 0, 0, 0, '', 0 ),
							signIn: setSizeSpacingStyles( '', 0, 0, 44, '', 0 ),
							signUp: setSizeSpacingStyles( '', 0, 0, 44, '', 0 ),
						},
					},
				};
				state.style = _style;
				break;

			case 'premade_3':
				state.settings.inputVariation = 'variation_4';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#fff',
					'#E9E9F0'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#6C6CFF',
					'#6C6E77'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#6C6CFF',
									'#6C6CFF',
									'#d6d6ff',
									'#6C6CFF',
									'#fff'
								),
								active: setFieldStyles(
									'#6C6CFF',
									'#6C6CFF',
									'#d6d6ff',
									'#6C6CFF',
									'#fff'
								),
								warning: setFieldStyles(
									'#6C6CFF',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'#fff'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#fff',
									'#fff',
									'#8484ff',
									'#E9E9F0',
									'##C8C8FF'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#8484ff',
									'#E9E9F0',
									'##C8C8FF'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'##C8C8FF'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#6C6CFF',
								'#fff',
								'#fff',
								'#fff',
								'#343A46',
								'#343A46'
							),
							signIn: setButtonStyles(
								'#fff',
								'#6C6CFF',
								'#6C6CFF',
								'#fff',
								'#343A46',
								'#343A46'
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'',
							'#6C6CFF',
							'#fff',
							'',
							'#6C6CFF',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						14,
						500,
						'',
						14,
						400,
						'',
						16,
						500,
						''
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 395, 1, 2, 16, 'left' ),
						button: setSizeSpacingStyles( 100, 0, 2, 16, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 1, 12, 0, '', 0 ),
							signIn: setSizeSpacingStyles(
								'',
								0,
								12,
								54,
								'',
								0
							),
							signUp: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_4':
				state.settings.inputVariation = 'variation_8';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#6C6E77'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#6C6E77'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#fff',
									'#C8C8D6',
									'#C8C8D6'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'',
									'#6C6CFF',
									'#C8C8D6'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'',
									'#FF6C6C',
									'#C8C8D6'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#fff',
									'#C8C8D6',
									'#C8C8D6'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'',
									'#6C6CFF',
									'#C8C8D6'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'',
									'#FF6C6C',
									'#C8C8D6'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#fff',
								'#343A46',
								'',
								'#fff',
								'#6C6CFF',
								''
							),
							signIn: setButtonStyles(
								'#fff',
								'#343A46',
								'',
								'#fff',
								'#6C6CFF',
								''
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'',
							'#6C6CFF',
							'',
							'',
							'',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						14,
						400,
						'',
						14,
						400,
						'',
						14,
						500,
						''
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 345, 1, 0, 7, 'left' ),
						button: setSizeSpacingStyles( 50, 0, 2, 13, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 1, 12, 0, '', 0 ),
							signIn: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
							signUp: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_5':
				state.settings.inputVariation = 'variation_6';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#6C6E77'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#fff',
					'#fff'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#fff',
									'#fff',
									'#7878ff',
									'#fff',
									'#E9E9F0'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#7878ff',
									'#0000ff',
									'#E9E9F0'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'#7878ff',
									'#FF6C6C',
									'#E9E9F0'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#fff',
									'#E9E9F0',
									''
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#fff',
									'#6C6CFF',
									''
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#fff',
									'#FF6C6C',
									''
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#fff',
								'#343A46',
								'',
								'#fff',
								'#6C6CFF',
								''
							),
							signIn: setButtonStyles(
								'#6C6CFF',
								'#fff',
								'',
								'#fff',
								'#343A46',
								''
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'',
							'',
							'#6C6CFF',
							'',
							'',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						14,
						400,
						'',
						14,
						400,
						'',
						14,
						500,
						''
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 390, 1, 2, 14, 'left' ),
						button: setSizeSpacingStyles( 60, 0, 2, 13, 'left' ),
						container: {
							main: setSizeSpacingStyles(
								1200,
								1,
								12,
								16,
								'',
								0
							),
							signIn: setSizeSpacingStyles(
								566,
								0,
								8,
								44,
								'',
								0
							),
							signUp: setSizeSpacingStyles(
								673,
								0,
								0,
								44,
								'',
								0
							),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_6':
				state.settings.inputVariation = 'variation_1';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#E9E9F0',
					'#C8C8D6'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#656565'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#141516',
									'#141516',
									'rgb(237 237 237)',
									'#656565',
									'#656565'
								),
								active: setFieldStyles(
									'#141516',
									'#141516',
									'rgb(237 237 237)',
									'#656565',
									'#656565'
								),
								warning: setFieldStyles(
									'#141516',
									'#FF6C6C',
									'rgba(20, 21, 22, 0.08)',
									'#FF6C6C',
									'#656565'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#E9E9F0',
									'#fff',
									'#141516',
									'#C8C8D6',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#E9E9F0',
									'#fff',
									'#141516',
									'#C8C8D6',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#E9E9F0',
									'#FF6C6C',
									'#141516',
									'#FF6C6C',
									'#6C6E77'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#141516',
								'#E9E9F0',
								'',
								'#141516',
								'#656565',
								''
							),
							signIn: setButtonStyles(
								'#fff',
								'#141516',
								'',
								'#fff',
								'#656565',
								''
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'rgba(20, 21, 22, 0.08)',
							'#141516',
							'',
							'',
							'#141516',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 395, 1, 2, 13, 'left' ),
						button: setSizeSpacingStyles( 100, 0, 2, 11, 'left' ),
						container: {
							main: setSizeSpacingStyles(
								1200,
								2,
								20,
								16,
								'',
								0
							),
							signIn: setSizeSpacingStyles( '', 0, 8, 54, '', 0 ),
							signUp: setSizeSpacingStyles(
								650,
								0,
								0,
								54,
								'',
								0
							),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_7':
				state.settings.inputVariation = 'variation_5';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#E9E9F0',
					'#E9E9F0'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#E9E9F0',
					'#E9E9F0'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#fff',
									'#fff',
									'#296ddb',
									'#E9E9F0',
									'#9DC2FF'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#296ddb',
									'#E9E9F0',
									'#9DC2FF'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'#9DC2FF'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#fff',
									'#fff',
									'#296ddb',
									'#E9E9F0',
									'#9DC2FF'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#296ddb',
									'#E9E9F0',
									'#9DC2FF'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'#9DC2FF'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#fff',
								'rgba(255, 255, 255, 0.26)',
								'#fff',
								'#fff',
								'#0051D4',
								'#fff'
							),
							signIn: setButtonStyles(
								'#fff',
								'rgba(255, 255, 255, 0.26)',
								'#fff',
								'#fff',
								'#0051D4',
								'#fff'
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'#FFF',
							'#0051D4',
							'#0051D4',
							'#fff',
							'#0051D4',
							'#fff'
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						'',
						'',
						'',
						'',
						'',
						'',
						'16'
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 395, 1, 2, 12, 'left' ),
						button: setSizeSpacingStyles( 100, 1, 2, 14, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 2, 20, 8, '', 0 ),
							signIn: setSizeSpacingStyles(
								'',
								7,
								12,
								54,
								'',
								0
							),
							signUp: setSizeSpacingStyles(
								'',
								7,
								12,
								54,
								'',
								0
							),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_8':
				state.settings.inputVariation = 'variation_1';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#fff',
					'#ADADAD'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#131313',
					'#555'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#131313',
									'#131313',
									'#E3E3E3',
									'#fff',
									'#767676'
								),
								active: setFieldStyles(
									'#131313',
									'#131313',
									'#E3E3E3',
									'#FEAD01',
									'#767676'
								),
								warning: setFieldStyles(
									'#131313',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'#767676'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#fff',
									'#FFF',
									'#2F2F2F',
									'#131313',
									'#767676'
								),
								active: setFieldStyles(
									'#fff',
									'#fff',
									'#2F2F2F',
									'#FEAD01',
									'#767676'
								),
								warning: setFieldStyles(
									'#fff',
									'#FF6C6C',
									'rgba(255, 108, 108, 0.24)',
									'#FF6C6C',
									'#767676'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#131313',
								'#FEAD01',
								'',
								'#131313',
								'#E9E9F0',
								''
							),
							signIn: setButtonStyles(
								'#fff',
								'#FEAD01',
								'',
								'#fff',
								'#141516',
								''
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'',
							'',
							'',
							'',
							'#131313',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						'',
						'',
						'',
						'',
						'',
						'',
						'16'
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 395, 1, 4, 13, 'left' ),
						button: setSizeSpacingStyles( 100, 0, 2, 10, 'left' ),
						container: {
							main: setSizeSpacingStyles(
								1200,
								0,
								20,
								16,
								'',
								0
							),
							signIn: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
							signUp: setSizeSpacingStyles(
								'670',
								0,
								8,
								54,
								'',
								0
							),
						},
					},
				};
				state.style = _style;
				break;
			case 'premade_9':
				state.settings.inputVariation = 'variation_1';
				state.registrationFormHeader.isHideDescription = false;
				state.loginFormHeader.isHideDescription = false;

				state.registrationFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#343A46'
				);
				state.loginFormHeader.styles = setHeaderStyles(
					'#343A46',
					'#343A46'
				);

				_style = {
					color: {
						field: {
							signIn: {
								// (labelColor, textColor, bgColor, borderColor, placeholderColor)
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#E9E9F0',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#6C6CFF',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#FFF',
									'#FF6C6C',
									'#6C6E77'
								),
							},
							signUp: {
								normal: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#E9E9F0',
									'#6C6E77'
								),
								active: setFieldStyles(
									'#343A46',
									'#343A46',
									'#FFF',
									'#6C6CFF',
									'#6C6E77'
								),
								warning: setFieldStyles(
									'#343A46',
									'#FF6C6C',
									'#FFF',
									'#FF6C6C',
									'#6C6E77'
								),
							},
						},
						button: {
							// (textColor, bgColor, borderColor, hoverText,hoverBG,hoverBorder)
							signUp: setButtonStyles(
								'#fff',
								'#6C6CFF',
								'',
								'#fff',
								'#1a1ac3',
								''
							),
							signIn: setButtonStyles(
								'#fff',
								'#6C6CFF',
								'',
								'#fff',
								'#1a1ac3',
								''
							),
						},
						// (mainBgColor, mainBorder, signInBgColor, signInBorder, signUpBgColor, signUpBorder,)
						container: setContainerStyles(
							'#FFF',
							'#E9E9F0',
							'#FFF',
							'',
							'#FFF',
							''
						),
					},
					// (labelSize, labelWeight, labelTransform, inputSize, inputWeight, inputTransform, buttonSize, buttonWeight, buttonTransform)
					typography: setTypographyStyles(
						14,
						500,
						'',
						14,
						400,
						'',
						14,
						500,
						''
					),
					sizeSpacing: {
						// (width, border, borderRadius, padding, align, separator)
						input: setSizeSpacingStyles( 416, 1, 2, 16, 'left' ),
						button: setSizeSpacingStyles( 100, 0, 2, 13, 'left' ),
						container: {
							main: setSizeSpacingStyles( 1200, 1, 16, 0, '', 1 ),
							signIn: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
							signUp: setSizeSpacingStyles( '', 0, 0, 54, '', 0 ),
						},
					},
				};
				state.style = _style;
				break;

			default:
				break;
		}

		return state;
	};

	const rowId = Date.now().toString().slice( 6 );

	const rowField = {
		id: 'row_' + rowId,
		type: 'row',
		columns: [],
		isMultiColumn: false,
	};

	const getFieldColumn = ( {
		type,
		label = 'Field Label',
		name = '',
		options = [],
		isCustomField = false,
	} ) => {
		const fieldObject = {
			id: Date.now().toString(),
			type,
			label,
			name: isCustomField ? name + '_' + Date.now().toString() : name,
			isLabelHide: false,
			placeholder: label,
			columnPosition: 'left',
			custom_field: isCustomField,
			status: true,
			conditions: {
				status: 'show',
				relation: 'all',
				tiers: [
					{
						_id: Date.now().toString(),
						condition: '',
						field: '',
						value: '',
						src: 'regregistration_formi',
					},
				],
			},
		};
		if ( name === 'wholesalex_registration_role' ) {
			options = wholesalex_overview.whx_form_builder_roles;
			options.unshift( { name: 'Select Role', value: '' } );
		}
		if (
			name === 'user_confirm_pass' ||
			name === 'user_confirm_pass' ||
			name === 'user_confirm_email' ||
			name === 'password' ||
			name === 'user_email' ||
			name === 'user_pass' ||
			name === 'wholesalex_registration_role'
		) {
			fieldObject.required = true;
		}
		switch ( type ) {
			case 'text':
			case 'number':
			case 'tel':
			case 'password':
			case 'textarea':
			case 'email':
			case 'date':
			case 'url':
			case 'file':
				return fieldObject;
			case 'select':
			case 'radio':
			case 'checkbox':
				fieldObject.option = options.length
					? options
					: [
							{ name: 'Value 1', value: 'val1' },
							{ name: 'Value 2', value: 'val2' },
							{ name: 'Value 3', value: 'val3' },
					  ];
				return fieldObject;
			case 'termCondition':
				fieldObject.default_text =
					'I agree to the Terms and Conditions {Privacy Policy}';
				return fieldObject;
			case 'privacy_policy_text':
				return fieldObject;
			default:
				return fieldObject;
		}
	};

	//case empty
	//type: newField, column
	const reducer = ( state, action ) => {
		const { type, field, index } = action;

		let _state = { ...state };
		let tempState = [ ...state.registrationFields ];
		let _row = {};
		let _column = {};
		let _parentIndex = '';
		let _propertyName = '';
		let _propertyValue = '';
		let _columnIndex = '';
		let _styles = {};
		switch ( type ) {
			case 'newFieldRow':
				_column = getFieldColumn( {
					type: field?.type,
					label: field?.label,
					name: field?.name,
					isCustomField: field?.isCustomField,
				} );
				rowField.columns.push( { ..._column, parent: 'row_' + rowId } );
				tempState.push( rowField );
				_state.registrationFields = tempState;
				break;
			case 'makeOneColumn':
				_row = tempState[ index ];
				_row.isMultiColumn = false;
				tempState[ index ] = { ..._row };
				if ( _row.columns.length === 2 ) {
					_column = getFieldColumn( {
						type: _row.columns[ 1 ]?.type,
						label: _row.columns[ 1 ]?.label,
						name: _row.columns[ 1 ]?.name,
					} );

					rowField.columns.push( {
						..._column,
						parent: 'row_' + rowId,
					} );
					tempState[ index ].columns.pop();
					tempState.splice( index + 1, 0, rowField );
				}
				_state.registrationFields = tempState;
				break;
			case 'makeTwoColumn':
				_row = tempState[ index ];
				_row.isMultiColumn = true;
				tempState[ index ] = _row;
				_state.registrationFields = tempState;
				break;
			case 'insertFieldOnColumn':
				_row = tempState[ index ];
				_column = getFieldColumn( {
					type: field?.type,
					label: field?.label,
					name: field?.name,
					isCustomField: field?.isCustomField,
				} );
				_row.columns.push( { ..._column, parent: _row.id } );
				tempState[ index ] = _row;
				_state.registrationFields = tempState;
				break;
			case 'insertFieldOnLeftColumn':
				_row = tempState[ index ];
				_column = getFieldColumn( {
					type: field?.type,
					label: field?.label,
					name: field?.name,
					isCustomField: field?.isCustomField,
				} );
				_row.columns.push( {
					..._column,
					parent: _row.id,
					columnPosition: 'left',
				} );
				tempState[ index ] = _row;
				_state.registrationFields = tempState;
				break;
			case 'insertFieldOnRightColumn':
				_row = tempState[ index ];
				_column = getFieldColumn( {
					type: field?.type,
					label: field?.label,
					name: field?.name,
					isCustomField: field?.isCustomField,
				} );
				_row.columns.push( {
					..._column,
					parent: _row.id,
					columnPosition: 'right',
				} );
				tempState[ index ] = _row;
				_state.registrationFields = tempState;
				break;
			case 'deleteRow':
				_row = tempState[ index ];
				tempState = tempState.filter( ( row, idx ) => idx !== index );
				_state.registrationFields = tempState;
				break;
			case 'deleteLeftColumn':
				_parentIndex = tempState.findIndex(
					( row ) => row.id === field.parent
				);
				_row = tempState[ _parentIndex ];

				_row.columns = _row.columns.filter(
					( _f ) => _f?.columnPosition !== 'left'
				);

				tempState[ _parentIndex ] = _row;
				_state.registrationFields = tempState;
				break;

			case 'deleteRightColumn':
				_parentIndex = tempState.findIndex(
					( row ) => row.id === field.parent
				);
				_row = tempState[ _parentIndex ];
				_row.columns = _row.columns.filter(
					( _f ) => _f?.columnPosition !== 'right'
				);
				tempState[ _parentIndex ] = _row;
				_state.registrationFields = tempState;
				break;
			case 'updateFullState':
				tempState = action.updatedState;
				_state.registrationFields = tempState;
				break;

			case 'updateColumns':
				_row = tempState[ index ];
				if ( _row.columns && _row.columns.length === 2 ) {
					_row.columns = action.updatedColumns;
					_row.columns[ 0 ].columnPosition = 'left';
					_row.columns[ 1 ].columnPosition = 'right';
					tempState[ index ] = _row;
					_state.registrationFields = tempState;
				}
				break;
			case 'updateField':
				_propertyName = action.property;
				_propertyValue = action.value;
				_columnIndex = action.columnIndex;

				_row = tempState[ index ];

				_row.columns[ _columnIndex ][ _propertyName ] = _propertyValue;

				tempState[ index ] = _row;
				_state.registrationFields = tempState;

				break;
			case 'toogleRegistrationFormTitle':
				_state.registrationFormHeader.isShowFormTitle = action.value;
				break;
			case 'toogleFormTitle':
				_state.settings.isShowFormTitle = action.value;
				break;
			case 'toogleRegistrationFormDescriptionStatus':
				_state.registrationFormHeader.isHideDescription = action.value;
				break;
			case 'updateRegistrationFormTitle':
				_state.registrationFormHeader.title = action.value;
				break;
			case 'updateRegistrationFormDescription':
				_state.registrationFormHeader.description = action.value;
				break;
			case 'toogleLoginFormTitle':
				_state.loginFormHeader.isShowFormTitle = action.value;
				break;
			case 'toogleLoginFormDescriptionStatus':
				_state.loginFormHeader.isHideDescription = action.value;
				break;
			case 'updateLoginFormTitle':
				_state.loginFormHeader.title = action.value;
				break;
			case 'updateLoginFormDescription':
				_state.loginFormHeader.description = action.value;
				break;
			case 'updateInputVariation':
				_state.settings.inputVariation = action.value;
				_state.styles = getVariationStates(
					state.styles,
					action.value
				);

				break;
			case 'toogleShowLoginFormStatus':
				_state.settings.isShowLoginForm = action.value;

				break;
			case 'updateStyle':
				_styles = _state.styles;

				if ( action.styles.elementState ) {
					_styles[ action.styles.type ][ action.styles.element ][
						action.styles.elementState
					][ action.styles.property ] = action.styles.value;
				} else {
					_styles[ action.styles.type ][ action.styles.element ][
						action.styles.property
					] = action.styles.value;
				}

				_state.styles = _styles;

				break;

			case 'updateFormHeadingStyle':
				if ( action.formType === 'Registration' ) {
					_state.registrationFormHeader.styles[ action.styleFor ][
						action.property
					] = action.value;
				} else if ( action.formType === 'Login' ) {
					_state.loginFormHeader.styles[ action.styleFor ][
						action.property
					] = action.value;
				}
				break;
			case 'init':
				_state = { ...initialState, ...action.value };
				break;
			case 'updateRegistrationButtonText':
				_state.registrationFormButton.title = action.value;
				break;
			case 'updateLoginButtonText':
				_state.loginFormButton.title = action.value;
				break;
			case 'updateFieldCondition':
				_row = _state.registrationFields[ action.parentIndex ];
				_row.columns[ action.columnIndex ].conditions =
					action.value.conditions;
				_state.registrationFields[ action.parentIndex ] = _row;
				break;
			case 'setPremade':
				_state = setPremade( state, action.premade );

				break;
			case 'setStyle':
				if ( action.stateName && action.loginOrSignUp ) {
					_state.style[ action.sectionName ][ action.panelName ][
						action.loginOrSignUp
					][ action.stateName ][ action.property ] = action.value;
				} else if ( action.loginOrSignUp && ! action.stateName ) {
					_state.style[ action.sectionName ][ action.panelName ][
						action.loginOrSignUp
					][ action.property ] = action.value;
				} else if ( ! action.loginOrSignUp && ! action.stateName ) {
					_state.style[ action.sectionName ][ action.panelName ][
						action.property
					] = action.value;
				}
				break;
			default:
				break;
		}
		return _state;
	};

	const [ registrationForm, setRegistrationForm ] = useReducer(
		reducer,
		setPremade( initialState, 'premade_1' )
	);
	return (
		<RegistrationFormContext.Provider
			value={ { registrationForm, setRegistrationForm } }
		>
			{ props.children }
		</RegistrationFormContext.Provider>
	);
};
