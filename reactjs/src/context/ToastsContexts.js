import React, { createContext, useReducer } from 'react';

export const ToastContextProvider = ( props ) => {
	const [ state, dispatch ] = useReducer( ( states, action ) => {
		switch ( action.type ) {
			case 'ADD_MESSAGE':
				return [ ...states, action.payload ];
			case 'DELETE_MESSAGE':
				return (
					states.length > 0 &&
					states.filter(
						( message ) => message.id !== action.payload
					)
				);
			default:
				return states;
		}
	}, [] );
	return (
		<ToastContexts.Provider value={ { state, dispatch } }>
			{ props.children }
		</ToastContexts.Provider>
	);
};
export const ToastContexts = createContext();
