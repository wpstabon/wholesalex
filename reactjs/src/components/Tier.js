import React from 'react';
import MultiSelect from '../pages/dynamic_rules/MultiSelect';
import Input from './Input';
import Icons from '../utils/Icons';
import Tooltip from './Tooltip';
import { __ } from '@wordpress/i18n';
import Button from './Button';
import '../assets/scss/Tier.scss';

const Tier = ( {
	fields,
	tier,
	setTier,
	tierName,
	tierFieldClass = '',
	index,
	setPopupStatus,
	src,
	labelClass,
	deleteSpaceLeft = 'auto',
} ) => {
	const inputData = ( fieldData, fieldName ) => {
		const flag =
			tier[ tierName ] &&
			tier[ tierName ].tiers &&
			tier[ tierName ].tiers[ index ] &&
			tier[ tierName ].tiers[ index ][ fieldName ];
		const defValue = flag
			? tier[ tierName ].tiers[ index ][ fieldName ]
			: fieldData.default;

		const onChangeHandler = ( e ) => {
			let tierValue = e.target.value;
			if (
				fieldData.type === 'number' &&
				tierValue !== '' &&
				tierValue < 0
			) {
				tierValue = 1;
			}
			const parent = tier[ tierName ] ? tier[ tierName ] : { tiers: [] };
			const copy = parent.tiers;
			if ( ! copy[ index ] ) {
				copy.push( {} );
			}
			copy[ index ][ e.target.getAttribute( 'data-name' ) ] = tierValue;
			parent.tiers = copy;
			setTier( { ...tier, [ tierName ]: parent } );
		};

		return (
			<Input
				label={ index === 0 ? fieldData.label : '' }
				id={ `${ fieldName }_${ index }` }
				data-name={ fieldName }
				type={ fieldData.type }
				name={ `${ fieldName }_${ index }` }
				value={ defValue }
				placeholder={ fieldData.placeholder }
				onChange={ onChangeHandler }
				inputClass="wsx-bg-base1"
				labelClass={ labelClass }
			/>
		);
	};

	const selectData = ( fieldData, fieldName ) => {
		const flag =
			tier[ tierName ] &&
			tier[ tierName ].tiers &&
			tier[ tierName ].tiers[ index ] &&
			tier[ tierName ].tiers[ index ][ fieldName ];
		const defValue = flag
			? tier[ tierName ].tiers[ index ][ fieldName ]
			: fieldData.default;

		const onChangeHandler = ( e ) => {
			const isLock = e?.target?.value?.startsWith( 'pro_' )
				? true
				: false;
			if ( isLock && setPopupStatus ) {
				setPopupStatus( true );
			} else {
				const parent = tier[ tierName ]
					? tier[ tierName ]
					: { tiers: [] };
				const copy = parent.tiers;
				if ( ! copy[ index ] ) {
					copy.push( {} );
				}
				copy[ index ][ fieldName ] = e.target.value;
				parent.tiers = copy;
				setTier( { ...tier, [ tierName ]: parent } );
			}
		};

		const getOptionsArray = ( options ) => {
			return Object.keys( options ).map( ( option ) => ( {
				value: option,
				label: options[ option ],
			} ) );
		};

		return (
			//    <Select
			//         name={`${fieldName}_${index}`}
			//         label={index==0?fieldData.label:''}
			//         labelClass={labelClass}
			//         options={getOptionsArray(fieldData.options)}
			//         value={defValue}
			//         onChange={onChangeHandler}
			//         inputBackground='base1'
			//     />
			<div className="wsx-tier-price-table-wrapper">
				<div className="wsx-input-label wsx-font-medium">
					{ index === 0 ? fieldData.label : '' }
				</div>
				<select
					name={ `${ fieldName }_${ index }` }
					value={ defValue }
					onChange={ onChangeHandler }
					className="wsx-tier-table-select"
					style={ { background: 'base1' } }
				>
					{ getOptionsArray( fieldData.options ).map(
						( option, idx ) => (
							<option key={ idx } value={ option.value }>
								{ option.label }
							</option>
						)
					) }
				</select>
			</div>
		);
	};

	const dependencyCheck = ( deps ) => {
		if ( ! deps ) {
			return true;
		}
		const _temp =
			tier[ tierName ] &&
			tier[ tierName ].tiers &&
			tier[ tierName ].tiers[ index ]
				? tier[ tierName ].tiers[ index ]
				: {};

		let _flag = true;
		deps.forEach( ( dep ) => {
			if ( _temp[ dep.key ] !== dep.value ) {
				_flag = false;
			}
		} );
		return _flag;
	};

	const multiselectData = ( fieldData, fieldName ) => {
		const flag =
			tier[ tierName ] &&
			tier[ tierName ].tiers &&
			tier[ tierName ].tiers[ index ] &&
			tier[ tierName ].tiers[ index ][ fieldName ];
		const defValue = flag
			? tier[ tierName ].tiers[ index ][ fieldName ]
			: fieldData.default;

		return (
			<MultiSelect
				name={ fieldName }
				value={ defValue }
				options={ fieldData.options }
				placeholder={ fieldData.placeholder }
				onMultiSelectChangeHandler={ (
					selectedFieldName,
					selectedValues
				) => {
					const parent = tier[ tierName ]
						? tier[ tierName ]
						: { tiers: [] };
					const copy = parent.tiers;
					if ( ! copy[ index ] ) {
						copy.push( {} );
					}
					copy[ index ][ selectedFieldName ] = [ ...selectedValues ];
					parent.tiers = copy;
					setTier( { ...tier, [ tierName ]: parent } );
				} }
				isAjax={ fieldData?.is_ajax }
				ajaxAction={ fieldData?.ajax_action }
				ajaxSearch={ fieldData?.ajax_search }
			/>
		);
	};

	const getTierLastIndex = () => {
		return tier &&
			tier[ tierName ] &&
			tier[ tierName ].tiers &&
			tier[ tierName ].tiers.length !== 0
			? tier[ tierName ].tiers.length - 1
			: 0;
	};

	const handleDeleteCondition = () => {
		const parent = { ...tier };
		let copy = [ ...parent[ tierName ].tiers ];
		if ( copy.length === 1 ) {
			parent[ tierName ].tiers[ 0 ]._conditions_for = '';
			parent[ tierName ].tiers[ 0 ]._conditions_operator = '';
			parent[ tierName ].tiers[ 0 ]._conditions_value = '';
			parent[ tierName ].tiers[ 0 ]._discount_type = '';
			parent[ tierName ].tiers[ 0 ]._min_quantity = '';
			parent[ tierName ].tiers[ 0 ]._discount_name = '';
			parent[ tierName ].tiers[ 0 ]._discount_amount = '';
			parent[ tierName ].tiers[ 0 ]._product_filter = '';
			parent[ tierName ].tiers = copy;
			setTier( parent );
			return;
		}
		copy = copy.filter( ( row, r ) => {
			return index !== r;
		} );
		parent[ tierName ].tiers = copy;
		setTier( parent );
	};

	return (
		<div
			className="wsx-tiers-fields"
			key={ `wsx-tier-fields-${ tierName }-${ index }` }
		>
			<div className="wsx-tier-wrapper">
				{ Object.keys( fields ).map( ( fieldName, i ) => {
					switch ( fields[ fieldName ].type ) {
						case 'number':
						case 'text':
							return (
								<div
									key={ `tier_field_${ i }` }
									className={ `tier-field ${ tierFieldClass }` }
								>
									{ inputData(
										fields[ fieldName ],
										fieldName
									) }
								</div>
							);
						case 'select':
							return (
								<div
									key={ `tier_field_${ i }` }
									className={ `tier-field ${ tierFieldClass }` }
								>
									{ selectData(
										fields[ fieldName ],
										fieldName
									) }
								</div>
							);
						case 'multiselect':
							return (
								dependencyCheck(
									fields[ fieldName ].depends_on
								) && (
									<div
										key={ `tier_field_${ i }` }
										className={ `tier-field multiselect ${ tierFieldClass }` }
									>
										{ multiselectData(
											fields[ fieldName ],
											fieldName
										) }
									</div>
								)
							);
						case 'filter':
							return (
								<div className="wsx-filter-group">
									{ fields[ fieldName ] &&
										Object.keys( fields[ fieldName ] ).map(
											( filterFieldName, idx ) => {
												switch (
													fields[ fieldName ][
														filterFieldName
													].type
												) {
													case 'select':
														return (
															<div
																key={ `tier_field_${ idx }` }
																className={ `tier-field ${ tierFieldClass }` }
															>
																{ selectData(
																	fields[
																		fieldName
																	][
																		filterFieldName
																	],
																	filterFieldName
																) }
															</div>
														);
													case 'multiselect':
														return (
															dependencyCheck(
																fields[
																	fieldName
																][
																	filterFieldName
																].depends_on
															) && (
																<div
																	key={ `tier_field_${ idx }` }
																	className={ `tier-field multiselect ${ tierFieldClass }` }
																>
																	{ multiselectData(
																		fields[
																			fieldName
																		][
																			filterFieldName
																		],
																		filterFieldName
																	) }
																</div>
															)
														);
													default:
														return <div></div>;
												}
											}
										) }
								</div>
							);
						default:
							return <div></div>;
					}
				} ) }

				<Tooltip
					content={ __( 'Delete', 'wholesalex' ) }
					direction="top"
					spaceLeft={ deleteSpaceLeft }
					className="wsx-rtl-left"
				>
					<span
						key={ `wsx-tier-field-delete-${ tierName }_${ index }` }
						className={ `wsx-tier-delete` }
						onClick={ handleDeleteCondition }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								handleDeleteCondition;
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ Icons.delete_24 }
					</span>
				</Tooltip>
			</div>

			{ index === getTierLastIndex() &&
				( tierName === 'quantity_based' ||
					tierName === 'conditions' ) && (
					<Button
						label={ __( 'Add New Condition', 'wholesalex' ) }
						iconName="plus_20"
						background="tertiary"
						customClass="wsx-mt-24"
						onClick={ () => {
							const parent = { ...tier };
							if ( ! parent[ tierName ] ) {
								parent[ tierName ] = { tiers: [] };
							}
							const copy = [ ...parent[ tierName ].tiers ];
							copy.push( {
								_id: Date.now().toString(),
								src: src ? src : 'dynamic_rule',
							} );
							parent[ tierName ].tiers = copy;
							setTier( parent );
						} }
					/>
				) }
		</div>
	);
};

export default Tier;
