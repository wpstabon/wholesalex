import React from 'react';
import '../assets/scss/ToggleSlider.scss';

import Tooltip from './Tooltip';
import Icons from '../utils/Icons';
import { __ } from '@wordpress/i18n';

const ToggleSlider = ( {
	label,
	labelColor,
	labelClass,
	name,
	value,
	onChange,
	tooltip,
	tooltipPosition,
} ) => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	return (
		<div className="wsx-toggle-slider-wrapper">
			<div
				className={ `wsx-input-label ${
					tooltip ? 'wsx-d-flex wsx-item-center' : ''
				} ${
					labelColor && `wsx-color-${ labelColor }`
				} ${ labelClass }` }
			>
				{ label }
				{ tooltip && (
					<Tooltip
						content={ tooltip }
						direction={ tooltipPosition }
						spaceLeft="8px"
					>
						{ Icons.help }
					</Tooltip>
				) }
			</div>
			<div className="wsx-toggle-switches-container">
				<input
					type="radio"
					id="wsx-toggle-switchMonthly"
					name="switchPlan"
					value="table_style"
					checked={ value === 'table_style' }
					onChange={ ( e ) => onChange( e, name, 'table_style' ) }
				/>
				<input
					type="radio"
					id="wsx-toggle-switchYearly"
					name="switchPlan"
					value="classic_style"
					checked={ value === 'classic_style' }
					onChange={ ( e ) => onChange( e, name, 'classic_style' ) }
				/>
				<label className="wsx-label" htmlFor="wsx-toggle-switchMonthly">
					{ __( 'Table Style', 'wholesalex' ) }
				</label>
				<label className="wsx-label" htmlFor="wsx-toggle-switchYearly">
					{ __( 'Classic Style', 'wholesalex' ) }
				</label>
				<div
					className="wsx-toggle-switch-wrapper"
					style={ {
						transform:
							value === 'table_style'
								? `translateX(${ isRTL ? '93%' : '0%' })`
								: `translateX(${ isRTL ? '0%' : '93%' })`,
					} }
				>
					<div className="wsx-toggle-switch">
						<div
							style={ {
								opacity: value === 'table_style' ? 1 : 0,
							} }
						>
							{ __( 'Table Style', 'wholesalex' ) }
						</div>
						<div
							style={ {
								opacity: value === 'classic_style' ? 1 : 0,
							} }
						>
							{ __( 'Classic Style', 'wholesalex' ) }
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ToggleSlider;
