import React, { useState, useEffect, useRef } from 'react';
import Icons from '../utils/Icons';
import ReactDOM from 'react-dom';

function Select( {
	id = '',
	name = '',
	value = '',
	noValue = false,
	label = '',
	labelClass = '',
	inputPadding = '',
	iconName = '',
	iconPosition = 'after',
	iconColor = '',
	iconGap = '8px',
	iconRotation = 'full',
	onChange,
	wrapperClass = '',
	customClass = '',
	inputBackground = 'transparent',
	inputColor = 'text-light',
	borderNone = false,
	borderColor = 'border-primary',
	borderRadius = 'md',
	containerBackground = 'base1',
	containerBorderRadius = 'md',
	containerBorderColor = 'base2',
	containerBorder = true,
	containerPadding = '4',
	minWidth = '',
	maxWidth = '',
	direction = 'ltr',
	textAlign = '',
	options = [],
	optionBorderBottom = false,
	optionBorderRadius = 'sm',
	optionBorderColor = 'border-light',
	optionCustomClass = '',
	selectedOptionClass = '',
	selectionText = 'Select an option',
} ) {
	const position = iconPosition === 'before' || iconPosition === 'left';
	const Icon = iconName && Icons[ iconName ] ? Icons[ iconName ] : null;

	const [ isDropdownOpen, setIsDropdownOpen ] = useState( false );
	const [ selectedOption, setSelectedOption ] = useState(
		value || selectionText
	);
	const [ dropdownPosition, setDropdownPosition ] = useState( {
		width: 0,
		top: 0,
		left: 0,
		isAbove: false,
		isRight: false,
	} );
	const dropdownRef = useRef( null );
	const contentRef = useRef( null );
	const inputRef = useRef( null );

	! textAlign &&
		( direction === 'ltr'
			? ( textAlign = 'left' )
			: ( textAlign = 'right' ) );

	const handleOutsideClick = ( e ) => {
		if (
			dropdownRef.current &&
			( dropdownRef.current.contains( e.target ) ||
				( contentRef.current &&
					contentRef.current.contains( e.target ) ) )
		) {
			return;
		}
		setIsDropdownOpen( false );
	};

	const toggleDropdown = () => {
		if ( ! isDropdownOpen ) {
			const rect = dropdownRef.current.getBoundingClientRect();
			const viewportHeight = window.innerHeight;
			const viewportWidth = window.innerWidth;

			setTimeout( () => {
				const contentHeight = contentRef.current
					? contentRef.current.getBoundingClientRect().height
					: 0;
				const contentWidth = contentRef.current
					? contentRef.current.getBoundingClientRect().width
					: 0;

				const isAbove =
					rect.top - contentHeight > 0
						? rect.bottom + contentHeight > viewportHeight
						: false;
				const isRight =
					rect.left - contentWidth > 0
						? rect.left + contentWidth > viewportWidth
						: false;

				setDropdownPosition( {
					width: rect.width,
					top: isAbove
						? rect.top + window.scrollY - contentHeight - 1
						: rect.bottom + window.scrollY + 1,
					left: isRight
						? rect.right + window.scrollX - contentWidth
						: rect.left + window.scrollX,
					isAbove,
					isRight,
				} );
			}, 0 );
		}
		setIsDropdownOpen( ! isDropdownOpen );
	};
	const handleOptionSelect = ( optionValue ) => {
		const isLock = optionValue?.startsWith( 'pro_' );

		if ( onChange ) {
			onChange( { target: { name, value: optionValue, id } } );
		}

		if ( isLock ) {
			setIsDropdownOpen( false );
			return;
		}

		const selected = options.find(
			( option ) => option.value === optionValue
		);
		setSelectedOption( selected ? selected.label : selectionText );
		setIsDropdownOpen( false );
	};
	useEffect( () => {
		const effectiveValue = value ? value : 'default';
		const selected = options.find(
			( option ) => option.value === effectiveValue
		);
		setSelectedOption( selected ? selected.label : selectionText );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ value ] );

	useEffect( () => {
		if ( isDropdownOpen ) {
			document.addEventListener( 'mousedown', handleOutsideClick );
		} else {
			document.removeEventListener( 'mousedown', handleOutsideClick );
		}
		return () => {
			document.removeEventListener( 'mousedown', handleOutsideClick );
		};
	}, [ isDropdownOpen ] );

	const dropdownContent = isDropdownOpen && ! noValue && (
		<div
			className={ `wsx-option-container wsx-scrollbar wsx-p-${ containerPadding } wsx-bg-${ containerBackground } wsx-br-${ containerBorderRadius } ${
				containerBorder
					? `wsx-border-default wsx-bc-${ containerBorderColor }`
					: ''
			}` }
			ref={ contentRef }
			style={ {
				zIndex: isDropdownOpen ? 999999 : -999999,
				visibility: isDropdownOpen ? 'visible' : 'hidden',
				opacity: isDropdownOpen ? 1 : 0,
				top: isDropdownOpen ? `${ dropdownPosition.top }px` : 0,
				left: isDropdownOpen ? `${ dropdownPosition.left }px` : 0,
				width: `${ dropdownPosition.width - 10 }px`,
			} }
		>
			{ options.map( ( option ) => (
				<div
					key={ option.value }
					className={ `wsx-option-item ${
						optionBorderBottom ? 'wsx-border-bottom' : ''
					} wsx-br-${ optionBorderRadius } wsx-bc-${ optionBorderColor } ${
						option.value !== 'default' &&
						option.label === selectedOption
							? 'active'
							: ''
					} ${ optionCustomClass }` }
					onClick={ () => handleOptionSelect( option.value ) }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							handleOptionSelect( option.value );
						}
					} }
					tabIndex={ 0 }
					role="button"
				>
					{ option.label }
				</div>
			) ) }
		</div>
	);

	const getIconRotation = ( dropdownOpen, rotation ) => {
		if ( dropdownOpen ) {
			if ( rotation === 'full' ) {
				return 'rotate(180deg)';
			} else if ( rotation === 'half' ) {
				return 'rotate(90deg)';
			}
		}
		return 'rotate(0deg)';
	};

	return (
		<div
			className={ `wsx-select-wrapper ${ wrapperClass } ${
				noValue ? 'wsx-d-flex wsx-item-center wsx-gap-8' : ''
			}` }
			ref={ inputRef }
		>
			{ label && (
				<div
					className={ `wsx-input-label wsx-font-medium ${ labelClass } ${
						noValue ? 'wsx-mb-0 wsx-no-value' : ''
					}` }
				>
					{ label }
				</div>
			) }
			<div
				className={ `wsx-input-inner-wrapper  ${
					! noValue &&
					( borderNone
						? ''
						: `wsx-border-default wsx-bc-${ borderColor }` )
				} wsx-bg-${ inputBackground } wsx-color-${ inputColor } wsx-br-${ borderRadius }` }
				ref={ dropdownRef }
				style={ {
					minWidth,
					maxWidth,
				} }
			>
				<div
					className={ `wsx-select ${ customClass }` }
					onClick={ toggleDropdown }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							toggleDropdown();
						}
					} }
					tabIndex={ 0 }
					role="button"
					style={ {
						textAlign,
						direction,
						padding:
							! noValue &&
							( inputPadding ? inputPadding : '10px 16px' ),
						gap: ! noValue && iconGap,
					} }
				>
					{ position && (
						<div
							className={ `wsx-icon` }
							style={ {
								transition: 'transform var(--transition-md)',
								transform: getIconRotation(
									isDropdownOpen,
									iconRotation
								),
								color: iconColor ? iconColor : 'unset',
							} }
						>
							{ Icon ? Icon : Icons.angleDown }
						</div>
					) }
					<div
						className={ `wsx-select-value wsx-ellipsis ${ selectedOptionClass }` }
					>
						{ ! noValue && selectedOption }
					</div>
					{ ! position && (
						<div
							className={ `wsx-icon` }
							style={ {
								transition: 'transform var(--transition-md)',
								transform: getIconRotation(
									isDropdownOpen,
									iconRotation
								),
								color: iconColor ? iconColor : 'unset',
							} }
						>
							{ Icon ? Icon : Icons.angleDown }
						</div>
					) }
				</div>
				{ ReactDOM.createPortal( dropdownContent, document.body ) }
			</div>
		</div>
	);
}

export default Select;
