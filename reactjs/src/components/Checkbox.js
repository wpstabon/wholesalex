import React, { useState } from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';

const Checkbox = ( props ) => {
	const {
		label,
		labelClass,
		onChange,
		option,
		isDisable,
		value,
		tooltip,
		tooltipPosition = 'top',
		help,
		helpClass,
		className,
		required,
		requiredColor,
		isLabelHide,
		checkboxSize = 'lg',
		name,
		id,
		defaultChecked,
		checkboxBackground,
	} = props;

	const [ isChecked, setIsChecked ] = useState(
		defaultChecked || ( value && value !== 'no' ) ? true : false
	);

	const handleCheckboxChange = ( event ) => {
		setIsChecked( event.target.checked );
		if ( onChange ) {
			onChange( event );
		}
	};

	return (
		<div className={ `wsx-checkbox-wrapper ${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label ${
						tooltip ? 'wsx-d-flex' : ''
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div
				className={ `wsx-checkbox-option-wrapper ${
					isChecked ? 'checked' : ''
				}` }
				style={ { alignItems: help ? 'start' : 'center' } }
			>
				<label className="wsx-label" htmlFor={ props.name }>
					{ ' ' }
					<input
						className="wsx-checkbox"
						type="checkbox"
						id={ id || name }
						name={ name }
						onChange={ handleCheckboxChange }
						defaultChecked={ isChecked }
						disabled={ isDisable }
					/>
					<div
						className={ `wsx-checkbox-mark wsx-checkbox-${ checkboxSize }` }
						style={ {
							backgroundColor: checkboxBackground,
							borderColor: checkboxBackground,
						} }
					></div>
				</label>
				<div>
					<div
						className="wsx-checkbox-option"
						style={ { marginBottom: help ? '8px' : '0' } }
					>
						{ option }
					</div>
					{ help && (
						<div
							className={ `wsx-switch-help wsx-help-message ${ helpClass }` }
						>
							{ help }
						</div>
					) }
				</div>
			</div>
		</div>
	);
};

export default Checkbox;
