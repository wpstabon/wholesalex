import React from 'react';
import Tooltip from './Tooltip';

const TextArea = ( props ) => {
	const {
		label,
		labelClass,
		type,
		name,
		value,
		onChange,
		tooltip,
		help,
		helpClass,
		rows,
		cols,
		placeholder,
		isLabelHide,
		required,
		className,
	} = props;

	return (
		<div className={ `wsx-textarea-wrapper ${ className }` }>
			{ ! isLabelHide && label && (
				<div className={ `wsx-input-label ${ labelClass }` }>
					{ label }{ ' ' }
					{ required && (
						<span className="wholesalex_required required">*</span>
					) }{ ' ' }
					{ tooltip && (
						<Tooltip content={ tooltip }>
							<span className="dashicons dashicons-editor-help wholesalex_tooltip_icon" />
						</Tooltip>
					) }
				</div>
			) }
			<div className="wholesalex_textarea_field__content">
				<textarea
					rows={ rows ? rows : 4 }
					cols={ cols ? cols : 50 }
					id={ name }
					name={ name }
					type={ type }
					value={ value }
					onChange={ onChange }
					placeholder={ placeholder }
					required={ required }
					className="wsx-textarea "
				/>
				{ help && (
					<div
						className={ `wsx-textarea-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default TextArea;
