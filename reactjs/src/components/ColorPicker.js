import React from 'react';
import Tooltip from './Tooltip';
import '../assets/scss/ColorPicker.scss';
import Icons from '../utils/Icons';

const ColorPicker = ( props ) => {
	let {
		label,
		labelClass,
		type = 'color',
		name,
		defaultValue,
		value,
		setValue,
		tooltip,
		tooltipPosition,
		help,
		helpClass,
		onChange,
	} = props;
	defaultValue = value && value[ name ] ? value[ name ] : defaultValue;
	return (
		<div className={ `wsx-color-picker-field wsx-${ type }-field` }>
			{ ( label || tooltip ) && (
				<div
					className={ `wsx-input-label ${
						tooltip ? 'wsx-d-flex' : ''
					} ${ labelClass }` }
				>
					{ label }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className="wsx-color-picker-wrapper">
				<div className="wsx-d-flex wsx-item-center wsx-color-picker-container">
					<input
						className="wsx-color-picker-input"
						type={ type }
						id={ name }
						name={ name }
						defaultValue={ defaultValue }
						onChange={
							onChange
								? onChange
								: ( e ) => {
										setValue( {
											...value,
											[ e.target.name ]: e.target.value,
										} );
								  }
						}
						style={ { backgroundColor: defaultValue } }
					/>
					<label
						htmlFor={ name }
						className="wsx-label wsx-color-picker-label"
					>
						{ defaultValue }
					</label>
				</div>
				{ help && (
					<div
						className={ `wsx-color-picker-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default ColorPicker;
