import React from 'react';
import '../assets/scss/FontSize.scss';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';
import Button from './Button';

const FontSize = ( {
	name,
	value,
	label,
	labelColor,
	labelClass,
	className,
	onChange,
	tooltip,
	tooltipPosition,
} ) => {
	const decreaseFontSize = ( e ) => {
		onChange( e, name, value > 1 ? value - 1 : value );
	};

	const increaseFontSize = ( e ) => {
		onChange( e, name, value + 1 );
	};

	return (
		<div className={ `wsx-font-wrapper ${ className || '' }` }>
			<div
				className={ `wsx-input-label ${
					tooltip ? 'wsx-d-flex wsx-item-center' : ''
				} ${
					labelColor && `wsx-color-${ labelColor }`
				} ${ labelClass }` }
			>
				{ label }
				{ tooltip && (
					<Tooltip
						content={ tooltip }
						direction={ tooltipPosition }
						spaceLeft="8px"
					>
						{ Icons.help }
					</Tooltip>
				) }
			</div>
			<div className="wsx-font-container">
				<Button
					iconName="minus"
					background="base2"
					onClick={ decreaseFontSize }
					smallButton={ true }
					padding="4px"
					customClass="wsx-left wsx-br-none"
				/>
				<div className="wsx-font-value">{ value }</div>
				<Button
					iconName="plus_20"
					background="base2"
					onClick={ increaseFontSize }
					smallButton={ true }
					padding="4px"
					customClass="wsx-right wsx-br-none"
				/>
			</div>
		</div>
	);
};

// FontSize.propTypes = {
// 	value: PropTypes.number,
// 	label: PropTypes.string,
// 	className: PropTypes.string,
// };

FontSize.defaultProps = {
	value: 14,
	label: '',
	className: '',
};

export default FontSize;
