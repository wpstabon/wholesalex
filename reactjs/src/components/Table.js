import React from 'react';
import Button from './Button';
const { __ } = wp.i18n;
import Icons from '../utils/Icons';
import '../assets/scss/Table.scss';

function Table( {
	headerItems = [],
	gap = '40px',
	tableItems = [],
	customClass,
} ) {
	const numberOfColumn = headerItems.length;

	const handleLinkAction = ( buttonLink ) => {
		window.open( buttonLink, '_blank' );
	};
	const renderContent = ( content ) => {
		if (
			typeof content === 'string' &&
			content.includes( '<' ) &&
			content.includes( '>' )
		) {
			return <div dangerouslySetInnerHTML={ { __html: content } } />;
		}
		return content;
	};

	return (
		<div
			className={ `wsx-table-container wsx-scrollbar ${
				customClass ? customClass : ''
			}` }
		>
			<div
				className="wsx-table-header wsx-lg-important-gap-16"
				style={ {
					gridTemplateColumns: `repeat(${ numberOfColumn }, 1fr)`,
					gap,
				} }
			>
				{ headerItems.map( ( headerItem, index ) => (
					<span
						style={ { minWidth: headerItem.minWidth } }
						key={ index }
					>
						{ headerItem.title }
					</span>
				) ) }
			</div>
			{ tableItems && tableItems.length !== 0 ? (
				<div className="wsx-table-body">
					{ tableItems.map( ( tableItem, rowIndex ) => (
						<div
							className="wsx-table-row wsx-lg-important-gap-16"
							style={ {
								gridTemplateColumns: `repeat(${ numberOfColumn }, 1fr)`,
								gap,
							} }
							key={ rowIndex }
						>
							{ tableItem && tableItem.length > 0
								? tableItem.map( ( item, itemIndex ) => {
										if ( item.content === 'action' ) {
											return (
												<div
													key={ itemIndex }
													className="wsx-btn-group wsx-gap-8"
												>
													<span className="wsx-btn-action">
														{ Icons.delete }
													</span>
													<span className="wsx-btn-action">
														{ Icons.copy }
													</span>
													<span className="wsx-btn-action">
														{ Icons.edit }
													</span>
												</div>
											);
										}
										return item.type === 'name_n_email' ? (
											<span
												className="wsx-d-flex wsx-item-center wsx-gap-12"
												style={ {
													minWidth: item.minWidth,
												} }
												key={ itemIndex }
											>
												<img
													className="wsx-user-image"
													src={ item.avatar }
													alt="User avatar"
												/>
												<span>
													<div className="wsx-font-medium wsx-color-text-medium">
														{ item.content }
													</div>
													<div className="wsx-ellipsis">
														{ item.userEmail }
													</div>
												</span>
											</span>
										) : (
											<span
												className="wsx-d-flex wsx-item-center wsx-gap-12 wsx-ellipsis"
												style={ {
													minWidth: item.minWidth,
												} }
												key={ itemIndex }
											>
												{ item.type !== 'button' ? (
													renderContent(
														item.content
													)
												) : (
													<Button
														label={ item.content }
														background="primary"
														onClick={ () =>
															handleLinkAction(
																item.link
															)
														}
													/>
												) }
											</span>
										);
								  } )
								: tableItem && (
										<span
											className="wsx-d-flex wsx-item-center wsx-gap-12"
											style={ {
												minWidth: tableItem.minWidth,
											} }
											key={ rowIndex }
										>
											{ tableItem.content }
										</span>
								  ) }
						</div>
					) ) }
				</div>
			) : (
				<span className="wsx-table-row">
					{ __( 'No New Customer Found!', 'wholesalex' ) }
				</span>
			) }
		</div>
	);
}

export default Table;
