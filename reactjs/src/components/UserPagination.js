import React, { useState } from 'react';
import { __ } from '@wordpress/i18n';
import Button from './Button';

const Pagination = ( {
	paginationData,
	fetchData,
	spaceTop = 0,
	spaceBottom = 0,
	spaceRight = 0,
	spaceLeft = 0,
} ) => {
	const [ itemsPerPage, setItemsPerPage ] = useState( 10 );
	const totalPages = Math.ceil( paginationData.totalUsers / itemsPerPage );
	const currentPage = paginationData.currentPage;

	const handleItemsPerPageChange = ( e ) => {
		const newLimit = parseInt( e.target.value );
		setItemsPerPage( newLimit );
		fetchData( 'get', 1, true, '', newLimit );
	};

	const getPages = () => {
		let pages = [];
		if ( totalPages <= 5 ) {
			for ( let i = 1; i <= totalPages; i++ ) {
				pages.push( i );
			}
		} else if ( currentPage <= 3 ) {
			pages = [ 1, 2, 3, 4, '...', totalPages ];
		} else if ( currentPage >= totalPages - 2 ) {
			pages = [
				1,
				'...',
				totalPages - 3,
				totalPages - 2,
				totalPages - 1,
				totalPages,
			];
		} else {
			pages = [
				1,
				'...',
				currentPage - 1,
				currentPage,
				currentPage + 1,
				'...',
				totalPages,
			];
		}
		return pages;
	};

	const goToPage = ( page ) => {
		if ( page !== '...' && page > 0 && page <= totalPages ) {
			fetchData( 'get', page, itemsPerPage );
		}
	};

	const previousPage = () => {
		if ( currentPage > 1 ) {
			fetchData( 'get', currentPage - 1, itemsPerPage );
		}
	};

	const nextPage = () => {
		if ( currentPage < totalPages ) {
			fetchData( 'get', currentPage + 1, itemsPerPage );
		}
	};

	return (
		<div
			className="wsx-pagination-wrapper"
			style={ {
				margin: `${ spaceTop }px ${ spaceRight }px ${ spaceBottom }px ${ spaceLeft }px`,
			} }
		>
			<div className="wsx-pagination-left wsx-btn-group wsx-gap-12">
				<Button
					iconName="angleLeft_24"
					iconClass="wsx-rtl-arrow"
					borderColor="tertiary"
					iconColor="tertiary"
					onClick={ previousPage }
					disable={ currentPage === 1 }
				/>
				{ getPages().map( ( page, i ) => (
					<Button
						key={ `pagination_number_${ i }` }
						borderColor="border-secondary"
						background="base1"
						label={ page }
						onClick={ () => goToPage( page ) }
						customClass={ currentPage === page ? 'active' : '' }
						disable={ page === '...' }
					/>
				) ) }
				<Button
					iconName="angleRight_24"
					iconClass="wsx-rtl-arrow"
					borderColor="tertiary"
					iconColor="tertiary"
					onClick={ nextPage }
					disable={ currentPage === totalPages }
				/>
			</div>

			<div className="wsx-pagination-right wsx-d-flex wsx-item-center wsx-gap-8">
				<span className="wsx-color-text-light">
					{ __( 'Show Result:', 'wholesalex' ) }
				</span>
				<select
					className="wsx-pagination-option-list wsx-select"
					value={ itemsPerPage }
					onChange={ handleItemsPerPageChange }
				>
					{ [ 10, 20, 50, 100 ].map( ( pageSize ) => (
						<option
							className="wsx-pagination-option"
							value={ pageSize }
							key={ pageSize }
						>
							{ pageSize }
						</option>
					) ) }
				</select>
			</div>
		</div>
	);
};

export default Pagination;
