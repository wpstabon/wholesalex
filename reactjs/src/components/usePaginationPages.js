import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { DataContext } from '../contexts/DataProvider';

export const usePaginationPages = ( { gotoPage, length, pageSize } ) => {
	const [ currentPage, setCurrentPage ] = useState( 1 );
	const { contextData, setContextData } = useContext( DataContext );
	const { globalCurrentPage } = contextData;
	const totalPages = useMemo( () => {
		return Math.ceil( length / pageSize );
	}, [ length, pageSize ] );

	const canGo = useMemo( () => {
		return {
			next: currentPage < totalPages,
			previous: currentPage - 1 > 0,
		};
	}, [ currentPage, totalPages ] );

	// currentPage siblings
	const pages = useMemo( () => {
		const start = Math.floor( ( currentPage - 1 ) / 4 ) * 4;
		const end = start + 5 > totalPages ? totalPages : start + 4;
		return Array.from( { length: end - start }, ( _, i ) => start + i + 1 );
	}, [ currentPage, totalPages ] );

	// programmatically call gotoPage when currentPage changes
	useEffect( () => {
		gotoPage( currentPage - 1 );
	}, [ currentPage, gotoPage ] );

	useEffect( () => {
		const newGlobalCurrentPage = currentPage;
		setContextData( { globalCurrentPage: newGlobalCurrentPage } );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ currentPage ] );

	useEffect( () => {
		setCurrentPage( globalCurrentPage );
	}, [ globalCurrentPage ] );

	// show first page when per page select options changes
	useEffect( () => {
		if ( pageSize ) {
			goTo( 1 );
		}
	}, [ pageSize ] );

	const goTo = ( page ) => {
		setCurrentPage( page );
	};

	const goNext = useCallback( () => {
		if ( canGo.next ) {
			setCurrentPage( ( prev ) => prev + 1 );
		}
	}, [ canGo ] );

	const goPrev = useCallback( () => {
		if ( canGo.previous ) {
			setCurrentPage( ( prev ) => prev - 1 );
		}
	}, [ canGo ] );

	return {
		canGo,
		currentPage,
		pages,
		goTo,
		goNext,
		goPrev,
	};
};
