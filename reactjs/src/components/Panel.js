import React, { useState } from 'react';

const Panel = ( {
	title,
	panelCloseIcon,
	panelOpenIcon,
	className,
	children,
} ) => {
	const [ status, setStatus ] = useState( false );

	if ( ! panelCloseIcon ) {
		panelCloseIcon = (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="16"
				height="16"
				viewBox="0 0 16 16"
				fill="none"
			>
				{ ' ' }
				<path
					d="M12.6667 5.66666L8.00004 10.3333L3.33337 5.66666"
					stroke="#6C6E77"
					strokeWidth="1.5"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
			</svg>
		);
	}
	if ( ! panelOpenIcon ) {
		panelOpenIcon = (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="16"
				height="16"
				viewBox="0 0 16 16"
				fill="none"
			>
				{ ' ' }
				<path
					d="M3.33335 10.3333L8.00002 5.66668L12.6667 10.3333"
					stroke="#6C6E77"
					strokeWidth="1.5"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>{ ' ' }
			</svg>
		);
	}
	return (
		<div
			className={ `wsx-component-panel ${ className } ${
				status ? 'wsx-component-panel__open' : ''
			}` }
		>
			<div
				className="wsx-component-panel__title wsx-d-flex wsx-justify-space wsx-gap-10"
				onClick={ () => setStatus( ! status ) }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						setStatus( ! status );
					}
				} }
				role="button"
				tabIndex="0"
			>
				{ title }
				<span className={ `wsx-component-panen__icon` }>
					{ status ? panelOpenIcon : panelCloseIcon }
				</span>
			</div>
			{ status && children && (
				<div className="wsx-component-panel__body">{ children }</div>
			) }
		</div>
	);
};

export default Panel;
