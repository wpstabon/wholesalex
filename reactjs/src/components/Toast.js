import React, { useContext, useEffect, useState } from 'react';
import Icons from '../utils/Icons';
import '../assets/scss/Toast.scss';
import { ToastContexts } from '../context/ToastsContexts';

const Toast = ( { position, delay } ) => {
	const { state, dispatch } = useContext( ToastContexts );
	const [ deleteID, setDeleteID ] = useState( null );

	const sleep = ( ms ) => {
		return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
	};

	const deleteMessage = async ( id ) => {
		setDeleteID( id );
		await sleep( 200 );
		dispatch( { type: 'DELETE_MESSAGE', payload: id } );
	};

	useEffect( () => {
		const interval = setInterval( () => {
			if ( delay && state.length > 0 ) {
				deleteMessage( state[ 0 ].id );
			}
		}, delay );

		return () => clearInterval( interval );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ state, delay ] );

	useEffect( () => {
		if ( state.length > 3 ) {
			deleteMessage( state[ 0 ].id );
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ state ] );

	const visibleToasts = state.slice( -3 );

	return (
		<div className="wsx-toast-wrapper">
			{ visibleToasts.length > 0 && (
				<div className="wsx-toast-container">
					{ visibleToasts.map( ( _message ) => (
						<div
							key={ `wsx-toast-${ _message.id }` }
							className={ `wsx-toast-message-wrapper wsx-${
								_message.type
							} wsx-bg-${
								_message.type === 'success'
									? 'positive'
									: 'negative'
							} ${ position } ${
								deleteID === _message.id
									? 'wsx-delete-toast'
									: ''
							}` }
						>
							<div className="wsx-d-flex wsx-item-center wsx-gap-12">
								<span className="wsx-lh-0">
									{ _message.type === 'success'
										? Icons.smile
										: Icons.sad }
								</span>
								<span className="wsx-toast-message">
									{ _message.message }
								</span>
							</div>
							<span
								className="wsx-lh-0 wsx-toast-close"
								onClick={ () => deleteMessage( _message.id ) }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										deleteMessage( _message.id );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.cross }
							</span>
						</div>
					) ) }
				</div>
			) }
		</div>
	);
};

export default Toast;
