import React, { useState } from 'react';

function TabContainer( {
	wrapperClass,
	tabItems = {},
	initialTabItem = '',
	onTabSelect,
	children,
	type = '',
	customClass = '',
	tabHeaderTitle,
	tabHeaderClass,
	tabHeaderAlign = 'center',
	tabHeaderDirection = 'row',
	tabHeaderJustify = 'start',
	tabHeaderWidth = 'full',
	tabHeaderSpace = '12',
	tabHeaderSpaceBottom = '0',
} ) {
	const [ activeTab, setActiveTab ] = useState( initialTabItem );
	const handleClick = ( tabName ) => {
		setActiveTab( tabName );
		if ( onTabSelect ) {
			onTabSelect( tabName );
		}
	};

	const selectedTab = tabItems.find( ( tab ) => tab.name === activeTab );

	return (
		<>
			<div
				className={ `wsx-mb-${ tabHeaderSpaceBottom } wsx-d-flex wsx-w-${ tabHeaderWidth } wsx-gap-${ tabHeaderSpace } wsx-item-${ tabHeaderAlign } wsx-justify-${ tabHeaderJustify } wsx-flex-${ tabHeaderDirection } ${
					wrapperClass ? wrapperClass : ''
				}` }
			>
				{ tabHeaderTitle && (
					<div className={ tabHeaderClass }>{ tabHeaderTitle }</div>
				) }
				<div
					className={ `wsx-tab-container${
						type && ( type !== 'primary' || type !== 'default' )
							? `-${ type }`
							: ''
					} ${ customClass ? customClass : '' }` }
				>
					{ tabItems.map( ( tabItem, index ) => (
						<span
							className={ `wsx-tab-item ${
								activeTab === tabItem.name ? 'active' : ''
							}` }
							style={ {
								cursor:
									activeTab !== tabItem.name
										? 'pointer'
										: 'default',
							} }
							key={ index }
							onClick={ () => {
								if ( activeTab !== tabItem.name ) {
									handleClick( tabItem.name );
								}
							} }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									if ( activeTab !== tabItem.name ) {
										handleClick( tabItem.name );
									}
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ tabItem.title }
						</span>
					) ) }
				</div>
			</div>

			{ children && (
				<div className="wsx-tab-content">
					{ selectedTab && children( selectedTab ) }
				</div>
			) }
		</>
	);
}

export default TabContainer;
