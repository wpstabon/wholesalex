import React, { useState } from 'react';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';
import { __ } from '@wordpress/i18n';
import Button from './Button';
import '../assets/scss/DragDropFileUpload.scss';

const DragDropFileUpload = ( {
	wrapperClass = '',
	label,
	labelColor,
	labelClass,
	help,
	helpClass,
	onChange,
	name,
	tooltip,
	tooltipPosition,
	allowedTypes = [],
} ) => {
	const [ dragActive, setDragActive ] = useState( false );
	const [ fileDetails, setFileDetails ] = useState( null );
	const [ errorMessage, setErrorMessage ] = useState( '' );

	const handleDrag = ( e ) => {
		e.preventDefault();
		e.stopPropagation();
		if ( e.type === 'dragenter' || e.type === 'dragover' ) {
			setDragActive( true );
		} else if ( e.type === 'dragleave' ) {
			setDragActive( false );
		}
	};

	const handleDrop = ( e ) => {
		e.preventDefault();
		e.stopPropagation();
		setDragActive( false );
		if ( e.dataTransfer.files && e.dataTransfer.files[ 0 ] ) {
			validateAndHandleFile( e.dataTransfer.files[ 0 ] );
		}
	};

	const handleFileSelect = ( e ) => {
		if ( e.target.files && e.target.files[ 0 ] ) {
			validateAndHandleFile( e.target.files[ 0 ] );
		}
	};

	const resetFileUpload = () => {
		setFileDetails( null );
		setErrorMessage( '' );
		onChange( null );
		const fileInput = document.getElementById( 'wsx_hidden_file_upload' );
		if ( fileInput ) {
			fileInput.value = '';
		}
	};

	const validateAndHandleFile = ( file ) => {
		if ( allowedTypes.length > 0 && ! allowedTypes.includes( file.type ) ) {
			setErrorMessage(
				<>
					<div>
						{ __(
							'Invalid file type. Allowed types are:',
							'wholesalex'
						) }
					</div>
					<div className="wsx-font-medium wsx-font-16">
						{ allowedTypes.join( ', ' ) }
					</div>
				</>
			);
			return;
		}
		setErrorMessage( '' );
		const { name: fileName, size, type } = file;
		const sizeInKB = ( size / 1024 ).toFixed( 2 ) + ' KB';
		setFileDetails( { fileName, size: sizeInKB, type } );
		onChange( file );
	};

	const triggerFileSelect = () => {
		document.getElementById( 'wsx_hidden_file_upload' ).click();
	};

	const getFileIcon = ( type ) => {
		if ( type.startsWith( 'image/' ) ) {
			return Icons.image;
		}
		if ( type === 'text/csv' ) {
			return Icons.csv;
		}
		if ( type === 'application/vnd.ms-excel' ) {
			return Icons.excel;
		}
		if (
			type ===
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		) {
			return Icons.excel;
		}
		if (
			type === 'application/msword' ||
			type ===
				'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
		) {
			return Icons.docFile;
		}
		return Icons.file;
	};

	return (
		<div
			className={ `wsx-drag-drop-file-upload-wrapper ${ wrapperClass }` }
		>
			{ label && (
				<div
					className={ `wsx-input-label wsx-font-18 wsx-font-medium wsx-mb-14 ${
						tooltip ? 'wsx-d-flex' : ''
					} ${
						labelColor && `wsx-color-${ labelColor }`
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<input
				className="wsx-input"
				type="file"
				id="wsx_hidden_file_upload"
				name={ name }
				style={ { display: 'none' } }
				onChange={ handleFileSelect }
				accept={ allowedTypes.join( ',' ) }
			/>

			{ fileDetails === null ? (
				<div
					className={ `wsx-drag-drop-file-upload-container ${
						dragActive ? 'active' : ''
					}` }
					onClick={ triggerFileSelect }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							triggerFileSelect;
						}
					} }
					role="button"
					tabIndex="0"
					onDragEnter={ handleDrag }
					onDragLeave={ handleDrag }
					onDragOver={ handleDrag }
					onDrop={ handleDrop }
				>
					<>
						<div className="wsx-icon">{ Icons.importFile }</div>
						<div className="wsx-font-16 wsx-font-medium wsx-color-text-medium">
							<span>
								{ __(
									'Drag and Drop File Here or',
									'wholesalex'
								) }{ ' ' }
							</span>
							<span className="wsx-color-primary wsx-border-bottom">
								{ __( 'Click to upload', 'wholesalex' ) }
							</span>
						</div>
					</>
					{ help && (
						<div
							className={ `wsx-drag-drop-file-help wsx-help-message ${ helpClass }` }
						>
							{ help }
						</div>
					) }
					{ errorMessage && (
						<div className="wsx-warning-message wsx-color-warning wsx-d-flex wsx-item-center wsx-gap-4">
							{ errorMessage }
						</div>
					) }
				</div>
			) : (
				<div className="wsx-drag-drop-file-upload-container wsx-relative">
					<div className="wsx-icon">
						{ getFileIcon( fileDetails.type ) }
					</div>
					<div className="wsx-file-name wsx-mb-4 wsx-font-16 wsx-font-medium wsx-color-text-medium">
						{ fileDetails.name }
					</div>
					<div className="wsx-file-size">
						{ __( 'File size:', 'wholesalex' ) + fileDetails.size }
					</div>
					<Button
						label={ __( 'Clear this file', 'wholesalex' ) }
						background="negative"
						smallButton={ true }
						onClick={ resetFileUpload }
						customClass="wsx-mt-12"
					/>
				</div>
			) }
		</div>
	);
};

export default DragDropFileUpload;
