import React from 'react';

const LoadingGif = ( {
	overlay = true,
	insideContainer = false,
	customClass,
	loaderClass,
} ) => {
	// decide what to render
	let content;
	if ( overlay ) {
		if ( insideContainer ) {
			content = (
				<div
					className="wsx-absolute wsx-z-999 wsx-top wsx-bottom wsx-left wsx-right wsx-d-flex wsx-item-center wsx-justify-center"
					style={ { backgroundColor: 'rgb(255 255 255 / 88%)' } }
				>
					<img
						className={ `wsx-absolute wsx-z-99999 wsx-top-20p ${ loaderClass }` }
						style={ { maxWidth: '100px', maxHeight: '100px' } }
						src={ `${ wholesalex.url }assets/img/wsx-loading.gif` }
						alt="loading-img"
					/>
				</div>
			);
		} else {
			content = (
				<div
					className={ `wsx-popup-overlay ${ customClass }` }
					style={ { backgroundColor: 'rgb(255 255 255 / 88%)' } }
				>
					<img
						className={ `${ loaderClass }` }
						style={ { maxWidth: '100px', maxHeight: '100px' } }
						src={ `${ wholesalex.url }assets/img/wsx-loading.gif` }
						alt="loading-gif"
					/>
				</div>
			);
		}
	} else {
		content = (
			<img
				className={ `${ loaderClass }` }
				style={ { maxWidth: '100px', maxHeight: '100px' } }
				src={ `${ wholesalex.url }assets/img/wsx-loading.gif` }
				alt="loading-image"
			/>
		);
	}

	return content;
};
export default LoadingGif;
