import React from 'react';
import handleOutsideClick from './OutsideClick';
import '../assets/scss/OverlayWindow.scss';
import '../assets/scss/SideModal.scss';

import Icons from '../utils/Icons';

const OverlayWindow = ( { windowRef, heading, onClose, content } ) => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	handleOutsideClick( windowRef, () => {
		onClose();
	} );

	return (
		<div className="wsx-side-modal-wrapper">
			<div
				className="wsx-side-modal-container"
				ref={ windowRef }
				style={ {
					opacity: 0,
					transform: `translateX(${ isRTL ? '-50%' : '50%' })`,
					transition: 'all var(--transition-md) ease-in-out',
				} }
			>
				<div className="wsx-side-menu-body">
					<div className="wsx-side-menu-header">
						<span className="wsx-font-medium wsx-font-16 wsx-color-text-medium">
							{ heading }
						</span>
						<div
							className="wsx-modal-close"
							onClick={ onClose }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									onClose;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ Icons.cross }
						</div>
					</div>
					<div className="wsx-side-menu-content wsx-scrollbar">
						{ content() }
					</div>
				</div>
			</div>
		</div>
	);
};

export default OverlayWindow;
