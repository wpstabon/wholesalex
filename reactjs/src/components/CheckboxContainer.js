import React from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';

const CheckboxContainer = ( props ) => {
	const {
		options,
		label,
		isLabelHide,
		isLabelSide,
		name,
		value = [],
		tooltip,
		tooltipPosition,
		help,
		onChange,
		helpClass,
		className,
		required,
		requiredColor,
	} = props;
	return (
		<div className={ `wsx-checkbox-wrapper ${ className }` }>
			{ ! isLabelHide && ! isLabelSide && label && (
				<div className="wsx-checkbox-label">
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className="wsx-checkbox-option-container">
				{ Object.keys( options ).map( ( option, k ) => (
					<label
						key={ k }
						className="wsx-label wsx-checkbox-option-wrapper"
						htmlFor={ `${ name }_${ option }` }
					>
						<input
							className="wsx-checkbox"
							type="checkbox"
							id={ `${ name }_${ option }` }
							name={ name }
							value={ option }
							onChange={ onChange }
							defaultChecked={ value.includes( option ) }
						/>
						<div className="wsx-checkbox-mark"></div>
						<div className="wsx-checkbox-option">
							{ options[ option ] }
						</div>
					</label>
				) ) }
			</div>
			{ ! isLabelHide && isLabelSide && label && (
				<div className="wsx-checkbox-label">
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			{ help && (
				<div
					className={ `wsx-checkbox-help wsx-help-message ${ helpClass }` }
				>
					{ help }
				</div>
			) }
		</div>
	);
};

export default CheckboxContainer;
