import React, { useEffect, useRef, useState } from 'react';
import { __ } from '@wordpress/i18n';

import Icons from '../utils/Icons';
import Button from './Button';

const Modal = ( {
	title,
	status,
	setStatus,
	onDelete,
	customClass,
	smallModal,
} ) => {
	const myRef = useRef();
	const [ isOpen, setIsOpen ] = useState( false );

	useEffect( () => {
		setIsOpen( true );
	}, [] );
	const handleClose = () => {
		setIsOpen( false );
	};

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setStatus( false );
		}
	};
	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [] );

	return (
		status && (
			<div
				className={ `wsx-modal-wrapper ${
					customClass ? customClass : ''
				} ${ isOpen ? 'open' : '' }` }
			>
				<div
					className={ `wsx-modal ${
						smallModal ? 'wsx-modal-sm' : ''
					}` }
					tabIndex="-1"
					aria-hidden="true"
				>
					<div ref={ myRef } className="wsx-card">
						<div className="wsx-modal-header">
							<div className="wsx-modal-title">
								{ __( 'Confirm Delete', 'wholesalex' ) }
							</div>
							<span
								className="wsx-modal-close"
								onClick={ handleClose }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										handleClose;
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.cross }
							</span>
						</div>

						<div className="wsx-modal-body">
							{ `${ __(
								'Do You Want to delete',
								'wholesalex'
							) } ${ title ? title : '' }? ${ __(
								'Be careful, this procedure is irreversible. Do you want to proceed?',
								'wholesalex'
							) }` }
						</div>

						<div className="wsx-modal-footer">
							<Button
								padding="4px 12px"
								borderColor="border-primary"
								color="text-light"
								background="base1"
								customClass="wsx-font-14"
								onClick={ () => {
									setStatus( false );
								} }
								label={ __( 'Cancel', 'wholesalex' ) }
							/>
							<Button
								padding="4px 12px"
								borderColor="border-primary"
								background="negative"
								customClass="wsx-font-14"
								onClick={ ( e ) => {
									e.preventDefault();
									onDelete();
								} }
								label={ __( 'Delete', 'wholesalex' ) }
							/>
						</div>
					</div>
				</div>
			</div>
		)
	);
};
export default Modal;
