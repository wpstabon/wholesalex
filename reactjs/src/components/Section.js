import React from 'react';
const Section = ( { title, children, className } ) => {
	return (
		<div className={ `wsx-component-section ${ className }` }>
			{ title && (
				<div className="wsx-component-section__title">{ title }</div>
			) }
			{ children && (
				<div className="wsx-component-section__body">{ children }</div>
			) }
		</div>
	);
};

export default Section;
