import React, { useState, useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import Icons from '../utils/Icons';
import '../assets/scss/DateSelector.scss';

const DateSelector = ( {
	onChange,
	value,
	id,
	name,
	className,
	maxWidth,
	borderColor = 'border-secondary',
	noCalendar = false,
	noCross = false,
	iconSeparator = false,
	inputPosition = 'start',
} ) => {
	// eslint-disable-next-line camelcase
	const isRTL = wholesalex_overview?.is_rtl_support;

	const [ isOpen, setIsOpen ] = useState( false );
	const [ selectedDate, setSelectedDate ] = useState(
		value ? new Date( value ) : ''
	);
	const [ currentDate, setCurrentDate ] = useState( new Date() );
	const [ inputValues, setInputValues ] = useState( {
		month: value
			? String( new Date( value ).getMonth() + 1 ).padStart( 2, '0' )
			: '',
		day: value
			? String( new Date( value ).getDate() ).padStart( 2, '0' )
			: '',
		year: value ? String( new Date( value ).getFullYear() ) : '',
	} );
	const [ position, setPosition ] = useState( {
		top: 0,
		left: 0,
		isAbove: false,
	} );
	const inputRefs = {
		month: useRef( null ),
		day: useRef( null ),
		year: useRef( null ),
	};
	const calendarRef = useRef( null );
	const pickerRef = useRef( null );

	const dayNames = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];

	useEffect( () => {
		const handleClickOutside = ( event ) => {
			if (
				pickerRef.current &&
				! pickerRef.current.contains( event.target )
			) {
				setIsOpen( false );
			}
		};

		document.addEventListener( 'click', handleClickOutside );
		return () => {
			document.removeEventListener( 'click', handleClickOutside );
		};
	}, [] );

	const toggleCalendar = ( fromInput = false ) => {
		if ( ! isOpen ) {
			const initialDate = selectedDate
				? new Date( selectedDate )
				: new Date();
			setCurrentDate( initialDate );

			const rect = inputRefs.month.current.getBoundingClientRect();
			const viewportHeight = window.innerHeight;

			setTimeout( () => {
				const calendarHeight = calendarRef.current
					? calendarRef.current.getBoundingClientRect().height
					: 0;

				const isAbove = rect.bottom + calendarHeight > viewportHeight;

				setPosition( {
					top: isAbove
						? rect.top + window.scrollY - calendarHeight - 10
						: rect.bottom + window.scrollY + 10,
					left: isRTL
						? rect.right - calendarHeight + 20
						: rect.left + window.scrollX,
					isAbove,
				} );
			}, 0 );
		}
		fromInput ? setIsOpen( true ) : setIsOpen( ! isOpen );
	};

	const clearDate = () => {
		setInputValues( { month: '', day: '', year: '' } );
		setSelectedDate( '' );
		onChange( '' );
		setIsOpen( false );
	};

	const daysInMonth = ( year, month ) =>
		new Date( year, month + 1, 0 ).getDate();

	const handleDateClick = ( date ) => {
		const month = String( date.getMonth() + 1 ).padStart( 2, '0' );
		const day = String( date.getDate() ).padStart( 2, '0' );
		const year = date.getFullYear();

		setInputValues( { month, day, year } );
		const formattedDate = `${ month }/${ day }/${ year }`;
		setSelectedDate( formattedDate );
		onChange( date.toISOString() );
		setIsOpen( false );
	};

	const renderDays = () => {
		const year = currentDate.getFullYear();
		const month = currentDate.getMonth();
		const days = daysInMonth( year, month );

		const firstDay = new Date( year, month, 1 ).getDay();
		const dayElements = [];

		for ( let i = 0; i < firstDay; i++ ) {
			dayElements.push(
				<div className="wsx-day" key={ `empty-${ i }` } />
			);
		}

		for ( let day = 1; day <= days; day++ ) {
			const date = new Date( year, month, day );
			const isSelected =
				`${ String( month + 1 ).padStart( 2, '0' ) }/${ String(
					day
				).padStart( 2, '0' ) }/${ year }` === selectedDate;
			const isToday = date.toDateString() === new Date().toDateString();

			dayElements.push(
				<div
					key={ day }
					className={ `wsx-day active ${
						isSelected ? 'selected' : ''
					} ${ isToday ? 'today' : '' }` }
					onClick={ () => handleDateClick( date ) }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							handleDateClick( date );
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ day }
				</div>
			);
		}

		return dayElements;
	};

	const handleInputChange = ( e, type ) => {
		const inputValue = e.target.value
			.replace( /\D/g, '' )
			.slice( 0, type === 'year' ? 4 : 2 );

		let formattedDate = null;

		setInputValues( ( prev ) => ( {
			...prev,
			[ type ]: inputValue,
		} ) );

		if ( type === 'month' && inputValue.length === 2 ) {
			inputValue > 12
				? alert( 'Warning: Month cannot go higher than 12' )
				: inputRefs.day.current.focus();
			const { day, year } = inputValues;
			if ( year && day ) {
				const parsedDate = new Date( year, inputValue - 1, day );
				if ( ! isNaN( parsedDate.getTime() ) ) {
					formattedDate = `${ inputValue }/${ day }/${ year }`;
					setSelectedDate( formattedDate );
					onChange( parsedDate.toISOString() );
				}
			}
		} else if ( type === 'day' && inputValue.length === 2 ) {
			inputValue > 31
				? alert( 'Warning: Day cannot go higher than 31' )
				: inputRefs.year.current.focus();
			const { month, year } = inputValues;
			if ( year && month ) {
				const parsedDate = new Date( year, month - 1, inputValue );
				if ( ! isNaN( parsedDate.getTime() ) ) {
					formattedDate = `${ month }/${ inputValue }/${ year }`;
					setSelectedDate( formattedDate );
					onChange( parsedDate.toISOString() );
				}
			}
		}

		if ( type === 'year' && inputValue.length === 4 ) {
			const { month, day } = inputValues;
			if ( month && day ) {
				const parsedDate = new Date( inputValue, month - 1, day );
				if ( ! isNaN( parsedDate.getTime() ) ) {
					formattedDate = `${ month }/${ day }/${ inputValue }`;
					setSelectedDate( formattedDate );
					onChange( parsedDate.toISOString() );
				}
			}
		}

		return formattedDate;
	};

	const handleKeyDown = ( e, type ) => {
		const { selectionStart, selectionEnd } = e.target;

		if ( e.key === 'ArrowRight' ) {
			if (
				selectionStart === selectionEnd &&
				selectionStart === e.target.value.length
			) {
				if ( type === 'month' && inputRefs.day.current ) {
					inputRefs.day.current.focus();
				} else if ( type === 'day' && inputRefs.year.current ) {
					inputRefs.year.current.focus();
				}
			}
		} else if ( e.key === 'ArrowLeft' ) {
			if ( selectionStart === selectionEnd && selectionStart === 0 ) {
				if ( type === 'year' && inputRefs.day.current ) {
					inputRefs.day.current.focus();
				} else if ( type === 'day' && inputRefs.month.current ) {
					inputRefs.month.current.focus();
				}
			}
		}
	};

	const renderCalendar = isOpen && (
		<div
			className="wsx-date-picker-calendar wsx-card wsx-w-fit"
			ref={ calendarRef }
			onClick={ ( e ) => e.stopPropagation() }
			onKeyDown={ ( e ) => {
				if ( e.key === 'Enter' || e.key === ' ' ) {
					e.stopPropagation();
				}
			} }
			role="button"
			tabIndex="0"
			style={ {
				position: 'absolute',
				top: isOpen ? `${ position.top }px` : 0,
				left: isOpen ? `${ position.left }px` : 0,
				zIndex: isOpen ? 999 : -999,
				margin: 0,
				padding: 0,
			} }
		>
			<div className="wsx-control-wrapper">
				<div
					className="wsx-calendar-controller wsx-lh-0"
					onClick={ () =>
						setCurrentDate(
							new Date(
								currentDate.getFullYear(),
								currentDate.getMonth() - 1,
								1
							)
						)
					}
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							setCurrentDate(
								new Date(
									currentDate.getFullYear(),
									currentDate.getMonth() - 1,
									1
								)
							);
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ Icons.angleLeft_24 }
				</div>
				<div className="wsx-month">
					{ currentDate.toLocaleString( 'default', {
						month: 'long',
						year: 'numeric',
					} ) }
				</div>
				<div
					className="wsx-calendar-controller wsx-lh-0"
					onClick={ () =>
						setCurrentDate(
							new Date(
								currentDate.getFullYear(),
								currentDate.getMonth() + 1,
								1
							)
						)
					}
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							() =>
								setCurrentDate(
									new Date(
										currentDate.getFullYear(),
										currentDate.getMonth() + 1,
										1
									)
								);
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ Icons.angleRight_24 }
				</div>
			</div>

			<div className="wsx-calendar">
				<div className="wsx-days-of-week">
					{ dayNames.map( ( d ) => (
						<div key={ d } className="wsx-day-name">
							{ d }
						</div>
					) ) }
				</div>
				<div className="wsx-days">{ renderDays() }</div>
			</div>
		</div>
	);

	let borderStyles = {};

	if ( iconSeparator ) {
		if ( inputPosition === 'end' ) {
			borderStyles = {
				borderLeft: `1px solid var(--color-${ borderColor })`,
			};
		} else if ( inputPosition === 'start' ) {
			borderStyles = {
				borderRight: `1px solid var(--color-${ borderColor })`,
			};
		} else {
			borderStyles = {
				borderRight: `1px solid var(--color-${ borderColor })`,
				borderLeft: `1px solid var(--color-${ borderColor })`,
			};
		}
	}

	return (
		<div
			className={ `wsx-input-wrapper-with-icon wsx-date-picker-wrapper wsx-bc-${ borderColor } ${ className }` }
			id={ id }
			name={ name }
			ref={ pickerRef }
		>
			{ inputPosition === 'end' && (
				<div
					className="wsx-d-flex wsx-item-center"
					style={
						iconSeparator
							? { padding: '0 8px' }
							: { paddingLeft: '8px' }
					}
				>
					<div
						className={ `wsx-icon ${
							! noCalendar && 'wsx-btn-action'
						}` }
						onClick={ () => toggleCalendar( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								toggleCalendar( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ Icons.calendar }
					</div>
					{ ! noCross && (
						<div
							className="wsx-icon wsx-icon-cross"
							onClick={ clearDate }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									clearDate;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ Icons.cross }
						</div>
					) }
				</div>
			) }

			{ inputPosition === 'center' && (
				<div
					className="wsx-d-flex wsx-item-center"
					style={
						iconSeparator
							? { padding: '0 8px' }
							: { paddingLeft: '8px' }
					}
				>
					<div
						className={ `wsx-icon ${
							! noCalendar && 'wsx-btn-action'
						}` }
						onClick={ () => toggleCalendar( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								toggleCalendar( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ Icons.calendar }
					</div>
				</div>
			) }

			<div
				className={ `wsx-date-picker-container wsx-d-flex wsx-item-center` }
				onClick={ () => toggleCalendar( true ) }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						toggleCalendar( true );
					}
				} }
				role="button"
				tabIndex="0"
				style={ {
					maxWidth: `${ maxWidth - 70 }px`,
					...borderStyles,
				} }
			>
				<input
					type="text"
					className="wsx-input-month"
					value={ inputValues.month }
					onChange={ ( e ) => handleInputChange( e, 'month' ) }
					placeholder="mm"
					ref={ inputRefs.month }
					onKeyDown={ ( e ) => handleKeyDown( e, 'month' ) }
				/>
				<div style={ { paddingRight: '2px' } }>/</div>
				<input
					type="text"
					className="wsx-input-day"
					value={ inputValues.day }
					onChange={ ( e ) => handleInputChange( e, 'day' ) }
					placeholder="dd"
					ref={ inputRefs.day }
					onKeyDown={ ( e ) => handleKeyDown( e, 'day' ) }
				/>
				<div style={ { paddingRight: '2px' } }>/</div>
				<input
					type="text"
					className="wsx-input-year"
					value={ inputValues.year }
					onChange={ ( e ) => handleInputChange( e, 'year' ) }
					placeholder="yyyy"
					ref={ inputRefs.year }
					onKeyDown={ ( e ) => handleKeyDown( e, 'year' ) }
				/>
			</div>

			{ inputPosition === 'center' && (
				<div
					className="wsx-d-flex wsx-item-center"
					style={
						iconSeparator
							? { padding: '0 8px' }
							: { paddingRight: '8px' }
					}
				>
					{ ! noCross && (
						<div
							className="wsx-icon wsx-icon-cross"
							onClick={ clearDate }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									clearDate;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ Icons.cross }
						</div>
					) }
				</div>
			) }

			{ inputPosition === 'start' && (
				<div
					className="wsx-d-flex wsx-item-center"
					style={
						iconSeparator
							? { padding: '0 8px' }
							: { paddingRight: '8px' }
					}
				>
					<div
						className={ `wsx-icon ${
							! noCalendar && 'wsx-btn-action'
						}` }
						onClick={ () => toggleCalendar( false ) }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								toggleCalendar( false );
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ Icons.calendar }
					</div>
					{ ! noCross && (
						<div
							className="wsx-icon wsx-icon-cross"
							onClick={ clearDate }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									clearDate;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ Icons.cross }
						</div>
					) }
				</div>
			) }

			{ ! noCalendar &&
				ReactDOM.createPortal( renderCalendar, document.body ) }
		</div>
	);
};

export default DateSelector;
