import React from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';
import { __ } from '@wordpress/i18n';

const Input = ( props ) => {
	const {
		label,
		labelColor,
		labelClass,
		isLabelHide,
		type = 'text',
		name,
		value,
		required,
		tooltip,
		tooltipPosition,
		help,
		helpClass,
		inputClass,
		className = '',
		onChange,
		disabled,
		placeholder,
		id,
		iconClass,
		smartTags,
		requiredColor,
	} = props;
	return (
		<div className={ `wsx-input-wrapper ${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label wsx-font-medium ${
						tooltip ? 'wsx-d-flex' : ''
					} ${
						labelColor && `wsx-color-${ labelColor }`
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className="wsx-input-container">
				<input
					{ ...props }
					id={ id || name }
					name={ name }
					type={ type }
					defaultValue={ value }
					onChange={ onChange }
					disabled={ disabled ? true : false }
					placeholder={ placeholder }
					required={ required }
					className={ `wsx-input ${ inputClass }` }
				/>
				{ iconClass && <span className={ iconClass }></span> }
				{ help && (
					<div
						className={ `wsx-input-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
				{ smartTags && (
					<div className="wsx-help-message">
						<div className="wsx-smart-tags-wrapper">
							<div className="wsx-smart-tags-heading">
								<div className="wsx-smart-tags-heading-text">
									{ wholesalex_overview?.i18n?.smartTags ||
										__(
											'Available Smart Tags:',
											'wholesalex'
										) }
								</div>
							</div>
							<div className="wsx-smart-tags">
								{ Object.keys( smartTags ).map( ( tag ) => {
									return (
										<code key={ tag }>
											{ tag } : { smartTags[ tag ] }
										</code>
									);
								} ) }
							</div>
						</div>
					</div>
				) }
			</div>
		</div>
	);
};

export default Input;
