import React from 'react';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';

const RadioButtons = ( props ) => {
	const {
		options,
		label,
		labelSpace,
		name,
		value,
		onChange,
		required,
		isLabelHide,
		className = '',
		tooltip,
		help,
		helpClass,
		tooltipPosition,
		flexView,
	} = props;
	return (
		<div className={ `wsx-radio-field ${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label ${
						labelSpace ? `wsx-mb-${ labelSpace }` : 'wsx-mb-32'
					} wsx-font-medium wsx-color-text-medium ${
						tooltip ? 'wsx-d-flex' : ''
					}` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className="wsx-radio-field-content">
				<div
					className={ `wsx-radio-field-options ${
						flexView ? 'wsx-radio-flex' : ''
					}` }
				>
					{ Object.keys( options ).map( ( option, k ) => (
						<div key={ k } className="wsx-radio-field-option">
							<input
								id={ option }
								name={ name }
								type="radio"
								value={ option }
								defaultChecked={ option === value }
								onChange={ onChange }
								className="wsx-radio"
							/>
							<label
								htmlFor={ option }
								className="wsx-label wsx-option-desc"
							>
								{ ' ' }
								{ options[ option ] }
							</label>
						</div>
					) ) }
				</div>
				{ help && (
					<div
						className={ `wsx-radio-field-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default RadioButtons;
