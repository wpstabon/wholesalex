import React, { useRef, useState } from 'react';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';
import Slider from './Slider';
import { __ } from '@wordpress/i18n';
import '../assets/scss/DragList.scss';

const DragListEdit = ( props ) => {
	const {
		label,
		labelSpace,
		isLock,
		options,
		desc,
		descClass,
		value,
		setValue,
		tooltip,
		tooltipPosition,
		name,
		className,
		iconColor,
		onChange,
	} = props;
	const draggingItem = useRef();
	const dragOverItem = useRef();

	const [ list, setList ] = useState( options );

	const handleDragStart = ( e, position ) => {
		draggingItem.current = position;
	};

	const handleDragEnter = ( e, position ) => {
		dragOverItem.current = position;
		const listCopy = [ ...list ];
		const draggingItemContent = listCopy[ draggingItem.current ];
		listCopy.splice( draggingItem.current, 1 );
		listCopy.splice( dragOverItem.current, 0, draggingItemContent );
		draggingItem.current = dragOverItem.current;
		dragOverItem.current = null;
		setList( listCopy );
	};

	const handleStatusChange = ( e, index ) => {
		const listCopy = [ ...list ];
		listCopy[ index ].status = e.target.checked;
		setList( listCopy );
		onChange && onChange( e, name, listCopy );
	};

	const [ editIndex, setEditIndex ] = useState( null );
	const [ editLabel, setEditLabel ] = useState( '' );

	const handleEditClick = ( index ) => {
		setEditIndex( index );
		setEditLabel( list[ index ].value );
	};

	const handleLabelChange = ( e ) => {
		setEditLabel( e.target.value );
	};

	const handleLabelSave = ( e, index ) => {
		const listCopy = [ ...list ];
		listCopy[ index ].value = editLabel;
		setList( listCopy );
		setEditIndex( null );
		onChange && onChange( e, name, listCopy );
	};

	return (
		<div className={ `wsx-drag-list-field ${ className }` }>
			<div
				className={ `wsx-input-label ${
					labelSpace ? `wsx-mb-${ labelSpace }` : 'wsx-mb-32'
				} wsx-font-medium wsx-color-text-medium ${
					tooltip ? 'wsx-d-flex' : ''
				}` }
			>
				{ label }{ ' ' }
				{ tooltip && (
					<Tooltip
						content={ tooltip }
						direction={ tooltipPosition }
						spaceLeft="8px"
					>
						{ Icons.help }
					</Tooltip>
				) }
			</div>
			<div className="wsx-drag-item-container">
				{ list &&
					list.map( ( item, index ) => (
						<div
							className="wsx-drag-item wsx-justify-space"
							onDragStart={ ( e ) =>
								! isLock && handleDragStart( e, index )
							}
							onDragOver={ ( e ) =>
								! isLock && e.preventDefault()
							}
							onDragEnter={ ( e ) =>
								! isLock && handleDragEnter( e, index )
							}
							onDragEnd={ () => {
								setValue( { ...value, [ name ]: list } );
							} }
							key={ index }
							draggable
						>
							<div className="wsx-d-flex wsx-item-center wsx-gap-12">
								<div
									className={ `wsx-lh-0 ${
										iconColor
											? `wsx-color-${ iconColor }`
											: 'wsx-color-primary'
									} wsx-icon` }
								>
									{ Icons.drag }
								</div>
								{ editIndex === index ? (
									<input
										type="text"
										value={ editLabel }
										onChange={ handleLabelChange }
										style={ {
											minHeight: 'unset',
											padding: '4px 6px',
										} }
										className="wsx-input"
									/>
								) : (
									<Tooltip
										content={ item.value }
										direction="top"
										onlyText={ true }
									>
										<div className="wsx-drag-item-value wsx-ellipsis">
											{ item.value }
										</div>
									</Tooltip>
								) }
								{ editIndex === index ? (
									<Tooltip
										content={ __(
											'Save the changes',
											'wholesalex'
										) }
										direction="top"
									>
										<div
											className="wsx-btn-action"
											onClick={ ( e ) =>
												handleLabelSave( e, index )
											}
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													handleLabelSave( e, index );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ Icons.save }
										</div>
									</Tooltip>
								) : (
									<Tooltip
										content={ __(
											'Edit this header',
											'wholesalex'
										) }
										direction="top"
									>
										<div
											className="wsx-btn-action"
											onClick={ () =>
												handleEditClick( index )
											}
											onKeyDown={ ( e ) => {
												if (
													e.key === 'Enter' ||
													e.key === ' '
												) {
													handleEditClick( index );
												}
											} }
											role="button"
											tabIndex="0"
										>
											{ Icons.edit }
										</div>
									</Tooltip>
								) }
							</div>
							<div className="wsx-drag-list-action">
								<Slider
									key={ index }
									name={ index }
									value={ item.status }
									className="wsx-slider-md"
									onChange={ ( e ) =>
										handleStatusChange( e, index )
									}
									isLabelSide={ true }
								/>
							</div>
						</div>
					) ) }
				{ desc && (
					<div
						className={ `wsx-drag-list-help wsx-help-message ${ descClass }` }
					>
						{ desc }
					</div>
				) }
			</div>
		</div>
	);
};

export default DragListEdit;
