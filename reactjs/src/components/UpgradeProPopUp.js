import React from 'react';
import { __ } from '@wordpress/i18n';
import PopupModal from './PopupModal';
import Button from './Button';

const UpgradeProPopUp = ( {
	UpgradeUrl = '',
	addonCount = '',
	title = __( 'Unlock', 'wholesalex' ),
	onClose,
} ) => {
	if ( UpgradeUrl === '' ) {
		UpgradeUrl = wholesalex?.pro_link;
	}

	const popupContent = () => {
		return (
			<>
				<img
					className="wsx-addon-popup-image"
					src={ wholesalex.url + '/assets/img/unlock.svg' }
					alt="Unlock Icon"
				/>
				<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mt-4">
					<div className="wsx-title wsx-font-18 wsx-font-medium">
						{ title }
					</div>
					{ addonCount && (
						<div className="wsx-title wsx-font-18 wsx-font-medium">
							{ addonCount }+{ ' ' }
							{ __( 'Addons with', 'wholesalex' ) }
						</div>
					) }
				</div>
				<div className="wsx-d-flex wsx-item-center wsx-justify-center wsx-gap-4 wsx-mb-16">
					<div className="wsx-title wsx-font-18 wsx-font-medium">
						{ __( 'WholesaleX', 'wholesalex' ) }
					</div>
					<div className="wsx-title wsx-font-18 wsx-font-medium wsx-color-primary">
						{ __( 'Pro', 'wholesalex' ) }
					</div>
				</div>
				<Button
					buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_addon-upgrade_to_pro&utm_campaign=wholesalex-DB"
					label={ __( 'Upgrade to Pro', 'wholesalex' ) }
					background="secondary"
					iconName="growUp"
					customClass="wsx-w-auto wsx-br-lg wsx-justify-center wsx-font-16"
				/>
			</>
		);
	};

	return (
		<PopupModal
			className="wsx-pro-modal"
			renderContent={ popupContent }
			onClose={ onClose }
		/>
	);
};

export default UpgradeProPopUp;
