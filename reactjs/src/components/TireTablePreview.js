import { useState, useEffect } from 'react';
import '../assets/scss/TireTablePreview.scss';
import Tooltip from './Tooltip';

const TireTablePreview = ( {
	data,
	columns,
	ColumnPriority,
	isHorizontal = false,
	activeRowBackgroundColor = '#6C6CFF',
	activeRowTextColor = '#ffffff',
	tableFontSize = '14px',
	tableTextColor = '#494949',
	tableBackgroundColor = 'transparent',
	isTableBorderRadius = true,
	tableBorderColor = '#E5E5E5',
	headerBackgroundColor = '#F7F7F7',
	headerTextColor = '#3A3A3A',
	isFixedPrice = false,
} ) => {
	const [ sortedColumns, setSortedColumns ] = useState( [] );

	useEffect( () => {
		const sorted = [ ...columns ]
			.filter( ( column ) => {
				const priorityItem = ColumnPriority.find(
					( col ) => col.label === column.key
				);
				return priorityItem && priorityItem.status;
			} )
			.sort( ( a, b ) => {
				const indexA = ColumnPriority.findIndex(
					( col ) => col.label === a.key
				);
				const indexB = ColumnPriority.findIndex(
					( col ) => col.label === b.key
				);
				return indexA - indexB;
			} );
		setSortedColumns( sorted );
	}, [ columns, ColumnPriority ] );

	const tableStyles = {
		fontSize: tableFontSize,
		color: tableTextColor,
		backgroundColor: tableBackgroundColor,
		borderRadius: isTableBorderRadius ? '8px' : '0',
		border: `1px solid ${ tableBorderColor }`,
	};

	const renderVerticalTable = () => (
		<div className="wsx-tire-table vertical" style={ tableStyles }>
			<div
				className="wsx-tire-header"
				style={ {
					backgroundColor: headerBackgroundColor,
					color: headerTextColor,
					borderColor: tableBorderColor,
				} }
			>
				<div className="wsx-tire-row">
					{ sortedColumns.map( ( column ) => (
						<div
							key={ column.key }
							className="wsx-tire-cell wsx-ellipsis"
							style={ { borderColor: tableBorderColor } }
						>
							<Tooltip
								onlyText={ true }
								content={ column.label }
								direction="top"
							>
								<div className="wsx-ellipsis">
									{ column.label }
								</div>
							</Tooltip>
						</div>
					) ) }
				</div>
			</div>
			<div className="wsx-tire-body">
				{ data.map( ( row, rowIndex ) => (
					<div
						key={ rowIndex }
						className="wsx-tire-row"
						style={ {
							backgroundColor:
								rowIndex % 2 === 0
									? 'transparent'
									: activeRowBackgroundColor,
							color:
								rowIndex % 2 === 0
									? tableTextColor
									: activeRowTextColor,
							borderColor: tableBorderColor,
						} }
					>
						{ sortedColumns.map( ( column ) => {
							let displayValue;

							if ( column.key === 'Discount' ) {
								if (
									! isFixedPrice &&
									! row[ column.key ].includes( '$' )
								) {
									displayValue = `${ row[ column.key ] }%`;
								} else if ( isFixedPrice ) {
									displayValue = `${ row[ column.key ] }$`;
								} else {
									displayValue = row[ column.key ];
								}
							} else {
								displayValue = row[ column.key ];
							}

							return (
								<div
									key={ column.key }
									className="wsx-tire-cell"
									style={ { borderColor: tableBorderColor } }
								>
									{ displayValue }
								</div>
							);
						} ) }
					</div>
				) ) }
			</div>
		</div>
	);

	const renderHorizontalTable = () => (
		<div className="wsx-tire-table horizontal" style={ tableStyles }>
			{ sortedColumns.map( ( column ) => (
				<div
					key={ column.key }
					className="wsx-tire-row"
					style={ { borderColor: tableBorderColor } }
				>
					<div
						className="wsx-tire-cell wsx-ellipsis"
						style={ {
							backgroundColor: headerBackgroundColor,
							color: headerTextColor,
							borderColor: tableBorderColor,
						} }
					>
						<Tooltip
							onlyText={ true }
							content={ column.label }
							direction="top"
						>
							<div className="wsx-ellipsis">{ column.label }</div>
						</Tooltip>
					</div>
					<div className="wsx-tire-cells">
						{ data.map( ( row, rowIndex ) => (
							<div
								key={ rowIndex }
								className={ `wsx-tire-cell ${
									column.key === 'pricePerUnit'
										? 'currency'
										: ''
								}` }
								style={ {
									backgroundColor:
										rowIndex % 2 === 0
											? 'transparent'
											: activeRowBackgroundColor,
									color:
										rowIndex % 2 === 0
											? tableTextColor
											: activeRowTextColor,
									borderColor: tableBorderColor,
								} }
							>
								{ column.key === 'discount' &&
								! isFixedPrice &&
								! row[ column.key ].includes( '$' )
									? `${ row[ column.key ] }%`
									: row[ column.key ] }
							</div>
						) ) }
					</div>
				</div>
			) ) }
		</div>
	);

	return isHorizontal ? renderHorizontalTable() : renderVerticalTable();
};

export default TireTablePreview;
