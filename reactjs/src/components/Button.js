import React, { forwardRef, useState } from 'react';
import Icons from '../utils/Icons';

const Button = forwardRef(
	(
		{
			label = '',
			padding = '',
			iconName = '',
			iconClass = '',
			iconColor = '',
			iconPosition = 'before',
			iconGap = '',
			iconAnimation = '',
			iconRotation,
			background = 'transparent',
			onClick = () => {},
			customClass = '',
			disable = false,
			onlyText = false,
			color = '',
			borderColor = '',
			dataTarget = '',
			buttonLink = '',
			sameTab = false,
			smallButton = false,
			style,
		},
		ref
	) => {
		const Icon = Icons[ iconName ] || null;
		const IconPosition =
			iconPosition === 'before' || iconPosition === 'right';

		const [ isActive, setIsActive ] = useState( false );

		const handleClick = ( e ) => {
			e.preventDefault();
			if ( ! disable ) {
				const button = e.currentTarget;
				setIsActive( ! isActive );
				button.style.transform = 'scale(0.95)';
				setTimeout(
					() => ( button.style.transform = 'scale(1)' ),
					150
				);
				if ( buttonLink ) {
					if ( sameTab ) {
						window.location.href = buttonLink;
					} else {
						window.open( buttonLink, '_blank' );
					}
				} else {
					onClick( e );
				}
			}
		};
		let transformValue;

		if ( iconRotation && isActive ) {
			if ( iconRotation === 'full' ) {
				transformValue = 'rotate(180deg)';
			} else if ( iconRotation === 'half' ) {
				transformValue = 'rotate(90deg)';
			} else {
				transformValue = 'rotate(0deg)';
			}
		} else {
			transformValue = 'rotate(0deg)';
		}

		return (
			<div
				ref={ ref }
				className={ `${ onlyText ? 'wsx-btn-text' : 'wsx-btn' } ${
					background ? `wsx-bg-${ background }` : ''
				} ${ color ? `wsx-color-${ color }` : '' } ${
					Icon ? 'wsx-btn-icon' : ''
				} ${ iconGap ? `wsx-gap-${ iconGap }` : '' } ${
					disable ? 'disable' : ''
				} ${
					borderColor
						? `wsx-border-default wsx-bc-${ borderColor }`
						: ''
				} ${ smallButton ? 'wsx-btn-sm' : '' } ${ customClass }` }
				style={ {
					padding,
					...style,
				} }
				onClick={ handleClick }
				data-target={ dataTarget }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						handleClick;
					}
				} }
				role="button"
				tabIndex="0"
			>
				{ IconPosition && Icon && (
					<div
						className={ `wsx-icon ${
							iconAnimation ? `wsx-anim-${ iconAnimation }` : ''
						} ${ iconColor ? `wsx-color-${ iconColor }` : '' } ${
							iconClass ? iconClass : ''
						}` }
						style={ {
							transition: 'transform var(--transition-md)',
							transform: transformValue,
						} }
					>
						{ Icon }
					</div>
				) }
				{ label }
				{ ! IconPosition && Icon && (
					<div
						className={ `wsx-icon ${
							iconAnimation ? `wsx-anim-${ iconAnimation }` : ''
						} ${ iconColor ? `wsx-color-${ iconColor }` : '' } ${
							iconClass ? iconClass : ''
						}` }
						style={ {
							transition: 'transform var(--transition-md)',
							transform: transformValue,
						} }
					>
						{ Icon }
					</div>
				) }
			</div>
		);
	}
);

export default Button;
