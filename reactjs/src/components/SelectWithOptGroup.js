import React, { useState, useEffect, useRef } from 'react';
import Tooltip from './Tooltip';
import ReactDOM from 'react-dom';
import Icons from '../utils/Icons';

const SelectWithOptGroup = ( props ) => {
	let {
		optionGroup,
		label,
		labelColor,
		isLabelHide,
		customClass,
		className,
		iconName = '',
		iconPosition = 'after',
		iconColor = '',
		iconGap = '8px',
		iconRotation = 'full',
		value,
		tooltip,
		tooltipPosition,
		help,
		helpClass,
		containerBackground = 'base1',
		containerBorderRadius = 'md',
		containerBorderColor = 'base2',
		containerBorder = true,
		containerPadding = '4',
		optionBorderBottom = false,
		optionBorderRadius = 'sm',
		optionBorderColor = 'border-light',
		optionCustomClass = '',
		selectedOptionClass = '',
		selectionText = 'Select an option',
		selectionValue = 'default',
		onChange,
		required,
		requiredColor,
		inputBackground,
		inputColor,
		inputPadding,
		borderNone = false,
		borderColor = 'border-primary',
		borderRadius = 'md',
		direction = 'ltr',
		textAlign = '',
		minWidth = '',
		maxWidth = '',
		maxHeight = '',
	} = props;

	! textAlign &&
		( direction === 'ltr'
			? ( textAlign = 'left' )
			: ( textAlign = 'right' ) );

	const dropdownRef = useRef( null );
	const contentRef = useRef( null );

	const position = iconPosition === 'before' || iconPosition === 'left';
	const Icon = iconName && Icons[ iconName ] ? Icons[ iconName ] : null;
	const [ selectedOption, setSelectedOption ] = useState(
		value || selectionText
	);

	const [ isDropdownOpen, setIsDropdownOpen ] = useState( false );
	const [ dropdownPosition, setDropdownPosition ] = useState( {
		width: 0,
		top: 0,
		left: 0,
		isAbove: false,
		isRight: false,
	} );

	const handleOutsideClick = ( e ) => {
		if (
			dropdownRef.current &&
			( dropdownRef.current.contains( e.target ) ||
				( contentRef.current &&
					contentRef.current.contains( e.target ) ) )
		) {
			return;
		}
		setIsDropdownOpen( false );
	};

	const toggleDropdown = () => {
		if ( ! isDropdownOpen ) {
			const rect = dropdownRef.current.getBoundingClientRect();
			const viewportHeight = window.innerHeight;
			const viewportWidth = window.innerWidth;

			setTimeout( () => {
				const contentHeight = contentRef.current
					? contentRef.current.getBoundingClientRect().height
					: 0;
				const contentWidth = contentRef.current
					? contentRef.current.getBoundingClientRect().width
					: 0;

				const isAbove =
					rect.top - contentHeight > 0
						? rect.bottom + contentHeight > viewportHeight
						: false;
				const isRight =
					rect.left - contentWidth > 0
						? rect.left + contentWidth > viewportWidth
						: false;

				setDropdownPosition( {
					width: rect.width,
					top: isAbove
						? rect.top + window.scrollY - contentHeight - 1
						: rect.bottom + window.scrollY + 1,
					left: isRight
						? rect.right + window.scrollX - contentWidth
						: rect.left + window.scrollX,
					isAbove,
					isRight,
				} );
			}, 0 );
		}
		setIsDropdownOpen( ! isDropdownOpen );
	};

	useEffect( () => {
		if ( isDropdownOpen ) {
			document.addEventListener( 'mousedown', handleOutsideClick );
		} else {
			document.removeEventListener( 'mousedown', handleOutsideClick );
		}
		return () => {
			document.removeEventListener( 'mousedown', handleOutsideClick );
		};
	}, [ isDropdownOpen ] );

	const handleOptionSelect = ( optionValue, optionText ) => {
		setSelectedOption( optionText );
		setIsDropdownOpen( false );
		if ( onChange ) {
			onChange( { target: { value: optionValue } } );
		}
	};

	const dropdownContent = isDropdownOpen && (
		<div
			className={ `wsx-option-container wsx-scrollbar wsx-p-${ containerPadding } wsx-bg-${ containerBackground } wsx-br-${ containerBorderRadius } ${
				containerBorder
					? `wsx-border-default wsx-bc-${ containerBorderColor }`
					: ''
			}` }
			ref={ contentRef }
			style={ {
				zIndex: isDropdownOpen ? 999999 : -999999,
				visibility: isDropdownOpen ? 'visible' : 'hidden',
				opacity: isDropdownOpen ? 1 : 0,
				top: isDropdownOpen ? `${ dropdownPosition.top }px` : 0,
				left: isDropdownOpen ? `${ dropdownPosition.left }px` : 0,
				width: `${ dropdownPosition.width - 10 }px`,
			} }
		>
			<div
				onClick={ () =>
					handleOptionSelect( selectionValue, selectionText )
				}
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						handleOptionSelect( selectionValue, selectionText );
					}
				} }
				role="button"
				tabIndex="0"
				className={ `wsx-option-item ${
					optionBorderBottom ? 'wsx-border-bottom' : ''
				} wsx-br-${ optionBorderRadius } wsx-bc-${ optionBorderColor } ${ optionCustomClass }` }
			>
				{ selectionText }
			</div>
			{ Object.keys( optionGroup ).map( ( optGroupKey ) => (
				<div
					key={ optionGroup[ optGroupKey ].label }
					className={ `wsx-option-item-container ${
						optionBorderBottom ? 'wsx-border-bottom' : ''
					} wsx-br-${ optionBorderRadius } wsx-bc-${ optionBorderColor } ${ optionCustomClass }` }
				>
					{ optionGroup[ optGroupKey ].label }
					{ Object.keys( optionGroup[ optGroupKey ].options ).map(
						( option, k ) => (
							<div
								key={ k }
								className={ `wsx-option-item ${
									option !== 'default' &&
									optionGroup[ optGroupKey ].options[
										option
									] === selectedOption
										? 'active'
										: ''
								} ${
									optionBorderBottom
										? 'wsx-border-bottom'
										: ''
								} wsx-br-${ optionBorderRadius } wsx-bc-${ optionBorderColor } ${ optionCustomClass }` }
								onClick={ () =>
									handleOptionSelect(
										option,
										optionGroup[ optGroupKey ].options[
											option
										]
									)
								}
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										handleOptionSelect(
											option,
											optionGroup[ optGroupKey ].options[
												option
											]
										);
									}
								} }
								role="button"
								tabIndex="0"
								value={ option }
							>
								{ optionGroup[ optGroupKey ].options[ option ] }
							</div>
						)
					) }
				</div>
			) ) }
		</div>
	);

	let transformValue;
	if ( isDropdownOpen ) {
		if ( iconRotation === 'full' ) {
			transformValue = 'rotate(180deg)';
		} else if ( iconRotation === 'half' ) {
			transformValue = 'rotate(90deg)';
		} else {
			transformValue = 'rotate(0deg)';
		}
	} else {
		transformValue = 'rotate(0deg)';
	}

	return (
		<div className={ `wsx-select-wrapper  ${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label wsx-font-medium ${
						labelColor && `wsx-color-${ labelColor }`
					}` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div
				className={ `wsx-input-inner-wrapper wsx-bg-${ inputBackground } wsx-color-${ inputColor } wsx-br-${ borderRadius } ${
					borderNone
						? ''
						: `wsx-border-default wsx-bc-${ borderColor }`
				}` }
				ref={ dropdownRef }
				style={ { minWidth, maxWidth, maxHeight } }
			>
				<div
					className={ `wsx-select ${ customClass }` }
					onClick={ toggleDropdown }
					style={ {
						textAlign,
						direction,
						padding: inputPadding ? inputPadding : '10px 16px',
						gap: iconGap,
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							toggleDropdown;
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ position && (
						<div
							className={ `wsx-icon` }
							style={ {
								transition: 'transform var(--transition-md)',
								transform: transformValue,
								color: iconColor ? iconColor : 'unset',
							} }
						>
							{ Icon ? Icon : Icons.angleDown }
						</div>
					) }
					<div
						className={ `wsx-select-value wsx-ellipsis ${ selectedOptionClass }` }
					>
						{ selectedOption }
					</div>
					{ ! position && (
						<div
							className={ `wsx-icon` }
							style={ {
								transition: 'transform var(--transition-md)',
								transform: transformValue,
								color: iconColor ? iconColor : 'unset',
							} }
						>
							{ Icon ? Icon : Icons.angleDown }
						</div>
					) }
					{ ReactDOM.createPortal( dropdownContent, document.body ) }
				</div>
				{ help && (
					<div
						className={ `wsx-select-group-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default SelectWithOptGroup;
