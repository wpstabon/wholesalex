import React, { useEffect, useState } from 'react';
import Icons from '../utils/Icons';

const PopupModal = ( {
	renderContent,
	onClose,
	className = '',
	wrapperClass = '',
} ) => {
	const [ isOpen, setIsOpen ] = useState( false );

	useEffect( () => {
		setIsOpen( true );
	}, [] );

	const handleClose = () => {
		setIsOpen( false );
		setTimeout( onClose, 300 );
	};

	return (
		<div
			className={ `wsx-modal-wrapper ${ className } ${
				isOpen ? 'open' : 'close'
			}` }
		>
			<div className={ `wsx-popup-content-wrapper ${ wrapperClass }` }>
				<div className="wsx-popup-content wsx-scrollbar">
					{ renderContent() }
				</div>
				<div
					className="wsx-modal-close wsx-icon-cross"
					onClick={ handleClose }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							handleClose;
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ Icons.cross }
				</div>
			</div>
		</div>
	);
};

export default PopupModal;
