import React from 'react';
import RadioButtons from './RadioButtons';
import Switch from './Switch';
import Input from './Input';
import ColorPicker from './ColorPicker';
import TextArea from './TextArea';
import DragList from './DragList';
import Choosebox from './Choosebox';
import Shortcode from './Shortcode';
import Slider from './Slider';
import Button from './Button';
import LoadingGif from './LoadingGif';
import Select from './Select';
import '../assets/scss/SideModal.scss';
import '../assets/scss/Setting.scss';

const SettingsModal = ( {
	showModal,
	toggleModal,
	tabData,
	fields,
	saveSettings,
	showLoader,
	getValue,
	getSlideValue,
	settingOnChangeHandler,
	settings,
	setSettings,
	customClass,
	windowRef,
} ) => {
	const isRTL = wholesalex_overview?.is_rtl_support;

	const getOptionsArray = ( options ) => {
		return Object.keys( options ).map( ( option ) => ( {
			value: option,
			label: options[ option ],
		} ) );
	};

	const renderTabData = () => {
		const renderField = ( fieldData, fieldKey ) => {
			switch ( fieldData.type ) {
				case 'select':
					return (
						<Select
							key={ fieldKey }
							wrapperClass="wsx-d-grid wsx-gap-48 wsx-item-center wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							tooltip={ fieldData.tooltip }
							options={ getOptionsArray( fieldData.options ) }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							inputBackground="base1"
							help={ fieldData?.help }
						/>
					);

				case 'switch':
					return (
						<Switch
							key={ fieldKey }
							helpClass="wholesalex_settings_help_text"
							className={ `wholesalex_settings_field ${ fieldData.class }` }
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'slider':
					return (
						<Slider
							key={ fieldKey }
							className="wsx-slider-sm"
							label={ fieldData.label }
							name={ fieldKey }
							value={
								getSlideValue( fieldKey, fieldData ) === 'yes'
									? true
									: false
							}
							onChange={ settingOnChangeHandler }
							tooltip={ fieldData.tooltip }
							isLabelSide={ true }
							help={ fieldData?.help }
							contentClass={
								fieldData.help
									? 'wsx-d-grid wsx-gap-48 wsx-item-center wsx-column-1fr-2fr wsx-sm-d-block'
									: ''
							}
						/>
					);

				case 'radio':
					return (
						<RadioButtons
							key={ fieldKey }
							label={ fieldData.label }
							options={ fieldData.options }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							tooltip={ fieldData.tooltip }
							labelSpace="20"
							flexView={ true }
						/>
					);

				case 'number':
					return (
						<Input
							key={ fieldKey }
							className="wholesalex_settings_field"
							type="number"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'text':
					return (
						<Input
							key={ fieldKey }
							className="wsx-d-grid wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-mt-8 wsx-sm-mt-0 wsx-text-space-break"
							inputClass="wsx-bg-base1"
							type="text"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
							smartTags={ fieldData.smart_tags || false }
						/>
					);

				case 'url':
					return (
						<Input
							key={ fieldKey }
							className="wsx-d-grid wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-mt-8 wsx-sm-mt-0 wsx-text-space-break"
							type="url"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'color':
					return (
						<ColorPicker
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'textarea':
					return (
						<TextArea
							key={ fieldKey }
							className="wsx-d-grid wsx-gap-48 wsx-item-start wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'dragList':
					return (
						<DragList
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							options={ getValue( fieldKey, fieldData ) }
							tooltip={ fieldData.tooltip }
							labelSpace="20"
							className="wsx-card-2 wsx-card-border"
							descClass="wsx-mt-16"
						/>
					);

				case 'choosebox':
					return (
						<Choosebox
							key={ fieldKey }
							label={ fieldData.label }
							name={ fieldKey }
							value={ getValue( fieldKey, fieldData ) }
							onChange={ settingOnChangeHandler }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							options={ fieldData.options }
							tooltip={ fieldData.tooltip }
						/>
					);

				case 'shortcode':
					return (
						<Shortcode
							key={ fieldKey }
							className="wsx-d-grid wsx-gap-48 wsx-item-center wsx-column-1fr-2fr wsx-sm-d-block"
							labelClass="wsx-text-space-break"
							label={ fieldData.label }
							name={ fieldKey }
							value={ settings }
							setValue={ setSettings }
							defaultValue={ fieldData.default }
							desc={ fieldData.desc }
							help={ fieldData.help }
							tooltip={ fieldData.tooltip }
						/>
					);

				default:
					return null;
			}
		};

		const renderAttributes = ( attributes ) => {
			return Object.keys( attributes ).map(
				( fieldKey, index ) =>
					fieldKey !== 'type' && (
						<div
							key={ `settings-field-content-${ index }` }
							className={
								attributes[ fieldKey ].type === 'choosebox'
									? 'wsx-choosebox wsx-settings-item'
									: 'wsx-settings-item'
							}
						>
							{ renderField( attributes[ fieldKey ], fieldKey ) }
						</div>
					)
			);
		};

		const getDynamicClass = ( type, defaultClass ) => {
			const classMap = {
				general_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				general_one: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				general_two:
					'wsx-card-2 wsx-card-border wsx-pt-32 wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-30',
				registration_zero:
					'wsx-card-2 wsx-card-border wsx-item-divider-wrapper',
				registration_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				price_zero:
					'wsx-card-2 wsx-card-border wsx-pt-32 wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-20',
				price_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				price_two: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				price_three:
					'wsx-card-2 wsx-card-border wsx-pb-32 wsx-d-flex wsx-flex-column wsx-gap-30',
				convertions_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				dynamic_rules_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				language_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				design_zero: 'wsx-design_zero',
				recaptcha_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				white_label_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				bulk_order_zero: 'wsx-d-flex wsx-flex-column wsx-gap-30',
				bulk_order_one: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				sub_account_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				wallet_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				dokan_wholesalex_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
				wcfm_wholesalex_zero: 'wsx-d-flex wsx-flex-column wsx-gap-40',
			};

			return classMap[ type ] || defaultClass;
		};

		// Usage
		const dynamicClassAttr = () =>
			getDynamicClass( tabData?.attr?.type, 'wsx-setting-zero' );
		const dynamicClassOne = () =>
			getDynamicClass( tabData?.attrGroupOne?.type, 'wsx-setting-one' );
		const dynamicClassTwo = () =>
			getDynamicClass( tabData?.attrGroupTwo?.type, 'wsx-setting-two' );
		const dynamicClassThree = () =>
			getDynamicClass(
				tabData?.attrGroupThree?.type,
				'wsx-setting-three'
			);
		return (
			<div className="wsx-side-menu-body">
				<div className="wsx-side-menu-header">
					<span className="wsx-font-medium wsx-font-16 wsx-color-text-medium">
						{ tabData?.label }
					</span>
					<Button
						label={ fields._save?.label }
						onClick={ saveSettings }
						background="positive"
						customClass="wsx-font-14"
					/>
				</div>
				<div className="wsx-side-menu-content wsx-scrollbar wsx-relative">
					{ showLoader && <LoadingGif insideContainer={ true } /> }
					{
						<div className={ dynamicClassAttr() }>
							{ tabData?.attr &&
								renderAttributes( tabData.attr ) }
						</div>
					}
					{ tabData?.attrGroupOne && (
						<div className={ dynamicClassOne() }>
							{ renderAttributes( tabData.attrGroupOne ) }
						</div>
					) }
					{ tabData?.attrGroupTwo && (
						<div className={ dynamicClassTwo() }>
							{ renderAttributes( tabData.attrGroupTwo ) }
						</div>
					) }
					{ tabData?.attrGroupThree && (
						<div className={ dynamicClassThree() }>
							{ renderAttributes( tabData.attrGroupThree ) }
						</div>
					) }
				</div>
			</div>
		);
	};

	return (
		showModal && (
			<div
				className={ `wsx-side-modal-wrapper ${ customClass }` }
				onClick={ toggleModal }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						toggleModal;
					}
				} }
				role="button"
				tabIndex="0"
			>
				<div
					className="wsx-side-modal-container"
					onClick={ ( e ) => e.stopPropagation() }
					ref={ windowRef }
					style={ {
						opacity: 0,
						transform: `translateX(${ isRTL ? '-50%' : '50%' })`,
						transition: 'all var(--transition-md) ease-in-out',
					} }
					onKeyDown={ ( e ) => {
						if ( e.key === 'Enter' || e.key === ' ' ) {
							e.stopPropagation();
						}
					} }
					role="button"
					tabIndex="0"
				>
					{ renderTabData() }
				</div>
			</div>
		)
	);
};

// Defining prop types for better reusability
// SettingsModal.propTypes = {
// 	showModal: PropTypes.bool.isRequired,
// 	toggleModal: PropTypes.func.isRequired,
// 	name: PropTypes.string,
// 	activeFields: PropTypes.array,
// 	tabData: PropTypes.object,
// 	fields: PropTypes.object,
// 	saveSettings: PropTypes.func.isRequired,
// 	showLoader: PropTypes.bool,
// 	getValue: PropTypes.func.isRequired,
// 	settingOnChangeHandler: PropTypes.func.isRequired,
// 	settings: PropTypes.object,
// 	setSettings: PropTypes.func.isRequired,
// };

export default SettingsModal;
