import React from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';

const Choosebox = ( props ) => {
	const {
		label,
		labelClass,
		labelColor,
		isLabelSide,
		className = '',
		name,
		options,
		value,
		tooltip,
		tooltipPosition,
		onChange,
	} = props;

	return (
		<div className="wsx-choose-box-field">
			{ ! isLabelSide && label && (
				<div
					className={ `wsx-input-label wsx-font-medium ${
						tooltip ? 'wsx-d-flex' : ''
					} ${
						labelColor && `wsx-color-${ labelColor }`
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className={ `wsx-choose-box-options ${ className }` }>
				{ Object.keys( options ).map( ( option, k ) => {
					const _lockField = option.startsWith( 'pro_' );
					return (
						<label
							className="wsx-label "
							htmlFor={ option }
							key={ `wsx-choose-box-${ k }` }
							id={ value === option ? 'choose-box-selected' : '' }
							style={ {
								position: `${ _lockField && 'relative' }`,
							} }
						>
							<input
								className="wsx-input"
								id={ option }
								name={ name }
								type="radio"
								value={ option }
								defaultChecked={ value === option }
								onChange={ onChange }
							/>
							<img
								className={ `wsx-choose-box-image ${
									_lockField ? 'locked' : ''
								}` }
								src={ options[ option ] }
								alt={ option }
							/>
							{ _lockField && (
								<span
									className="wsx-icon"
									style={ {
										position: 'absolute',
										top: '6px',
										right: '6px',
									} }
								>
									{ Icons.lock }
								</span>
							) }
						</label>
					);
				} ) }
			</div>
		</div>
	);
};

export default Choosebox;
