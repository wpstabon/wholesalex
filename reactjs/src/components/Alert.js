import React from 'react';
import Icons from '../utils/Icons';
import { __ } from '@wordpress/i18n';
import '../assets/scss/Alert.scss';

const Alert = ( {
	title,
	description,
	onClose,
	onConfirm,
	confirmText = __( 'OK', 'wholesalex' ),
	cancelText = __( 'Cancel', 'wholesalex' ),
} ) => {
	const handleClose = () => {
		if ( onClose ) {
			onClose();
		}
	};

	const handleConfirm = () => {
		if ( onConfirm ) {
			onConfirm();
		}
	};

	return (
		<div className="wsx-alert-popup-overlay">
			<div className="wsx-alert-popup">
				<div className="wsx-alert-header">
					<span className="wsx-alert-title-wrapper">
						<span
							style={ {
								lineHeight: 0,
								transform: 'rotate(180deg)',
								display: 'inline-block',
							} }
						>
							{ Icons.information }
						</span>
						<span>{ title }</span>
					</span>
					<div
						className="wsx-modal-close"
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								handleClose;
							}
						} }
						onClick={ handleClose }
						role="button"
						tabIndex="0"
					>
						{ Icons.cross }
					</div>
				</div>
				<div className="wsx-alert-body">
					<div>{ description }</div>
				</div>
				<div className="wsx-alert-footer">
					{ onConfirm && (
						<div
							className="wsx-btn wsx-btn-sm wsx-bg-approve"
							onClick={ handleConfirm }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									handleConfirm;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ confirmText }
						</div>
					) }
					<div
						className="wsx-btn wsx-btn-sm wsx-bg-tertiary"
						onClick={ handleClose }
						onKeyDown={ ( e ) => {
							if ( e.key === 'Enter' || e.key === ' ' ) {
								handleClose();
							}
						} }
						role="button"
						tabIndex="0"
					>
						{ cancelText }
					</div>
				</div>
			</div>
		</div>
	);
};

export default Alert;
