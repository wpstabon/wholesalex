import '../assets/scss/TierClassicTablePreview.scss';

const TierClassicTablePreview = ( {
	data,
	activeRowBackgroundColor = '#6C6CFF',
	activeRowTextColor = '#ffffff',
	tableFontSize = '14px',
	tableTextColor = '#494949',
	isTableBorderRadius = true,
	discountTextColor = '#ffffff',
	discountBackgroundColor = '#070707',
	tableBorderColor = '#E2E4E9',
	headerBackgroundColor = '#F7F7F7',
	headerTextColor = '#6C6CFF',
	isClassicalHorizontalTable = false,
	selectedItem = null,
} ) => {
	const renderClassicalHorizontalTable = () => (
		<div
			className="wsx-tire-classical-horizontal wsx-d-flex wsx-flex-wrap wsx-gap-12"
			style={ {
				backgroundColor: headerBackgroundColor,
				borderRadius: isTableBorderRadius ? '8px' : '0',
			} }
		>
			{ data.map( ( item, index ) => (
				<div className="wsx-tire-list-item" key={ index }>
					<div
						className={ `wsx-tire-list-content` }
						style={ { borderColor: tableBorderColor } }
					>
						<div className="wsx-tire-price-tag wsx-d-flex wsx-item-center wsx-gap-6 wsx-mb-8">
							<span
								className="price wsx-font-medium"
								style={ {
									fontSize: `${ tableFontSize }px`,
									color:
										selectedItem === index
											? activeRowTextColor
											: headerTextColor,
								} }
							>
								{ item.pricePerUnit }
							</span>
							{ item.discount && (
								<span
									className="discount-badge"
									style={ {
										color: discountTextColor,
										backgroundColor:
											discountBackgroundColor,
										borderRadius: isTableBorderRadius
											? '2px'
											: '0',
										fontSize: `${ tableFontSize - 4 }px`,
									} }
								>
									-{ item.discount }%
								</span>
							) }
						</div>
						<div
							className="wsx-tire-pieces"
							style={ {
								color:
									selectedItem === index
										? activeRowTextColor
										: tableTextColor,
								fontSize: `${ tableFontSize - 2 }px`,
							} }
						>
							<span className="wsx-font-bold">
								{ item.quantityRange }
							</span>{ ' ' }
							Pieces
						</div>
						{ selectedItem === index && (
							<div
								className="wsx-tier-selected-overlay"
								style={ {
									backgroundColor: activeRowBackgroundColor,
									borderRadius: isTableBorderRadius
										? '8px'
										: '0',
								} }
							></div>
						) }
					</div>
				</div>
			) ) }
		</div>
	);

	const renderClassicalVerticalTable = () => (
		<div
			className="wsx-tire-classical-vertical"
			style={ {
				backgroundColor: headerBackgroundColor,
				borderRadius: isTableBorderRadius ? '8px' : '0',
			} }
		>
			{ data.map( ( item, index ) => (
				<div
					key={ index }
					className={ `wsx-tire-list-item wsx-d-flex wsx-item-center wsx-gap-12` }
					style={ {
						backgroundColor:
							selectedItem === index
								? activeRowBackgroundColor
								: 'transparent',
						borderRadius: isTableBorderRadius ? '4px' : '0',
					} }
				>
					<div
						className="price wsx-font-medium"
						style={ {
							color:
								selectedItem === index
									? activeRowTextColor
									: headerTextColor,
							fontSize: tableFontSize,
						} }
					>
						{ item.pricePerUnit }
					</div>
					<div
						className="separator"
						style={ {
							color:
								selectedItem === index
									? activeRowTextColor
									: tableTextColor,
							fontSize: tableFontSize - 2,
						} }
					>
						/
					</div>
					<div
						className="pieces"
						style={ {
							color:
								selectedItem === index
									? activeRowTextColor
									: tableTextColor,
							fontSize: tableFontSize - 2,
						} }
					>
						<span className="wsx-font-bold">
							{ item.quantityRange }
						</span>{ ' ' }
						Pieces
					</div>
				</div>
			) ) }
		</div>
	);

	return isClassicalHorizontalTable
		? renderClassicalVerticalTable()
		: renderClassicalHorizontalTable();
};

export default TierClassicTablePreview;
