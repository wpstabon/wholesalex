import React from 'react';
import Icons from '../utils/Icons';
import Tooltip from '../components/Tooltip';

const Search = ( props ) => {
	const {
		label,
		labelColor,
		labelClass,
		type,
		name,
		value,
		required,
		requiredColor,
		tooltip,
		tooltipPosition,
		help,
		helpClass,
		inputClass,
		className = '',
		onChange,
		isDisable,
		placeholder,
		isLabelHide,
		id,
		iconName,
		iconColor,
		background,
		borderColor,
		maxHeight,
	} = props;

	const Icon = iconName && Icons[ iconName ] ? Icons[ iconName ] : null;

	return (
		<div className={ `${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label wsx-font-medium ${
						tooltip ? 'wsx-d-flex' : ''
					} ${
						labelColor && `wsx-color-${ labelColor }`
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div
				className={ `wsx-input-wrapper-with-icon ${
					background ? `wsx-bg-${ background }` : ''
				}` }
				style={ {
					borderColor: borderColor ? borderColor : 'unset',
					maxHeight: maxHeight ? maxHeight : 'unset',
				} }
			>
				<input
					key={ `${ name }}` }
					id={ id || name }
					name={ name }
					type={ type }
					value={ value }
					onChange={ onChange }
					disabled={ isDisable ? true : false }
					placeholder={ placeholder }
					required={ required }
					className={ `wsx-input ${ inputClass }` }
				/>
				{ Icon && (
					<span
						className="wsx-lh-0 wsx-icon wsx-icon-right"
						style={ {
							color: iconColor ? iconColor : 'unset',
							borderColor: borderColor ? borderColor : 'unset',
						} }
					>
						{ Icon }
					</span>
				) }
				{ help && (
					<div
						className={ `wsx-search-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default Search;
