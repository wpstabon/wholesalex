import React, { useState } from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';

const Shortcode = ( props ) => {
	const { className, label, labelClass, defaultValue, value, tooltip } =
		props;
	const defValue = value?.name || defaultValue;
	const [ isCopied, setIsCopies ] = useState( false );

	const copyToClipboard = async ( text ) => {
		if ( ! text ) {
			return;
		}
		setIsCopies( false );
		try {
			if ( navigator.clipboard && window.isSecureContext ) {
				await navigator.clipboard.writeText( text );
			} else {
				fallBackCopyToClipboard( text );
			}
			setIsCopies( true );
		} catch ( err ) {
			// err
		}
	};

	// Fallback for older browsers
	const fallBackCopyToClipboard = ( text ) => {
		const textarea = document.createElement( 'textarea' );
		textarea.value = text;
		document.body.appendChild( textarea );
		textarea.select();
		try {
			document.execCommand( 'copy' );
		} catch ( err ) {
			// err
		}
		document.body.removeChild( textarea );
	};

	return (
		<div className={ `wsx-shortcode-field ${ className }` }>
			{ label && (
				<div
					className={ `wsx-input-label ${
						tooltip ? 'wsx-d-flex' : ''
					} ${ labelClass }` }
				>
					{ label }
					{ tooltip && (
						<Tooltip content={ tooltip } spaceLeft="8px">
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }

			<div
				className="wsx-shortcode-field-content"
				onClick={ () => copyToClipboard( defValue ) }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						copyToClipboard( defValue );
					}
				} }
				role="button"
				tabIndex="0"
			>
				<span
					className={ `wsx-get-shortcode-text ${
						tooltip ? 'wsx-d-flex' : ''
					}` }
				>
					{ defValue }
				</span>
				<Tooltip
					content={ isCopied ? 'Copied' : 'Copy to Clipboard' }
					spaceLeft="8px"
				>
					{ Icons.doc }
				</Tooltip>
			</div>
		</div>
	);
};
export default Shortcode;
