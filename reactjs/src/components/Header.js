import React, { useRef, useState, useLayoutEffect } from 'react';
import Dropdown from './Dropdown';
import { Link } from '../contexts/RouterContext';
import Button from './Button';
import { __ } from '@wordpress/i18n';

const Header = ( { isFrontend } ) => {
	const menuRef = useRef( null );
	const linkRefs = useRef( [] );
	const [ exceedingIndex, setExceedingIndex ] = useState( 0 );

	useLayoutEffect( () => {
		let tempIndex = -1;
		const calculateWidths = () => {
			let totalLinkWidth = 0;
			if ( menuRef.current ) {
				const menuWidth = menuRef.current.offsetWidth;
				if ( linkRefs.current ) {
					for (
						let index = 0;
						index < linkRefs.current.length;
						index++
					) {
						const linkRef = linkRefs.current[ index ];
						if ( linkRef ) {
							totalLinkWidth += linkRef.offsetWidth + 40;

							if (
								( tempIndex === -1 &&
									totalLinkWidth > menuWidth ) ||
								( tempIndex !== -1 &&
									totalLinkWidth + 64 > menuWidth )
							) {
								tempIndex = index;
								setExceedingIndex( Math.max( index - 1, 1 ) );
								break;
							} else if (
								index + 1 ===
								linkRefs.current.length - 1
							) {
								tempIndex = -1;
								setExceedingIndex( 0 );
							}
						}
					}
				}
			}
		};

		calculateWidths();

		window.addEventListener( 'resize', calculateWidths );
		return () => window.removeEventListener( 'resize', calculateWidths );
	}, [] );

	const menuItems = [
		{ to: '/', label: 'Dashboard' },
		{ to: '/dynamic-rules', label: 'Dynamic Rules' },
		{ to: '/user-role', label: 'User Roles' },
		{ to: '/registration', label: 'Registration Form' },
		{ to: '/addons', label: 'Addons' },
		{ to: '/users', label: 'Users' },
		{ to: '/emails', label: 'Emails' },
		{ to: '/settings', label: 'Settings' },
		// { to: '/quick-support', label: 'Quick Support' },
		// { to: '/conversation', label: 'Conversation' },
		// { to: '/features', label: 'Features' },
		// { to: '/license', label: 'License' },
	];

	const helpLinks = [
		{
			iconClass: 'dashicons-phone',
			label: 'Get Supports',
			link: 'https://getwholesalex.com/contact/?utm_source=wholesalex-menu&utm_medium=features_page-support&utm_campaign=wholesalex-DB',
		},
		{
			iconClass: 'dashicons-book',
			label: 'Getting Started Guide',
			link: 'https://getwholesalex.com/docs/wholesalex/getting-started/?utm_source=wholesalex-menu&utm_medium=features_page-guide&utm_campaign=wholesalex-DB',
		},
		{
			iconClass: 'dashicons-facebook-alt',
			label: 'Join Community',
			link: 'https://www.facebook.com/groups/wholesalexcommunity',
		},
		{
			iconClass: 'dashicons-book',
			label: 'Feature Request',
			link: 'https://getwholesalex.com/roadmap/?utm_source=wholesalex-menu&utm_medium=features_page-feature_request&utm_campaign=wholesalex-DB',
		},
		{
			iconClass: 'dashicons-youtube',
			label: 'Youtube Tutorials',
			link: 'https://www.youtube.com/@WholesaleX',
		},
		{
			iconClass: 'dashicons-book',
			label: 'Documentation',
			link: 'https://getwholesalex.com/documentation/?utm_source=wholesalex-menu&utm_medium=features_page-documentation&utm_campaign=wholesalex-DB',
		},
		{
			iconClass: 'dashicons-edit',
			label: 'What’s New',
			link: 'https://getwholesalex.com/roadmap/?utm_source=wholesalex-menu&utm_medium=features_page-what’s_new&utm_campaign=wholesalex-DB',
		},
	];

	const renderHelpDropdownContent = () => {
		return (
			<ul className="wsx-list wsx-d-flex wsx-flex-column wsx-gap-16">
				{ helpLinks.map( ( help ) => {
					return (
						<li className="wsx-list-item" key={ help.link }>
							<a
								href={ help.link }
								className="wsx-link wsx-list-link wsx-d-flex wsx-item-center wsx-gap-8"
								target="_blank"
								rel="noreferrer"
							>
								<span
									className={ `dashicons ${ help.iconClass } wsx-list-icon` }
								></span>
								<span className="wsx-list-label">
									{ help.label }
								</span>
							</a>
						</li>
					);
				} ) }
			</ul>
		);
	};

	const headerDropdownContent = () => {
		return (
			<div
				className="wsx-height-full wsx-width-70v wsx-width-360"
				onClick={ ( e ) => e.stopPropagation() }
				onKeyDown={ ( e ) => {
					if ( e.key === 'Enter' || e.key === ' ' ) {
						e.stopPropagation();
					}
				} }
				role="button"
				tabIndex="0"
			>
				<div className="wsx-logo wsx-justify-end">
					<img
						src={ wholesalex?.logo_url }
						className="wsx-logo"
						alt="wholesalex_logo"
					/>
					{ ! isFrontend && (
						<span className="wsx-version">{ `v${ wholesalex.current_version }` }</span>
					) }
				</div>
				<div className="wsx-menu wsx-flex-column wsx-flex-nowrap wsx-gap-24 wsx-mt-48 wsx-scrollbar">
					{ menuItems.map( ( item, index ) => (
						<Link navLink={ true } key={ index } to={ item.to }>
							{ item.label }
						</Link>
					) ) }
				</div>
			</div>
		);
	};

	return (
		<div className="wsx-header-wrapper">
			<div className="wsx-header">
				<div className="wsx-logo wsx-lg-d-none">
					<img
						src={ wholesalex?.logo_url }
						className="wsx-logo"
						alt="wholesalex_logo"
					/>
					{ ! isFrontend && (
						<span className="wsx-version">{ `v${ wholesalex.current_version }` }</span>
					) }
				</div>

				<div
					className="wsx-d-none wsx-lg-d-block wsx-icon"
					style={ { width: '40px', marginLeft: '-15px' } }
				>
					<Dropdown
						iconClass="wsx-hamburger-icon"
						contentClass="wsx-header-side-menu"
						iconName="menu"
						iconRotation="half"
						iconColor="tertiary"
						renderContent={ headerDropdownContent }
					/>
				</div>

				<div className="wsx-header-content wsx-lg-justify-end">
					<div className="wsx-menu wsx-lg-d-none" ref={ menuRef }>
						{ exceedingIndex !== 0 && (
							<>
								{ menuItems
									.slice( 0, exceedingIndex )
									.map( ( item, index ) => {
										return (
											<Link
												ref={ ( el ) =>
													( linkRefs.current[
														index
													] = el )
												}
												navLink={ true }
												key={ index }
												to={ item.to }
											>
												{ item.label }
											</Link>
										);
									} ) }
								<Dropdown
									title={ __( 'More', 'wholesalex' ) }
									contentClass="wsx-down-4 wsx-pt-12 wsx-pb-12"
									renderContent={ () => (
										<div className="wsx-d-flex wsx-flex-column wsx-gap-12">
											{ menuItems
												.slice( exceedingIndex )
												.map( ( item, index ) => {
													return (
														<Link
															ref={ ( el ) =>
																( linkRefs.current[
																	exceedingIndex +
																		index
																] = el )
															}
															navLink={ true }
															key={ index }
															to={ item.to }
														>
															{ item.label }
														</Link>
													);
												} ) }
										</div>
									) }
								/>
							</>
						) }
						{ exceedingIndex === 0 &&
							menuItems.map( ( item, index ) => (
								<Link
									ref={ ( el ) =>
										( linkRefs.current[ index ] = el )
									}
									navLink={ true }
									key={ index }
									to={ item.to }
								>
									{ item.label }
								</Link>
							) ) }
					</div>

					<div className="wsx-btn-group">
						{ ! wholesalex?.is_pro_active && (
							<Button
								label="Upgrade to Pro"
								iconName="growUp"
								background="secondary"
								customClass="wsx-text-space-nowrap"
								buttonLink="https://getwholesalex.com/pricing/?utm_source=wholesalex-menu&utm_medium=email-unlock_addon-upgrade_to_pro&utm_campaign=wholesalex-DB"
							/>
						) }
						{ ! isFrontend && (
							<Dropdown
								iconName="help"
								iconRotation="none"
								iconColor="tertiary"
								renderContent={ renderHelpDropdownContent }
								contentClass="wsx-down-4 wsx-right wsx-text-space-nowrap"
							/>
						) }
					</div>
				</div>
			</div>
		</div>
	);
};

export default Header;
