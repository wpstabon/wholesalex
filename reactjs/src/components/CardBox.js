import React from 'react';
import Icons from '../utils/Icons';

const CardBox = ( { icon, title, description, linkText, linkUrl } ) => {
	return (
		<div className="wsx-card wsx-mb-0">
			<div className="wsx-card-item-heading">
				<img src={ icon } alt={ title } />
				<div className="wsx-title wsx-font-18">{ title }</div>
			</div>
			<div className="wsx-card-item-content">
				<div className="wsx-card-item-description">{ description }</div>
				<a
					className="wsx-link wsx-btn-group wsx-gap-6"
					href={ linkUrl }
				>
					{ linkText }{ ' ' }
					<div className="wsx-icon wsx-rtl-arrow">
						{ Icons.arrowRight }
					</div>
				</a>
			</div>
		</div>
	);
};

export default CardBox;
