import React, { useRef } from 'react';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';

const Slider = ( props ) => {
	let {
		label,
		name,
		isLabelHide,
		isLabelSide,
		labelClass,
		isDisable,
		required,
		value,
		onChange,
		help,
		helpClass,
		className,
		isLock,
		tooltip,
		tooltipPosition,
		sliderClass,
		activeBackground,
		inputBackground,
		thumbColor,
		labelColor,
		requiredColor,
		contentClass,
	} = props;

	if ( ! name ) {
		name = `wsx-slider-${ Date.now().toString() }`;
	}

	const trackRef = useRef( null );

	return (
		<div className={ `wsx-slider-wrapper ${ className }` }>
			{ ! isLabelHide && ! isLabelSide && label && (
				<div
					className={ `wsx-input-label ${
						tooltip ? 'wsx-d-flex wsx-item-center' : ''
					} ${
						labelColor && `wsx-color-${ labelColor }`
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ required && (
						<div
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</div>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
							className="wsx-rtl-tooltip-left"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className={ `wsx-slider-content ${ contentClass }` }>
				<div className="wsx-d-flex wsx-item-center wsx-gap-8">
					<label
						htmlFor={ name }
						className={ `wsx-label wsx-slider ${
							value && value !== 'no' && 'active'
						} ${ sliderClass }` }
					>
						<div
							ref={ trackRef }
							className="wsx-slider-track wsx-relative"
							style={ {
								backgroundColor:
									value && value !== 'no'
										? activeBackground ||
										  inputBackground ||
										  '#6C6CFF'
										: inputBackground || '#E2E4E9',
							} }
						>
							<input
								name={ name }
								id={ name }
								type="checkbox"
								checked={
									value && value !== 'no' ? true : false
								}
								onChange={ onChange }
								className="wsx-slider-input"
								disabled={ isDisable ? true : false }
							/>
							<div
								className="wsx-slider-thumb"
								style={ {
									backgroundColor: thumbColor || '#ffffff',
								} }
							/>
						</div>
						{ ! isLabelHide && isLabelSide && label && (
							<div
								className={ `wsx-input-label wsx-mb-0 ${
									tooltip ? 'wsx-d-flex wsx-item-center' : ''
								} ${
									labelColor && `wsx-color-${ labelColor }`
								} ${ labelClass }` }
							>
								{ label }{ ' ' }
								{ required && (
									<div
										className="wsx-required"
										style={ {
											color: requiredColor || '#fc143f',
										} }
									>
										*
									</div>
								) }
								{ tooltip && (
									<Tooltip
										content={ tooltip }
										direction={ tooltipPosition }
										spaceLeft="8px"
										className="wsx-rtl-tooltip-left"
									>
										{ Icons.help }
									</Tooltip>
								) }
							</div>
						) }
					</label>
					{ isLock && ( ! value || 'no' === value ) && (
						<span className="wsx-lh-0">{ Icons.lock }</span>
					) }
				</div>
				{ help && (
					<div
						className={ `wsx-slider-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default Slider;
