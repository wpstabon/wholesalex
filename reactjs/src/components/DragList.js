import React, { useRef, useState } from 'react';
import Tooltip from './Tooltip';
import Icons from '../utils/Icons';
import '../assets/scss/DragList.scss';

const DragList = ( props ) => {
	const {
		label,
		labelSpace,
		isLock,
		options,
		desc,
		descClass,
		value,
		setValue,
		tooltip,
		tooltipPosition,
		name,
		className,
		iconColor,
	} = props;
	const draggingItem = useRef();
	const dragOverItem = useRef();

	const [ list, setList ] = useState( options );

	const handleDragStart = ( e, position ) => {
		draggingItem.current = position;
	};

	const handleDragEnter = ( e, position ) => {
		dragOverItem.current = position;
		const listCopy = [ ...list ];
		const draggingItemContent = listCopy[ draggingItem.current ];
		listCopy.splice( draggingItem.current, 1 );
		listCopy.splice( dragOverItem.current, 0, draggingItemContent );
		draggingItem.current = dragOverItem.current;
		dragOverItem.current = null;
		setList( listCopy );
	};

	return (
		<div className={ `wsx-drag-list-field ${ className }` }>
			<div
				className={ `wsx-input-label ${
					labelSpace ? `wsx-mb-${ labelSpace }` : 'wsx-mb-32'
				} wsx-font-medium wsx-color-text-medium ${
					tooltip ? 'wsx-d-flex' : ''
				}` }
			>
				{ label }{ ' ' }
				{ tooltip && (
					<Tooltip
						content={ tooltip }
						direction={ tooltipPosition }
						spaceLeft="8px"
					>
						{ Icons.help }
					</Tooltip>
				) }
			</div>
			<div className="wsx-drag-item-container">
				{ list &&
					list.map( ( item, index ) => (
						<div
							className="wsx-drag-item"
							onDragStart={ ( e ) =>
								! isLock && handleDragStart( e, index )
							}
							onDragOver={ ( e ) =>
								! isLock && e.preventDefault()
							}
							onDragEnter={ ( e ) =>
								! isLock && handleDragEnter( e, index )
							}
							onDragEnd={ () => {
								setValue( { ...value, [ name ]: list } );
							} }
							key={ index }
							draggable
						>
							<span
								className={ `wsx-lh-0 ${
									iconColor
										? `wsx-color-${ iconColor }`
										: 'wsx-color-primary'
								} wsx-icon` }
							>
								{ Icons.drag }
							</span>
							{ item
								.replace( '_', ' ' )
								.charAt( 0 )
								.toUpperCase() +
								item.replace( '_', ' ' ).slice( 1 ) }
						</div>
					) ) }
				{ desc && (
					<div
						className={ `wsx-drag-list-help wsx-help-message ${ descClass }` }
					>
						{ desc }
					</div>
				) }
			</div>
		</div>
	);
};

export default DragList;
