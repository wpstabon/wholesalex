import React, { useEffect, useRef, useState } from 'react';
import { __ } from '@wordpress/i18n';

import Icons from '../utils/Icons';
import Tooltip from './Tooltip';

const MultiSelect = ( {
	name,
	value,
	options,
	placeholder,
	customClass,
	onMultiSelectChangeHandler,
	isDisable,
} ) => {
	const [ showList, setShowList ] = useState( false );
	const [ selectedOptions, setSelectedOptions ] = useState( value );
	const [ optionList, setOptionList ] = useState( options );
	const [ searchValue, setSearchValue ] = useState( '' );
	const myRef = useRef();

	const onSearch = ( key ) => {
		let selectedOptionValues = [];
		if ( selectedOptions.length > 0 ) {
			selectedOptionValues = selectedOptions.map(
				( option ) => option.value
			);
		}
		const searchResult = options.filter( ( option ) => {
			const { name: optionName, value: optionValue } = option;
			return (
				optionName.toLowerCase().includes( key.toLowerCase() ) &&
				selectedOptionValues.indexOf( optionValue ) === -1
			);
		} );
		return searchResult;
	};

	const onInputChangeHandler = ( e ) => {
		setShowList( false );
		setSearchValue( e.target.value );
		setOptionList( onSearch( e.target.value ) );
		setShowList( true );
	};

	const optionClickHandler = ( option ) => {
		setShowList( false );
		setSelectedOptions( [ ...selectedOptions, option ] );
		setSearchValue( '' );
		setOptionList( onSearch( '' ) );
		onMultiSelectChangeHandler( name, [ ...selectedOptions, option ] );
	};

	const onDeleteOption = ( indexToBeDeleted ) => {
		setShowList( false );
		const selectedOptionAfterDeleted = selectedOptions.filter(
			( val, index ) => {
				return index !== indexToBeDeleted;
			}
		);
		setSelectedOptions( selectedOptionAfterDeleted );
		setOptionList( onSearch( '' ) );
		onMultiSelectChangeHandler( name, selectedOptionAfterDeleted );
		setShowList( true );
	};

	const handleClickOutside = ( e ) => {
		if ( myRef?.current && ! myRef.current.contains( e.target ) ) {
			setShowList( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	useEffect( () => {
		setSelectedOptions( value );
	}, [ value ] );

	useEffect( () => {
		setOptionList( options );
		onMultiSelectChangeHandler( name, selectedOptions );
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ options, name ] );

	return (
		<div
			className={ `wsx-multiselect-wrapper ${
				isDisable ? 'locked' : ''
			}` }
			key={ `wsx-multiselect-${ name }` }
		>
			<div className="wsx-multiselect-inputs">
				<div className="wsx-multiselect-input-wrapper">
					{ selectedOptions.length > 0 &&
						selectedOptions.map( ( option, index ) => {
							return (
								<span
									key={ `wsx-multiselect-opt-${ name }-${ option.value }-${ index }` }
									className="wsx-selected-option"
								>
									<span
										tabIndex={ -1 }
										className="wsx-icon-cross wsx-lh-0"
										onClick={ () =>
											onDeleteOption( index )
										}
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												onDeleteOption( index );
											}
										} }
										role="button"
									>
										{ Icons.cross }
									</span>
									<Tooltip
										content={ option.name }
										position="top"
										onlyText={ true }
									>
										<div className="multiselect-option-name">
											{ option.name }
										</div>
									</Tooltip>
								</span>
							);
						} ) }
					<div
						className={ `wsx-multiselect-option-wrapper ${
							selectedOptions.length &&
							selectedOptions.length !== 0
								? ''
								: 'wsx-w-full'
						}` }
					>
						<input
							key={ `wsx-input-${ name }` }
							disabled={ isDisable ? true : false }
							id={ name }
							tabIndex={ 0 }
							autoComplete="off"
							value={ searchValue }
							className={ `wsx-input ${ customClass }` }
							placeholder={
								selectedOptions.length > 0 ? '' : placeholder
							}
							onChange={ ( e ) => onInputChangeHandler( e ) }
							onClick={ ( e ) => onInputChangeHandler( e ) }
						/>
					</div>
				</div>
			</div>
			<div ref={ myRef } key={ `wsx-${ name }` }>
				{ showList && optionList.length > 0 && (
					<div
						className="wsx-card wsx-multiselect-options wsx-scrollbar"
						key={ `wsx-opt-${ name }` }
					>
						{ optionList.map( ( option, index ) => {
							return (
								<div
									className="wsx-multiselect-option"
									key={ `wsx-opt-${ name }-${ option.value }-${ index }` }
									onClick={ () =>
										optionClickHandler( option )
									}
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											optionClickHandler( option );
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ option.name }
								</div>
							);
						} ) }
					</div>
				) }
				{ showList && optionList.length === 0 && (
					<div
						key={ `wsx-${ name }-not-found` }
						className="wsx-card wsx-multiselect-options wsx-scrollbar"
					>
						<div className="wsx-multiselect-option-message">
							{ __( 'No Data Found!', 'wholesalex' ) }
						</div>
					</div>
				) }
			</div>
		</div>
	);
};
export default MultiSelect;
