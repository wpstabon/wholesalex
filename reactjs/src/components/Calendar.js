import React, { useState, forwardRef, useContext, useEffect } from 'react';
import Icons from '../utils/Icons';
import { DataContext } from '../contexts/DataProvider';
import DateSelector from './DateSelector';
import '../assets/scss/Calendar.scss';

const Calendar = forwardRef( ( { numberOfMonthView = 2 } ) => {
	const [ hoveredDate, setHoveredDate ] = useState( null );
	const [ startDate, setStartDate ] = useState( null );
	const [ endDate, setEndDate ] = useState( null );
	const [ currentMonth, setCurrentMonth ] = useState( new Date().getMonth() ); // Current month (0 = Jan, 11 = Dec)
	const [ currentYear, setCurrentYear ] = useState(
		new Date().getFullYear()
	); // Current year
	const { setContextData } = useContext( DataContext );

	const daysInMonth = ( month, year ) =>
		new Date( year, month + 1, 0 ).getDate();

	const generateCalendar = ( month, year ) => {
		const days = [];
		const totalDays = daysInMonth( month, year );
		const firstDay = new Date( year, month, 1 ).getDay();

		for ( let i = 0; i < firstDay; i++ ) {
			days.push( null ); // Empty spaces for days before the first day of the month
		}
		for ( let i = 1; i <= totalDays; i++ ) {
			days.push( new Date( year, month, i ) );
		}
		return days;
	};

	const isInRange = ( date ) => {
		if ( ! startDate || ! endDate ) {
			return false;
		}
		return date >= startDate && date <= endDate;
	};

	const isHoveredRange = ( date ) => {
		if ( ! startDate || ! hoveredDate || endDate ) {
			return false;
		}
		return (
			( date > startDate && date <= hoveredDate ) || // HoveredDate is after startDate
			( hoveredDate < startDate &&
				date >= hoveredDate &&
				date < startDate ) // HoveredDate is before startDate
		);
	};

	const handleDayClick = ( date ) => {
		if ( ! startDate || ( startDate && endDate ) ) {
			setStartDate( date );
			setEndDate( null ); // Clear end date if new range starts
		} else if ( date < startDate ) {
			setEndDate( startDate );
			setStartDate( date );
		} else {
			setEndDate( date );
		}
	};

	useEffect( () => {
		setContextData( { startDateRange: startDate, endDateRange: endDate } );
	}, [ startDate, endDate, setContextData ] );

	const handlePrevMonth = () => {
		if ( currentMonth === 0 ) {
			setCurrentMonth( 11 );
			setCurrentYear( ( prev ) => prev - 1 );
		} else {
			setCurrentMonth( ( prev ) => prev - 1 );
		}
	};

	const handleNextMonth = () => {
		if ( currentMonth === 11 ) {
			setCurrentMonth( 0 );
			setCurrentYear( ( prev ) => prev + 1 );
		} else {
			setCurrentMonth( ( prev ) => prev + 1 );
		}
	};

	const firstMonthCalendar = generateCalendar( currentMonth, currentYear );
	const secondMonthCalendar = generateCalendar(
		currentMonth === 11 ? 0 : currentMonth + 1,
		currentMonth === 11 ? currentYear + 1 : currentYear
	);

	const monthNames = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];
	const dayNames = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];

	return (
		<div className="wsx-calendar-range-wrapper">
			<div className="wsx-calendar-header">
				<div className="wsx-calendar-value">
					<div className="wsx-d-flex wsx-gap-12 wsx-item-center wsx-justify-space">
						<span>From</span>
						<DateSelector
							onChange={ ( date ) => {
								setStartDate( new Date( date ) );
							} }
							value={ startDate ? startDate : '' }
							id="startDateValue"
							name="startDateValue"
							noCalendar={ true }
							inputPosition="center"
							iconSeparator={ true }
							maxWidth={ 200 }
						/>
					</div>
					<div className="wsx-d-flex wsx-gap-12 wsx-item-center wsx-justify-space">
						<span>To</span>
						<DateSelector
							onChange={ ( date ) => {
								setEndDate( new Date( date ) );
							} }
							value={ endDate ? endDate : '' }
							id="endDateValue"
							name="endDateValue"
							noCalendar={ true }
							inputPosition="center"
							iconSeparator={ true }
							maxWidth={ 200 }
						/>
					</div>
				</div>
			</div>
			<div className="wsx-calendar-container">
				<div className="wsx-calendar-wrapper wsx-w-full">
					<div className="wsx-control-wrapper">
						<div
							className="wsx-calendar-controller wsx-lh-0"
							onClick={ handlePrevMonth }
							onKeyDown={ ( e ) => {
								if ( e.key === 'Enter' || e.key === ' ' ) {
									handlePrevMonth;
								}
							} }
							role="button"
							tabIndex="0"
						>
							{ Icons.angleLeft_24 }
						</div>
						<div className="wsx-month">
							{ monthNames[ currentMonth ] } { currentYear }
						</div>
						{ numberOfMonthView === 1 && (
							<div
								className="wsx-calendar-controller wsx-lh-0"
								onClick={ handleNextMonth }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										handleNextMonth;
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ Icons.angleRight_24 }
							</div>
						) }
					</div>
					<div className="wsx-calendar">
						<div className="wsx-days-of-week">
							{ dayNames.map( ( d ) => (
								<div key={ d } className="wsx-day-name">
									{ d }
								</div>
							) ) }
						</div>
						<div className="wsx-days">
							{ firstMonthCalendar.map( ( date, idx ) => (
								<div
									key={ idx }
									className={ `wsx-day ${
										date ? 'active' : ''
									} 
                                        ${ isInRange( date ) ? 'in-range' : '' }
                                        ${
											date &&
											date.getTime() ===
												startDate?.getTime()
												? 'start'
												: ''
										}
                                        ${
											date &&
											date.getTime() ===
												endDate?.getTime()
												? 'end'
												: ''
										}
                                        ${
											isHoveredRange( date )
												? 'hovered-range'
												: ''
										}
                                    ` }
									onClick={ () =>
										date && handleDayClick( date )
									}
									onMouseEnter={ () =>
										setHoveredDate( date )
									}
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											date && handleDayClick( date );
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ date ? date.getDate() : '' }
								</div>
							) ) }
						</div>
					</div>
				</div>
				{ numberOfMonthView === 2 && (
					<div className="wsx-calendar-wrapper wsx-w-full">
						<div className="wsx-control-wrapper">
							<div className="wsx-month">
								{
									monthNames[
										currentMonth === 11
											? 0
											: currentMonth + 1
									]
								}{ ' ' }
								{ currentMonth === 11
									? currentYear + 1
									: currentYear }
							</div>
							{ numberOfMonthView === 2 && (
								<div
									className="wsx-calendar-controller wsx-lh-0"
									onClick={ handleNextMonth }
									onKeyDown={ ( e ) => {
										if (
											e.key === 'Enter' ||
											e.key === ' '
										) {
											handleNextMonth;
										}
									} }
									role="button"
									tabIndex="0"
								>
									{ Icons.angleRight_24 }
								</div>
							) }
						</div>
						<div className="wsx-calendar">
							<div className="wsx-days-of-week">
								{ [
									'Sun',
									'Mon',
									'Tue',
									'Wed',
									'Thu',
									'Fri',
									'Sat',
								].map( ( d ) => (
									<div key={ d } className="wsx-day-name">
										{ d }
									</div>
								) ) }
							</div>
							<div className="wsx-days">
								{ secondMonthCalendar.map( ( date, idx ) => (
									<div
										key={ idx }
										className={ `wsx-day ${
											date ? 'active' : ''
										} 
                                            ${
												isInRange( date )
													? 'in-range'
													: ''
											}
                                            ${
												date &&
												date.getTime() ===
													startDate?.getTime()
													? 'start'
													: ''
											}
                                            ${
												date &&
												date.getTime() ===
													endDate?.getTime()
													? 'end'
													: ''
											}
                                            ${
												isHoveredRange( date )
													? 'hovered-range'
													: ''
											}
                                        ` }
										onClick={ () =>
											date && handleDayClick( date )
										}
										onMouseEnter={ () =>
											setHoveredDate( date )
										}
										onKeyDown={ ( e ) => {
											if (
												e.key === 'Enter' ||
												e.key === ' '
											) {
												date && handleDayClick( date );
											}
										} }
										role="button"
										tabIndex="0"
									>
										{ date ? date.getDate() : '' }
									</div>
								) ) }
							</div>
						</div>
					</div>
				) }
			</div>
		</div>
	);
} );

export default Calendar;
