import { useState, useEffect, memo } from 'react';
import { usePaginationPages } from './usePaginationPages';
import Button from './Button';

function Pagination( {
	gotoPage,
	length,
	pageSize,
	setPageSize,
	items = [],
	showItemsPerPage = 0,
} ) {
	const [ perPage, setPerPage ] = useState( pageSize );

	const { canGo, currentPage, pages, goTo, goNext, goPrev } =
		usePaginationPages( {
			gotoPage,
			length,
			pageSize,
		} );

	useEffect( () => {
		setPageSize( Number( perPage ) );
	}, [ perPage, setPageSize ] );

	return (
		<div
			className="wsx-pagination-wrapper"
			style={ {
				display:
					items.length > showItemsPerPage || items.length === 0
						? 'flex'
						: 'none',
			} }
		>
			<div className="wsx-pagination-left wsx-btn-group wsx-gap-12">
				<Button
					iconName="angleLeft_24"
					iconClass="wsx-rtl-arrow"
					borderColor="tertiary"
					iconColor="tertiary"
					onClick={ goPrev }
					disable={ ! canGo.previous }
				/>
				{ pages.map( ( page, i ) => (
					<Button
						key={ i }
						borderColor="border-secondary"
						background="base1"
						label={ page }
						onClick={ () => goTo( page ) }
						customClass={ currentPage === page ? 'active' : '' }
					/>
				) ) }
				<Button
					iconName="angleRight_24"
					iconClass="wsx-rtl-arrow"
					borderColor="tertiary"
					iconColor="tertiary"
					onClick={ goNext }
					disable={ ! canGo.next }
				/>
			</div>
			<div className="wsx-pagination-right wsx-d-flex wsx-item-center wsx-gap-8">
				<span className="wsx-color-text-light">Show Result:</span>
				<select
					className="wsx-pagination-option-list wsx-select"
					value={ pageSize }
					onChange={ ( e ) => setPerPage( e.target.value ) }
				>
					{ [ 10, 20, 50, 100 ].map( ( page ) => (
						<option
							className="wsx-pagination-option"
							value={ page }
							key={ page }
						>
							{ page }
						</option>
					) ) }
				</select>
			</div>
		</div>
	);
}

export default memo( Pagination );
