import React from 'react';
import Icons from '../utils/Icons';
import { Link } from '../contexts/RouterContext';

function CounterMessageCard( {
	isActive = false,
	value,
	title = '',
	buttonText = '',
	buttonLink = '#',
} ) {
	return (
		<div className="wsx-br-md wsx-bg-base2 wsx-p-16 wsx-d-flex wsx-gap-16 wsx-mb-8">
			<div className="wsx-icon-wrapper wsx-relative">
				{ Icons.emailFill }
				{ isActive && <span className="wsx-notification-icon"></span> }
			</div>
			<div className="wsx-content-wrapper">
				<div className="wsx-d-flex wsx-item-center wsx-gap-12 wsx-mb-14">
					<span className="wsx-color-tertiary wsx-font-bold wsx-font-18">
						{ value }
					</span>
					<span className="wsx-color-text-light wsx-font-medium">
						{ title }
					</span>
				</div>
				<Link to={ buttonLink } className="wsx-card-item-link">
					{ ' ' }
					{ buttonText }{ ' ' }
				</Link>
			</div>
		</div>
	);
}
export default CounterMessageCard;
