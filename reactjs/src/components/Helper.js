const styleGenerate = ( wholesalex = {} ) => {
	const styles = {
		primary_button: {
			color: wholesalex?.settings?._settings_primary_button_color,
			backgroundColor: wholesalex?.settings?._settings_primary_color,
			'&:hover': {
				backgroundColor:
					wholesalex?.settings?._settings_primary_hover_color,
			},
		},
		text: {
			color: wholesalex?.settings?._settings_text_color,
		},
		primary_color: {
			color: wholesalex?.settings?._settings_primary_color,
		},
		border_color: {
			borderColor: wholesalex?.settings?._settings_border_color,
		},
	};
	return styles;
};
export default styleGenerate;
