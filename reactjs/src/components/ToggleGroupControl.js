import React from 'react';

const ToggleGroupControl = ( {
	title,
	options,
	className,
	value,
	onChange,
} ) => {
	return (
		<div className={ `wsx-component-toggle-group-control ${ className }` }>
			{ title && (
				<div className="wsx-component-toogle-group-control__title">
					{ ' ' }
					{ title }{ ' ' }
				</div>
			) }
			{ options && (
				<div className="wsx-component-toggle-group-control-options wsx-d-flex wsx-justify-space wsx-gap-10">
					{ Object.keys( options ).map( ( option ) => {
						return option === value ? (
							<div
								className={ `wsx-component-toggle-group-control-options__option` }
								data-active-item
								aria-label={ options[ option ] }
								data-wsx-value={ option }
								onClick={ () => onChange( option ) }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										onChange( option );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ options[ option ] }
							</div>
						) : (
							<div
								className={ `wsx-component-toggle-group-control-options__option` }
								aria-label={ options[ option ] }
								data-wsx-value={ option }
								onClick={ () => onChange( option ) }
								onKeyDown={ ( e ) => {
									if ( e.key === 'Enter' || e.key === ' ' ) {
										onChange( option );
									}
								} }
								role="button"
								tabIndex="0"
							>
								{ options[ option ] }
							</div>
						);
					} ) }
				</div>
			) }
		</div>
	);
};

export default ToggleGroupControl;
