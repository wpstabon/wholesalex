import React from 'react';
import Tooltip from './Tooltip';

import Icons from '../utils/Icons';

const Switch = ( props ) => {
	const {
		label,
		labelClass,
		name,
		onChange,
		isDisable,
		value,
		tooltip,
		tooltipPosition,
		help,
		helpClass,
		desc,
		className,
		required,
		requiredColor,
		isLabelHide,
		inputFieldClass,
	} = props;
	return (
		<div className={ `wsx-switch-field ${ className }` }>
			{ ! isLabelHide && label && (
				<div
					className={ `wsx-input-label ${
						tooltip ? 'wsx-d-flex' : ''
					} ${ labelClass }` }
				>
					{ label }{ ' ' }
					{ required && (
						<span
							className="wsx-required"
							style={ { color: requiredColor || '#fc143f' } }
						>
							*
						</span>
					) }
					{ tooltip && (
						<Tooltip
							content={ tooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
							className="wsx-rtl-tooltip-left"
						>
							{ Icons.help }
						</Tooltip>
					) }
				</div>
			) }
			<div className="wsx-switch-field-wrapper">
				<label
					className="wsx-label wsx-switch-field-desc wsx-d-flex wsx-item-center wsx-w-fit wsx-curser-pointer"
					htmlFor={ name }
				>
					<div className="wsx-checkbox-option-wrapper">
						<input
							id={ name }
							name={ name }
							type="checkbox"
							defaultChecked={
								value && value !== 'no' ? true : false
							}
							onChange={ onChange }
							disabled={ isDisable }
							className={ inputFieldClass ? inputFieldClass : '' }
						/>
						<span className="wsx-checkbox-mark"></span>
					</div>
					{ desc }{ ' ' }
					{ props?.descTooltip && (
						<Tooltip
							content={ props?.descTooltip }
							direction={ tooltipPosition }
							spaceLeft="8px"
							className="wsx-rtl-tooltip-left"
						>
							{ Icons.help }
						</Tooltip>
					) }{ ' ' }
				</label>
				{ help && (
					<div
						className={ `wsx-switch-help wsx-help-message ${ helpClass }` }
					>
						{ help }
					</div>
				) }
			</div>
		</div>
	);
};

export default Switch;
