import { __ } from '@wordpress/i18n';
import { registerPaymentMethod } from '@woocommerce/blocks-registry';
import { decodeEntities } from '@wordpress/html-entities';
import { getSetting } from '@woocommerce/settings';

const settings = getSetting( 'wholesalex_subaccount_order_approval_data', {} );

const defaultLabel = __( 'Sent to Parent Account For Approval', 'wholesalex' );

const label = decodeEntities( settings.title ) || defaultLabel;
/**
 * Content component
 */
const Content = () => {
	return decodeEntities( settings.description || '' );
};

/**
 * Label component
 *
 * @param {*} props Props from payment API.
 */
const Label = ( props ) => {
	const { PaymentMethodLabel } = props.components;
	return (
		<>
			<PaymentMethodLabel text={ label } />
		</>
	);
};

/**
 * Wallet payment method config object.
 */
const Wallet = {
	name: 'wholesalex_subaccount_order_approval',
	label: <Label />,
	content: <Content />,
	edit: <Content />,
	ariaLabel: label,
	canMakePayment: () => settings.canMakePayment,
	supports: {
		features: settings.supports,
	},
};

registerPaymentMethod( Wallet );
