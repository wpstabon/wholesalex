'use strict';
const path = require( 'path' );
const TerserPlugin = require( 'terser-webpack-plugin' );
// const DependencyExtractionWebpackPlugin = require('@wordpress/dependency-extraction-webpack-plugin');

const config = {
	mode: 'production',
	entry: {
		whx_overview: './reactjs/src/pages/overview/index.js',
		whx_cat: './reactjs/src/pages/category/index.js',
	},
	output: {
		path: path.join( __dirname, './assets/js/' ),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(scss|css)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
			{
				test: /\.(png|jp(e*)g|svg|gif)$/,
				type: 'asset/resource',
			},
		],
	},
	optimization: {
		concatenateModules: false,
		minimizer: [
			new TerserPlugin( {
				parallel: true,
				terserOptions: {
					output: {
						comments: /translators:/i,
					},
					compress: {
						passes: 2,
					},
					mangle: {
						reserved: [ '__', '_n', '_nx', '_x' ],
					},
				},
				extractComments: false,
			} ),
		],
		splitChunks: {
			chunks: 'async',
			minSize: 20000,
			minRemainingSize: 0,
			minChunks: 1,
			maxAsyncRequests: 30,
			maxInitialRequests: 30,
			enforceSizeThreshold: 50000,
			cacheGroups: {
				vendors: {
					name: 'node_vendors',
					test: /[\\/]node_modules[\\/]/,
					chunks: 'initial',
					priority: -10,
					reuseExistingChunk: true,
				},
				common: {
					name: 'wholesalex_components',
					test: /[\\/]reactjs[\\/]src[\\/]components[\\/]/,
					chunks: 'all',
					priority: -10,
					reuseExistingChunk: true,
				},
			},
		},
	},
	// devtool: "source-map",
	externals: {
		react: 'React',
		'react-dom': 'ReactDOM',
		'@wordpress/element': 'wp.element',
		'@wordpress/components': 'wp.components',
		'@woocommerce/components': 'wc.components',
		moment: 'moment',
		lodash: 'lodash',
		'@woocommerce/date': 'wc.date',
		'@wordpress/i18n': 'wp.i18n',
	},
};

const addonConfig = {
	mode: 'production',
	entry: {
		wholesalex_bulkorder: './reactjs/src/pages/bulkorder/index.js',
		wholesalex_subaccount: './reactjs/src/pages/subaccount/index.js',
		wholesalex_wallet: './reactjs/src/pages/wallet/index.js',
		whx_conversation: './reactjs/src/pages/Conversation/index.js',
	},
	output: {
		path: path.join( __dirname, './assets/js/' ),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(scss|css)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
			{
				test: /\.(png|jp(e*)g|svg|gif)$/,
				type: 'asset/resource',
			},
		],
	},
	optimization: {
		concatenateModules: false,
		minimizer: [
			new TerserPlugin( {
				parallel: true,
				terserOptions: {
					output: {
						comments: /translators:/i,
					},
					compress: {
						passes: 2,
					},
					mangle: {
						reserved: [ '__', '_n', '_nx', '_x' ],
					},
				},
				extractComments: false,
			} ),
		],
	},
	// plugins: [new DependencyExtractionWebpackPlugin()],
	devtool: 'source-map',
	externals: {
		react: 'React',
		'react-dom': 'ReactDOM',
		'@wordpress/element': 'wp.element',
		'@wordpress/components': 'wp.components',
		'@woocommerce/components': 'wc.components',
		moment: 'moment',
		lodash: 'lodash',
		'@woocommerce/date': 'wc.date',
		'@wordpress/i18n': 'wp.i18n',
	},
};

const supportsConfig = {
	mode: 'production',
	entry: {
		whx_wallet_gateway: './reactjs/src/supports/WalletGateway.js',
		whx_parent_approval_gateway:
			'./reactjs/src/supports/ParentApprovalGateway.js',
	},
	output: {
		path: path.join( __dirname, './assets/js/' ),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(scss|css)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
			{
				test: /\.(png|jp(e*)g|svg|gif)$/,
				type: 'asset/resource',
			},
		],
	},
	optimization: {
		concatenateModules: false,
		minimizer: [
			new TerserPlugin( {
				parallel: true,
				terserOptions: {
					output: {
						comments: /translators:/i,
					},
					compress: {
						passes: 2,
					},
					mangle: {
						reserved: [ '__', '_n', '_nx', '_x' ],
					},
				},
				extractComments: false,
			} ),
		],
	},
	// plugins: [new DependencyExtractionWebpackPlugin()],
	devtool: 'source-map',
	externals: {
		'@woocommerce/blocks-registry': 'wc.wcBlocksRegistry',
		'@woocommerce/price-format': 'wc.priceFormat',
		'@woocommerce/settings': 'wc.wcSettings',
		react: 'React',
		'react-dom': 'ReactDOM',
		'@wordpress/element': 'wp.element',
		'@wordpress/components': 'wp.components',
		'@woocommerce/components': 'wc.components',
		moment: 'moment',
		lodash: 'lodash',
		'@woocommerce/date': 'wc.date',
		'@wordpress/i18n': 'wp.i18n',
	},
};

const blockConfig = {
	mode: 'production',
	entry: {
		wholesalex_forms_block: './reactjs/src/blocks/forms/index.js',
	},
	output: {
		path: path.join( __dirname, './assets/js/' ),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(s(a|c)ss)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
		],
	},
	plugins: [],
	externals: {
		'@woocommerce/blocks-registry': 'wc.wcBlocksRegistry',
		'@woocommerce/price-format': 'wc.priceFormat',
		'@woocommerce/settings': 'wc.wcSettings',
		react: 'React',
		'react-dom': 'ReactDOM',
		'@wordpress/element': 'wp.element',
		'@wordpress/components': 'wp.components',
		'@woocommerce/components': 'wc.components',
		moment: 'moment',
		lodash: 'lodash',
		'@woocommerce/date': 'wc.date',
		'@wordpress/i18n': 'wp.i18n',
	},
};

// module.exports = [config , addonConfig,supportsConfig,blockConfig];
module.exports = [ config, blockConfig ];
