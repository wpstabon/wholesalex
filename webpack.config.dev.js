'use strict';
const path = require( 'path' );
const TerserPlugin = require( 'terser-webpack-plugin' );

const commonRules = [
	{
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		use: { loader: 'babel-loader' },
	},
	{
		test: /\.(scss|css)$/,
		use: [ 'style-loader', 'css-loader', 'sass-loader' ],
	},
	{
		test: /\.(png|jp(e*)g|svg|gif)$/,
		type: 'asset/resource',
	},
];

const terserOptimization = {
	concatenateModules: false,
	minimizer: [
		new TerserPlugin( {
			parallel: true,
			terserOptions: {
				output: {
					comments: /translators:/i,
				},
				compress: {
					passes: 2,
				},
				mangle: {
					reserved: [ '__', '_n', '_nx', '_x' ],
				},
			},
			extractComments: false,
		} ),
	],
};

const createConfig = ( entry, outputPath, externals = {} ) => ( {
	mode: 'development',
	entry,
	output: {
		path: path.join( __dirname, outputPath ),
		filename: '[name].js',
	},
	module: { rules: commonRules },
	optimization: terserOptimization,
	devtool: 'source-map',
	externals,
	watch: true, // Enable watch mode for rebuilding on file changes
	devServer: {
		static: {
			directory: path.join( __dirname, './assets/js/' ),
		},
		compress: true,
		port: 3000, // Port for dev server
		hot: true, // Enable hot module replacement
		open: true, // Open in default browser
	},
} );

module.exports = [
	createConfig(
		{
			whx_overview: './reactjs/src/pages/overview/index.js',
			whx_cat: './reactjs/src/pages/category/index.js',
			whx_product: './reactjs/src/pages/single_product/index.js',
			whx_profile: './reactjs/src/pages/user_profile/index.js',
			whx_migration_tools:
				'./reactjs/src/pages/migration_plugin/index.js',
		},
		'./assets/js/',
		{
			react: 'React',
			'react-dom': 'ReactDOM',
			'@wordpress/element': 'wp.element',
			'@wordpress/components': 'wp.components',
			'@woocommerce/components': 'wc.components',
			moment: 'moment',
			lodash: 'lodash',
			'@woocommerce/date': 'wc.date',
			'@wordpress/i18n': 'wp.i18n',
		}
	),
	createConfig(
		{
			wholesalex_bulkorder: './reactjs/src/pages/bulkorder/index.js',
			wholesalex_subaccount: './reactjs/src/pages/subaccount/index.js',
			wholesalex_wallet: './reactjs/src/pages/wallet/index.js',
			whx_conversation: './reactjs/src/pages/Conversation/index.js',
		},
		'./assets/js/'
	),
	createConfig(
		{
			whx_wallet_gateway: './reactjs/src/supports/WalletGateway.js',
		},
		'./assets/js/',
		{
			'@woocommerce/blocks-registry': 'wc-blocks-registry',
			'@woocommerce/price-format': 'wc-price-format',
			'@woocommerce/settings': 'wc-settings',
			'@wordpress/i18n': 'wp.i18n',
		}
	),
	createConfig(
		{
			wholesalex_forms_block: './reactjs/src/blocks/forms/index.js',
		},
		'./assets/js/'
	),
];
